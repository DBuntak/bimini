package hr.project.bimini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.ChimneyMan;

public interface ChimneyManRepository extends JpaRepository<ChimneyMan, Long> {

}
