package hr.project.bimini.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.SnowSchedule;

public interface SnowScheduleRepository extends JpaRepository<SnowSchedule, Long> {

	Optional<SnowSchedule> findByBuilding(Building building);
	
	void deleteByBuilding(Building building);
	
	boolean existsByBuilding(Building building);
}
