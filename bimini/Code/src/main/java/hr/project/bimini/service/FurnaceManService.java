package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.model.FurnaceMan;

public interface FurnaceManService {

	List<FurnaceMan> getAll();
	Optional<FurnaceMan> getById(final Long id);
	FurnaceMan update(final Long id, final FurnaceMan furnaceMan);
	FurnaceMan save(final FurnaceMan furnaceMan);
	void delete(final Long id);
}
