package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.mail.MessagingException;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.ContractDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.BuildingContracts;
import hr.project.bimini.model.ContractType;
import hr.project.bimini.model.PurchaseContract;


public interface BuildingContractsService {

	List<BuildingContracts> getAll();
	Optional<BuildingContracts> getById(final Long id);
	BuildingContracts save(final BuildingContracts buildingContracts);
	void delete(final Long id);
	List<BuildingContracts> getContractsByBuildingAndType(final Building building, final ContractType type);
	String storeContract(MultipartFile file, final ContractType type, final Building building, ContractDTO contractDTO);
	void deleteContract(String fileName);
	void sendEmailWithContractsForBuildingAndType(String to, String subject, String text, Building building, ContractType type) throws MessagingException;
	BuildingContracts update(Long id, ContractDTO buildingContracts, ContractType type);
	String updateContract(MultipartFile file, Long contractId);
	BuildingContracts loadContractAsResource(Long id);
	List<ContractDTO> getContractsByBuildingAndTypeDTO(Building building, ContractType type);
}
