package hr.project.bimini.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.CalendarDTO;
import hr.project.bimini.controller.DTO.CalendarGetDTO;
import hr.project.bimini.controller.DTO.EventDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.ChimneyMan;
import hr.project.bimini.model.Event;
import hr.project.bimini.model.EventDoer;
import hr.project.bimini.model.EventType;
import hr.project.bimini.model.FurnaceInspection;
import hr.project.bimini.model.FurnaceMan;
import hr.project.bimini.service.BuildingService;
import hr.project.bimini.service.BuildingServiceJpa;
import hr.project.bimini.service.ChimneyInspectionService;
import hr.project.bimini.service.ChimneyManService;
import hr.project.bimini.service.EventDoerService;
import hr.project.bimini.service.EventService;
import hr.project.bimini.service.EventTypeService;
import hr.project.bimini.service.FurnaceInspectionService;
import hr.project.bimini.service.FurnaceManService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("calendar")
@AllArgsConstructor
@Slf4j
@RestController
public class CalendarController {

	@Autowired
	private BuildingController buildingController;
	
	@Autowired
	private BuildingService buildingService;
	
	@Autowired
	private EventTypeService eventTypeService;
	
	@Autowired
	private FurnaceInspectionService furnaceInspectionService;
	
	@Autowired
	private ChimneyInspectionService chimneyInspectionService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ChimneyManService chimneyManService;
	
	@Autowired
	private FurnaceManService furnaceManService;
	
	@Autowired
	private EventDoerService eventDoerService;
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/{id}")
	public ResponseEntity<List<CalendarGetDTO>> getEvents(@PathVariable("id") Long buildingId){
		Optional<Building> buildingResponse = buildingService.getById(buildingId);
		if(!buildingResponse.isPresent()) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.get();
		//List<ChimneyInspection> chimneyInspections = chimneyInspectionService.getByBuildingAndDate(building);
		//List<FurnaceInspection> furnaceInspections = furnaceInspectionService.getByBuildingAndDate(building);
		List<Event> eventss = eventService.getByBuildingAndDate(building);
		List<CalendarGetDTO> events = new ArrayList<>();
		/*for(ChimneyInspection ci : chimneyInspections) {
			EventDTO event = new EventDTO(ci.getId(), ci.getTitle(), ci.getStart(), ci.getEnd(), ci.getDescription(), "Dimnjak");
			events.add(event);
		}
		for(FurnaceInspection fi : furnaceInspections) {
			EventDTO event = new EventDTO(fi.getId(), fi.getTitle(), fi.getStart(), fi.getEnd(), fi.getDescription(), "Peć");
			events.add(event);
		}*/

		for(Event e : eventss) {
			CalendarGetDTO event = null;
			event = new CalendarGetDTO(e.getId(), e.getTitle(), e.getStart(), e.getEnd(), e.getResource(), e.getEventType().getName(), e.getWorker());
			events.add(event);
		}
		return ResponseEntity.ok(events);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/building/{id}")
	public ResponseEntity<CalendarDTO> saveEvent(@PathVariable("id") Long buildingId, @RequestBody CalendarDTO event){
		ResponseEntity<Building> buildingResponse = buildingController.getById(buildingId);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		/*if(event.getType().equals("chimneyInspection")) {
			ChimneyMan man = chimneyManService.getById(workerId).get();
			ChimneyInspection chimneyInspection = new ChimneyInspection(null, event.getTitle(), event.getResource(), man, building,
					event.getStart(), event.getEnd(), null);
			chimneyInspection = chimneyInspectionService.save(chimneyInspection);
			event.setId(chimneyInspection.getId());
		}
		else if(event.getType().equals("furnaceInspection")) {
			FurnaceMan man = furnaceManService.getById(workerId).get();
			FurnaceInspection furnaceInspection = new FurnaceInspection(null, event.getTitle(), event.getResource(), man, event.getStart(),
					event.getEnd(), building, null);
			furnaceInspection = furnaceInspectionService.save(furnaceInspection);
			event.setId(furnaceInspection.getId());
		}
		else if(event.getType().equals("cleaning") || event.getType().equals("waste") || event.getType().equals("rest")) {
			EventDoer man = eventDoerService.getById(workerId).get();
			Event e = new Event(null, event.getTitle(), event.getResource(), man, EventType.valueOf(event.getType().toUpperCase()), building, event.getStart(),
					event.getEnd(), null);
			e = eventService.save(e);
			event.setId(e.getId());
		}
		
		EventDoer man = null;
		Long workerId = event.getWorkerId();
		if(workerId != 99999) {
			man = eventDoerService.getById(workerId).get();
		}*/
		EventType type = null;
		if(eventTypeService.existsById(event.getTypeId())) {
			type = eventTypeService.getById(event.getTypeId()).get();
		}
		Event e = new Event(null, event.getTitle(), event.getResource(), event.getWorker(), type, building, event.getStart(),
				event.getEnd(), null);
		e = eventService.save(e);
		event.setId(e.getId());
		return ResponseEntity.ok(event);
	}
}
