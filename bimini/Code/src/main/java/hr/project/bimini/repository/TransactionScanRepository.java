package hr.project.bimini.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Transaction;
import hr.project.bimini.model.TransactionScan;

public interface TransactionScanRepository extends JpaRepository<TransactionScan, Long> {

	boolean existsByTransaction(Transaction transaction);
	
	TransactionScan findByTransaction(Transaction transaction);

	Optional<TransactionScan> findByFileName(String fileName);
	
	boolean existsByFileName(String fileName);
	
	void deleteByFileName(String fileName);
}
