package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.ManagerDTO;
import hr.project.bimini.controller.DTO.NoticePositionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Manager;
import hr.project.bimini.service.ManagerServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("manager")
@AllArgsConstructor
@Slf4j
@RestController
public class ManagerController {

	@Autowired
	private ManagerServiceJpa managerService;
	
	@Autowired
	private BuildingController buildingController;
	
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Manager>> getAll() {
		return ResponseEntity.ok(managerService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Manager> getById(@PathVariable("id") final Long id) {
		Optional<Manager> manager = managerService.getById(id);
        if (!manager.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(manager.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Manager> updateById(@PathVariable("id") final Long id, @RequestBody final ManagerDTO manager) {
        if (!managerService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(managerService.update(id, manager));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final ManagerDTO manager) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(manager.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(managerService.save(manager, building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!managerService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        managerService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/building/{id}")
	public ResponseEntity<List<Manager>> getByBuilding(@PathVariable Long id){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(managerService.getByBuilding(building));
	}
}
