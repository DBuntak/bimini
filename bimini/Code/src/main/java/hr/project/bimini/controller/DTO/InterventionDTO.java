package hr.project.bimini.controller.DTO;

import java.util.Date;

import hr.project.bimini.model.Apartment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InterventionDTO {

	private Long id;
	private String apartmentName;
	private String description;
	private Double amount;
	private Date date;
}
