package hr.project.bimini.controller.DTO;

import java.util.List;

import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantRoles;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TenantRoleDTO {

	private List<Tenant> tenants;
	private TenantRoles role;
}
