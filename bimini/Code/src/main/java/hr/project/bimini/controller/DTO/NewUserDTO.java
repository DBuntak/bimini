package hr.project.bimini.controller.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NewUserDTO {

	private String username;
	private String password;
	private Long apartmentId;
	private Long buildingId;
}
