package hr.project.bimini.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class BuildingContracts {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "building_id")
	private Building building;
	
	private String fileName;
	private String realName;
	private String contentType;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contract_type_id")
	private ContractType contractType;
	
	private String firm;
	private Date conclusionDate;
	private Integer duration;
	private Date expirationDate;
	private Integer noticePeriod;
	private String name;
	private String surname;
	private String email;
	private String mobilePhone;
	private String emergencyNumber;
	private String contractId;
	private String contractName;
	private String description;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] data;
	
}
