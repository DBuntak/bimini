package hr.project.bimini.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ParkingPlace {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer number;
	
	private ParkingType type;
	
	private Date rentStart;
	private Date rentEnd;
	private ParkingRentType rentType;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
	private Building building;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="owner_id")
	private Tenant owner;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="rented_id")
	private Tenant rented;

	public ParkingPlace(Long id, Integer number, ParkingType type, Building building) {
		super();
		this.id = id;
		this.number = number;
		this.type = type;
		this.building = building;
	}
	
	
	
	
}
