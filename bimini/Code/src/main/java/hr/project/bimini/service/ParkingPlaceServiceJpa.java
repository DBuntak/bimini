package hr.project.bimini.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import hr.project.bimini.controller.DTO.ParkingPlaceDTO;
import hr.project.bimini.controller.DTO.ParkingRentDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Event;
import hr.project.bimini.model.ParkingPlace;
import hr.project.bimini.model.ParkingRentType;
import hr.project.bimini.model.ParkingType;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.repository.ParkingPlaceRepository;

@Service
public class ParkingPlaceServiceJpa implements ParkingPlaceService {

	@Autowired
	private ParkingPlaceRepository parkingPlaceRepository;
	
	@Autowired
	private JavaMailSender emailSender;
	
	@Override
	public List<ParkingPlace> getAll() {
		return parkingPlaceRepository.findAll();
	}

	@Override
	public Optional<ParkingPlace> getById(final Long id) {
		return parkingPlaceRepository.findById(id);
	}

	@Override
	public ParkingPlace update(final Long id, final ParkingPlaceDTO parkingPlace) {//, Tenant owner, Tenant rented) {
		ParkingType type = null;
		if(parkingPlace.getType() != null && parkingPlace.getType() != "") {
			type = ParkingType.valueOf(parkingPlace.getType());
		}
		ParkingPlace place = this.getById(id).get();
		place.setNumber(parkingPlace.getNumber());
		place.setType(type);
		return parkingPlaceRepository.save(place);
	}

	@Override
	public ParkingPlace save(final ParkingPlaceDTO parkingPlace, final Building building, Tenant owner, Tenant rented, ParkingRentType rentType) {
		ParkingType type = null;
		if(parkingPlace.getType() != null && parkingPlace.getType() != "") {
			type = ParkingType.valueOf(parkingPlace.getType());
		}
		ParkingPlace trans = new ParkingPlace(null, parkingPlace.getNumber(), type, parkingPlace.getRentStart(), parkingPlace.getRentEnd(), rentType, building, owner, rented);
		return parkingPlaceRepository.save(trans);
	}

	@Override
	public void delete(final Long id) {
		parkingPlaceRepository.deleteById(id);
	}
	
	@Override
	public List<ParkingPlace> getByBuilding(Building building){
		return parkingPlaceRepository.findByBuilding(building);
	}
	
	@Override
	public List<ParkingPlace> getByBuildingAndType(Building building, ParkingType type){
		return parkingPlaceRepository.findByBuildingAndType(building, type);
	}

	@Override
	public ParkingPlace attachOwnerOrRented(Long id, String ownership, Tenant owner, ParkingRentDTO rent) {
		ParkingPlace parking = this.getById(id).get();
		if(ownership.equals("owner")) {
			parking.setOwner(owner);
		}
		else if(ownership.equals("rented")) {
			parking.setRented(owner);
			parking.setRentStart(rent.getRentStart());
			parking.setRentEnd(rent.getRentEnd());
			parking.setRentType(ParkingRentType.valueOf(rent.getRentType()));
		}
		return parkingPlaceRepository.save(parking);
	}
	
	@Scheduled(cron = "0 0 2 * * ?")
	public void sendRentExpirationEmail() throws MessagingException, IOException {
		
		Date date = new Date();
		Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        c.add(Calendar.DATE, 15);
        Date start = c.getTime();
        c.add(Calendar.DATE, 1);
        Date end = c.getTime();
        
        List<ParkingPlace> parkings = parkingPlaceRepository.findByRentEndAfterAndRentEndBefore(start, end);
        
        for(ParkingPlace p : parkings) {
    		MimeMessage message = emailSender.createMimeMessage();
       	     
    	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
    	    
    	    helper.setFrom("project.bimini10@gmail.com");
    	    helper.setSubject("Podsjetnik: "+p.getNumber());
    	    helper.setText("Najam parkirnog mjesta "+p.getNumber()+" istječe za 15 dana");
    		helper.setTo(p.getRented().getPrivateEmail());
    		//mail upraviteljuu helper.setCc();
    	    emailSender.send(message);
        }
	}
}
