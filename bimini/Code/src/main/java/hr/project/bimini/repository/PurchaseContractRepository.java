package hr.project.bimini.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.PurchaseContract;

public interface PurchaseContractRepository extends JpaRepository<PurchaseContract,Long> {
	
	boolean existsByApartment(Apartment apartment);
	
	PurchaseContract findByApartment(Apartment apartment);

	Optional<PurchaseContract> findByFileName(String fileName);
	
	boolean existsByFileName(String fileName);
	
	void deleteByFileName(String fileName);
	
	/*@Modifying
	@Query(value = "DELETE FROM APARTMENT_CONTRACTS WHERE CONTRACT_ID = ?1", nativeQuery=true)
	void deleteFromApartment(Long contract_id);*/
}
