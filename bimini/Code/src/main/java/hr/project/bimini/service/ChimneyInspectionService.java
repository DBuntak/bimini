package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.ChimneyReport;
import hr.project.bimini.model.FurnaceInspection;
import hr.project.bimini.model.FurnaceReport;

public interface ChimneyInspectionService {

	List<ChimneyInspection> getAll();
	Optional<ChimneyInspection> getById(final Long id);
	ChimneyInspection update(final Long id, final ChimneyInspection chimneyInspection);
	ChimneyInspection save(final ChimneyInspection chimneyInspection);
	void delete(final Long id);
	List<ChimneyInspection> getByBuilding(Building building);
	ChimneyReport getReport(ChimneyInspection chimneyInspection);
	String storeReport(MultipartFile file, final ChimneyInspection ci);
	List<ChimneyInspection> getByBuildingAndDate(Building building);
}
