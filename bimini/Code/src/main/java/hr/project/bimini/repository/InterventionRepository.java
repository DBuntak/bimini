package hr.project.bimini.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.Intervention;

public interface InterventionRepository extends JpaRepository<Intervention, Long> {

	List<Intervention> findByBuilding(Building building);
	
	boolean existsByBuilding(Building building);
	
	List<Intervention> findByBuildingAndDateAfterAndDateBefore(Building building, Date start, Date end);
}
