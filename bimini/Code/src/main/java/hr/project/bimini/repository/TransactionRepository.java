package hr.project.bimini.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Transaction;
import hr.project.bimini.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
