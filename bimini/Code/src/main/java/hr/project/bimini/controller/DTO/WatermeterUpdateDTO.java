package hr.project.bimini.controller.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WatermeterUpdateDTO {

	private Integer tenantsNumber;
}
