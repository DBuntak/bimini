package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.File;
import hr.project.bimini.model.FileType;

public interface FileService {

	List<File> getAll();
	Optional<File> getById(final Long id);
	File update(final Long id, final File file);
	File save(final File file);
	void delete(final Long id);
	File getFileByBuildingAndType(final Building building, final FileType type);
	File storeFile(MultipartFile file, final FileType type, final Building building);
	void deleteFile(String fileName);
	boolean existsByBuildingAndType(Building building, FileType type);
	File loadFileAsResource(Long id);
}
