package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FormDTO {

	private String theme;
	private String optionalName;
	private int optional;
	private Long buildingId;
	private String referendum1;
	private String referendum2;
}
