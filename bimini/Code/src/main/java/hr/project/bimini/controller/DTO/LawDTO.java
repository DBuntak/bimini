package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LawDTO {

	private Long tenantId;
	private Long lawyerId;
	private Double debt;
	private Date periodFrom;
	private Date periodTo;
	private String type;
}
