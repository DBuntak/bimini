package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Analytics;
import hr.project.bimini.model.AnalyticsType;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.File;
import hr.project.bimini.model.FileType;
import hr.project.bimini.service.AnalyticsService;
import hr.project.bimini.service.ChimneyInspectionService;
import hr.project.bimini.service.FileService;
import hr.project.bimini.service.FurnaceInspectionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("analytics")
@AllArgsConstructor
@Slf4j
@RestController
public class AnalyticsController {

	@Autowired
	private AnalyticsService analyticsService;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/upload/{id}")
	public ResponseEntity<List<Analytics>> uploadAnalytics(@RequestParam("file") MultipartFile file, @RequestParam("type") String type, @PathVariable("id") final Long building_id) throws IOException{
		ResponseEntity<Building> buildingResponse = buildingController.getById(building_id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		/*if(!file.getContentType().equals("text/csv")) {
			return ResponseEntity.badRequest().build();
		}*/
		AnalyticsType aType = AnalyticsType.valueOf(type);
		FileType fType = FileType.valueOf(type);
		if(fileService.existsByBuildingAndType(building, fType)) {
			fileService.deleteFile(fileService.getFileByBuildingAndType(building, fType).getFileName());
		}
		fileService.storeFile(file, fType, building);
		return ResponseEntity.ok(analyticsService.uploadAnalytics(file, aType, building));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}/{type}")
	public ResponseEntity<List<Analytics>> getAnalytics(@PathVariable("id") Long buildingId, @PathVariable("type") String type){
		ResponseEntity<Building> buildingResponse = buildingController.getById(buildingId);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		AnalyticsType aType = AnalyticsType.valueOf(type);
		
		return ResponseEntity.ok(analyticsService.getAnalytics(building, aType));
	}
}
