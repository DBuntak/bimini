package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.controller.DTO.NewTenantDTO;
import hr.project.bimini.controller.DTO.TenantRoleDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;

public interface TenantService {

	List<Tenant> getAll();
	Optional<Tenant> getById(final Long id);
	Tenant update(final Long id, final NewTenantDTO tenant);
	Tenant save(final NewTenantDTO tenantDTO, final Apartment apartment);
	void delete(final Long id);
	void removeFromApartment(final Long tenant_id, final Apartment apartment);
	List<Tenant> getNotInApartment();
	void addToApartment(Apartment apartment, Long tenantId);
	TenantRoleDTO getWithRole(Building building);
	List<Tenant> getByApartment(Apartment apartment);
	List<Tenant> getByBuilding(Building building);
	Tenant saveRemote(final NewTenantDTO tenantDTO, final Building building);
	List<Tenant> getByApartmentAndType(Apartment apartment, List<TenantTypes> types);
}
