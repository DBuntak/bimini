package hr.project.bimini.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TextAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTable.XWPFBorderType;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.docx4j.jaxb.Context;
import org.docx4j.model.table.TblFactory;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.Color;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.Tc;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.FormDTO;
import hr.project.bimini.controller.DTO.ReturnFormDTO;
import hr.project.bimini.controller.DTO.TenantRoleDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Intervention;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantRoles;
import hr.project.bimini.model.TenantTypes;
import hr.project.bimini.service.WarrantyServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("form")
@AllArgsConstructor
@Slf4j
@RestController
public class FormController {
	
	@Autowired
	private BuildingController buildingController;
	
	@Autowired
	private ApartmentController apartmentController;
	
	@Autowired
	private TenantController tenantController;

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/consent")
	public ResponseEntity<?> generateConsent(@RequestBody FormDTO form) throws IOException {
		ResponseEntity<Building> buildingResponse = buildingController.getById(form.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		ResponseEntity<List<Apartment>> apartmentResponse = buildingController.getApartmentsByBuilding(form.getBuildingId());
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			return ResponseEntity.badRequest().build();
		}
		List<Apartment> apartments = apartmentResponse.getBody();
		
		TenantRoleDTO tenantsWithRole = tenantController.getWithRole(building.getId()).getBody();
		List<Tenant> representors = tenantsWithRole.getTenants();
		
		String role = "";
		if(tenantsWithRole.getRole() == TenantRoles.predstavnik) {
			role = "Predstavnik stanara: ";
		}
		else if(tenantsWithRole.getRole() == TenantRoles.stambeno_vijeće) {
			role = "Vijećnik: ";
		}
		
		
		
		XWPFDocument document = new XWPFDocument();
		
		XWPFParagraph title = document.createParagraph();
		title.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun titleRun = title.createRun();
		titleRun.setText("SUVLASNICI NEKRETNINE " + building.getAddress() + " iz Zagreba");
		//titleRun.setColor();
		titleRun.setBold(true);
		titleRun.setFontFamily("Courier");
		titleRun.setFontSize(16);
		titleRun.setUnderline(UnderlinePatterns.SINGLE);
		
		XWPFParagraph uvod = document.createParagraph();
		uvod.setAlignment(ParagraphAlignment.LEFT);
		String uvodText = "Potvrđujemo vlastoručnim potpisom, ili potpisom zakonskog zastupnika za suvlasnika koji je fizička osoba a potpisom i pečatom zastupnika za suvlasnika koji je pravna osoba kako slijedi:";
		XWPFRun uvodRun = uvod.createRun();
		uvodRun.setText(uvodText);
		
		XWPFParagraph para1 = document.createParagraph();
		para1.setAlignment(ParagraphAlignment.LEFT);
		String string1 = form.getTheme();
		XWPFRun para1Run = para1.createRun();
		para1Run.setBold(true);
		if (string1.contains("\n")) {
            String[] lines = string1.split("\n");
            para1Run.setText(lines[0], 0); // set first line into XWPFRun
            for(int i=1;i<lines.length;i++){
                // add break and insert new text
                para1Run.addBreak();
                para1Run.setText(lines[i]);
            }
        } else {
            para1Run.setText(string1, 0);
        }

		XWPFParagraph kraj = document.createParagraph();
		kraj.setAlignment(ParagraphAlignment.LEFT);
		String krajText = "Popis suvlasnika s utvrđenim suvlasničkim udjelima i drugim podacima u troškovima održavanja:";
		XWPFRun krajRun = kraj.createRun();
		krajRun.setText(krajText);
		
		XWPFTable table = document.createTable();
		table.setCellMargins(0, 50, 0, 50);
		String[] cell0 = "Redni\nbroj".split("\n");
		String[] cell1 = "Prezime i Ime (su)vlasnika\nposebnog dijela zgrade".split("\n");
		String[] cell2 = " Površina\nposebnog\ndijela (u\nm2)".split("\n");
		String[] cell3 = "Udio\n(%)".split("\n");
		
		XWPFTableRow tableRowOne = table.getRow(0);
        XWPFParagraph para = tableRowOne.getCell(0).getParagraphs().get(0);
        tableRowOne.getCell(0).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell0){
            XWPFRun run = para.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para0 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(1).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell1){
            XWPFRun run = para0.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para2 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(2).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell2){
            XWPFRun run = para2.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para3 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(3).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell3){
            XWPFRun run = para3.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para4 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(4).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1*1440));
        tableRowOne.getCell(4).setVerticalAlignment(XWPFVertAlign.CENTER);
        para4.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = para4.createRun();
        run.setText("Potpis");
	    for(int i = 0; i<form.getOptional(); i++) {
	    	 XWPFParagraph para5 = tableRowOne.addNewTableCell().getParagraphs().get(0);
	    	 tableRowOne.getCell(5+i).setVerticalAlignment(XWPFVertAlign.CENTER);
	         XWPFRun run2 = para5.createRun();
	         run2.setText(form.getOptionalName());
	    }
	    int i=1;
	    for(Apartment a : apartments) {
	    	ResponseEntity<List<Tenant>> tenantResponse = apartmentController.getTenantsByApartment(a.getId());
			if(tenantResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
				return ResponseEntity.badRequest().build();
			}
			List<Tenant> tenants = tenantResponse.getBody();
			String owners = "";
			for(Tenant t : tenants) {
				if(t.getType() == TenantTypes.vlasnik || t.getType() == TenantTypes.vanjski_vlasnik) {
					owners += t.getSurname() + " " + t.getName() + " ";
				}
			}
			if(owners.equals("") && tenants.size() > 0) {
				owners = tenants.get(0).getSurname() + " " + tenants.get(0).getName();
			}
	    	XWPFTableRow tableRowTwo = table.createRow();
	        tableRowTwo.getCell(0).setText(i+".");
	        tableRowTwo.getCell(1).setText(owners);
	        tableRowTwo.getCell(2).setText(a.getQuadrature().toString());
	        tableRowTwo.getCell(3).setText(a.getShare().toString());
	        i++;
	    }
	    setTableAlign(table, ParagraphAlignment.CENTER);
	    
	    XWPFParagraph potpisi = document.createParagraph();
		potpisi.setAlignment(ParagraphAlignment.RIGHT);
		XWPFRun potpisiRun = potpisi.createRun();
		potpisiRun.addBreak();
		potpisiRun.addBreak();
		for(int j=0; j<representors.size(); j++) {
			potpisiRun.setText("____________________________");
			potpisiRun.addBreak();
			potpisiRun.setText(role + representors.get(j).getName() + " " + representors.get(j).getSurname());
			potpisiRun.addBreak();
			potpisiRun.addBreak();
		}
		
		FileOutputStream out = new FileOutputStream("test.docx");
		document.write(out);
		out.close();
		document.close();
		File f = new File("test.docx");
		FileInputStream fis = new FileInputStream(f);
	    byte[] doc = IOUtils.toByteArray(fis);
	    ReturnFormDTO dto = new ReturnFormDTO("Obrazac za suglasnost stanara", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", doc);
		

        // Try to determine file's content type
        String contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(contentType));
        header.setContentLength(doc.length);
        header.set("Content-Disposition", "attachment; filename=" + "Obrazac za suglasnost stanara");
        f.delete();
        return new ResponseEntity<>(doc, header, HttpStatus.OK);		
		
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/referendum")
	public ResponseEntity<?> generateReferendum(@RequestBody FormDTO form) throws IOException {
		ResponseEntity<Building> buildingResponse = buildingController.getById(form.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		ResponseEntity<List<Apartment>> apartmentResponse = buildingController.getApartmentsByBuilding(form.getBuildingId());
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			return ResponseEntity.badRequest().build();
		}
		List<Apartment> apartments = apartmentResponse.getBody();
		
		TenantRoleDTO tenantsWithRole = tenantController.getWithRole(building.getId()).getBody();
		List<Tenant> representors = tenantsWithRole.getTenants();
		
		String role = "";
		if(tenantsWithRole.getRole() == TenantRoles.predstavnik) {
			role = "Predstavnik stanara: ";
		}
		else if(tenantsWithRole.getRole() == TenantRoles.stambeno_vijeće) {
			role = "Vijećnik: ";
		}
		
		
		XWPFDocument document = new XWPFDocument();
		
		XWPFParagraph title = document.createParagraph();
		title.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun titleRun = title.createRun();
		titleRun.setText("SUVLASNICI NEKRETNINE " + building.getAddress() + " iz Zagreba");
		//titleRun.setColor();
		titleRun.setBold(true);
		titleRun.setFontFamily("Courier");
		titleRun.setFontSize(16);
		titleRun.setUnderline(UnderlinePatterns.SINGLE);
		
		/*XWPFParagraph uvod = document.createParagraph();
		uvod.setAlignment(ParagraphAlignment.LEFT);
		String uvodText = "Potvrđujemo vlastoručnim potpisom, ili potpisom zakonskog zastupnika za suvlasnika koji je fizička osoba a potpisom i pečatom zastupnika za suvlasnika koji je pravna osoba kako slijedi:";
		XWPFRun uvodRun = uvod.createRun();
		uvodRun.setText(uvodText);*/
		
		XWPFParagraph para1 = document.createParagraph();
		para1.setAlignment(ParagraphAlignment.LEFT);
		String string1 = "REFERENDUMSKO PITANJE: " + form.getTheme();
		XWPFRun para1Run = para1.createRun();
		//para1Run.setBold(true);
		if (string1.contains("\n")) {
            String[] lines = string1.split("\n");
            para1Run.setText(lines[0], 0); // set first line into XWPFRun
            for(int i=1;i<lines.length;i++){
                // add break and insert new text
                para1Run.addBreak();
                para1Run.setText(lines[i]);
            }
        } else {
            para1Run.setText(string1, 0);
        }

		/*XWPFParagraph kraj = document.createParagraph();
		kraj.setAlignment(ParagraphAlignment.LEFT);
		String krajText = "Popis suvlasnika s utvrđenim suvlasničkim udjelima i drugim podacima u troškovima održavanja:";
		XWPFRun krajRun = kraj.createRun();
		krajRun.setText(krajText);*/
		
		XWPFTable table = document.createTable();
		table.setCellMargins(0, 50, 0, 50);
		String[] cell0 = "Redni\nbroj".split("\n");
		String[] cell1 = "Ime i prezime".split("\n");
		String[] cell2 = " Površina \n(u m2)".split("\n");
		String[] cell3 = "Izvedbeni\n(%)".split("\n");
		String[] pitanje1 = form.getReferendum1().split("\n");
		String[] pitanje2 = form.getReferendum2().split("\n");
		
		XWPFTableRow tableRowOne = table.getRow(0);
        XWPFParagraph para = tableRowOne.getCell(0).getParagraphs().get(0);
        tableRowOne.getCell(0).setVerticalAlignment(XWPFVertAlign.CENTER);
        para.setAlignment(ParagraphAlignment.CENTER);
        for(String text : cell0){
            XWPFRun run = para.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para0 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(1).setVerticalAlignment(XWPFVertAlign.CENTER);
        para0.setAlignment(ParagraphAlignment.CENTER);
        for(String text : cell1){
            XWPFRun run = para0.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para2 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(2).setVerticalAlignment(XWPFVertAlign.CENTER);
        para2.setAlignment(ParagraphAlignment.CENTER);
        for(String text : cell2){
            XWPFRun run = para2.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para3 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(3).setVerticalAlignment(XWPFVertAlign.CENTER);
        para3.setAlignment(ParagraphAlignment.CENTER);
        for(String text : cell3){
            XWPFRun run = para3.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph paraPitanje1 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(4).setVerticalAlignment(XWPFVertAlign.CENTER);
        paraPitanje1.setAlignment(ParagraphAlignment.CENTER);
        for(String text : pitanje1){
            XWPFRun run = paraPitanje1.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph paraPitanje2 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(5).setVerticalAlignment(XWPFVertAlign.CENTER);
        paraPitanje2.setAlignment(ParagraphAlignment.CENTER);
        for(String text : pitanje2){
            XWPFRun run = paraPitanje2.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para4 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(6).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1*1440));
        tableRowOne.getCell(6).setVerticalAlignment(XWPFVertAlign.CENTER);
        para4.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = para4.createRun();
        run.setText("Potpis");
	    int i=1;
	    for(Apartment a : apartments) {
	    	ResponseEntity<List<Tenant>> tenantResponse = apartmentController.getTenantsByApartment(a.getId());
			if(tenantResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
				return ResponseEntity.badRequest().build();
			}
			List<Tenant> tenants = tenantResponse.getBody();
			String owners = "";
			for(Tenant t : tenants) {
				if(t.getType() == TenantTypes.vlasnik || t.getType() == TenantTypes.vanjski_vlasnik) {
					owners += t.getSurname() + " " + t.getName() + " ";
				}
			}
			if(owners.equals("") && tenants.size() > 0) {
				owners = tenants.get(0).getSurname() + " " + tenants.get(0).getName();
			}
	    	XWPFTableRow tableRowTwo = table.createRow();
	        tableRowTwo.getCell(0).setText(i+".");
	        tableRowTwo.getCell(1).setText(owners);
	        tableRowTwo.getCell(2).setText(a.getQuadrature().toString());
	        tableRowTwo.getCell(3).setText(a.getShare().toString());
	        i++;
	    }
	    setTableAlign(table, ParagraphAlignment.CENTER);
	    
	    XWPFParagraph potpisi = document.createParagraph();
		potpisi.setAlignment(ParagraphAlignment.RIGHT);
		XWPFRun potpisiRun = potpisi.createRun();
		potpisiRun.addBreak();
		potpisiRun.addBreak();
		for(int j=0; j<representors.size(); j++) {
			potpisiRun.setText("____________________________");
			potpisiRun.addBreak();
			potpisiRun.setText(role + representors.get(j).getName() + " " + representors.get(j).getSurname());
			potpisiRun.addBreak();
			potpisiRun.addBreak();
		}
		
		FileOutputStream out = new FileOutputStream("test2.docx");
		document.write(out);
		out.close();
		document.close();
		File f = new File("test2.docx");
		FileInputStream fis = new FileInputStream(f);
	    byte[] doc = IOUtils.toByteArray(fis);
	    ReturnFormDTO dto = new ReturnFormDTO("Obrazac za referendum", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", doc);
		
	    
	 // Try to determine file's content type
        String contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(contentType));
        header.setContentLength(doc.length);
        header.set("Content-Disposition", "attachment; filename=" + "Obrazac za referendum");
        f.delete();
        return new ResponseEntity<>(doc, header, HttpStatus.OK);		
		
		
	}
	
	public void setTableAlign(XWPFTable table,ParagraphAlignment align) {
	    CTTblPr tblPr = table.getCTTbl().getTblPr();
	    CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
	    STJc.Enum en = STJc.Enum.forInt(align.getValue());
	    jc.setVal(en);
	}
}
