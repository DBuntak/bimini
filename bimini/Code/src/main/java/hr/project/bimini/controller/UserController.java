package hr.project.bimini.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.model.User;
import hr.project.bimini.controller.DTO.NewUserDTO;
import hr.project.bimini.controller.DTO.UserBuildingDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Role;
import hr.project.bimini.model.RoleName;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.service.ApartmentService;
import hr.project.bimini.service.UserServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("user")
@AllArgsConstructor
@Slf4j
@RestController
public class UserController {
	@Autowired
	private UserServiceJpa userService;
	
	@Autowired
	private ApartmentService apartmentService;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<User>> getAll() {
		return ResponseEntity.ok(userService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<User> getById(@PathVariable("id") final Long id) {
		Optional<User> user = userService.getById(id);
        if (!user.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(user.get());
	}
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/username/{username}")
	public ResponseEntity<User> getByUsername(@PathVariable("username") final String username) {
		Optional<User> user = userService.getByUsername(username);
        if (!user.isPresent()) {
            log.error("Username " + username + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(user.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<User> updateById(@PathVariable("id") final Long id, @RequestBody final User user) {
        if (!userService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(userService.update(id, user));
	}
	@PreAuthorize("hasRole('APARTMENT')")
	@PutMapping("/apartment/{id}")
	public ResponseEntity<User> updateApartmentById(@PathVariable("id") final Long id, @RequestBody final User user) {
        Optional<User> us = userService.getById(id);
		if (!us.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
		if(us.get().getRole().getName() != RoleName.ROLE_APARTMENT) {
			log.error("Nedozvoljena promjena podataka korisnika");
            ResponseEntity.badRequest().build();
		}

        return ResponseEntity.ok(userService.update(id, user));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final User user) {
		return ResponseEntity.ok(userService.save(user));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!userService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        userService.delete(id);

        return ResponseEntity.ok().build();
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/role/{id}")
	public ResponseEntity<List<User>> getByRole(@PathVariable("id") final Long id) {
		Optional<Role> roleResponse = userService.getRoleById(id);
        if (!roleResponse.isPresent()) {
            log.error("Role " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Role role = roleResponse.get();
        Optional<List<User>> users = userService.getByRole(role);
        if(users.isPresent()) {
        	return ResponseEntity.ok(users.get());
        }
        else {
        	return ResponseEntity.ok(new ArrayList<>());
        }
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/apartment/{id}")
	public ResponseEntity<List<User>> getByApartment(@PathVariable("id") final Long id) {
		Optional<Apartment> apartmentResponse = apartmentService.getById(id);
        if (!apartmentResponse.isPresent()) {
            log.error("Apartment " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Apartment apartment = apartmentResponse.get();
        Optional<List<User>> users = userService.getByApartment(apartment);
        if(users.isPresent()) {
        	return ResponseEntity.ok(users.get());
        }
        else {
        	return ResponseEntity.ok(new ArrayList<>());
        }
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/register/admin")
	public ResponseEntity<User> createAdmin(@RequestBody NewUserDTO user){
		if(userService.getByUsername(user.getUsername()).isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(userService.createAdmin(user));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/register/tenant")
	public ResponseEntity<User> createTenant(@RequestBody NewUserDTO user){
		ResponseEntity<Building> buildingResponse = buildingController.getById(user.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		Optional<Apartment> apartmentResponse = apartmentService.getById(user.getApartmentId());
        if (!apartmentResponse.isPresent()) {
            log.error("Apartment " + user.getApartmentId() + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Apartment apartment = apartmentResponse.get();
        
		if(userService.getByUsername(user.getUsername()).isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(userService.createTenant(user, building, apartment));
	}
	
	@PreAuthorize("hasRole('APARTMENT')")
	@GetMapping("/{username}/building")
	public ResponseEntity<UserBuildingDTO> getBuildingForUser(@PathVariable("username") String username){
		Optional<User> user = userService.getByUsername(username);
		if(!user.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(userService.getBuildingForUser(user.get()));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/admin/building/{id}")
	public ResponseEntity<List<User>> getAdminsAndByBuilding(@PathVariable("id") Long buildingId){
		ResponseEntity<Building> buildingResponse = buildingController.getById(buildingId);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(userService.getAdminsAndByBuilding(building));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/tenant/{id}")
	public ResponseEntity<User> updateTenant(@PathVariable("id") Long userId, @RequestBody NewUserDTO user){
		Building building = null;
		Apartment apartment = null;
		if(user.getBuildingId() != null) {
			ResponseEntity<Building> buildingResponse = buildingController.getById(user.getBuildingId());
			if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
				ResponseEntity.badRequest().build();
			}
			building = buildingResponse.getBody();
		}
		if(user.getApartmentId() != null) {
			Optional<Apartment> apartmentResponse = apartmentService.getById(user.getApartmentId());
	        if (!apartmentResponse.isPresent()) {
	            log.error("Apartment " + user.getApartmentId() + " is not existed");
	            ResponseEntity.badRequest().build();
	        }
	        apartment = apartmentResponse.get();
		}
        return ResponseEntity.ok(userService.updateApartment(userId, user, apartment, building));
        
	}
}