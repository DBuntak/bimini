package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;

import hr.project.bimini.controller.DTO.SupplierDTO;
import hr.project.bimini.controller.DTO.SupplierInquiryDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Supplier;

public interface SupplierService {

	List<Supplier> getAll();
	Optional<Supplier> getById(final Long id);
	void delete(final Long id);
	Supplier save(SupplierDTO supplier, Building building);
	List<Supplier> getByBuilding(Building building);
	Supplier update(Long id, SupplierDTO supplier);
	void sendInquiry(Building building, SupplierInquiryDTO inquiry) throws MessagingException;
}
