package hr.project.bimini.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Transaction;
import hr.project.bimini.model.TransactionScan;
import hr.project.bimini.repository.TransactionRepository;
import hr.project.bimini.repository.TransactionScanRepository;

@Service
public class TransactionScanServiceJpa implements TransactionScanService {

	@Autowired
	private TransactionScanRepository transactionScanRepository;
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	
	@Override
	public List<TransactionScan> getAll() {
		return transactionScanRepository.findAll();
	}

	@Override
	public Optional<TransactionScan> getById(final Long id) {
		return transactionScanRepository.findById(id);
	}

	@Override
	public TransactionScan update(final Long id, final TransactionScan transactionScan) {
		return transactionScanRepository.save(transactionScan);
	}

	@Override
	public TransactionScan save(final TransactionScan transactionScan) {
		return transactionScanRepository.save(transactionScan);
	}
	
	@Override
	@Transactional
    public String storeContract(MultipartFile file, final Transaction transaction) {

		if(transactionScanRepository.existsByTransaction(transaction)) {
			TransactionScan pc = transactionScanRepository.findByTransaction(transaction);
			deleteContract(pc.getFileName());
		}
        // Normalize file name
        String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }

            TransactionScan newContract = new TransactionScan(null, fileName, file.getOriginalFilename(), file.getContentType(), new Date(), file.getBytes(), transaction);
            TransactionScan contract = transactionScanRepository.save(newContract);
            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public TransactionScan loadContractAsResource(String fileName) {
		TransactionScan oldContract = transactionScanRepository.findByFileName(fileName).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldContract != null) {
		    return oldContract;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + fileName);
		}
    }
	
    @Override
    @Transactional
    public void deleteContract(String fileName) {

		if(transactionScanRepository.existsByFileName(fileName)) {
			TransactionScan pc = transactionScanRepository.findByFileName(fileName).get();
			//transactionScanRepository.deleteFromTransaction(pc.getId());
			transactionScanRepository.deleteByFileName(fileName);
		}
    }

	@Override
	public void delete(final Long id) {
		transactionScanRepository.deleteById(id);
	}
	
	public TransactionScan getByTransaction(Transaction transaction) {
		return transactionScanRepository.findByTransaction(transaction);
	}
}
