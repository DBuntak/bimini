package hr.project.bimini.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Analytics;
import hr.project.bimini.model.AnalyticsType;
import hr.project.bimini.model.Building;
import hr.project.bimini.repository.AnalyticsRepository;

@Service
public class AnalyticsServiceJpa implements AnalyticsService {

	@Autowired
	private AnalyticsRepository analyticsRepository;
	
	@Override
	public List<Analytics> getAll() {
		return analyticsRepository.findAll();
	}

	@Override
	public Optional<Analytics> getById(final Long id) {
		return analyticsRepository.findById(id);
	}

	@Override
	public Analytics update(final Long id, final Analytics analytics) {
		return analyticsRepository.save(analytics);
	}

	@Override
	public Analytics save(final Analytics analytics) {
		return analyticsRepository.save(analytics);
	}

	@Override
	public void delete(final Long id) {
		analyticsRepository.deleteById(id);
	}
	
	@Override
	@Transactional
	public List<Analytics> uploadAnalytics(MultipartFile file, AnalyticsType type, Building building) throws IOException{
		Map<String, Double> records = new LinkedHashMap<>();
		List<Analytics> result = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        double value = 0.0;
		        if(values.length != 2) continue;
		        try {
		        	value = Double.parseDouble(values[1]);
		        } catch (NullPointerException | NumberFormatException e) {
		        	continue;
		        }
		        if(records.containsKey(values[0])) {
		        	records.replace(values[0], records.get(values[0])+value);
		        }
		        else {
		        	records.put(values[0], value);
		        }
		    }
		}
		analyticsRepository.deleteByBuildingAndType(building, type);
		for(Map.Entry<String, Double> entry : records.entrySet()) {
			//System.out.println(entry);
			Analytics analytics = new Analytics(entry.getKey(), entry.getValue(), type, building);
			result.add(save(analytics));
		}
		return result;
	}
	
	@Override
	public List<Analytics> getAnalytics(Building building, AnalyticsType type){
		return analyticsRepository.findByBuildingAndType(building, type);
	}
}
