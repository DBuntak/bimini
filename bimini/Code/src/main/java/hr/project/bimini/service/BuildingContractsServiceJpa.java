package hr.project.bimini.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.activation.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.ContractDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.BuildingContracts;
import hr.project.bimini.model.ContractType;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.repository.BuildingContractsRepository;
import hr.project.bimini.repository.BuildingRepository;

@Service
public class BuildingContractsServiceJpa implements BuildingContractsService {

	@Autowired
	private BuildingContractsRepository buildingContractsRepository;
	
	@Autowired
	private BuildingService buildingService;
	
	@Autowired
	private JavaMailSender emailSender;

	
	@Override
	public List<BuildingContracts> getAll() {
		return buildingContractsRepository.findAll();
	}

	@Override
	public Optional<BuildingContracts> getById(final Long id) {
		return buildingContractsRepository.findById(id);
	}

	@Override
	public BuildingContracts update(final Long id, final ContractDTO buildingContracts, ContractType type) {
		BuildingContracts old = this.getById(id).get();
		old.setConclusionDate(buildingContracts.getConclusionDate());
		old.setContractId(buildingContracts.getContractId());
		old.setContractName(buildingContracts.getContractName());
		old.setContractType(type);
		old.setDescription(buildingContracts.getDescription());
		old.setDuration(buildingContracts.getDuration());
		old.setEmail(buildingContracts.getEmail());
		old.setEmergencyNumber(buildingContracts.getEmergencyNumber());
		Calendar c = Calendar.getInstance();
        c.setTime(buildingContracts.getConclusionDate());
        c.add(Calendar.YEAR, buildingContracts.getDuration());
        Date expirationDate = c.getTime();
		old.setExpirationDate(expirationDate);
		old.setFirm(buildingContracts.getFirm());
		old.setMobilePhone(buildingContracts.getMobilePhone());
		old.setName(buildingContracts.getName());
		old.setNoticePeriod(buildingContracts.getNoticePeriod());
		old.setSurname(buildingContracts.getSurname());
		return buildingContractsRepository.save(old);
	}

	@Override
	public BuildingContracts save(final BuildingContracts buildingContracts) {
		return buildingContractsRepository.save(buildingContracts);
	}

	@Override
	public void delete(final Long id) {
		buildingContractsRepository.deleteById(id);
	}

	@Override
	public List<ContractDTO> getContractsByBuildingAndTypeDTO(final Building building, final ContractType type) {
		 List<BuildingContracts> contracts = buildingContractsRepository.findByBuildingAndContractType(building, type);
		 List<ContractDTO> dtos = new ArrayList<>();
		 for(BuildingContracts bc : contracts) {
			 ContractDTO dto = new ContractDTO(bc.getId(), bc.getFirm(), bc.getContractType().getName(), bc.getConclusionDate(), bc.getDuration(), bc.getNoticePeriod(), bc.getExpirationDate(),
					 bc.getName(), bc.getSurname(), bc.getEmail(), bc.getMobilePhone(), bc.getEmergencyNumber(), bc.getContractId(), bc.getContractName(), bc.getDescription());
			 dtos.add(dto);
		 }
		 return dtos;
	}
	
	@Override
	public List<BuildingContracts> getContractsByBuildingAndType(final Building building, final ContractType type) {
		 return buildingContractsRepository.findByBuildingAndContractType(building, type);
	}
	
	@Override
    public String storeContract(MultipartFile file, final ContractType type, final Building building, ContractDTO contractDTO) {

		/*if(buildingContractsRepository.existsByBuildingAndContractType(building, type)) {
			BuildingContracts bc = buildingContractsRepository.findByBuildingAndContractType(building, type);
			deleteContract(bc.getFileName());
		}*/
        // Normalize file name
        String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }
            Calendar c = Calendar.getInstance();
            c.setTime(contractDTO.getConclusionDate());
            c.add(Calendar.YEAR, contractDTO.getDuration());
            Date expirationDate = c.getTime();
            BuildingContracts newContract = new BuildingContracts(null, building, fileName, file.getOriginalFilename(), file.getContentType(), type, contractDTO.getFirm(), contractDTO.getConclusionDate(),
            		contractDTO.getDuration(), expirationDate, contractDTO.getNoticePeriod(), contractDTO.getName(), contractDTO.getSurname(),
            		contractDTO.getEmail(), contractDTO.getMobilePhone(), contractDTO.getEmergencyNumber(),
            		contractDTO.getContractId(), contractDTO.getContractName(), contractDTO.getDescription(), file.getBytes());
            BuildingContracts contract = buildingContractsRepository.save(newContract);
            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
    public String updateContract(MultipartFile file, Long contractId) {
		BuildingContracts old = this.getById(contractId).get();
        // Normalize file name
        String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }
            old.setContentType(file.getContentType());
            old.setData(file.getBytes());
            old.setFileName(fileName);
            old.setRealName(file.getOriginalFilename());
            BuildingContracts contract = buildingContractsRepository.save(old);
            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public BuildingContracts loadContractAsResource(Long id) {
		BuildingContracts oldContract = buildingContractsRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldContract != null) {
		    return oldContract;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + id);
		}
    }
	
    @Override
    @Transactional
    public void deleteContract(String fileName) {

		if(buildingContractsRepository.existsByFileName(fileName)) {
			buildingContractsRepository.deleteByFileName(fileName);
		}
    }
    
    @Override
    public void sendEmailWithContractsForBuildingAndType(String to, String subject, String text, Building building, ContractType type) throws MessagingException {

    	List<BuildingContracts> contracts = this.getContractsByBuildingAndType(building, type);
    	
	    MimeMessage message = emailSender.createMimeMessage();
	     
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    
	    helper.setFrom("project.bimini10@gmail.com");
	    helper.setTo(to);
	    helper.setSubject(subject);
	    helper.setText(text);
	        
	    for(BuildingContracts bc : contracts) {
		    ByteArrayDataSource ds = new ByteArrayDataSource(bc.getData(), bc.getContentType());
		    helper.addAttachment(bc.getRealName(), ds);
	    }
	    emailSender.send(message);
	}
    
}
