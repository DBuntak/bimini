package hr.project.bimini.controller.DTO;

import java.util.List;

import hr.project.bimini.model.Tenant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SnowEntryDTO {

	private Long id;
	private List<SnowApartmentsDTO> apartments;
	private List<Tenant> tenants;
	private String period;
}
