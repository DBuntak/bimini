package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.DatesDTO;
import hr.project.bimini.controller.DTO.EventDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Event;
import hr.project.bimini.model.EventReport;
import hr.project.bimini.model.EventType;
import hr.project.bimini.model.File;

public interface EventService {

	List<Event> getAll();
	Optional<Event> getById(final Long id);
	Event update(final Long id, final Event event);
	void delete(final Long id);
	EventReport getReport(Event event);
	List<Event> getByBuildingAndDate(Building d);
	List<Event> getByBuildingAndType(Building building, EventType type);
	String storeReport(MultipartFile file, Event event);
	Event updateDates(Long id, DatesDTO dates);
	List<Event> getByBuilding(Building building);
	Event save(EventDTO event, Building building, EventType type);
	Event save(Event event);
}
