package hr.project.bimini.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.Event;
import hr.project.bimini.model.EventType;

public interface EventRepository extends JpaRepository<Event, Long> {

	List<Event> findByBuildingAndEventType(Building building, EventType type);
	
	List<Event> findByBuildingAndStartAfter(Building building, Date start);
	
	List<Event> findByBuildingAndStartAfterAndEndBefore(Building building, Date start, Date end);
	
	List<Event> findByBuilding(Building building);
}
