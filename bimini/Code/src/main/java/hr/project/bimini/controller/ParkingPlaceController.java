package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.ParkingPlaceDTO;
import hr.project.bimini.controller.DTO.ParkingRentDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.ParkingPlace;
import hr.project.bimini.model.ParkingRentType;
import hr.project.bimini.model.ParkingType;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.service.ParkingPlaceServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("parking")
@AllArgsConstructor
@Slf4j
@RestController
public class ParkingPlaceController {

	@Autowired
	private ParkingPlaceServiceJpa parkingPlaceService;
	
	@Autowired
	private BuildingController buildingController;
	
	@Autowired
	private TenantController tenantController;
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping
	public ResponseEntity<List<ParkingPlace>> getAll() {
		return ResponseEntity.ok(parkingPlaceService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<ParkingPlace> getById(@PathVariable("id") final Long id) {
		Optional<ParkingPlace> parkingPlace = parkingPlaceService.getById(id);
        if (!parkingPlace.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(parkingPlace.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<ParkingPlace> updateById(@PathVariable("id") final Long id, @RequestBody final ParkingPlaceDTO parkingPlace) {
        if (!parkingPlaceService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
       /* ResponseEntity<Building> buildingResponse = buildingController.getById(parkingPlace.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		ResponseEntity<Tenant> ownerResponse = tenantController.getById(parkingPlace.getOwnerId());
		if(ownerResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Tenant rented = null;
		if(parkingPlace.getRentedId() != null) {
			ResponseEntity<Tenant> rentedResponse = tenantController.getById(parkingPlace.getRentedId());
			if(rentedResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
				ResponseEntity.badRequest().build();
			}
			rented = rentedResponse.getBody();
		}
		Building building = buildingResponse.getBody();
		Tenant owner = ownerResponse.getBody();*/
        return ResponseEntity.ok(parkingPlaceService.update(id, parkingPlace));//, building, owner, rented));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final ParkingPlaceDTO parkingPlace) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(parkingPlace.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Tenant owner = null;
		if(parkingPlace.getOwnerId() != null) {
			ResponseEntity<Tenant> ownerResponse = tenantController.getById(parkingPlace.getOwnerId());
			if(ownerResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
				return ResponseEntity.badRequest().build();
			}
			owner = ownerResponse.getBody();
		}
		Tenant rented = null;
		if(parkingPlace.getRentedId() != null) {
			if(parkingPlace.getRentedId() != null) {
				ResponseEntity<Tenant> rentedResponse = tenantController.getById(parkingPlace.getRentedId());
				if(rentedResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
					return ResponseEntity.badRequest().build();
				}
				rented = rentedResponse.getBody();
			}
		}
		ParkingRentType rentType = null;
		if(parkingPlace.getRentType() != null) {
			rentType = ParkingRentType.valueOf(parkingPlace.getRentType());
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(parkingPlaceService.save(parkingPlace, building, owner, rented, rentType));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!parkingPlaceService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        parkingPlaceService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/building/{id}")
	public ResponseEntity<List<ParkingPlace>> getByBuilding(@PathVariable Long id){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(parkingPlaceService.getByBuilding(building));
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/building/{id}/{type}")
	public ResponseEntity<List<ParkingPlace>> getByBuildingAndType(@PathVariable("id") Long id, @PathVariable("type") String type){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		ParkingType pType = null;
		try {
			pType = ParkingType.valueOf(type);
		}catch(Exception e) {
			ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(parkingPlaceService.getByBuildingAndType(building, pType));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}/{ownership}/{owner_id}")
	public ResponseEntity<ParkingPlace> attachOwerOrRented(@PathVariable("id") final Long id, @PathVariable("ownership") String ownership, 
			@PathVariable("owner_id") Long ownerId, @RequestBody ParkingRentDTO rent) {
        if (!parkingPlaceService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            return ResponseEntity.badRequest().build();
        }
		ResponseEntity<Tenant> ownerResponse = tenantController.getById(ownerId);
		if(ownerResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			return ResponseEntity.badRequest().build();
		}
		if(ParkingRentType.valueOf(rent.getRentType()) == null) {
			return ResponseEntity.badRequest().build();
		}

		Tenant owner = ownerResponse.getBody();
        return ResponseEntity.ok(parkingPlaceService.attachOwnerOrRented(id, ownership, owner, rent));
	}
	
}
