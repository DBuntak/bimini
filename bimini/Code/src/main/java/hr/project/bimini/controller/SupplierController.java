package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.SupplierDTO;
import hr.project.bimini.controller.DTO.SupplierInquiryDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Supplier;
import hr.project.bimini.service.SupplierServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("supplier")
@AllArgsConstructor
@Slf4j
@RestController
public class SupplierController {

	@Autowired
	private SupplierServiceJpa supplierService;
	
	@Autowired
	private BuildingController buildingController;
	
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Supplier>> getAll() {
		return ResponseEntity.ok(supplierService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Supplier> getById(@PathVariable("id") final Long id) {
		Optional<Supplier> supplier = supplierService.getById(id);
        if (!supplier.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(supplier.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Supplier> updateById(@PathVariable("id") final Long id, @RequestBody final SupplierDTO supplier) {
        if (!supplierService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(supplierService.update(id, supplier));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final SupplierDTO supplier) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(supplier.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(supplierService.save(supplier, building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!supplierService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        supplierService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}")
	public ResponseEntity<List<Supplier>> getByBuilding(@PathVariable Long id){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(supplierService.getByBuilding(building));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/inquiry/building/{id}")
	public ResponseEntity<?> sendInquiry(@PathVariable("id") Long id, @RequestBody SupplierInquiryDTO inquiry) throws MessagingException{
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		supplierService.sendInquiry(building, inquiry);
		return ResponseEntity.ok().build();
	}
}
