package hr.project.bimini.controller.DTO;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SupplierInquiryDTO {

	private String title;
	private String content;
	
	private List<Long> supplierIds;
}
