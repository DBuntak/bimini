package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import hr.project.bimini.controller.DTO.UploadFileResponse;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.FurnaceInspection;
import hr.project.bimini.model.FurnaceReport;
import hr.project.bimini.service.FurnaceInspectionServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("furnace_inspection")
@AllArgsConstructor
@Slf4j
@RestController
public class FurnaceInspectionController {
	@Autowired
	private FurnaceInspectionServiceJpa furnaceInspectionService;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<FurnaceInspection>> getAll() {
		return ResponseEntity.ok(furnaceInspectionService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<FurnaceInspection> getById(@PathVariable("id") final Long id) {
		Optional<FurnaceInspection> furnaceInspection = furnaceInspectionService.getById(id);
        if (!furnaceInspection.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(furnaceInspection.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<FurnaceInspection> updateById(@PathVariable("id") final Long id, @RequestBody final FurnaceInspection furnaceInspection) {
        if (!furnaceInspectionService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(furnaceInspectionService.save(furnaceInspection));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final FurnaceInspection furnaceInspection) {
		return ResponseEntity.ok(furnaceInspectionService.save(furnaceInspection));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!furnaceInspectionService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        furnaceInspectionService.delete(id);

        return ResponseEntity.ok().build();
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}")
	public ResponseEntity<List<FurnaceInspection>> getByBuilding(@PathVariable("id") Long building_id){
		ResponseEntity<Building> buildingResponse = buildingController.getById(building_id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(furnaceInspectionService.getByBuilding(building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}/report")
	public ResponseEntity<?> getReport(@PathVariable("id") Long inspectionId){
		FurnaceInspection fi = getById(inspectionId).getBody();
		
		FurnaceReport resource = furnaceInspectionService.getReport(fi);
		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getFileName());
      
        return new ResponseEntity<>(resource.getData(), header, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/{id}")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") final Long inspectionId) {
		FurnaceInspection fi = getById(inspectionId).getBody();
		
		String fileName = furnaceInspectionService.storeReport(file, fi);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/furnace_inspection/")
                .path(String.valueOf(fi.getId()))
                .path("/report")
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
	}

}

