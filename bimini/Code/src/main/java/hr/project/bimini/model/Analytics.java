package hr.project.bimini.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Analytics {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String date;	
	private double value;
	private AnalyticsType type;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Building building;

	public Analytics(String date, double value, AnalyticsType type, Building building) {
		super();
		this.date = date;
		this.value = value;
		this.type = type;
		this.building = building;
	}
	
	
	
}
