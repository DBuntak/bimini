package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.model.ChimneyMan;

public interface ChimneyManService {

	List<ChimneyMan> getAll();
	Optional<ChimneyMan> getById(final Long id);
	ChimneyMan update(final Long id, final ChimneyMan chimneyMan);
	ChimneyMan save(final ChimneyMan chimneyMan);
	void delete(final Long id);
}
