package hr.project.bimini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.EventReport;

public interface EventReportRepository extends JpaRepository<EventReport, Long> {

}
