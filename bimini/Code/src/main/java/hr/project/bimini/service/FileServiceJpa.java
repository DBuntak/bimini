package hr.project.bimini.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.File;
import hr.project.bimini.model.FileType;
import hr.project.bimini.repository.FileRepository;

@Service
public class FileServiceJpa implements FileService {

	@Autowired
	private FileRepository fileRepository;
	
	@Autowired
	private BuildingService buildingService;
	
	@Override
	public List<File> getAll() {
		return fileRepository.findAll();
	}

	@Override
	public Optional<File> getById(final Long id) {
		return fileRepository.findById(id);
	}

	@Override
	public File update(final Long id, final File file) {
		return fileRepository.save(file);
	}

	@Override
	public File save(final File file) {
		return fileRepository.save(file);
	}

	@Override
	public void delete(final Long id) {
		fileRepository.deleteById(id);
	}
	
	@Override
	public boolean existsByBuildingAndType(Building building, FileType type) {
		return fileRepository.existsByBuildingAndFileType(building, type);
	}

	@Override
	public File getFileByBuildingAndType(final Building building, final FileType type) {
		 return fileRepository.findByBuildingAndFileType(building, type);
		 
	}
	
	@Override
    public File storeFile(MultipartFile file, final FileType type, final Building building) {

		if(fileRepository.existsByBuildingAndFileType(building, type)) {
			File bc = fileRepository.findByBuildingAndFileType(building, type);
			deleteFile(bc.getFileName());
		}
        // Normalize file name
        //String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);
		String fileName = file.getOriginalFilename();
		
        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }

            File newFile = new File(null, building, fileName, file.getContentType(), type, new Date(), file.getBytes());
            File saved = fileRepository.save(newFile);
            return saved;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public File loadFileAsResource(Long id) {
		File oldContract = fileRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldContract != null) {
		    return oldContract;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + id);
		}
    }
	
    @Override
    @Transactional
    public void deleteFile(String fileName) {

		if(fileRepository.existsByFileName(fileName)) {
			fileRepository.deleteByFileName(fileName);
		}
    }
}
