package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.controller.DTO.TransactionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Transaction;
import hr.project.bimini.repository.TransactionRepository;

@Service
public class TransactionServiceJpa implements TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;
	
	@Override
	public List<Transaction> getAll() {
		return transactionRepository.findAll();
	}

	@Override
	public Optional<Transaction> getById(final Long id) {
		return transactionRepository.findById(id);
	}

	@Override
	public Transaction update(final Long id, final TransactionDTO transaction, Building building) {
		Transaction trans = new Transaction(id, building, transaction.getDate(), transaction.getAmount(), transaction.getTitle(), transaction.getDescription());
		return transactionRepository.save(trans);
	}

	@Override
	public Transaction save(final TransactionDTO transaction, final Building building) {
		Transaction trans = new Transaction(null, building, transaction.getDate(), transaction.getAmount(), transaction.getTitle(), transaction.getDescription());
		return transactionRepository.save(trans);
	}

	@Override
	public void delete(final Long id) {
		transactionRepository.deleteById(id);
	}

}
