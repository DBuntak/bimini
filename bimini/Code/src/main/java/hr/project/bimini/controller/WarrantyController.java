package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.gson.Gson;

import hr.project.bimini.controller.DTO.WarrantyDTO;
import hr.project.bimini.controller.DTO.UploadFileResponse;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Warranty;
import hr.project.bimini.model.WarrantyType;
import hr.project.bimini.service.WarrantyServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("warranty")
@AllArgsConstructor
@Slf4j
@RestController
public class WarrantyController {

	@Autowired
	private WarrantyServiceJpa warrantyService;
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Warranty>> getAll() {
		return ResponseEntity.ok(warrantyService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Warranty> getById(@PathVariable("id") final Long id) {
		Optional<Warranty> warranty = warrantyService.getById(id);
        if (!warranty.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(warranty.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Warranty> updateById(@PathVariable("id") final Long id, @RequestBody final WarrantyDTO warranty) {
        if (!warrantyService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
		if(WarrantyType.valueOf(warranty.getType()) == null) {
			ResponseEntity.badRequest().build();
		}

        return ResponseEntity.ok(warrantyService.update(id, warranty, WarrantyType.valueOf(warranty.getType())));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final Warranty warranty) {
		return ResponseEntity.ok(warrantyService.save(warranty));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!warrantyService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        warrantyService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}/type/{type}")
	public ResponseEntity<?> getWarrantysByBuildingAndType(@PathVariable("id") final Long id, @PathVariable("type") String typeName, HttpServletRequest request){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		if(WarrantyType.valueOf(typeName) == null) {
			ResponseEntity.badRequest().build();
		}
		WarrantyType type = WarrantyType.valueOf(typeName);
		List<WarrantyDTO> resource = warrantyService.getWarrantysByBuildingAndTypeDTO(building, type);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		//contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
      
        return new ResponseEntity<>(resource, HttpStatus.OK);
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/building/{id}")
    public Warranty uploadFile(@RequestParam("bill") MultipartFile bill, @RequestParam("warranty") MultipartFile warranty, @RequestParam("data") String json, @PathVariable("id") final Long building_id) {
		Gson gson = new Gson();
		WarrantyDTO warrantyDTO = gson.fromJson(json, WarrantyDTO.class);
		ResponseEntity<Building> buildingResponse = buildingController.getById(building_id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		if(WarrantyType.valueOf(warrantyDTO.getType()) == null) {
			ResponseEntity.badRequest().build();
		}
		WarrantyType type = WarrantyType.valueOf(warrantyDTO.getType());
		
		Warranty war = warrantyService.storeWarranty(bill, warranty, type, building, warrantyDTO);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/warranty/building/")
                .path(String.valueOf(building.getId()))
                .path("/type/")
                .path(type.name())
                .toUriString();

        return war;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PutMapping("/file/{id}/type/{type}")//type je bill ili warranty
    public Warranty updateFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long warrantyId, @PathVariable("type") String fileType) {
		 if (!warrantyService.getById(warrantyId).isPresent()) {
	            log.error("Id " + warrantyId + " is not existed");
	            ResponseEntity.badRequest().build();
	        }
		
		Warranty warranty = warrantyService.updateWarranty(file, warrantyId, fileType);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/warranty/download/")
                .path(warrantyId.toString())
                .toUriString();

        return warranty;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}/type/{type}")
    public ResponseEntity<?> downloadWarranty(@PathVariable("id") Long warrantyId, HttpServletRequest request, @PathVariable("type") String fileType) {
        // Load file as Resource
        Warranty resource = warrantyService.loadWarrantyAsResource(warrantyId);

        // Try to determine file's content type
        String contentType = null;
        byte[] data = null;
        String name = null;
		if(fileType.equals("bill")) {
			contentType = resource.getBillType();
			data = resource.getBillScan();
			name = resource.getBillName();
		}
		else if(fileType.equals("warranty")) {
			contentType = resource.getWarrantyType();
			data = resource.getWarrantyScan();
			name = resource.getWarrantyName();
		}

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(contentType));
        header.setContentLength(data.length);
        header.set("Content-Disposition", "attachment; filename=" + name);
      
        return new ResponseEntity<>(data, header, HttpStatus.OK);
     }

}
