package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.PurchaseContract;

public interface PurchaseContractService {

	List<PurchaseContract> getAll();
	Optional<PurchaseContract> getById(final Long id);
	PurchaseContract update(final Long id, final PurchaseContract purchaseContract);
	PurchaseContract save(final PurchaseContract purchaseContract);
	void delete(final Long id);
	String storeContract(MultipartFile file, final Apartment apartment);
	PurchaseContract loadContractAsResource(String fileName);
	void deleteContract(String fileName);
}
