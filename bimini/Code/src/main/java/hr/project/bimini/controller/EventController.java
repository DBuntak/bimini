package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import hr.project.bimini.controller.DTO.DatesDTO;
import hr.project.bimini.controller.DTO.EventDTO;
import hr.project.bimini.controller.DTO.UploadFileResponse;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.ChimneyReport;
import hr.project.bimini.model.Event;
import hr.project.bimini.model.EventReport;
import hr.project.bimini.model.EventType;
import hr.project.bimini.model.File;
import hr.project.bimini.service.BuildingService;
import hr.project.bimini.service.EventServiceJpa;
import hr.project.bimini.service.EventTypeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("event")
@AllArgsConstructor
@Slf4j
@RestController
public class EventController {

	@Autowired
	private EventServiceJpa eventService;
	
	@Autowired
	private BuildingController buildingController;
	
	@Autowired
	private EventTypeService eventTypeService;
	
	@Autowired
	private BuildingService buildingService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Event>> getAll() {
		return ResponseEntity.ok(eventService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Event> getById(@PathVariable("id") final Long id) {
		Optional<Event> event = eventService.getById(id);
        if (!event.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(event.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Event> updateById(@PathVariable("id") final Long id, @RequestBody final Event event) {
        if (!eventService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(eventService.update(id, event));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/dates/{id}")
	public ResponseEntity<Event> updateDates(@PathVariable("id") Long id, @RequestBody DatesDTO dates){
		return ResponseEntity.ok(eventService.updateDates(id, dates));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final EventDTO event) {
		Optional<Building> buildingResponse = buildingService.getById(event.getBuildingId());
		if(!buildingResponse.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.get();
		Optional<EventType> typeResponse = eventTypeService.getById(event.getTypeId());
		if(!typeResponse.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(eventService.save(event, building, typeResponse.get()));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!eventService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        eventService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/building/{id}/type/{type}")
	public ResponseEntity<List<Event>> getByBuildingAndType(@PathVariable("id") Long building_id, @PathVariable("type") String type){
		Optional<Building> buildingResponse = buildingService.getById(building_id);
		if(!buildingResponse.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.get();
		Optional<EventType> typeResponse = eventTypeService.getByName(type);
		if(!typeResponse.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(eventService.getByBuildingAndType(building, typeResponse.get()));
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/building/{id}")
	public ResponseEntity<List<Event>> getByBuilding(@PathVariable("id") Long building_id){
		Optional<Building> buildingResponse = buildingService.getById(building_id);
		if(!buildingResponse.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.get();
		return ResponseEntity.ok(eventService.getByBuilding(building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}/report")
	public ResponseEntity<?> getReport(@PathVariable("id") Long inspectionId){
		Event event = getById(inspectionId).getBody();
		
		EventReport resource = eventService.getReport(event);
		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getFileName());
      
        return new ResponseEntity<>(resource.getData(), header, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/{id}")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") final Long inspectionId) {
		Event event = getById(inspectionId).getBody();
		
		String fileName = eventService.storeReport(file, event);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/event/")
                .path(String.valueOf(event.getId()))
                .path("/report")
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
	}
}
