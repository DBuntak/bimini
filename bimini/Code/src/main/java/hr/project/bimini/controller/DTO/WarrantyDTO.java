package hr.project.bimini.controller.DTO;

import java.util.Date;

import hr.project.bimini.model.WarrantyType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WarrantyDTO {

	private Long id;
	private String firm;
	private String contractNumber;
	private Date warrantyStart;
	private Date warrantyEnd;
	private String type;
}
