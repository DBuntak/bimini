package hr.project.bimini.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import hr.project.bimini.model.Building;

public interface BuildingRepository extends JpaRepository<Building,Long> {

	@Modifying
	@Query(value = "DELETE FROM BUILDING_CONTRACTS WHERE BUILDING_ID = ?1", nativeQuery=true)
	void deleteLinkedContracts(Long building_id);
}
