package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.model.EventDoer;
import hr.project.bimini.model.EventType;

public interface EventDoerService {

	List<EventDoer> getAll();
	Optional<EventDoer> getById(final Long id);
	EventDoer update(final Long id, final EventDoer eventDoer);
	EventDoer save(final EventDoer eventDoer);
	void delete(final Long id);
	List<EventDoer> getDoersForType(EventType type);
}
