package hr.project.bimini.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.repository.BuildingRepository;

@Service
public class BuildingServiceJpa implements BuildingService {

	@Autowired
	private BuildingRepository buildingRepository;
	
	@Autowired
	private ApartmentService apartmentService;
	
	@Override
	public List<Building> getAll() {
		return buildingRepository.findAll();
	}

	@Override
	public Optional<Building> getById(final Long id) {
		return buildingRepository.findById(id);
	}

	@Override
	public Building update(final Long id, final Building building) {
		Building old = getById(id).get();
		old.setName(building.getName());
		old.setAddress(building.getAddress());
		old.setManagamentType(building.getManagamentType());
		return buildingRepository.save(old);
	}

	@Override
	public Building save(final Building building) {
		return buildingRepository.save(building);
	}

	@Override
	@Transactional
	public void delete(final Long id) {
		Optional<Building> building = buildingRepository.findById(id);
		if(building.isPresent()) {
			for(Apartment a : apartmentService.getByBuilding(building.get())) {
				apartmentService.delete(a.getId());
			}
		}
		buildingRepository.deleteLinkedContracts(id);
		buildingRepository.deleteById(id);
	}

	@Override
	public List<Apartment> getApartmentsByBuilding(Building building) {
		return apartmentService.getByBuilding(building);
	}

	@Override
	public Building updateBuilding(Building building, double balance) {
		building.setBalance(balance);
		return buildingRepository.save(building);
	}
	
}
