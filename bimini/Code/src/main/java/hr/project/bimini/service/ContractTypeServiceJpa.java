package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.model.ContractType;
import hr.project.bimini.repository.ContractTypeRepository;

@Service
public class ContractTypeServiceJpa implements ContractTypeService {

	@Autowired
	private ContractTypeRepository contractTypeRepository;
	
	@Override
	public List<ContractType> getAll() {
		return contractTypeRepository.findAll();
	}

	@Override
	public Optional<ContractType> getById(final Long id) {
		return contractTypeRepository.findById(id);
	}

	@Override
	public ContractType update(final Long id, final ContractType contractType) {
		return contractTypeRepository.save(contractType);
	}

	@Override
	public ContractType save(final ContractType contractType) {
		return contractTypeRepository.save(contractType);
	}

	@Override
	public void delete(final Long id) {
		contractTypeRepository.deleteById(id);
	}
	
	@Override
	public Optional<ContractType> getByName(final String name) {
		return contractTypeRepository.findByName(name);
	}
	
	@Override
	public boolean existsByName(String name) {
		return contractTypeRepository.existsByName(name);
	}
}
