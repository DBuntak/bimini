package hr.project.bimini.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.EventType;

public interface EventTypeRepository extends JpaRepository<EventType, Long> {

	Optional<EventType> findByName(String name);
	boolean existsByName(String name);
	boolean existsById(Long id);
}
