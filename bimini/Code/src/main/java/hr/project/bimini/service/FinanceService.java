package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.FinanceDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Finance;
import hr.project.bimini.model.FinanceType;

public interface FinanceService {
	List<Finance> getAll();
	Optional<Finance> getById(final Long id);
	Finance save(final Finance finance);
	void delete(final Long id);
	List<Finance> getFinancesByBuildingAndType(final Building building, final FinanceType type);
	Finance update(Long id, FinanceDTO finance, FinanceType type);
	Finance updateFinance(MultipartFile file, Long financeId);
	Finance loadFinanceAsResource(Long id);
	List<FinanceDTO> getFinancesByBuildingAndTypeDTO(Building building, FinanceType type);
	Finance storeFinance(MultipartFile bill, FinanceType type, Building building, FinanceDTO financeDTO);
}
