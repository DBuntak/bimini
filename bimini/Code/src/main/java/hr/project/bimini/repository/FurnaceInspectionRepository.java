package hr.project.bimini.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.FurnaceInspection;

public interface FurnaceInspectionRepository extends JpaRepository<FurnaceInspection, Long> {

	List<FurnaceInspection> findByBuilding(Building building);
	
	List<FurnaceInspection> findByBuildingAndStartAfter(Building building, Date start);
}
