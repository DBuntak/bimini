package hr.project.bimini.controller.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SupplierDTO {

	private String firm;
	private String description;
	
	//predstavnik
	private String name;
	private String surname;
	private String email;
	private String mobilePhone;
	
	private Long buildingId;
}
