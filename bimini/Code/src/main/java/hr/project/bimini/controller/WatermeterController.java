package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.WatermeterDTO;
import hr.project.bimini.controller.DTO.WatermeterUpdateDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Watermeter;
import hr.project.bimini.service.WatermeterServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("watermeter")
@AllArgsConstructor
@Slf4j
@RestController
public class WatermeterController {

	@Autowired
	private WatermeterServiceJpa watermeterService;
	
	@Autowired
	private ApartmentController apartmentController;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping
	public ResponseEntity<List<Watermeter>> getAll() {
		return ResponseEntity.ok(watermeterService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Watermeter> getById(@PathVariable("id") final Long id) {
		Optional<Watermeter> watermeter = watermeterService.getById(id);
        if (!watermeter.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(watermeter.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Watermeter> updateById(@PathVariable("id") final Long id, @RequestBody final WatermeterDTO watermeter) throws MessagingException, IOException {
        if (!watermeterService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(watermeterService.update(id, watermeter));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/tenants-number/{id}")
	public ResponseEntity<Watermeter> updateTenantsNumberById(@PathVariable("id") final Long id, @RequestBody final WatermeterUpdateDTO tenantsNumber) throws MessagingException, IOException, InvalidFormatException {
        if (!watermeterService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(watermeterService.updateTenantsNumber(id, tenantsNumber.getTenantsNumber()));
	}
	/*@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final WatermeterDTO watermeter) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(watermeter.getApartmentId());
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		return ResponseEntity.ok(watermeterService.save(watermeter, apartment));
	}*/
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!watermeterService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        watermeterService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}")
	public ResponseEntity<List<Watermeter>> getByBuilding(@PathVariable("id") final Long id) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();

        return ResponseEntity.ok(watermeterService.getByBuilding(building));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/apartment/{id}")
	public ResponseEntity<List<Watermeter>> getByApartment(@PathVariable("id") final Long id) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();

        return ResponseEntity.ok(watermeterService.getByApartment(apartment));
	}
}
