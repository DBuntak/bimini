package hr.project.bimini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.FurnaceMan;

public interface FurnaceManRepository extends JpaRepository<FurnaceMan, Long> {

}
