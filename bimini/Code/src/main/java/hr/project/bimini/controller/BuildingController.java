package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.BalanceDTO;
import hr.project.bimini.controller.DTO.DebtDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.service.BuildingServiceJpa;
import hr.project.bimini.service.TenantService;
import hr.project.bimini.service.TenantServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("building")
@AllArgsConstructor
@Slf4j
@RestController
public class BuildingController {
	@Autowired
	private BuildingServiceJpa buildingService;
	
	@Autowired
	private TenantService tenantService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Building>> getAll() {
		return ResponseEntity.ok(buildingService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Building> getById(@PathVariable("id") final Long id) {
		Optional<Building> building = buildingService.getById(id);
        if (!building.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(building.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Building> updateById(@PathVariable("id") final Long id, @RequestBody final Building building) {
        if (!buildingService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(buildingService.update(id, building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final Building building) {
		return ResponseEntity.ok(buildingService.save(building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!buildingService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        buildingService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}/tenants")
	public ResponseEntity<List<Tenant>> getTenantsByBuilding(@PathVariable("id") final Long id){
		ResponseEntity<Building> buildingResponse = getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(tenantService.getByBuilding(building));
		
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}/apartments")
	public ResponseEntity<List<Apartment>> getApartmentsByBuilding(@PathVariable("id") final Long id){
		ResponseEntity<Building> buildingResponse = getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(buildingService.getApartmentsByBuilding(building));
		
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/update/balance/{id}")
	public ResponseEntity<Building> updateBuilding(@PathVariable("id") Long id, @RequestBody BalanceDTO balance){
		ResponseEntity<Building> buildingResponse = getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(buildingService.updateBuilding(building, balance.getBalance()));
	}
}