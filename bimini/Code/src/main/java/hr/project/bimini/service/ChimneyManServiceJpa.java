package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.model.ChimneyMan;
import hr.project.bimini.repository.ChimneyManRepository;

@Service
public class ChimneyManServiceJpa implements ChimneyManService {

	@Autowired
	private ChimneyManRepository chimneyManRepository;
	
	@Override
	public List<ChimneyMan> getAll() {
		return chimneyManRepository.findAll();
	}

	@Override
	public Optional<ChimneyMan> getById(final Long id) {
		return chimneyManRepository.findById(id);
	}

	@Override
	public ChimneyMan update(final Long id, final ChimneyMan chimneyMan) {
		return chimneyManRepository.save(chimneyMan);
	}

	@Override
	public ChimneyMan save(final ChimneyMan chimneyMan) {
		return chimneyManRepository.save(chimneyMan);
	}

	@Override
	public void delete(final Long id) {
		chimneyManRepository.deleteById(id);
	}

}
