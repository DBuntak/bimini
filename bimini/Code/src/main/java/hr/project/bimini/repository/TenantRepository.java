package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;

public interface TenantRepository extends JpaRepository<Tenant,Long> {

	@Modifying
	@Query(value = "DELETE FROM APARTMENT_TENANTS WHERE TENANT_ID = ?1", nativeQuery=true)
	void deleteFromApartment(Long tenant_id);
	
	@Query(value = "SELECT * FROM TENANT WHERE id NOT IN (SELECT tenant_id FROM APARTMENT_TENANTS)", nativeQuery=true)
	List<Tenant> notInApartment();
	
	List<Tenant> findByBuildingAndRoleNotNull(Building building);
	
	List<Tenant> findByApartmentIsNull();
	
	List<Tenant> findByApartment(Apartment apartment);
	
	List<Tenant> findByApartmentAndTypeIn(Apartment apartment, List<TenantTypes> type);
	
	List<Tenant> findByBuilding(Building building);
}
