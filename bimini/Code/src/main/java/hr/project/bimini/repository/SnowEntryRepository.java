package hr.project.bimini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.SnowEntry;

public interface SnowEntryRepository extends JpaRepository<SnowEntry, Long> {

}
