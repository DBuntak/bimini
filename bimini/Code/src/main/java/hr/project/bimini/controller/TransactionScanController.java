package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import hr.project.bimini.controller.DTO.UploadFileResponse;
import hr.project.bimini.model.Transaction;
import hr.project.bimini.model.TransactionScan;
import hr.project.bimini.service.TransactionScanServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("transaction-scan")
@AllArgsConstructor
@Slf4j
@RestController
public class TransactionScanController {

	@Autowired
	private TransactionScanServiceJpa transactionScanService;
	
	@Autowired
	private TransactionController transactionController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<TransactionScan>> getAll() {
		return ResponseEntity.ok(transactionScanService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<TransactionScan> getById(@PathVariable("id") final Long id) {
		Optional<TransactionScan> transactionScan = transactionScanService.getById(id);
        if (!transactionScan.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(transactionScan.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<TransactionScan> updateById(@PathVariable("id") final Long id, @RequestBody final TransactionScan transactionScan) {
        if (!transactionScanService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(transactionScanService.save(transactionScan));
	}
	/*@PostMapping
	public ResponseEntity save(@RequestBody final TransactionScan transactionScan) {
		return ResponseEntity.ok(transactionScanService.save(transactionScan));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!transactionScanService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        transactionScanService.delete(id);

        return ResponseEntity.ok().build();
	}*/
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/transaction/{id}")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") final Long transaction_id) {
		ResponseEntity<Transaction> transactionResponse = transactionController.getById(transaction_id);
		if(transactionResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Transaction transaction = transactionResponse.getBody();
		
		String fileName = transactionScanService.storeContract(file, transaction);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/purchase_contract/download/")
                .path(String.valueOf(transaction.getId()))
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}")
    public ResponseEntity<?> downloadContract(@PathVariable("id") Long transactionId, HttpServletRequest request) {
		ResponseEntity<Transaction> transactionResponse = transactionController.getById(transactionId);
		if(transactionResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Transaction transaction = transactionResponse.getBody();
		// Load file as Resource		
		TransactionScan resource = transactionScanService.getByTransaction(transaction);
        //TransactionScan resource = transactionScanService.loadContractAsResource(contractName);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getFileName());
      
        return new ResponseEntity<>(resource.getData(), header, HttpStatus.OK);
     }
	
	@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{contractName:.+}")
    public ResponseEntity<?> delete(@PathVariable String contractName, HttpServletRequest request) throws IOException {
        transactionScanService.deleteContract(contractName);
    	return ResponseEntity.ok().build();
    }
}
