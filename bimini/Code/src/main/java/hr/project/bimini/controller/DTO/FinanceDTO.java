package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FinanceDTO {

	private Long id;
	private String description;
	private Double amount;
	private Date date;
	private String type;
}
