package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.TransactionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Transaction;
import hr.project.bimini.service.ChimneyManServiceJpa;
import hr.project.bimini.service.TransactionServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("transaction")
@AllArgsConstructor
@Slf4j
@RestController
public class TransactionController {

	@Autowired
	private TransactionServiceJpa transactionService;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping
	public ResponseEntity<List<Transaction>> getAll() {
		return ResponseEntity.ok(transactionService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Transaction> getById(@PathVariable("id") final Long id) {
		Optional<Transaction> transaction = transactionService.getById(id);
        if (!transaction.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(transaction.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Transaction> updateById(@PathVariable("id") final Long id, @RequestBody final TransactionDTO transaction) {
        if (!transactionService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        ResponseEntity<Building> buildingResponse = buildingController.getById(transaction.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
        return ResponseEntity.ok(transactionService.update(id, transaction, building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final TransactionDTO transaction) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(transaction.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(transactionService.save(transaction, building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!transactionService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        transactionService.delete(id);

        return ResponseEntity.ok().build();
	}
}
