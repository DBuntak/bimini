package hr.project.bimini.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class Event {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String title;
	private String resource;

	private String worker;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "event_type_id")
	private EventType eventType;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "building_id")
	private Building building;
	
	private Date start;
	private Date end;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@OneToOne(fetch = FetchType.LAZY) @JoinColumn(name="event_report_id")
    private EventReport eventReport;

	public Event(Long id, String title, String resource, String worker, EventType eventType, Building building,
			Date start, Date end) {
		super();
		this.id = id;
		this.title = title;
		this.resource = resource;
		this.worker = worker;
		this.eventType = eventType;
		this.building = building;
		this.start = start;
		this.end = end;
	}
	
	
}
