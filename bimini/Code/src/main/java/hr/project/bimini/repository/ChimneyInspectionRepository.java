package hr.project.bimini.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;

public interface ChimneyInspectionRepository extends JpaRepository<ChimneyInspection, Long> {

	List<ChimneyInspection> findByBuilding(Building building);
	
	List<ChimneyInspection> findByBuildingAndStartAfter(Building building, Date start);
}
