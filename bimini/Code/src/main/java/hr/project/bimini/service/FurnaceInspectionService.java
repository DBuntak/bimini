package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.FurnaceInspection;
import hr.project.bimini.model.FurnaceReport;

public interface FurnaceInspectionService {

	List<FurnaceInspection> getAll();
	Optional<FurnaceInspection> getById(final Long id);
	FurnaceInspection update(final Long id, final FurnaceInspection furnaceInspection);
	FurnaceInspection save(final FurnaceInspection furnaceInspection);
	void delete(final Long id);
	List<FurnaceInspection> getByBuilding(Building building);
	FurnaceReport getReport(FurnaceInspection furnaceInspection);
	String storeReport(MultipartFile file, final FurnaceInspection fi);
	List<FurnaceInspection> getByBuildingAndDate(Building building);
}
