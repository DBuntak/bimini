package hr.project.bimini.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.project.bimini.controller.DTO.NewApartmentDTO;
import hr.project.bimini.controller.DTO.NewTenantDTO;
import hr.project.bimini.controller.DTO.TenantRoleDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.SnowEntry;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;
import hr.project.bimini.model.TenantRoles;
import hr.project.bimini.repository.ApartmentRepository;
import hr.project.bimini.repository.TenantRepository;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TenantServiceJpa implements TenantService {
	
	@Autowired
	private TenantRepository tenantRepository;
	
	@Autowired
	private ApartmentRepository apartmentRepository;
	
	public List<Tenant> getAll() {
		return tenantRepository.findAll();
	 }
	public Optional<Tenant> getById(final Long id) {
		return tenantRepository.findById(id);
	 }
	public Tenant update(final Long id, final NewTenantDTO tenantDTO) {
		Tenant t = this.getById(id).get();
		if(TenantTypes.valueOf(tenantDTO.getType()) == TenantTypes.vanjski_vlasnik ||
				tenantDTO.getApartmentId() == null) {
			tenantDTO.setLivingInBuilding(false);
		}
		else {
			tenantDTO.setLivingInBuilding(true);
		}
		TenantTypes type = null;
		TenantRoles role = null;
		if(tenantDTO.getType() != null && tenantDTO.getType() != "") {
			type = TenantTypes.valueOf(tenantDTO.getType());
		}
		if(tenantDTO.getRole() != null && tenantDTO.getRole() != "") {
			role = TenantRoles.valueOf(tenantDTO.getRole());
		}
		Tenant tenant = new Tenant(id, tenantDTO.getName(), tenantDTO.getSurname(),
				tenantDTO.getMobilePhone(), tenantDTO.getPrivateEmail(), tenantDTO.getWorkEmail(), type,
				role, tenantDTO.isLivingInBuilding(), t.getApartment(), t.getApartment().getBuilding());
		return tenantRepository.save(tenant);
	 }
	public Tenant save(final NewTenantDTO tenantDTO, final Apartment apartment) {
		if(TenantTypes.valueOf(tenantDTO.getType()) == TenantTypes.vanjski_vlasnik ||
				tenantDTO.getApartmentId() == null) {
			tenantDTO.setLivingInBuilding(false);
		}
		else {
			tenantDTO.setLivingInBuilding(true);
		}
		TenantTypes type = null;
		TenantRoles role = null;
		if(tenantDTO.getType() != null && tenantDTO.getType() != "") {
			type = TenantTypes.valueOf(tenantDTO.getType());
		}
		if(tenantDTO.getRole() != null && tenantDTO.getRole() != "") {
			role = TenantRoles.valueOf(tenantDTO.getRole());
		}
		Tenant tenant = new Tenant(null, tenantDTO.getName(), tenantDTO.getSurname(),
				tenantDTO.getMobilePhone(), tenantDTO.getPrivateEmail(), tenantDTO.getWorkEmail(), type,
				role, tenantDTO.isLivingInBuilding(), apartment, apartment.getBuilding());
		Tenant saved = tenantRepository.save(tenant);
		return saved;
	}
	 
	@Transactional
	public void delete(final Long id) {
		tenantRepository.deleteById(id);
	}
	@Override
	public void removeFromApartment(Long tenant_id, Apartment apartment) {
		Optional<Tenant> tenantResponse = getById(tenant_id);
		if(tenantResponse.isPresent()) {
			Tenant tenant = tenantResponse.get();
			tenant.setApartment(null);
			tenantRepository.save(tenant);
		}
	}
	@Override
	public List<Tenant> getNotInApartment() {
		return tenantRepository.findByApartmentIsNull();
	}
	
	@Override
	public void addToApartment(Apartment apartment, Long tenantId) {
		Tenant t = tenantRepository.findById(tenantId).orElseThrow(() -> new NoSuchElementException("Taj entry ne postoji"));
		t.setApartment(apartment);
		t.setBuilding(apartment.getBuilding());
		tenantRepository.save(t);
	}
	
	@Override
	public TenantRoleDTO getWithRole(Building building) {
		List<Tenant> tenants = tenantRepository.findByBuildingAndRoleNotNull(building);
		return new TenantRoleDTO(tenants, building.getManagamentType());
	}
	
	@Override
	public List<Tenant> getByApartment(Apartment apartment){
		return tenantRepository.findByApartment(apartment);
	}
	
	@Override
	public List<Tenant> getByApartmentAndType(Apartment apartment, List<TenantTypes> types){
		return tenantRepository.findByApartmentAndTypeIn(apartment, types);
	}
	
	@Override
	public List<Tenant> getByBuilding(Building building){
		return tenantRepository.findByBuilding(building);
	}
	
	@Override
	public Tenant saveRemote(final NewTenantDTO tenantDTO, final Building building) {
		Tenant tenant = new Tenant(null, tenantDTO.getName(), tenantDTO.getSurname(),
				tenantDTO.getMobilePhone(), tenantDTO.getPrivateEmail(), tenantDTO.getWorkEmail(), false, building);
		Tenant saved = tenantRepository.save(tenant);
		return saved;
	}
}
