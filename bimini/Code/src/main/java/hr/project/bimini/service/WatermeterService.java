package hr.project.bimini.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import hr.project.bimini.controller.DTO.WatermeterDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Watermeter;

public interface WatermeterService {

	List<Watermeter> getAll();
	Optional<Watermeter> getById(final Long id);
	void delete(final Long id);
	//Watermeter save(WatermeterDTO watermeter, Apartment apartment);
	List<Watermeter> getByApartment(Apartment apartment);
	List<Watermeter> getByBuilding(Building building);
	void sendWaterChangeEmail(String to, String subject, String text, Watermeter meter)
			throws MessagingException, IOException, InvalidFormatException;
	Watermeter update(Long id, WatermeterDTO watermeter);
	Watermeter updateTenantsNumber(Long id, Integer tenantsNum) throws MessagingException, IOException, InvalidFormatException;
	Watermeter save(Watermeter watermeter);
}
