package hr.project.bimini.controller.DTO;

import java.util.Date;

import hr.project.bimini.model.EventDoer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventDTO {

	private Long id;
	private String title;
	private Date start;
	private Date end;
	private String resource;
	private Long typeId;
	private String worker;
	private Long buildingId;	
}
