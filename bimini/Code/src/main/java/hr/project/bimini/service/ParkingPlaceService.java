package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.controller.DTO.ParkingPlaceDTO;
import hr.project.bimini.controller.DTO.ParkingRentDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.ParkingPlace;
import hr.project.bimini.model.ParkingRentType;
import hr.project.bimini.model.ParkingType;
import hr.project.bimini.model.Tenant;

public interface ParkingPlaceService {

	List<ParkingPlace> getAll();
	Optional<ParkingPlace> getById(final Long id);
	void delete(final Long id);
	ParkingPlace save(ParkingPlaceDTO parkingPlace, Building building, Tenant owner, Tenant rented, ParkingRentType rentType);
	ParkingPlace update(Long id, ParkingPlaceDTO parkingPlace);
	List<ParkingPlace> getByBuilding(Building building);
	List<ParkingPlace> getByBuildingAndType(Building building, ParkingType type);
	ParkingPlace attachOwnerOrRented(Long id, String ownership, Tenant owner, ParkingRentDTO rent);
}
