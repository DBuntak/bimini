package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;

public interface ApartmentRepository extends JpaRepository<Apartment,Long> {

	/*@Modifying
	@Query(value = "DELETE FROM BUILDING_APARTMENTS WHERE APARTMENT_ID = ?1", nativeQuery=true)
	void deleteFromBuilding(Long apartment_id);*/
	
	@Modifying
	@Query(value = "DELETE FROM USER WHERE APARTMENT_ID = ?1", nativeQuery=true)
	void deleteLinkedUsers(Long apartment_id);
	
	List<Apartment> findByDebtGreaterThan(double debt);
	
	List<Apartment> findByBuilding(Building building);
	
	
}
