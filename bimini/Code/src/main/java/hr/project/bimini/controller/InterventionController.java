package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.gson.Gson;

import hr.project.bimini.controller.DTO.InterventionDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Intervention;
import hr.project.bimini.service.InterventionServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("intervention")
@AllArgsConstructor
@Slf4j
@RestController
public class InterventionController {

	@Autowired
	private InterventionServiceJpa interventionService;
	@Autowired
	private BuildingController buildingController;
	@Autowired
	private ApartmentController apartmentController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Intervention>> getAll() {
		return ResponseEntity.ok(interventionService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Intervention> getById(@PathVariable("id") final Long id) {
		Optional<Intervention> intervention = interventionService.getById(id);
        if (!intervention.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(intervention.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Intervention> updateById(@PathVariable("id") final Long id, @RequestBody final InterventionDTO intervention) {
        if (!interventionService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(interventionService.update(id, intervention));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final Intervention intervention) {
		return ResponseEntity.ok(interventionService.save(intervention));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!interventionService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        interventionService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}")
	public ResponseEntity<?> getInterventionsByBuilding(@PathVariable("id") final Long id, HttpServletRequest request){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		List<InterventionDTO> resource = interventionService.getInterventionsByBuilding(building);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		//contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
      
        return new ResponseEntity<>(resource, HttpStatus.OK);
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/apartment/{id}")
    public Intervention uploadFile(@RequestParam("record") MultipartFile record, @RequestParam("data") String json, @PathVariable("id") final Long apartment_id) {
		Gson gson = new Gson();
		InterventionDTO interventionDTO = gson.fromJson(json, InterventionDTO.class);
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartment_id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		
		Intervention war = interventionService.storeIntervention(record, apartment, interventionDTO);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/intervention/apartment/")
                .path(String.valueOf(apartment.getId()))
                .toUriString();

        return war;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PutMapping("/file/{id}")
    public Intervention updateFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long interventionId) {
		 if (!interventionService.getById(interventionId).isPresent()) {
	            log.error("Id " + interventionId + " is not existed");
	            ResponseEntity.badRequest().build();
	        }
		
		Intervention intervention = interventionService.updateIntervention(file, interventionId);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/intervention/download/")
                .path(interventionId.toString())
                .toUriString();

        return intervention;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}")
    public ResponseEntity<?> downloadIntervention(@PathVariable("id") Long interventionId, HttpServletRequest request) {
        // Load file as Resource
        Intervention resource = interventionService.loadInterventionAsResource(interventionId);

        // Try to determine file's content type
        String contentType = null;
        byte[] data = null;
        String name = null;
		contentType = resource.getRecordType();
		data = resource.getRecordScan();
		name = resource.getRecordName();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(contentType));
        header.setContentLength(data.length);
        header.set("Content-Disposition", "attachment; filename=" + name);
      
        return new ResponseEntity<>(data, header, HttpStatus.OK);
     }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/report/building/{id}/year/{year}")
	public ResponseEntity<?> getReportByBuildingAndDate(@PathVariable("id") final Long id, @PathVariable("year") Integer year) throws IOException{
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();

        byte[] report = interventionService.getReportByBuildingAndDate(building, year);
        
        String contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(contentType));
        header.setContentLength(report.length);
        header.set("Content-Disposition", "attachment; filename=" + "Obrazac za referendum");
        return new ResponseEntity<>(report, header, HttpStatus.OK);		
		
	}

}
