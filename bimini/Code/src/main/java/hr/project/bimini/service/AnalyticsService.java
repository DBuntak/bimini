package hr.project.bimini.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Analytics;
import hr.project.bimini.model.AnalyticsType;
import hr.project.bimini.model.Building;

public interface AnalyticsService {

	List<Analytics> getAll();
	Optional<Analytics> getById(final Long id);
	Analytics update(final Long id, final Analytics analytics);
	Analytics save(final Analytics analytics);
	void delete(final Long id);
	List<Analytics> uploadAnalytics(MultipartFile file, AnalyticsType type, Building building) throws IOException;
	List<Analytics> getAnalytics(Building building, AnalyticsType type);
}
