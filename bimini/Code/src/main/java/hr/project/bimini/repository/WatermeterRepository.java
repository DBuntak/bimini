package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Watermeter;

public interface WatermeterRepository extends JpaRepository<Watermeter, Long> {

	List<Watermeter> findByApartment(Apartment apartment);
	List<Watermeter> findByBuilding(Building building);
}
