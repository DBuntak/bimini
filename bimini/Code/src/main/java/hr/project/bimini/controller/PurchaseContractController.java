package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import hr.project.bimini.controller.DTO.UploadFileResponse;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.service.PurchaseContractServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("purchase_contract")
@AllArgsConstructor
@Slf4j
@RestController
public class PurchaseContractController {
	@Autowired
	private PurchaseContractServiceJpa purchaseContractService;
	
	@Autowired
	private ApartmentController apartmentController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<PurchaseContract>> getAll() {
		return ResponseEntity.ok(purchaseContractService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<PurchaseContract> getById(@PathVariable("id") final Long id) {
		Optional<PurchaseContract> purchaseContract = purchaseContractService.getById(id);
        if (!purchaseContract.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(purchaseContract.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<PurchaseContract> updateById(@PathVariable("id") final Long id, @RequestBody final PurchaseContract purchaseContract) {
        if (!purchaseContractService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(purchaseContractService.save(purchaseContract));
	}
	/*@PostMapping
	public ResponseEntity save(@RequestBody final PurchaseContract purchaseContract) {
		return ResponseEntity.ok(purchaseContractService.save(purchaseContract));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!purchaseContractService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        purchaseContractService.delete(id);

        return ResponseEntity.ok().build();
	}*/
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/apartment/{id}")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") final Long apartment_id) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartment_id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		
		String fileName = purchaseContractService.storeContract(file, apartment);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/purchase_contract/download/")
                .path(String.valueOf(apartment.getId()))
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}")
    public ResponseEntity<?> downloadContract(@PathVariable("id") Long apartmentId, HttpServletRequest request) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartmentId);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		// Load file as Resource		
		PurchaseContract resource = purchaseContractService.getByApartment(apartment);
        //PurchaseContract resource = purchaseContractService.loadContractAsResource(contractName);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getFileName());
      
        return new ResponseEntity<>(resource.getData(), header, HttpStatus.OK);
     }
	
	@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{contractName:.+}")
    public ResponseEntity<?> delete(@PathVariable String contractName, HttpServletRequest request) throws IOException {
        purchaseContractService.deleteContract(contractName);
    	return ResponseEntity.ok().build();
    }
    
}
