package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.Warranty;
import hr.project.bimini.model.WarrantyType;

public interface WarrantyRepository extends JpaRepository<Warranty, Long> {

	List<Warranty> findByBuildingAndType(Building building, WarrantyType type);
	
	boolean existsByBuildingAndType(Building building, WarrantyType type);
}
