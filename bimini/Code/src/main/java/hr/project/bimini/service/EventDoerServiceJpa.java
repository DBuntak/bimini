package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.model.EventDoer;
import hr.project.bimini.model.EventType;
import hr.project.bimini.repository.EventDoerRepository;

@Service
public class EventDoerServiceJpa implements EventDoerService {

	@Autowired
	private EventDoerRepository eventDoerRepository;
	
	@Override
	public List<EventDoer> getAll() {
		return eventDoerRepository.findAll();
	}

	@Override
	public Optional<EventDoer> getById(final Long id) {
		return eventDoerRepository.findById(id);
	}

	@Override
	public EventDoer update(final Long id, final EventDoer eventDoer) {
		return eventDoerRepository.save(eventDoer);
	}

	@Override
	public EventDoer save(final EventDoer eventDoer) {
		return eventDoerRepository.save(eventDoer);
	}

	@Override
	public void delete(final Long id) {
		eventDoerRepository.deleteById(id);
	}
	
	@Override
	public List<EventDoer> getDoersForType(EventType type){
		return eventDoerRepository.findByEventType(type);
	}
}
