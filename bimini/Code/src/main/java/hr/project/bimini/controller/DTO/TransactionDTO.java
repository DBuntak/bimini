package hr.project.bimini.controller.DTO;

import java.util.Date;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.Transaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransactionDTO {

	private Date date;
	private Double amount;
	private String title;
	private String description;
	private Long buildingId;
}
