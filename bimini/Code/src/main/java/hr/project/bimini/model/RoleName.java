package hr.project.bimini.model;

/**
 * 
 * @author Adrian
 * This enumeration is used to determine user
 *  role (permissions) in application.
 */
/*public enum RoleName {
	employee, employer, admin;

}
*/
public enum  RoleName {
    ROLE_APARTMENT,
    ROLE_ADMIN,
}