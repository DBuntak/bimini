package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import hr.project.bimini.controller.DTO.SnowApartmentsDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.SnowEntry;
import hr.project.bimini.model.SnowSchedule;

public interface SnowScheduleService {

	List<SnowSchedule> getAll();
	Optional<SnowSchedule> getById(final Long id);
	SnowSchedule update(final Long id, final SnowSchedule snowSchedule);
	SnowSchedule save(final SnowSchedule snowSchedule);
	void delete(final Long id);
	void removeApartmentFromEntry(Apartment apartment, Long snowEntryId);
	void addApartmentToEntry(Apartment apartment, Long snowEntryId);
	Set<SnowApartmentsDTO> getApartmentsToAddToEntry(Long buildingId, Long snowEntryId);
	void deleteByBuilding(Building building);
	void replaceApartments(Apartment apartment1, Apartment apartment2);
	void changeApartmentEntry(SnowEntry snowEntry, Apartment apartment);
}
