package hr.project.bimini.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.DatesDTO;
import hr.project.bimini.controller.DTO.EventDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.Event;
import hr.project.bimini.model.EventReport;
import hr.project.bimini.model.EventType;
import hr.project.bimini.model.File;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.Watermeter;
import hr.project.bimini.model.ChimneyReport;
import hr.project.bimini.model.Event;
import hr.project.bimini.repository.EventReportRepository;
import hr.project.bimini.repository.EventRepository;

@Service
public class EventServiceJpa implements EventService {

	@Autowired
	private EventRepository eventRepository;
	
	@Autowired
	private EventReportRepository eventReportRepository;
	
	@Autowired
	private BuildingService buildingService;
	
	@Autowired
	private TenantService tenantService;
	
	@Autowired
	private JavaMailSender emailSender;
	
	@Override
	public List<Event> getAll() {
		return eventRepository.findAll();
	}

	@Override
	public Optional<Event> getById(final Long id) {
		return eventRepository.findById(id);
	}

	@Override
	public Event update(final Long id, final Event event) {
		return eventRepository.save(event);
	}
	
	@Override
	public Event updateDates(Long id, DatesDTO dates) {
		Event event = getById(id).get();
		event.setStart(dates.getStart());
		event.setEnd(dates.getEnd());
		return eventRepository.save(event);
	}

	@Override
	public Event save(final EventDTO event, Building building, EventType type) {
		Event e = new Event(null, event.getTitle(), event.getResource(), event.getWorker(), type,
				building, event.getStart(), event.getEnd());
		return eventRepository.save(e);
	}
	
	@Override
	public Event save(final Event event) {
		return eventRepository.save(event);
	}

	@Override
	public void delete(final Long id) {
		eventRepository.deleteById(id);
	}
	
	@Override
	public List<Event> getByBuildingAndType(Building building, EventType type) {
		return eventRepository.findByBuildingAndEventType(building, type);
	}
	
	@Override
	public List<Event> getByBuilding(Building building) {
		return eventRepository.findByBuilding(building);
	}
	
	@Override
	public EventReport getReport(Event event) {
		return event.getEventReport();
	}
	
	@Override
	public List<Event> getByBuildingAndDate(Building building){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1);
		return eventRepository.findByBuildingAndStartAfter(building, cal.getTime());
	}

	@Override
	@Transactional
	public String storeReport(MultipartFile file, Event event) {
		// Normalize file name
        String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }

            EventReport newReport = new EventReport(null, fileName, file.getContentType(), new Date(), file.getBytes());
            if(event.getEventReport() != null) {
            	eventReportRepository.deleteById(event.getEventReport().getId());
            }
            event.setEventReport(eventReportRepository.save(newReport));
            eventRepository.save(event);
            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
	}
	
	@Scheduled(cron = "0 0 2 * * ?")
	public void sendNotificationEmail() throws MessagingException, IOException {
		
		Date date = new Date();
		Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        c.add(Calendar.DATE, 5);
        Date start = c.getTime();
        c.add(Calendar.DATE, 1);
        Date end = c.getTime();
        
        List<Building> buildings = buildingService.getAll();
        
        for(Building b : buildings) {
        	List<Tenant> tenants = tenantService.getByBuilding(b);
        	List<Event> events = eventRepository.findByBuildingAndStartAfterAndEndBefore(b, start, end);
        	for(Event e : events) {
        		MimeMessage message = emailSender.createMimeMessage();
	       	     
        	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
        	    
        	    helper.setFrom("project.bimini10@gmail.com");
        	    helper.setSubject("Podsjetnik: "+e.getTitle());
        	    helper.setText(e.getResource());
	        	for(Tenant t : tenants) {
	        		helper.setTo(t.getPrivateEmail());
	        	    emailSender.send(message);
	        	}
        	}
        }
	}
}
