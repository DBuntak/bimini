package hr.project.bimini.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.FinanceDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Finance;
import hr.project.bimini.model.FinanceType;
import hr.project.bimini.repository.FinanceRepository;

@Service
public class FinanceServiceJpa implements FinanceService {

	@Autowired
	private FinanceRepository financeRepository;
	
	@Autowired
	private BuildingService buildingService;

	
	@Override
	public List<Finance> getAll() {
		return financeRepository.findAll();
	}

	@Override
	public Optional<Finance> getById(final Long id) {
		return financeRepository.findById(id);
	}

	@Override
	public Finance update(final Long id, final FinanceDTO finance, FinanceType type) {
		Finance old = this.getById(id).get();
		if(type == FinanceType.godisnji_plan) {
			return old;
		}
		else if(type == FinanceType.sitni_racuni) {
			old.setAmount(finance.getAmount());
		}
		old.setDate(finance.getDate());
		old.setDescription(finance.getDescription());

		return financeRepository.save(old);
	}

	@Override
	public Finance save(final Finance finance) {
		return financeRepository.save(finance);
	}

	@Override
	public void delete(final Long id) {
		financeRepository.deleteById(id);
	}

	@Override
	public List<FinanceDTO> getFinancesByBuildingAndTypeDTO(final Building building, final FinanceType type) {
		 List<Finance> finance = financeRepository.findByBuildingAndType(building, type);
		 List<FinanceDTO> dtos = new ArrayList<>();
		 for(Finance bc : finance) {
			 FinanceDTO dto = new FinanceDTO(bc.getId(), bc.getDescription(), bc.getAmount(), bc.getDate(), type.name());
			 dtos.add(dto);
		 }
		 return dtos;
	}
	
	@Override
	public List<Finance> getFinancesByBuildingAndType(final Building building, final FinanceType type) {
		 return financeRepository.findByBuildingAndType(building, type);
	}
	
	@Override
    public Finance storeFinance(MultipartFile bill, final FinanceType type, final Building building, FinanceDTO financeDTO) {

		String billName = null;
		String billType = null;
		byte[] billData = null;
        try {
            // Check if the file's name contains invalid characters
        	if(bill != null) {
	            if(bill.getOriginalFilename().contains("..")) {
	                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + bill.getOriginalFilename());
	            }
	            billName = bill.getOriginalFilename();
	            billType = bill.getContentType();
	            billData = bill.getBytes();
        	}
            Finance newFinance = new Finance();
            if(type == FinanceType.godisnji_plan) {
            	LocalDate ld = LocalDate.now();
            	newFinance.setDate(new Date());
            	newFinance.setDescription("Godišnji plan "+ld.getDayOfMonth()+"."+ld.getMonthValue()+"."+ld.getYear()+".");
            	newFinance.setBillName(billName);
                newFinance.setBillType(billType);
                newFinance.setBillScan(billData);
                newFinance.setType(type);
                newFinance.setBuilding(building);
            	return financeRepository.save(newFinance);
            }
            else if(type == FinanceType.sitni_racuni) {
            	newFinance.setAmount(financeDTO.getAmount());
            }
            newFinance.setDate(financeDTO.getDate());
            newFinance.setDescription(financeDTO.getDescription());
            newFinance.setBillName(billName);
            newFinance.setBillType(billType);
            newFinance.setBillScan(billData);
            newFinance.setType(type);
            newFinance.setBuilding(building);
            Finance war = financeRepository.save(newFinance);
            return war;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + billName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
    public Finance updateFinance(MultipartFile file, Long financeId) {
		Finance old = this.getById(financeId).get();
        try {
            // Check if the file's name contains invalid characters
            if(file.getOriginalFilename().contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + file.getOriginalFilename());
            }
        	old.setBillName(file.getOriginalFilename());
        	old.setBillType(file.getContentType());
        	old.setBillScan(file.getBytes());

            return financeRepository.save(old);
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + file.getOriginalFilename() + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public Finance loadFinanceAsResource(Long id) {
		Finance oldFinance = financeRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldFinance != null) {
		    return oldFinance;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + id);
		}
    }
}
