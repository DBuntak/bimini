package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.NewApartmentDTO;
import hr.project.bimini.controller.DTO.NewTenantDTO;
import hr.project.bimini.controller.DTO.TenantRoleDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.service.TenantServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping("tenant")
@AllArgsConstructor
@Slf4j
@RestController
public class TenantController {
	@Autowired
	private TenantServiceJpa tenantService;
	
	@Autowired
	private ApartmentController apartmentController;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Tenant>> getAll() {
		return ResponseEntity.ok(tenantService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Tenant> getById(@PathVariable("id") final Long id) {
		Optional<Tenant> tenant = tenantService.getById(id);
        if (!tenant.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(tenant.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Tenant> updateById(@PathVariable("id") final Long id, @RequestBody final NewTenantDTO tenant) {
        if (!tenantService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(tenantService.update(id, tenant));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final NewTenantDTO tenantDTO) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(tenantDTO.getApartmentId());
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		return ResponseEntity.ok(tenantService.save(tenantDTO, apartment));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!tenantService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        tenantService.delete(id);

        return ResponseEntity.ok().build();
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/{tenant_id}/apartment/{apartment_id}")
	public ResponseEntity removeFromApartment(@PathVariable("tenant_id") final Long tenant_id,
			@PathVariable("apartment_id") final Long apartment_id) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartment_id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		tenantService.removeFromApartment(tenant_id, apartment);
		return ResponseEntity.ok().build();
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/not-in-apartment")
	public ResponseEntity<List<Tenant>> getNotInApartment() {
		return ResponseEntity.ok(tenantService.getNotInApartment());
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/{tenant_id}/add/apartment/{apartment_id}")
	public ResponseEntity addToApartment(@PathVariable("tenant_id") final Long tenant_id,
			@PathVariable("apartment_id") final Long apartment_id) {
		if(tenant_id == 99999 || tenant_id == null) {
			return ResponseEntity.badRequest().build();
		}
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartment_id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		tenantService.addToApartment(apartment, tenant_id);
		return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}/with-role")
	public ResponseEntity<TenantRoleDTO> getWithRole(@PathVariable("id") Long buildingId) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(buildingId);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(tenantService.getWithRole(building));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/building/{id}/remote")
	public ResponseEntity remoteTenantSave(@RequestBody final NewTenantDTO tenantDTO, @PathVariable("id") Long buildingId) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(buildingId);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(tenantService.saveRemote(tenantDTO, building));
	}
}
