package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.controller.DTO.NewUserDTO;
import hr.project.bimini.controller.DTO.UserBuildingDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Role;
import hr.project.bimini.model.User;

public interface UserService {

	List<User> getAll();
	Optional<User> getById(final Long id);
	Optional<User> getByUsername(final String username);
	Optional<List<User>> getByRole(final Role role);
	Optional<List<User>> getByApartment(final Apartment apartment);
	Optional<Role> getRoleById(final Long roleId);
	User update(final Long id, final User user);
	User save(final User user);
	void delete(final Long id);
	UserBuildingDTO getBuildingForUser(User user);
	List<User> getAdminsAndByBuilding(Building building);
	User createAdmin(NewUserDTO user);
	User createTenant(NewUserDTO user, Building building, Apartment apartment);
	User updateApartment(Long id, NewUserDTO user, Apartment apartment, Building building);
}
