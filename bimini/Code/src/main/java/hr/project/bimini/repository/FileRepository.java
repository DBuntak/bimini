package hr.project.bimini.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.File;
import hr.project.bimini.model.FileType;

public interface FileRepository extends JpaRepository<File, Long> {

	File findByBuildingAndFileType(Building building, FileType type);
	
	boolean existsByBuildingAndFileType(Building building, FileType type);
	
	Optional<File> findByFileName(String fileName);
	
	boolean existsByFileName(String fileName);
	
	void deleteByFileName(String fileName);
}
