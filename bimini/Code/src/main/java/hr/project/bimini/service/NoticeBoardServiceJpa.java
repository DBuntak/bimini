package hr.project.bimini.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.controller.DTO.NoticeBoardDTO;
import hr.project.bimini.controller.DTO.NoticePositionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.NoticeBoard;
import hr.project.bimini.model.NoticeType;
import hr.project.bimini.model.ParkingType;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.repository.NoticeBoardRepository;

@Service
public class NoticeBoardServiceJpa implements NoticeBoardService {

	@Autowired
	private NoticeBoardRepository noticeBoardRepository;
	
	@Override
	public List<NoticeBoard> getAll() {
		return noticeBoardRepository.findAll();
	}

	@Override
	public Optional<NoticeBoard> getById(final Long id) {
		return noticeBoardRepository.findById(id);
	}

	@Override
	public NoticeBoard update(final Long id, final NoticeBoardDTO noticeBoard) {
		NoticeBoard nb = this.getById(id).get();
		nb.setDate(new Date());
		nb.setMessage(noticeBoard.getMessage());
		nb.setTitle(noticeBoard.getTitle());
		return noticeBoardRepository.save(nb);
	}

	@Override
	public NoticeBoard updatePosition(final Long id, final NoticePositionDTO noticeBoard) {
		NoticeBoard nb = this.getById(id).get();
		if(noticeBoard.getX() == null) {
			noticeBoard.setX(0);
		}
		if(noticeBoard.getY() == null) {
			noticeBoard.setY(0);
		}
		nb.setX(nb.getX() + noticeBoard.getX());
		nb.setY(nb.getY() + noticeBoard.getY());
		return noticeBoardRepository.save(nb);
	}
	
	@Override
	public NoticeBoard save(final NoticeBoardDTO noticeBoard, final Building building, NoticeType type) {
		NoticeBoard board = new NoticeBoard(null, noticeBoard.getTitle(), noticeBoard.getMessage(), new Date(), type, noticeBoard.getX(), noticeBoard.getY(), building);
		return noticeBoardRepository.save(board);
	}

	@Override
	public void delete(final Long id) {
		noticeBoardRepository.deleteById(id);
	}
	
	@Override
	public List<NoticeBoard> getByBuilding(Building building){
		return noticeBoardRepository.findByBuilding(building);
	}
	
	@Override
	public List<NoticeBoard> getByBuildingAndType(Building building, NoticeType type){
		return noticeBoardRepository.findByBuildingAndType(building, type);
	}
}
