package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.NoticeBoardDTO;
import hr.project.bimini.controller.DTO.NoticePositionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.NoticeBoard;
import hr.project.bimini.model.NoticeType;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.service.NoticeBoardServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("notice-board")
@AllArgsConstructor
@Slf4j
@RestController
public class NoticeBoardController {

	@Autowired
	private NoticeBoardServiceJpa noticeBoardService;
	
	@Autowired
	private BuildingController buildingController;
	
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping
	public ResponseEntity<List<NoticeBoard>> getAll() {
		return ResponseEntity.ok(noticeBoardService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<NoticeBoard> getById(@PathVariable("id") final Long id) {
		Optional<NoticeBoard> noticeBoard = noticeBoardService.getById(id);
        if (!noticeBoard.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(noticeBoard.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<NoticeBoard> updateById(@PathVariable("id") final Long id, @RequestBody final NoticeBoardDTO noticeBoard) {
        if (!noticeBoardService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(noticeBoardService.update(id, noticeBoard));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/position/{id}")
	public ResponseEntity<NoticeBoard> updatePositionById(@PathVariable("id") final Long id, @RequestBody final NoticePositionDTO noticeBoard) {
        if (!noticeBoardService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(noticeBoardService.updatePosition(id, noticeBoard));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final NoticeBoardDTO noticeBoard) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(noticeBoard.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		if(NoticeType.valueOf(noticeBoard.getType()) == null) {
			ResponseEntity.badRequest().build();
		}
		NoticeType type = NoticeType.valueOf(noticeBoard.getType());
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(noticeBoardService.save(noticeBoard, building, type));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!noticeBoardService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        noticeBoardService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/building/{id}")
	public ResponseEntity<List<NoticeBoard>> getByBuilding(@PathVariable Long id){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(noticeBoardService.getByBuilding(building));
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/building/{id}/type/{type}")
	public ResponseEntity<List<NoticeBoard>> getByBuildingAndType(@PathVariable("id") Long id, @PathVariable("type") String type){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		if(NoticeType.valueOf(type) == null) {
			ResponseEntity.badRequest().build();
		}
		NoticeType typee = NoticeType.valueOf(type);
		return ResponseEntity.ok(noticeBoardService.getByBuildingAndType(building, typee));
	}
}
