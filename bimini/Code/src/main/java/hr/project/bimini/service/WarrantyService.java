package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.WarrantyDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Warranty;
import hr.project.bimini.model.WarrantyType;

public interface WarrantyService {

	List<Warranty> getAll();
	Optional<Warranty> getById(final Long id);
	Warranty save(final Warranty warranty);
	void delete(final Long id);
	List<Warranty> getWarrantysByBuildingAndType(final Building building, final WarrantyType type);
	Warranty update(Long id, WarrantyDTO warranty, WarrantyType type);
	Warranty updateWarranty(MultipartFile file, Long warrantyId, String fileType);
	Warranty loadWarrantyAsResource(Long id);
	List<WarrantyDTO> getWarrantysByBuildingAndTypeDTO(Building building, WarrantyType type);
	Warranty storeWarranty(MultipartFile bill, MultipartFile warranty, WarrantyType type, Building building,
			WarrantyDTO warrantyDTO);
}
