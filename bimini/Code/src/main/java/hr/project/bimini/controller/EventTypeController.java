package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.model.EventType;
import hr.project.bimini.service.ChimneyManService;
import hr.project.bimini.service.EventTypeServiceJpa;
import hr.project.bimini.service.FurnaceManService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("event-type")
@AllArgsConstructor
@Slf4j
@RestController
public class EventTypeController {

	@Autowired
	private EventTypeServiceJpa eventTypeService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<EventType>> getAll() {
		return ResponseEntity.ok(eventTypeService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<EventType> getById(@PathVariable("id") final Long id) {
		Optional<EventType> eventType = eventTypeService.getById(id);
        if (!eventType.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(eventType.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<EventType> updateById(@PathVariable("id") final Long id, @RequestBody final EventType eventType) {
        if (!eventTypeService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(eventTypeService.save(eventType));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final EventType eventType) {
		return ResponseEntity.ok(eventTypeService.save(eventType));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!eventTypeService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        eventTypeService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/name/{name}")
	public ResponseEntity<EventType> getByName(@PathVariable("name") final String name) {
		Optional<EventType> eventType = eventTypeService.getByName(name);
        if (!eventType.isPresent()) {
            log.error("Name " + name + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(eventType.get());
	}
}
