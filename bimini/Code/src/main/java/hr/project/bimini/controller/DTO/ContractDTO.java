package hr.project.bimini.controller.DTO;

import java.util.Date;

import hr.project.bimini.model.EventDoer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContractDTO {

	private Long id;
	private String firm;
	private String contractType;
	private Date conclusionDate;
	private Integer duration;
	private Integer noticePeriod;
	private Date expirationDate;
	private String name;
	private String surname;
	private String email;
	private String mobilePhone;
	private String emergencyNumber;
	private String contractId;
	private String contractName;
	private String description;
}
