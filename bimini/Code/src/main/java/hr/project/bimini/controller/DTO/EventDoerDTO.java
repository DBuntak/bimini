package hr.project.bimini.controller.DTO;

import hr.project.bimini.model.EventDoer;
import hr.project.bimini.model.EventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class EventDoerDTO {

	private Long id;
	private String name;
	private String surname;
	private String firmName;
	private String mobilePhone;
	private String email;
}
