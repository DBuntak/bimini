package hr.project.bimini.controller.DTO;

import java.util.Date;

import hr.project.bimini.model.EventDoer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MailDTO {

	private String to;
	private String subject;
	private String text;
}
