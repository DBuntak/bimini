package hr.project.bimini.model;

public enum FileType {
	WATER, GAS, ELECTRICITY, CLEANING, WASTE, REST, KUĆNI_RED, PRIMOPREDAJA_ZGRADE;
}
