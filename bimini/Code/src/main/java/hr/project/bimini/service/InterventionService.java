package hr.project.bimini.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.InterventionDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Intervention;

public interface InterventionService {

	List<Intervention> getAll();
	Optional<Intervention> getById(final Long id);
	Intervention save(final Intervention intervention);
	void delete(final Long id);
	Intervention update(Long id, InterventionDTO intervention);
	Intervention updateIntervention(MultipartFile file, Long interventionId);
	Intervention loadInterventionAsResource(Long id);
	List<InterventionDTO> getInterventionsByBuilding(Building building);
	Intervention storeIntervention(MultipartFile record, Apartment apartment, InterventionDTO interventionDTO);
	byte[] getReportByBuildingAndDate(Building building, Integer year) throws IOException;
}
