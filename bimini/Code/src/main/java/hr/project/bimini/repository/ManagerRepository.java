package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.Manager;

public interface ManagerRepository extends JpaRepository<Manager, Long> {

	List<Manager> findByBuilding(Building building);
}
