package hr.project.bimini.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.project.bimini.controller.DTO.SnowApartmentsDTO;
import hr.project.bimini.controller.DTO.SnowEntryDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.SnowEntry;
import hr.project.bimini.model.SnowPeriods;
import hr.project.bimini.model.SnowSchedule;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;
import hr.project.bimini.repository.SnowEntryRepository;
import hr.project.bimini.repository.SnowScheduleRepository;
import javassist.NotFoundException;

@Service
public class SnowScheduleServiceJpa implements SnowScheduleService {

	@Autowired
	private SnowScheduleRepository snowScheduleRepository;
	
	@Autowired
	private SnowEntryRepository snowEntryRepository;
	
	@Autowired
	private BuildingService buildingService;
	
	@Autowired
	private TenantService tenantService;
	
	@Autowired
	private ApartmentService apartmentService;
	
	@Override
	public List<SnowSchedule> getAll() {
		return snowScheduleRepository.findAll();
	}

	@Override
	public Optional<SnowSchedule> getById(final Long id) {
		return snowScheduleRepository.findById(id);
	}

	@Override
	public SnowSchedule update(final Long id, final SnowSchedule snowSchedule) {
		return snowScheduleRepository.save(snowSchedule);
	}

	@Override
	public SnowSchedule save(final SnowSchedule snowSchedule) {
		return snowScheduleRepository.save(snowSchedule);
	}

	@Override
	public void delete(final Long id) {
		snowScheduleRepository.deleteById(id);
	}
	
	public SnowSchedule getByBuilding(Long buildingId) {
		Building building = buildingService.getById(buildingId).orElseThrow(() -> new NoSuchElementException("Ne postoji zgrada s tim id-om"));
		return snowScheduleRepository.findByBuilding(building).orElseThrow(() -> new NoSuchElementException("Ne postoji raspored za tu zgradu"));
	}
	
	public List<Tenant> getTenantsForPeriod(Long snowEntryId) {
		SnowEntry se = snowEntryRepository.findById(snowEntryId).get();
		List<Tenant> tenants = new ArrayList<>();
		for(Apartment a : se.getApartments()) {
			tenants.addAll(tenantService.getByApartment(a));
		}
		return tenants;
	}
	
	public List<SnowEntryDTO> getTableSnowEntries(Long buildingId){
		Building building = buildingService.getById(buildingId).orElseThrow(() -> new NoSuchElementException("Ne postoji zgrada s tim id-om"));
		Optional<SnowSchedule> oss = snowScheduleRepository.findByBuilding(building);
		SnowSchedule ss;
		if(oss.isPresent()) {
			ss = oss.get();
		}
		else {
			return new ArrayList<SnowEntryDTO>();
		}
		List<SnowEntryDTO> entries = new ArrayList<>();
		for(SnowEntry se : ss.getSchedule()) {
			//String apartments = "";
			List<SnowApartmentsDTO> apartments = new ArrayList<>();
			for(Apartment a : se.getApartments()) {
				//apartments += a.getName() + " ";
				apartments.add(new SnowApartmentsDTO(a.getId(), a.getName()));
			}
			SnowEntryDTO entry = new SnowEntryDTO(se.getId(), apartments, se.getOwners(), se.getPeriod());
			entries.add(entry);
		}
		return entries;
	}
	
	@Transactional
	public List<SnowEntryDTO> createSchedule(final Long buildingId) {
		Building building = buildingService.getById(buildingId).orElseThrow(() -> new NoSuchElementException("Ne postoji zgrada s tim id-om"));
		SnowSchedule old = null;
		if(snowScheduleRepository.existsByBuilding(building)) {
			old = snowScheduleRepository.findByBuilding(building).get();
		}
		this.deleteByBuilding(building);
		List<Apartment> apartments = apartmentService.getByBuilding(building);
		SnowSchedule ss = new SnowSchedule();
		ss.setBuilding(building);
		List<SnowEntry> schedule = new ArrayList<>();
		Map<Integer, List<Apartment>> grouped = new TreeMap<>();
		List<List<Tenant>> surnames = new ArrayList<>();
		for(Apartment a : apartments) {
			List<Apartment> list = grouped.get(a.getFloor());
			if(list == null) list = new ArrayList<Apartment>();
			list.add(a);
			grouped.put(a.getFloor(), list);
		}
		List<TenantTypes> types = new ArrayList<>();
		types.add(TenantTypes.vanjski_vlasnik);
		types.add(TenantTypes.vlasnik);
		List<List<Apartment>> all = new ArrayList<>(grouped.values());
		for(List<Apartment> list : all) {
			List<Tenant> sur = new ArrayList<>();
			for(Apartment a : list) {
				List<Tenant> tens = tenantService.getByApartmentAndType(a, types);
				for(Tenant t : tens) {
					sur.add(t);
					break;
				}
				if(tens.size() == 0) {
					sur.add(null);
				}
			}
			surnames.add(sur);
		}
		int length = all.size();
		Random rand = new Random();
		Integer n = null;
		if(old != null) {
			do {
				n = rand.nextInt(length);
			}while(n==old.getLastRand());
		}
		else {
			n = rand.nextInt(length);
		}
		ss.setLastRand(n);
	
		for(SnowPeriods sp : SnowPeriods.values()) {
			SnowEntry se = new SnowEntry(all.get(n%all.size()), sp.toString(), surnames.get(n%surnames.size()));
			schedule.add(se);
			n++;
		}
		ss.setSchedule(schedule);
		snowScheduleRepository.save(ss);
		return getTableSnowEntries(buildingId);
		
	}
	
	public List<SnowEntryDTO> shiftSchedule(Long buildingId) {
		Building building = buildingService.getById(buildingId).orElseThrow(() -> new NoSuchElementException("Ne postoji zgrada s tim id-om"));
		SnowSchedule snowSchedule = snowScheduleRepository.findByBuilding(building).orElseThrow(() -> new NoSuchElementException("Ne postoji raspored za tu zgradu"));
		String firstPeriod = snowSchedule.getSchedule().get(0).getPeriod();
		List<SnowEntry> helpList = new ArrayList<>();
		for(int i=0; i<snowSchedule.getSchedule().size(); i++) {
			SnowEntry se = snowSchedule.getSchedule().get(i);
			if(i<snowSchedule.getSchedule().size()-1) {
				se.setPeriod(snowSchedule.getSchedule().get(i+1).getPeriod());
			}
			else {
				se.setPeriod(firstPeriod);
			}
			helpList.add(i, se);
		}
		snowSchedule.setSchedule(helpList);
		snowScheduleRepository.save(snowSchedule);
		return getTableSnowEntries(buildingId);
	}
	
	@Override
	public void removeApartmentFromEntry(Apartment apartment, Long snowEntryId) {
		SnowEntry se = snowEntryRepository.findById(snowEntryId).orElseThrow(() -> new NoSuchElementException("Taj entry ne postoji"));
		int index = se.getApartments().indexOf(apartment);
		se.getOwners().remove(index);
		se.getApartments().remove(apartment);
		snowEntryRepository.save(se);
	}
	
	@Override
	public void addApartmentToEntry(Apartment apartment, Long snowEntryId) {
		SnowEntry se = snowEntryRepository.findById(snowEntryId).orElseThrow(() -> new NoSuchElementException("Taj entry ne postoji"));
		se.getApartments().add(apartment);
		List<TenantTypes> types = new ArrayList<>();
		types.add(TenantTypes.vanjski_vlasnik);
		types.add(TenantTypes.vlasnik);
		Tenant sur = null;
		List<Tenant> tens = tenantService.getByApartmentAndType(apartment, types);
		for(Tenant t : tens) {
			sur = t;
			break;
		}
		if(tens.size() == 0) {
			sur = null;
		}
		se.getOwners().add(sur);
		snowEntryRepository.save(se);
	}
	
	@Override
	public Set<SnowApartmentsDTO> getApartmentsToAddToEntry(Long buildingId, Long snowEntryId){
		Building building = buildingService.getById(buildingId).orElseThrow(() -> new NoSuchElementException("Ta zgrada ne postoji"));
		List<Apartment> apartments = apartmentService.getByBuilding(building);
		SnowEntry se = snowEntryRepository.findById(snowEntryId).orElseThrow(() -> new NoSuchElementException("Taj entry ne postoji"));
		apartments.removeAll(se.getApartments());
		Set<SnowApartmentsDTO> apartmentsDTO = new HashSet<>();
		for(Apartment a : apartments) {
			apartmentsDTO.add(new SnowApartmentsDTO(a.getId(), a.getName()));
		}
		return apartmentsDTO;
	}
	
	@Override
	@Transactional
	public void deleteByBuilding(Building building) {
		snowScheduleRepository.deleteByBuilding(building);
	}
	
	@Override
	public void changeApartmentEntry(SnowEntry snowEntry, Apartment apartment) {
		SnowSchedule schedule = this.getByBuilding(apartment.getBuilding().getId());
		List<SnowEntry> entries = schedule.getSchedule();
		List<Apartment> apartments = snowEntry.getApartments();
		List<TenantTypes> types = new ArrayList<>();
		types.add(TenantTypes.vanjski_vlasnik);
		types.add(TenantTypes.vlasnik);
		for(SnowEntry se : entries) {
			if(se.getApartments().contains(apartment)) {
				int index = se.getApartments().indexOf(apartment);
				se.getApartments().remove(apartment);
				List<Tenant> sur = new ArrayList<>();
				for(Apartment a : se.getApartments()) {
					List<Tenant> tens = tenantService.getByApartmentAndType(a, types);
					for(Tenant t : tens) {
						sur.add(t);
						break;
					}
					if(tens.size() == 0) {
						sur.add(null);
					}
				}
				se.setOwners(sur);
			}
		}
		for(SnowEntry se : entries) {
			if(se.getApartments().containsAll(apartments)) {
				se.getApartments().add(apartment);
				List<Tenant> sur = new ArrayList<>();
				for(Apartment a : se.getApartments()) {
					List<Tenant> tens = tenantService.getByApartmentAndType(a, types);
					for(Tenant t : tens) {
						sur.add(t);
						break;
					}
					if(tens.size() == 0) {
						sur.add(null);
					}
				}
				se.setOwners(sur);
			}
		}
		schedule.setSchedule(entries);
		snowScheduleRepository.save(schedule);
	}
	
	@Override
	public void replaceApartments(Apartment apartment1, Apartment apartment2) {
		SnowSchedule ss = this.getByBuilding(apartment1.getBuilding().getId());
		List<SnowEntry> newSchedule = ss.getSchedule();
		for(SnowEntry se : newSchedule) {
			if(se.getApartments().contains(apartment1)) {
				List<Apartment> apartments = se.getApartments();
				int index = apartments.indexOf(apartment1);
				apartments.set(index, apartment2);
				List<TenantTypes> types = new ArrayList<>();
				types.add(TenantTypes.vanjski_vlasnik);
				types.add(TenantTypes.vlasnik);
				List<Tenant> sur = new ArrayList<>();
				for(Apartment a : apartments) {
					List<Tenant> tens = tenantService.getByApartmentAndType(a, types);
					for(Tenant t : tens) {
						sur.add(t);
						break;
					}
					if(tens.size() == 0) {
						sur.add(null);
					}
				}
				se.setApartments(apartments);
				se.setOwners(sur);
			}
			else if(se.getApartments().contains(apartment2)) {
				List<Apartment> apartments = se.getApartments();
				int index = apartments.indexOf(apartment2);
				apartments.set(index, apartment1);
				List<TenantTypes> types = new ArrayList<>();
				types.add(TenantTypes.vanjski_vlasnik);
				types.add(TenantTypes.vlasnik);
				List<Tenant> sur = new ArrayList<>();
				for(Apartment a : apartments) {
					List<Tenant> tens = tenantService.getByApartmentAndType(a, types);
					for(Tenant t : tens) {
						sur.add(t);
						break;
					}
					if(tens.size() == 0) {
						sur.add(null);
					}
				}
				se.setApartments(apartments);
				se.setOwners(sur);
			}
		}
		ss.setSchedule(newSchedule);
		snowScheduleRepository.save(ss);
	}
}
