package hr.project.bimini.model;

public enum AnalyticsType {

	WATER, GAS, ELECTRICITY;
}
