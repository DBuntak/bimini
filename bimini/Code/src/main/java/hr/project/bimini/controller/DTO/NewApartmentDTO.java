package hr.project.bimini.controller.DTO;

import java.util.Set;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.model.Tenant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NewApartmentDTO {

	private String name;
	private Long buildingId;
	private boolean rented;
	private int number;
	private int floor;
	private String comment;
	private Double quadrature;
	private Integer share;
	
}
