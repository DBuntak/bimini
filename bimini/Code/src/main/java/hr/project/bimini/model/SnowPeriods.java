package hr.project.bimini.model;

public enum SnowPeriods {
	p1("1.1.-7.1."),
	p2("8.1.-14.1."),
	p3("15.1.-21.1."),
	p4("22.1.-28.1."),
	p5("29.1.-4.1."),
	p6("5.2.-11.2."),
	p7("12.2.-18.2."),
	p8("19.2.-25.2."),
	p9("26.2.-4.3.");
	
	private String value;
    private SnowPeriods(String value)
    {
       this.value = value;
    }

    public String toString()
    {
       return this.value;
    }
}
