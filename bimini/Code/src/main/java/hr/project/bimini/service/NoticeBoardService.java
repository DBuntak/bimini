package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.controller.DTO.NoticeBoardDTO;
import hr.project.bimini.controller.DTO.NoticePositionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.NoticeBoard;
import hr.project.bimini.model.NoticeType;
import hr.project.bimini.model.Tenant;

public interface NoticeBoardService {

	List<NoticeBoard> getAll();
	Optional<NoticeBoard> getById(final Long id);
	void delete(final Long id);
	List<NoticeBoard> getByBuilding(Building building);
	NoticeBoard updatePosition(Long id, NoticePositionDTO noticeBoard);
	NoticeBoard update(Long id, NoticeBoardDTO noticeBoard);
	NoticeBoard save(NoticeBoardDTO noticeBoard, Building building, NoticeType type);
	List<NoticeBoard> getByBuildingAndType(Building building, NoticeType type);
}
