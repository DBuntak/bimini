package hr.project.bimini.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.ContractDTO;
import hr.project.bimini.controller.DTO.WarrantyDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Warranty;
import hr.project.bimini.model.WarrantyType;
import hr.project.bimini.repository.WarrantyRepository;

@Service
public class WarrantyServiceJpa implements WarrantyService {

	@Autowired
	private WarrantyRepository warrantyRepository;
	
	@Autowired
	private BuildingService buildingService;

	
	@Override
	public List<Warranty> getAll() {
		return warrantyRepository.findAll();
	}

	@Override
	public Optional<Warranty> getById(final Long id) {
		return warrantyRepository.findById(id);
	}

	@Override
	public Warranty update(final Long id, final WarrantyDTO warranty, WarrantyType type) {
		Warranty old = this.getById(id).get();
		if(type == WarrantyType.aparati) {
			old.setWarrantyStart(warranty.getWarrantyStart());
			old.setWarrantyEnd(warranty.getWarrantyEnd());
		}
		else if(type == WarrantyType.radovi) {
			old.setWarrantyStart(warranty.getWarrantyStart());
			old.setWarrantyEnd(warranty.getWarrantyEnd());
			old.setFirm(warranty.getFirm());
			old.setContractNumber(warranty.getContractNumber());
		}

		return warrantyRepository.save(old);
	}

	@Override
	public Warranty save(final Warranty warranty) {
		return warrantyRepository.save(warranty);
	}

	@Override
	public void delete(final Long id) {
		warrantyRepository.deleteById(id);
	}

	@Override
	public List<WarrantyDTO> getWarrantysByBuildingAndTypeDTO(final Building building, final WarrantyType type) {
		 List<Warranty> warranty = warrantyRepository.findByBuildingAndType(building, type);
		 List<WarrantyDTO> dtos = new ArrayList<>();
		 for(Warranty bc : warranty) {
			 WarrantyDTO dto = new WarrantyDTO(bc.getId(), bc.getFirm(), bc.getContractNumber(), bc.getWarrantyStart(),
					 bc.getWarrantyEnd(), bc.getType().name());
			 dtos.add(dto);
		 }
		 return dtos;
	}
	
	@Override
	public List<Warranty> getWarrantysByBuildingAndType(final Building building, final WarrantyType type) {
		 return warrantyRepository.findByBuildingAndType(building, type);
	}
	
	@Override
    public Warranty storeWarranty(MultipartFile bill, MultipartFile warranty, final WarrantyType type, final Building building, WarrantyDTO warrantyDTO) {

		String billName = null;
		String billType = null;
		byte[] billData = null;
		String warrantyName = null;
		String warrantyType = null;
		byte[] warrantyData = null;
        try {
            // Check if the file's name contains invalid characters
        	if(bill != null) {
	            if(bill.getOriginalFilename().contains("..")) {
	                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + bill.getOriginalFilename());
	            }
	            billName = bill.getOriginalFilename();
	            billType = bill.getContentType();
	            billData = bill.getBytes();
        	}
        	if(warranty != null) {
	            if(warranty.getOriginalFilename().contains("..")) {
	                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + warranty.getOriginalFilename());
	            }
	            warrantyName = warranty.getOriginalFilename();
	            warrantyType = warranty.getContentType();
	            warrantyData = warranty.getBytes();
        	}
            Warranty newWarranty = null;
            if(type == WarrantyType.aparati) {
            	newWarranty = new Warranty(null, null, null, warrantyDTO.getWarrantyStart(), warrantyDTO.getWarrantyEnd(), type, building,
            			billName, billType, billData, warrantyName, warrantyType, warrantyData);
            }
            else if(type == WarrantyType.radovi) {
            	newWarranty = new Warranty(null, warrantyDTO.getFirm(), warrantyDTO.getContractNumber(), warrantyDTO.getWarrantyStart(), warrantyDTO.getWarrantyEnd(), type, building,
            			billName, billType, billData, warrantyName, warrantyType, warrantyData);
            }
            Warranty war = warrantyRepository.save(newWarranty);
            return war;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + billName + " " + warrantyName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
    public Warranty updateWarranty(MultipartFile file, Long warrantyId, String fileType) {
		Warranty old = this.getById(warrantyId).get();
        try {
            // Check if the file's name contains invalid characters
            if(file.getOriginalFilename().contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + file.getOriginalFilename());
            }
            if(fileType.equals("bill")) {
            	old.setBillName(file.getOriginalFilename());
            	old.setBillType(file.getContentType());
            	old.setBillScan(file.getBytes());
            }
            else if(fileType.equals("warranty")) {
            	old.setWarrantyName(file.getOriginalFilename());
            	old.setWarrantyType(file.getContentType());
            	old.setWarrantyScan(file.getBytes());
            }
            Warranty contract = warrantyRepository.save(old);
            return old;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + file.getOriginalFilename() + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public Warranty loadWarrantyAsResource(Long id) {
		Warranty oldContract = warrantyRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldContract != null) {
		    return oldContract;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + id);
		}
    }
}
