package hr.project.bimini.service;

import java.io.File;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import hr.project.bimini.controller.DTO.SupplierDTO;
import hr.project.bimini.controller.DTO.SupplierInquiryDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Supplier;
import hr.project.bimini.repository.SupplierRepository;

@Service
public class SupplierServiceJpa implements SupplierService {

	@Autowired
	private SupplierRepository supplierRepository;
	
	@Autowired
	private JavaMailSender emailSender;
	
	@Override
	public List<Supplier> getAll() {
		return supplierRepository.findAll();
	}

	@Override
	public Optional<Supplier> getById(final Long id) {
		return supplierRepository.findById(id);
	}

	@Override
	public Supplier update(final Long id, final SupplierDTO supplier) {
		Supplier su = this.getById(id).get();
		su.setFirm(supplier.getFirm());
		su.setDescription(supplier.getDescription());
		
		su.setName(supplier.getName());
		su.setSurname(supplier.getSurname());
		su.setMobilePhone(supplier.getMobilePhone());
		su.setEmail(supplier.getEmail());
		return supplierRepository.save(su);
	}

	
	@Override
	public Supplier save(final SupplierDTO supplier, final Building building) {
		Supplier su = new Supplier(null, building, supplier.getFirm(), supplier.getDescription(),
				supplier.getName(), supplier.getSurname(), supplier.getEmail(), supplier.getMobilePhone());
		return supplierRepository.save(su);
	}

	@Override
	public void delete(final Long id) {
		supplierRepository.deleteById(id);
	}
	
	@Override
	public List<Supplier> getByBuilding(Building building){
		return supplierRepository.findByBuilding(building);
	}

	@Override
	public void sendInquiry(Building building, SupplierInquiryDTO inquiry) throws MessagingException {
		List<Supplier> suppliers = supplierRepository.findByIdIn(inquiry.getSupplierIds());
		
		String[] emails = new String[suppliers.size()];
		int i=0;
		for(Supplier s : suppliers) {
			emails[i] = s.getEmail();
			i++;
		}
		
		MimeMessage message = emailSender.createMimeMessage();
	     
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    
	    helper.setFrom("project.bimini10@gmail.com");
	    helper.setTo(emails);
	    helper.setSubject(inquiry.getTitle());
	    helper.setText(inquiry.getContent());
	    
	    emailSender.send(message);
	}
}
