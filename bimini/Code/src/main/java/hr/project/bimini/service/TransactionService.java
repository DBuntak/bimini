package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.controller.DTO.TransactionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Transaction;

public interface TransactionService {

	List<Transaction> getAll();
	Optional<Transaction> getById(final Long id);
	void delete(final Long id);
	Transaction save(TransactionDTO transaction, Building building);
	Transaction update(Long id, TransactionDTO transaction, Building building);
}
