package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.NewApartmentDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.model.Tenant;


public interface ApartmentService {

	List<Apartment> getAll();
	Optional<Apartment> getById(final Long id);
	Apartment update(final Long id, final Apartment apartment);
	Apartment save(final NewApartmentDTO apartmentDTO, final Building building);
	void delete(final Long id);
	List<Apartment> getDebtors(Building building);
	Apartment updateApartment(Apartment apartment, double debt);
	List<Apartment> getByBuilding(Building building);
}
