package hr.project.bimini.controller.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NewTenantDTO {

	private String name;
	private String surname;
	private String mobilePhone;
	private String privateEmail;
	private String workEmail;
	private Long apartmentId;
	private String type;
	private String role;
	private boolean livingInBuilding;
}
