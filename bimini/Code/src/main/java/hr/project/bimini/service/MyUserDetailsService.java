package hr.project.bimini.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import hr.project.bimini.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class MyUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if(userRepository.existsByUsername(s)) {
        	hr.project.bimini.model.User us = userRepository.findByUsername(s).get();
        	List<GrantedAuthority> helpList = new ArrayList<>();
        	helpList.add(new SimpleGrantedAuthority(us.getRole().getName().name()));
        	return new User(us.getUsername(), us.getPassword(), helpList);
        }
        throw new UsernameNotFoundException("Ne postoji taj korisnik");
    }
}
