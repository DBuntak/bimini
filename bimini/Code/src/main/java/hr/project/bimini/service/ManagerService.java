package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.controller.DTO.ManagerDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Manager;

public interface ManagerService {

	List<Manager> getAll();
	Optional<Manager> getById(final Long id);
	void delete(final Long id);
	Manager save(ManagerDTO manager, Building building);
	List<Manager> getByBuilding(Building building);
	Manager update(Long id, ManagerDTO manager);
}
