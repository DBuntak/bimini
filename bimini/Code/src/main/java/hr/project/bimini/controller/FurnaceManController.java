package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.model.FurnaceMan;
import hr.project.bimini.service.FurnaceManServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("furnace_man")
@AllArgsConstructor
@Slf4j
@RestController
public class FurnaceManController {
	@Autowired
	private FurnaceManServiceJpa furnaceManService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<FurnaceMan>> getAll() {
		return ResponseEntity.ok(furnaceManService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<FurnaceMan> getById(@PathVariable("id") final Long id) {
		Optional<FurnaceMan> furnaceMan = furnaceManService.getById(id);
        if (!furnaceMan.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(furnaceMan.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<FurnaceMan> updateById(@PathVariable("id") final Long id, @RequestBody final FurnaceMan furnaceMan) {
        if (!furnaceManService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(furnaceManService.save(furnaceMan));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final FurnaceMan furnaceMan) {
		return ResponseEntity.ok(furnaceManService.save(furnaceMan));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!furnaceManService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        furnaceManService.delete(id);

        return ResponseEntity.ok().build();
	}
}
