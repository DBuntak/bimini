package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CalendarDTO {

	private Long id;
	private String title;
	private Date start;
	private Date end;
	private String resource;
	private Long typeId;
	private String worker;
}
