package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.LawDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Law;
import hr.project.bimini.model.LawType;
import hr.project.bimini.model.Supplier;
import hr.project.bimini.model.Tenant;

public interface LawService {

	List<Law> getAll();
	Optional<Law> getById(final Long id);
	Law save(final Law law);
	void delete(final Long id);
	List<Law> getLawsByBuildingAndType(final Building building, final LawType type);
	Law updateLaw(MultipartFile file, Long lawId);
	Law loadLawAsResource(Long id);
	List<Law> getLawsByBuildingAndTypeDTO(Building building, LawType type);
	Law update(Long id, LawDTO law, Supplier lawyer, Tenant tenant);
	Law storeLaw(MultipartFile card, LawType type, Building building, LawDTO lawDTO, Tenant tenant, Supplier lawyer);
}
