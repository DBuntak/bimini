package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Tenant;

public interface BuildingService {

	List<Building> getAll();
	Optional<Building> getById(final Long id);
	Building update(final Long id, final Building building);
	Building save(final Building building);
	void delete(final Long id);
	List<Apartment> getApartmentsByBuilding(final Building building);
	Building updateBuilding(Building building, double balance);
}
