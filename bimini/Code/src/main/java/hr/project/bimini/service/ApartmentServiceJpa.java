package hr.project.bimini.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.NewApartmentDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;
import hr.project.bimini.model.Watermeter;
import hr.project.bimini.repository.ApartmentRepository;
import hr.project.bimini.repository.BuildingRepository;

@Service
public class ApartmentServiceJpa implements ApartmentService {

	@Autowired
	private ApartmentRepository apartmentRepository;
	
	@Autowired
	private BuildingRepository buildingRepository;
	
	@Autowired TenantService tenantService;
	
	@Autowired
	private WatermeterService watermeterService;
	
	@Override
	public List<Apartment> getAll() {
		return apartmentRepository.findAll();
	}

	@Override
	public Optional<Apartment> getById(final Long id) {
		return apartmentRepository.findById(id);
	}

	@Override
	public Apartment update(final Long id, final Apartment apartment) {
		Apartment old = getById(id).get();
		old.setName(apartment.getName());
		old.setRented(apartment.isRented());
		old.setFloor(apartment.getFloor());
		old.setComment(apartment.getComment());
		old.setNumber(apartment.getNumber());
		old.setQuadrature(apartment.getQuadrature());
		old.setShare(apartment.getShare());
		return apartmentRepository.save(old);
	}

	@Override
	@Transactional
	public Apartment save(final NewApartmentDTO apartmentDTO, final Building building) {
		Apartment apartment = new Apartment();
		apartment.setName(apartmentDTO.getName());
		apartment.setRented(apartmentDTO.isRented());
		apartment.setFloor(apartmentDTO.getFloor());
		apartment.setComment(apartmentDTO.getComment());
		apartment.setNumber(apartmentDTO.getNumber());
		apartment.setDebt(0.0);
		apartment.setBuilding(building);
		apartment.setQuadrature(apartmentDTO.getQuadrature());
		apartment.setShare(apartmentDTO.getShare());
		Apartment saved = apartmentRepository.save(apartment);
		Watermeter watermeter = new Watermeter();
		watermeter.setApartment(saved);
		watermeter.setBuilding(apartment.getBuilding());
		watermeterService.save(watermeter);
		return saved;
	}

	@Override
	@Transactional
	public void delete(final Long id) {
		//apartmentRepository.deleteFromBuilding(id);
		Apartment a = this.getById(id).get();
		List<Tenant> tenants = tenantService.getByApartment(a);
		for(Tenant t : tenants) {
			tenantService.delete(t.getId());
		}
		apartmentRepository.deleteLinkedUsers(id);
		apartmentRepository.deleteById(id);
	}


	@Override
	public List<Apartment> getDebtors(Building building){
		return apartmentRepository.findByDebtGreaterThan(0.0);
	}
	
	@Override
	public Apartment updateApartment(Apartment apartment, double debt) {
		apartment.setDebt(debt);
		return apartmentRepository.save(apartment);
	}
	
	@Override
	public List<Apartment> getByBuilding(Building building){
		return apartmentRepository.findByBuilding(building);
	}
}
