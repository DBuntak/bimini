package hr.project.bimini.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Role;
import hr.project.bimini.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	boolean existsByUsername(String username);
	
	Optional<User> findByUsername(String username);
	
	Optional<List<User>> findByRole(Role role);
	
	Optional<List<User>> findByApartment(Apartment apartment);
	
	List<User> findByBuildingOrRole(Building building, Role role);
	
}
