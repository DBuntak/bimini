package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.NoticeBoard;
import hr.project.bimini.model.NoticeType;

public interface NoticeBoardRepository extends JpaRepository<NoticeBoard, Long> {

	List<NoticeBoard> findByBuilding(Building building);
	
	List<NoticeBoard> findByBuildingAndType(Building building, NoticeType type);
}
