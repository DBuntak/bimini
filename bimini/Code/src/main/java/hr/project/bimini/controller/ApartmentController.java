package hr.project.bimini.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.DebtDTO;
import hr.project.bimini.controller.DTO.NewApartmentDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;
import hr.project.bimini.service.ApartmentServiceJpa;
import hr.project.bimini.service.BuildingServiceJpa;
import hr.project.bimini.service.TenantService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("apartment")
@AllArgsConstructor
@Slf4j
@RestController
public class ApartmentController {
	@Autowired
	private ApartmentServiceJpa apartmentService;
	
	@Autowired
	private TenantService tenantService;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Apartment>> getAll() {
		return ResponseEntity.ok(apartmentService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Apartment> getById(@PathVariable("id") final Long id) {
		Optional<Apartment> apartment = apartmentService.getById(id);
        if (!apartment.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(apartment.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Apartment> updateById(@PathVariable("id") final Long id, @RequestBody final Apartment apartment) {
        if (!apartmentService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(apartmentService.update(id, apartment));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final NewApartmentDTO apartmentDTO) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(apartmentDTO.getBuildingId());
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(apartmentService.save(apartmentDTO, building));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!apartmentService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        apartmentService.delete(id);

        return ResponseEntity.ok().build();
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}/tenants")
	public ResponseEntity<List<Tenant>> getTenantsByApartment(@PathVariable("id") final Long id){
		ResponseEntity<Apartment> apartmentResponse = getById(id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		return ResponseEntity.ok(tenantService.getByApartment(apartment));
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}/owner")
	public ResponseEntity<List<Tenant>> getOwnerByApartment(@PathVariable("id") final Long id){
		ResponseEntity<Apartment> apartmentResponse = getById(id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		List<TenantTypes> types = new ArrayList<>();
		types.add(TenantTypes.vanjski_vlasnik);
		types.add(TenantTypes.vlasnik);
		return ResponseEntity.ok(tenantService.getByApartmentAndType(apartment, types));
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/debt/{id}")
	public ResponseEntity<List<Apartment>> getDebtors(@PathVariable("id") Long id){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		return ResponseEntity.ok(apartmentService.getDebtors(building));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/update/debt/{id}")
	public ResponseEntity<Apartment> updateDebt(@PathVariable("id") Long id, @RequestBody DebtDTO debt){
		ResponseEntity<Apartment> apartmentResponse = getById(id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		return ResponseEntity.ok(apartmentService.updateApartment(apartment, debt.getDebt()));
	}
}