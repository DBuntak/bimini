package hr.project.bimini.model;

public enum TenantTypes {
	vanjski_vlasnik, vlasnik, stanar, glavni_podstanar, podstanar
}
