package hr.project.bimini.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Warranty {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String firm;
	private String contractNumber;
	private Date warrantyStart;
	private Date warrantyEnd;
	private WarrantyType type;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "building_id")
	private Building building;
	
	private String billName;
	private String billType;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] billScan;
	
	private String warrantyName;
	private String warrantyType;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] warrantyScan;
}
