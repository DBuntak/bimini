package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParkingPlaceDTO {

	private Long buildingId;
	private Long ownerId;
	private Long rentedId;
	private Integer number;
	private String type;
	private Date rentStart;
	private Date rentEnd;
	private String rentType;
}
