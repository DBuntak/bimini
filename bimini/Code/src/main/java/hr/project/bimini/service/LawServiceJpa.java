package hr.project.bimini.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.LawDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Law;
import hr.project.bimini.model.LawType;
import hr.project.bimini.model.Supplier;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.repository.LawRepository;

@Service
public class LawServiceJpa implements LawService {

	@Autowired
	private LawRepository lawRepository;
	
	@Autowired
	private BuildingService buildingService;

	
	@Override
	public List<Law> getAll() {
		return lawRepository.findAll();
	}

	@Override
	public Optional<Law> getById(final Long id) {
		return lawRepository.findById(id);
	}

	@Override
	public Law update(final Long id, final LawDTO law, Supplier lawyer, Tenant tenant) {
		Law old = this.getById(id).get();
		old.setDebt(law.getDebt());
		old.setLawyer(lawyer);
		old.setPeriodFrom(law.getPeriodFrom());
		old.setPeriodTo(law.getPeriodTo());
		old.setTenant(tenant);

		return lawRepository.save(old);
	}

	@Override
	public Law save(final Law law) {
		return lawRepository.save(law);
	}

	@Override
	public void delete(final Long id) {
		lawRepository.deleteById(id);
	}

	@Override
	public List<Law> getLawsByBuildingAndTypeDTO(final Building building, final LawType type) {
		 List<Law> law = lawRepository.findByBuildingAndType(building, type);
		 for(Law l : law) {
			 l.setCardScan(null);
		 }
		 return law;
	}
	
	@Override
	public List<Law> getLawsByBuildingAndType(final Building building, final LawType type) {
		 return lawRepository.findByBuildingAndType(building, type);
	}
	
	@Override
    public Law storeLaw(MultipartFile card, final LawType type, final Building building, LawDTO lawDTO, Tenant tenant, Supplier lawyer) {

		String cardName = null;
		String cardType = null;
		byte[] cardData = null;
        try {
            // Check if the file's name contains invalid characters
        	if(card != null) {
	            if(card.getOriginalFilename().contains("..")) {
	                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + card.getOriginalFilename());
	            }
	            cardName = card.getOriginalFilename();
	            cardType = card.getContentType();
	            cardData = card.getBytes();
        	}
            Law newLaw = new Law(null, building, tenant, lawyer, type, lawDTO.getDebt(), lawDTO.getPeriodFrom(), lawDTO.getPeriodTo(),
            		cardType, cardName, cardData);
            
            Law l = lawRepository.save(newLaw);
            return l;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + cardName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
    public Law updateLaw(MultipartFile file, Long lawId) {
		Law old = this.getById(lawId).get();
        try {
            // Check if the file's name contains invalid characters
            if(file.getOriginalFilename().contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + file.getOriginalFilename());
            }
        	old.setCardName(file.getOriginalFilename());
        	old.setCardType(file.getContentType());
        	old.setCardScan(file.getBytes());

            return lawRepository.save(old);
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + file.getOriginalFilename() + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public Law loadLawAsResource(Long id) {
		Law oldLaw = lawRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldLaw != null) {
		    return oldLaw;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + id);
		}
    }
}
