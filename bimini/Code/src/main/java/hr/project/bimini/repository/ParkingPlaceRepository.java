package hr.project.bimini.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.ParkingPlace;
import hr.project.bimini.model.ParkingType;

public interface ParkingPlaceRepository extends JpaRepository<ParkingPlace, Long> {

	List<ParkingPlace> findByBuilding(Building building);
	List<ParkingPlace> findByBuildingAndType(Building building, ParkingType type);
	List<ParkingPlace> findByRentEndAfterAndRentEndBefore(Date start, Date end);
}
