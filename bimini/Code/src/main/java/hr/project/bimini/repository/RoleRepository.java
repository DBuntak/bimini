package hr.project.bimini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Role;
import hr.project.bimini.model.RoleName;

public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByName(RoleName name);
}
