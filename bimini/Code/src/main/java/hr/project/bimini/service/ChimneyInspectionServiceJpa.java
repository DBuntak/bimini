package hr.project.bimini.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.ChimneyReport;
import hr.project.bimini.model.FurnaceInspection;
import hr.project.bimini.model.FurnaceReport;
import hr.project.bimini.repository.ChimneyInspectionRepository;

@Service
public class ChimneyInspectionServiceJpa implements ChimneyInspectionService {

	@Autowired
	private ChimneyInspectionRepository chimneyInspectionRepository;
	
	@Override
	public List<ChimneyInspection> getAll() {
		return chimneyInspectionRepository.findAll();
	}

	@Override
	public Optional<ChimneyInspection> getById(final Long id) {
		return chimneyInspectionRepository.findById(id);
	}

	@Override
	public ChimneyInspection update(final Long id, final ChimneyInspection chimneyInspection) {
		return chimneyInspectionRepository.save(chimneyInspection);
	}

	@Override
	public ChimneyInspection save(final ChimneyInspection chimneyInspection) {
		return chimneyInspectionRepository.save(chimneyInspection);
	}

	@Override
	public void delete(final Long id) {
		chimneyInspectionRepository.deleteById(id);
	}

	@Override
	public List<ChimneyInspection> getByBuilding(Building building) {
		return chimneyInspectionRepository.findByBuilding(building);
	}
	
	@Override
	public ChimneyReport getReport(ChimneyInspection chimneyInspection) {
		return chimneyInspection.getChimneyReport();
	}

	@Override
	public String storeReport(MultipartFile file, ChimneyInspection ci) {
		// Normalize file name
        String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }

            ChimneyReport newReport = new ChimneyReport(null, fileName, file.getContentType(), new Date(), file.getBytes());
            ci.setChimneyReport(newReport);
            chimneyInspectionRepository.save(ci);
            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
	}
	
	@Override
	public List<ChimneyInspection> getByBuildingAndDate(Building building){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1);
		return chimneyInspectionRepository.findByBuildingAndStartAfter(building, cal.getTime());
	}
}
