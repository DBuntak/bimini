package hr.project.bimini.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.ContractType;

public interface ContractTypeRepository extends JpaRepository<ContractType, Long> {

	Optional<ContractType> findByName(String name);
	boolean existsByName(String name);
}
