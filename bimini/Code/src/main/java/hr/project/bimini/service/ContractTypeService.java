package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.model.ContractType;

public interface ContractTypeService {

	List<ContractType> getAll();
	Optional<ContractType> getById(final Long id);
	ContractType update(final Long id, final ContractType contractType);
	ContractType save(final ContractType contractType);
	void delete(final Long id);
	Optional<ContractType> getByName(String name);
	boolean existsByName(String name);
}
