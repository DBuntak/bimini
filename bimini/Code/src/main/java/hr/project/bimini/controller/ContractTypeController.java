package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.model.ContractType;
import hr.project.bimini.service.ContractTypeServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("contract-type")
@AllArgsConstructor
@Slf4j
@RestController
public class ContractTypeController {

	@Autowired
	private ContractTypeServiceJpa contractTypeService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<ContractType>> getAll() {
		return ResponseEntity.ok(contractTypeService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<ContractType> getById(@PathVariable("id") final Long id) {
		Optional<ContractType> contractType = contractTypeService.getById(id);
        if (!contractType.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(contractType.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<ContractType> updateById(@PathVariable("id") final Long id, @RequestBody final ContractType contractType) {
        if (!contractTypeService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(contractTypeService.save(contractType));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final ContractType contractType) {
		return ResponseEntity.ok(contractTypeService.save(contractType));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!contractTypeService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        contractTypeService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/name/{name}")
	public ResponseEntity<ContractType> getByName(@PathVariable("name") final String name) {
		Optional<ContractType> contractType = contractTypeService.getByName(name);
        if (!contractType.isPresent()) {
            log.error("Name " + name + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(contractType.get());
	}
}
