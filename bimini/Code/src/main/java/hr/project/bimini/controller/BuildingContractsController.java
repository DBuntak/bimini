package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.gson.Gson;

import hr.project.bimini.controller.DTO.ContractDTO;
import hr.project.bimini.controller.DTO.MailDTO;
import hr.project.bimini.controller.DTO.UploadFileResponse;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.BuildingContracts;
import hr.project.bimini.model.ContractType;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.service.BuildingContractsServiceJpa;
import hr.project.bimini.service.BuildingService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("building_contracts")
@AllArgsConstructor
@Slf4j
@RestController
public class BuildingContractsController {
	@Autowired
	private BuildingContractsServiceJpa buildingContractsService;
	@Autowired
	private BuildingController buildingController;
	@Autowired
	private ContractTypeController contractTypeController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<BuildingContracts>> getAll() {
		return ResponseEntity.ok(buildingContractsService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<BuildingContracts> getById(@PathVariable("id") final Long id) {
		Optional<BuildingContracts> buildingContracts = buildingContractsService.getById(id);
        if (!buildingContracts.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(buildingContracts.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<BuildingContracts> updateById(@PathVariable("id") final Long id, @RequestBody final ContractDTO buildingContracts) {
        if (!buildingContractsService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        ResponseEntity<ContractType> typeResponse = contractTypeController.getByName(buildingContracts.getContractType());
		if(typeResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		ContractType type = typeResponse.getBody();

        return ResponseEntity.ok(buildingContractsService.update(id, buildingContracts, type));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final BuildingContracts buildingContracts) {
		return ResponseEntity.ok(buildingContractsService.save(buildingContracts));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!buildingContractsService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        buildingContractsService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}/type/{type}")
	public ResponseEntity<?> getContractsByBuildingAndType(@PathVariable("id") final Long id, @PathVariable("type") String typeName, HttpServletRequest request){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		ResponseEntity<ContractType> typeResponse = contractTypeController.getByName(typeName);
		if(typeResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		ContractType type = typeResponse.getBody();
		List<ContractDTO> resource = buildingContractsService.getContractsByBuildingAndTypeDTO(building, type);
        // Load file as Resource
        //BuildingContracts resource = buildingContractsService.loadContractAsResource(contractName);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		//contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        /*HttpHeaders header = new HttpHeaders();
        //header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getFileName());*/
      
        return new ResponseEntity<>(resource, HttpStatus.OK);
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/building/{id}")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("data") String json, @PathVariable("id") final Long building_id) {
		Gson gson = new Gson();
		ContractDTO contractDTO = gson.fromJson(json, ContractDTO.class);
		ResponseEntity<Building> buildingResponse = buildingController.getById(building_id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		ResponseEntity<ContractType> typeResponse = contractTypeController.getByName(contractDTO.getContractType());
		if(typeResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		ContractType type = typeResponse.getBody();
		
		String fileName = buildingContractsService.storeContract(file, type, building, contractDTO);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/building_contracts/building/")
                .path(String.valueOf(building.getId()))
                .path("/type/")
                .path(type.getName())
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PutMapping("/file/{id}")
    public UploadFileResponse updateFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long contractId) {
		 if (!buildingContractsService.getById(contractId).isPresent()) {
	            log.error("Id " + contractId + " is not existed");
	            ResponseEntity.badRequest().build();
	        }
		
		String fileName = buildingContractsService.updateContract(file, contractId);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/building_contracts/download/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}")
    public ResponseEntity<?> downloadContract(@PathVariable("id") Long contractId, HttpServletRequest request) {
        // Load file as Resource
        BuildingContracts resource = buildingContractsService.loadContractAsResource(contractId);

        // Try to determine file's content type
        String contentType = null;
		contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getRealName());
      
        return new ResponseEntity<>(resource.getData(), header, HttpStatus.OK);
     }
	
	@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/filename/{contractName:.+}")
    public ResponseEntity<?> delete(@PathVariable String contractName, HttpServletRequest request) throws IOException {
        buildingContractsService.deleteContract(contractName);
    	return ResponseEntity.ok().build();
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/mail/building/{id}/type/{type}")
	public ResponseEntity<?> sendEmailForBuildingAndType(@PathVariable("id") final Long id, @PathVariable("type") Long typeId, @RequestBody MailDTO mail){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		ResponseEntity<ContractType> typeResponse = contractTypeController.getById(typeId);
		if(typeResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		ContractType type = typeResponse.getBody();
		
		try {
			buildingContractsService.sendEmailWithContractsForBuildingAndType(mail.getTo(), mail.getSubject(), mail.getText(), building, type);
		} catch (MessagingException e) {
			ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok().build();
	}
	
}