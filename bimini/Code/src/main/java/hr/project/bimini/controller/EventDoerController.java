package hr.project.bimini.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.EventDoerDTO;
import hr.project.bimini.model.ChimneyMan;
import hr.project.bimini.model.EventDoer;
import hr.project.bimini.model.EventType;
import hr.project.bimini.model.FurnaceMan;
import hr.project.bimini.service.ChimneyManService;
import hr.project.bimini.service.EventDoerServiceJpa;
import hr.project.bimini.service.EventTypeService;
import hr.project.bimini.service.FurnaceManService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("event-doer")
@AllArgsConstructor
@Slf4j
@RestController
public class EventDoerController {

	@Autowired
	private EventDoerServiceJpa eventDoerService;
	
	@Autowired
	private EventTypeService eventTypeService;
	
	@Autowired
	private ChimneyManService chimneyManService;
	
	@Autowired
	private FurnaceManService furnaceManService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<EventDoer>> getAll() {
		return ResponseEntity.ok(eventDoerService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<EventDoer> getById(@PathVariable("id") final Long id) {
		Optional<EventDoer> eventDoer = eventDoerService.getById(id);
        if (!eventDoer.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(eventDoer.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<EventDoer> updateById(@PathVariable("id") final Long id, @RequestBody final EventDoer eventDoer) {
        if (!eventDoerService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(eventDoerService.save(eventDoer));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final EventDoer eventDoer) {
		return ResponseEntity.ok(eventDoerService.save(eventDoer));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!eventDoerService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        eventDoerService.delete(id);

        return ResponseEntity.ok().build();
	}
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/type/{type}")
	public ResponseEntity<List<EventDoerDTO>> getDoersByType(@PathVariable("type") String type) {
		List<EventDoerDTO> doers = new ArrayList<>();
		/*if(type.equals("chimneyInspection")) {
			List<ChimneyMan> chimney = chimneyManService.getAll();
			for(ChimneyMan man : chimney) {
				EventDoerDTO doer = new EventDoerDTO(man.getId(), man.getOib(), man.getName(), man.getSurname(), man.getFirmName(), man.getMobilePhone(), man.getEmail());
				doers.add(doer);
			}
		}
		else if(type.equals("furnaceInspection")) {
			List<FurnaceMan> furnace = furnaceManService.getAll();
			for(FurnaceMan man : furnace) {
				EventDoerDTO doer = new EventDoerDTO(man.getId(), man.getOib(), man.getName(), man.getSurname(), man.getFirmName(), man.getMobilePhone(), man.getEmail());
				doers.add(doer);
			}
		}
		else if(type.equals("cleaning") || type.equals("waste") || type.equals("rest")) {
			List<EventDoer> doe = eventDoerService.getDoersForType(EventType.valueOf(type.toUpperCase()));
			for(EventDoer man : doe) {
				EventDoerDTO doer = new EventDoerDTO(man.getId(), man.getOib(), man.getName(), man.getSurname(), man.getFirmName(), man.getMobilePhone(), man.getEmail());
				doers.add(doer);
			}
		}*/
		EventType typee = null;
		if(eventTypeService.existsByName(type)) {
			typee = eventTypeService.getByName(type).get();
		}
		else {
			typee = null;
		}
		
		List<EventDoer> doe = eventDoerService.getDoersForType(typee);
		for(EventDoer man : doe) {
			EventDoerDTO doer = new EventDoerDTO(man.getId(), man.getName(), man.getSurname(), man.getFirmName(), man.getMobilePhone(), man.getEmail());
			doers.add(doer);
		}
		return ResponseEntity.ok(doers);
	}
}
