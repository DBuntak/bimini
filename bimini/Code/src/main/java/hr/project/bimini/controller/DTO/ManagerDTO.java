package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ManagerDTO {

	private String firm;
	private String identificationNumber;
	private String IBAN;
	private String firmEmail;
	private String address;
	
	//predstavnik
	private String name;
	private String surname;
	private String email;
	private String mobilePhone;
	
	private Long buildingId;
}
