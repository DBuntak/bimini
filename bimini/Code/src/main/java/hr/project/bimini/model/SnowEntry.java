package hr.project.bimini.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class SnowEntry {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "snow_entry_apartments",
            joinColumns = @JoinColumn(name = "snow_entry_id"),
            inverseJoinColumns = @JoinColumn(name = "apartment_id")
            )
	private List<Apartment> apartments;
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "snow_entry_owners",
            joinColumns = @JoinColumn(name = "snow_entry_id"),
            inverseJoinColumns = @JoinColumn(name = "owner_id")
            )
	private List<Tenant> owners;
	private String period;
	public SnowEntry(List<Apartment> apartments, String period, List<Tenant> owners) {
		super();
		this.apartments = apartments;
		this.period = period;
		this.owners = owners;
	}
	
	
}

