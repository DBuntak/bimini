package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParkingRentDTO {

	private Date rentStart;
	private Date rentEnd;
	private String rentType;
}
