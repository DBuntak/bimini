package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {

	List<Supplier> findByBuilding(Building building);
	
	List<Supplier> findByIdIn(List<Long> ids);
}
