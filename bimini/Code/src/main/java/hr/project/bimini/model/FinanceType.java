package hr.project.bimini.model;

public enum FinanceType {
	sitni_racuni, osobni_racuni, financijski_izvjestaj, godisnji_plan,
	stanje_kupaca, program_upravljanja, financijska_rekapitulacija_zgrade,
	analiticka_specifikacija_troskova
}
