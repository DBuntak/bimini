package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NoticeBoardDTO {

	private String title;
	private String message;
	private Long buildingId;
	private Integer x;
	private Integer y;
	private String type;
}
