package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.EventDoer;
import hr.project.bimini.model.EventType;

public interface EventDoerRepository extends JpaRepository<EventDoer, Long> {

	List<EventDoer> findByEventType(EventType type);
}
