package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.gson.Gson;

import hr.project.bimini.controller.DTO.FinanceDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Finance;
import hr.project.bimini.model.FinanceType;
import hr.project.bimini.service.FinanceServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("finance")
@AllArgsConstructor
@Slf4j
@RestController
public class FinanceController {

	@Autowired
	private FinanceServiceJpa financeService;
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Finance>> getAll() {
		return ResponseEntity.ok(financeService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Finance> getById(@PathVariable("id") final Long id) {
		Optional<Finance> finance = financeService.getById(id);
        if (!finance.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(finance.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Finance> updateById(@PathVariable("id") final Long id, @RequestBody final FinanceDTO finance) {
        if (!financeService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
		if(FinanceType.valueOf(finance.getType()) == null) {
			ResponseEntity.badRequest().build();
		}

        return ResponseEntity.ok(financeService.update(id, finance, FinanceType.valueOf(finance.getType())));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final Finance finance) {
		return ResponseEntity.ok(financeService.save(finance));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!financeService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        financeService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}/type/{type}")
	public ResponseEntity<?> getFinancesByBuildingAndType(@PathVariable("id") final Long id, @PathVariable("type") String typeName, HttpServletRequest request){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		if(FinanceType.valueOf(typeName) == null) {
			ResponseEntity.badRequest().build();
		}
		FinanceType type = FinanceType.valueOf(typeName);
		List<FinanceDTO> resource = financeService.getFinancesByBuildingAndTypeDTO(building, type);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		//contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
      
        return new ResponseEntity<>(resource, HttpStatus.OK);
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/building/{id}")
    public Finance uploadFile(@RequestParam("bill") MultipartFile bill, @RequestParam("data") String json, @PathVariable("id") final Long building_id) {
		Gson gson = new Gson();
		FinanceDTO financeDTO = gson.fromJson(json, FinanceDTO.class);
		ResponseEntity<Building> buildingResponse = buildingController.getById(building_id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		if(FinanceType.valueOf(financeDTO.getType()) == null) {
			ResponseEntity.badRequest().build();
		}
		FinanceType type = FinanceType.valueOf(financeDTO.getType());
		
		Finance war = financeService.storeFinance(bill, type, building, financeDTO);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/finance/building/")
                .path(String.valueOf(building.getId()))
                .path("/type/")
                .path(type.name())
                .toUriString();

        return war;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PutMapping("/file/{id}")
    public Finance updateFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long financeId) {
		 if (!financeService.getById(financeId).isPresent()) {
	            log.error("Id " + financeId + " is not existed");
	            ResponseEntity.badRequest().build();
	        }
		
		Finance finance = financeService.updateFinance(file, financeId);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/finance/download/")
                .path(financeId.toString())
                .toUriString();

        return finance;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}")
    public ResponseEntity<?> downloadFinance(@PathVariable("id") Long financeId, HttpServletRequest request) {
        // Load file as Resource
        Finance resource = financeService.loadFinanceAsResource(financeId);

        // Try to determine file's content type
        String contentType = null;
        byte[] data = null;
        String name = null;
		contentType = resource.getBillType();
		data = resource.getBillScan();
		name = resource.getBillName();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(contentType));
        header.setContentLength(data.length);
        header.set("Content-Disposition", "attachment; filename=" + name);
      
        return new ResponseEntity<>(data, header, HttpStatus.OK);
     }

}
