package hr.project.bimini.model;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Tenant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String surname;
	private String mobilePhone;
	private String privateEmail;
	private String workEmail;
	private TenantTypes type;
	private TenantRoles role;
	private boolean livingInBuilding;
	
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
	private Apartment apartment;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
	private Building building;

	public Tenant(Long id, String name, String surname, String mobilePhone, String privateEmail, String workEmail, boolean livingInBuilding,
			Building building) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.mobilePhone = mobilePhone;
		this.privateEmail = privateEmail;
		this.workEmail = workEmail;
		this.livingInBuilding = livingInBuilding;
		this.building = building;
	}
	
}
