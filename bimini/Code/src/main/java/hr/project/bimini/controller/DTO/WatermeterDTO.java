package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WatermeterDTO {

	private String number;
	private String systemNumber;
	private Date lastChange;
}
