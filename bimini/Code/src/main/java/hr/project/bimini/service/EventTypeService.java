package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import hr.project.bimini.model.EventType;

public interface EventTypeService {

	List<EventType> getAll();
	Optional<EventType> getById(final Long id);
	EventType update(final Long id, final EventType eventType);
	EventType save(final EventType eventType);
	void delete(final Long id);
	Optional<EventType> getByName(String name);
	boolean existsByName(String name);
	boolean existsById(Long id);
}
