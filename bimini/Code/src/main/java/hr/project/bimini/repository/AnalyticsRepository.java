package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Analytics;
import hr.project.bimini.model.AnalyticsType;
import hr.project.bimini.model.Building;

public interface AnalyticsRepository extends JpaRepository<Analytics, Long> {

	void deleteByBuildingAndType(Building building, AnalyticsType type);
	
	List<Analytics> findByBuildingAndType(Building building, AnalyticsType type);
}
