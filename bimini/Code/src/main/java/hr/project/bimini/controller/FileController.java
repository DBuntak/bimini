package hr.project.bimini.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import hr.project.bimini.controller.DTO.UploadFileResponse;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.File;
import hr.project.bimini.model.FileType;
import hr.project.bimini.service.FileServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("file")
@AllArgsConstructor
@Slf4j
@RestController
public class FileController {

	@Autowired
	private FileServiceJpa fileService;
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<File>> getAll() {
		return ResponseEntity.ok(fileService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<File> getById(@PathVariable("id") final Long id) {
		Optional<File> file = fileService.getById(id);
        if (!file.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(file.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<File> updateById(@PathVariable("id") final Long id, @RequestBody final File file) {
        if (!fileService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(fileService.save(file));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final File file) {
		return ResponseEntity.ok(fileService.save(file));
	}
	/*@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!fileService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        fileService.delete(id);

        return ResponseEntity.ok().build();
	}*/
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}/type/{type}")
	public ResponseEntity<?> getFileByBuildingAndType(@PathVariable("id") final Long id, @PathVariable("type") String type, HttpServletRequest request){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		FileType fType = FileType.valueOf(type);
		File resource = fileService.getFileByBuildingAndType(building, fType);
        // Load file as Resource
        //File resource = fileService.loadContractAsResource(contractName);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getFileName());
      
        return new ResponseEntity<>(resource.getData(), header, HttpStatus.OK);
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/building/{id}")
    public File uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("type") String type, @PathVariable("id") final Long building_id) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(building_id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		FileType fType = FileType.valueOf(type);
		File result = fileService.storeFile(file, fType, building);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/file/building/")
                .path(String.valueOf(building.getId()))
                .path("/type/")
                .path(type)
                .toUriString();

        return result;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}")
    public ResponseEntity<?> downloadFile(@PathVariable("id") Long id, HttpServletRequest request) {
        // Load file as Resource
        File resource = fileService.loadFileAsResource(id);

        // Try to determine file's content type
        String contentType = null;
		contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(resource.getContentType()));
        header.setContentLength(resource.getData().length);
        header.set("Content-Disposition", "attachment; filename=" + resource.getFileName());
      
        return new ResponseEntity<>(resource.getData(), header, HttpStatus.OK);
     }
	
	@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{contractName:.+}")
    public ResponseEntity<?> delete(@PathVariable String fileName, HttpServletRequest request) throws IOException {
        fileService.deleteFile(fileName);
    	return ResponseEntity.ok().build();
    }
}
