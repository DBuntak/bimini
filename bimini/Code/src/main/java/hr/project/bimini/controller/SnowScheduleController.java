package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.controller.DTO.SnowApartmentsDTO;
import hr.project.bimini.controller.DTO.SnowEntryDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.SnowEntry;
import hr.project.bimini.model.SnowSchedule;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.service.SnowScheduleServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("snowSchedule")
@AllArgsConstructor
@Slf4j
@RestController
public class SnowScheduleController {
	@Autowired
	private SnowScheduleServiceJpa snowScheduleService;
	
	@Autowired
	private ApartmentController apartmentController;
	
	@Autowired
	private BuildingController buildingController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<SnowSchedule>> getAll() {
		return ResponseEntity.ok(snowScheduleService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<SnowSchedule> getById(@PathVariable("id") final Long id) {
		Optional<SnowSchedule> snowSchedule = snowScheduleService.getById(id);
        if (!snowSchedule.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(snowSchedule.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<SnowSchedule> updateById(@PathVariable("id") final Long id, @RequestBody final SnowSchedule snowSchedule) {
        if (!snowScheduleService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(snowScheduleService.save(snowSchedule));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final SnowSchedule snowSchedule) {
		return ResponseEntity.ok(snowScheduleService.save(snowSchedule));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!snowScheduleService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        snowScheduleService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN') or hasRole('APARTMENT')")
	@GetMapping("/table/{id}")
	public ResponseEntity<List<SnowEntryDTO>> getTableSnowEntries(@PathVariable("id") Long buildingId){
		return ResponseEntity.ok(snowScheduleService.getTableSnowEntries(buildingId));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/tenants/{id}")
	public ResponseEntity<List<Tenant>> getTenantsForPeriod(@PathVariable("id") Long snowEntryId){
		return ResponseEntity.ok(snowScheduleService.getTenantsForPeriod(snowEntryId));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}")
	public ResponseEntity<SnowSchedule> getByBuilding(@PathVariable("id") Long buildingId){
		return ResponseEntity.ok(snowScheduleService.getByBuilding(buildingId));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/create-schedule/{id}")
	public ResponseEntity<List<SnowEntryDTO>> createSchedule(@PathVariable("id") Long buildingId){
		return ResponseEntity.ok(snowScheduleService.createSchedule(buildingId));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/shift-schedule/{id}")
	public ResponseEntity<List<SnowEntryDTO>> shiftSchedule(@PathVariable("id") Long buildingId){
		return ResponseEntity.ok(snowScheduleService.shiftSchedule(buildingId));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/snow-entry/{snow_entry_id}/apartment/{apartment_id}")
	public ResponseEntity removeApartmentFromEntry(@PathVariable("snow_entry_id") final Long snow_entry_id,
			@PathVariable("apartment_id") final Long apartment_id) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartment_id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		snowScheduleService.removeApartmentFromEntry(apartment, snow_entry_id);
		return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/snow-entry/{snow_entry_id}/add/apartment/{apartment_id}")
	public ResponseEntity addApartmentToEntry(@PathVariable("snow_entry_id") final Long snow_entry_id,
			@PathVariable("apartment_id") final Long apartment_id) {
		if(apartment_id == 99999 || apartment_id == null) {
			return ResponseEntity.badRequest().build();
		}
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartment_id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		snowScheduleService.addApartmentToEntry(apartment, snow_entry_id);
		return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{building_id}/apartments/for-snow-entry/{snow_entry_id}")
	public ResponseEntity<Set<SnowApartmentsDTO>> getApartmentsToAddToEntry(@PathVariable("snow_entry_id") final Long snow_entry_id,
			@PathVariable("building_id") final Long building_id) {
		return ResponseEntity.ok(snowScheduleService.getApartmentsToAddToEntry(building_id, snow_entry_id));
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/building/{id}")
	public ResponseEntity deleteByBuilding(@PathVariable final Long id) {
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();

        snowScheduleService.deleteByBuilding(building);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/change/apartment/{apartment_id}/entry/{entry_id}")
	public ResponseEntity changeApartmentEntry(@PathVariable("apartment_id") final Long apartment_id,
			@PathVariable("entry_id") final Long entry_id) {
		ResponseEntity<Apartment> apartmentResponse = apartmentController.getById(apartment_id);
		if(apartmentResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment = apartmentResponse.getBody();
		
		SnowSchedule snowSchedule = snowScheduleService.getByBuilding(apartment.getBuilding().getId());
		SnowEntry se = null;
		for(SnowEntry s : snowSchedule.getSchedule()) {
			if(s.getId() == entry_id) {
				se = s;
				break;
			}
		}
		if(se == null) {
			ResponseEntity.badRequest().build();
		}
		snowScheduleService.changeApartmentEntry(se, apartment);
		return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/replace/apartment/{first_id}/{second_id}")
	public ResponseEntity replaceApartments(@PathVariable("first_id") final Long first_id,
			@PathVariable("second_id") final Long second_id) {
		ResponseEntity<Apartment> apartmentResponse1 = apartmentController.getById(first_id);
		if(apartmentResponse1.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment1 = apartmentResponse1.getBody();
		
		ResponseEntity<Apartment> apartmentResponse2 = apartmentController.getById(second_id);
		if(apartmentResponse2.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Apartment apartment2 = apartmentResponse2.getBody();
		if(apartment1.getBuilding() != apartment2.getBuilding()) {
			ResponseEntity.badRequest().build();
		}
		snowScheduleService.replaceApartments(apartment1, apartment2);
		return ResponseEntity.ok().build();
	}
	
}
