package hr.project.bimini.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.BuildingContracts;
import hr.project.bimini.model.ContractType;
import hr.project.bimini.model.PurchaseContract;

public interface BuildingContractsRepository extends JpaRepository<BuildingContracts, Long> {

	List<BuildingContracts> findByBuildingAndContractType(Building building, ContractType type);
	
	boolean existsByBuildingAndContractType(Building building, ContractType type);
	
	Optional<BuildingContracts> findByFileName(String fileName);
	
	boolean existsByFileName(String fileName);
	
	void deleteByFileName(String fileName);
}
