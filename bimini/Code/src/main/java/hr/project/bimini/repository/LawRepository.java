package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.Law;
import hr.project.bimini.model.LawType;

public interface LawRepository extends JpaRepository<Law, Long> {

	List<Law> findByBuildingAndType(Building building, LawType type);
	
	boolean existsByBuildingAndType(Building building, LawType type);
}
