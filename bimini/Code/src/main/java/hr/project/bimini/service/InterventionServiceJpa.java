package hr.project.bimini.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.DTO.InterventionDTO;
import hr.project.bimini.controller.DTO.InterventionReportDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Intervention;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;
import hr.project.bimini.repository.InterventionRepository;

@Service
public class InterventionServiceJpa implements InterventionService {

	@Autowired
	private InterventionRepository interventionRepository;
	
	@Autowired
	private BuildingService buildingService;

	
	@Override
	public List<Intervention> getAll() {
		return interventionRepository.findAll();
	}

	@Override
	public Optional<Intervention> getById(final Long id) {
		return interventionRepository.findById(id);
	}

	@Override
	public Intervention update(final Long id, final InterventionDTO intervention) {
		Intervention old = this.getById(id).get();
		old.setAmount(intervention.getAmount());
		old.setDate(intervention.getDate());
		old.setDescription(intervention.getDescription());

		return interventionRepository.save(old);
	}

	@Override
	public Intervention save(final Intervention intervention) {
		return interventionRepository.save(intervention);
	}

	@Override
	public void delete(final Long id) {
		interventionRepository.deleteById(id);
	}

	@Override
	public List<InterventionDTO> getInterventionsByBuilding(final Building building) {
		 List<Intervention> intervention = interventionRepository.findByBuilding(building);
		 List<InterventionDTO> dtos = new ArrayList<>();
		 for(Intervention bc : intervention) {
			 InterventionDTO dto = new InterventionDTO(bc.getId(), bc.getApartment().getName(), bc.getDescription(), bc.getAmount(), bc.getDate());
			 dtos.add(dto);
		 }
		 return dtos;
	}
	
	
	@Override
    public Intervention storeIntervention(MultipartFile record, final Apartment apartment, InterventionDTO interventionDTO) {

		String recordName = null;
		String recordType = null;
		byte[] recordData = null;
        try {
            // Check if the file's name contains invalid characters
        	if(record != null) {
	            if(record.getOriginalFilename().contains("..")) {
	                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + record.getOriginalFilename());
	            }
	            recordName = record.getOriginalFilename();
	            recordType = record.getContentType();
	            recordData = record.getBytes();
        	}
            Intervention newIntervention = new Intervention(null, apartment.getBuilding(), apartment, interventionDTO.getDate(), interventionDTO.getAmount(),
            		interventionDTO.getDescription(), recordName, recordType, recordData);
           
            Intervention war = interventionRepository.save(newIntervention);
            return war;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + recordName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
    public Intervention updateIntervention(MultipartFile file, Long interventionId) {
		Intervention old = this.getById(interventionId).get();
        try {
            // Check if the file's name contains invalid characters
            if(file.getOriginalFilename().contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + file.getOriginalFilename());
            }
        	old.setRecordName(file.getOriginalFilename());
        	old.setRecordType(file.getContentType());
        	old.setRecordScan(file.getBytes());

            return interventionRepository.save(old);
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + file.getOriginalFilename() + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public Intervention loadInterventionAsResource(Long id) {
		Intervention oldIntervention = interventionRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldIntervention != null) {
		    return oldIntervention;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + id);
		}
    }

	@Override
	public byte[] getReportByBuildingAndDate(Building building, Integer year) throws IOException {
		Calendar c = Calendar.getInstance();
		c.set(year, 0, 1);
		Date start = c.getTime();
		c.set(year, 11, 31);
		Date end = c.getTime();
		List<Intervention> interventions = interventionRepository.findByBuildingAndDateAfterAndDateBefore(building, start, end);
		
		return generateReport(interventions, building, year);
	}
	
	private byte[] generateReport(List<Intervention> interventions, Building building, Integer year) throws IOException {
		Double amount = 0.0;
		for(Intervention i : interventions) {
			amount+=i.getAmount();
		}
		
		XWPFDocument document = new XWPFDocument();
		
		XWPFParagraph title = document.createParagraph();
		title.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun titleRun = title.createRun();
		titleRun.setText("Godišni izvještaj intervencija za zgradu " + building.getAddress() + " i " + year + ". godinu");
		//titleRun.setColor();
		titleRun.setBold(true);
		titleRun.setFontFamily("Courier");
		titleRun.setFontSize(16);
		titleRun.setUnderline(UnderlinePatterns.SINGLE);
		titleRun.addBreak();
		titleRun.addBreak();
		
		XWPFTable table = document.createTable();
		table.setCellMargins(0, 50, 0, 50);
		String[] cell = "Redni\nbroj".split("\n");
		String[] cell0 = "Stan".split("\n");
		String[] cell1 = "Datum".split("\n");
		String[] cell2 = " Opis".split("\n");
		String[] cell3 = "Iznos".split("\n");
		
		XWPFTableRow tableRowOne = table.getRow(0);
		XWPFParagraph par = tableRowOne.getCell(0).getParagraphs().get(0);
        tableRowOne.getCell(0).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell){
            XWPFRun run = par.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(1).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell0){
            XWPFRun run = para.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para0 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(2).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell1){
            XWPFRun run = para0.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para2 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(3).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell2){
            XWPFRun run = para2.createRun();
            run.setText(text);
            run.addBreak();
        }
        XWPFParagraph para3 = tableRowOne.addNewTableCell().getParagraphs().get(0);
        tableRowOne.getCell(4).setVerticalAlignment(XWPFVertAlign.CENTER);
        for(String text : cell3){
            XWPFRun run = para3.createRun();
            run.setText(text);
            run.addBreak();
        }
	    int i=1;
	    for(Intervention in : interventions) {
	    	XWPFTableRow tableRowTwo = table.createRow();
	        tableRowTwo.getCell(0).setText(i+".");
	        tableRowTwo.getCell(1).setText(in.getApartment().getName());
	        Calendar c = Calendar.getInstance();
	        c.setTime(in.getDate());
	        int month = c.get(Calendar.MONTH)+1;
	        tableRowTwo.getCell(2).setText(c.get(Calendar.DAY_OF_MONTH) + "." + month + "." + c.get(Calendar.YEAR)+".");
	        tableRowTwo.getCell(3).setText(in.getDescription());
	        tableRowTwo.getCell(4).setText(in.getAmount().toString());
	        i++;
	    }
	    setTableAlign(table, ParagraphAlignment.CENTER);
	    
	    XWPFParagraph potpisi = document.createParagraph();
		potpisi.setAlignment(ParagraphAlignment.RIGHT);
		XWPFRun potpisiRun = potpisi.createRun();
		potpisiRun.addBreak();
		potpisiRun.addBreak();
		potpisiRun.setText("Ukupan iznos: " + amount + "kn");
		
		FileOutputStream out = new FileOutputStream("intervencije.docx");
		document.write(out);
		out.close();
		document.close();
		File f = new File("intervencije.docx");
		FileInputStream fis = new FileInputStream(f);
	    byte[] doc = IOUtils.toByteArray(fis);
	    f.delete();
	    return doc;
	}
	
	private void setTableAlign(XWPFTable table,ParagraphAlignment align) {
	    CTTblPr tblPr = table.getCTTbl().getTblPr();
	    CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
	    STJc.Enum en = STJc.Enum.forInt(align.getValue());
	    jc.setVal(en);
	}
}
