package hr.project.bimini.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.controller.ApartmentController;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.PurchaseContract;
import hr.project.bimini.repository.ApartmentRepository;
import hr.project.bimini.repository.PurchaseContractRepository;


@Service
public class PurchaseContractServiceJpa implements PurchaseContractService {

	@Autowired
	private PurchaseContractRepository purchaseContractRepository;
	
	@Autowired
	private ApartmentRepository apartmentRepository;
	
	
	@Override
	public List<PurchaseContract> getAll() {
		return purchaseContractRepository.findAll();
	}

	@Override
	public Optional<PurchaseContract> getById(final Long id) {
		return purchaseContractRepository.findById(id);
	}

	@Override
	public PurchaseContract update(final Long id, final PurchaseContract purchaseContract) {
		return purchaseContractRepository.save(purchaseContract);
	}

	@Override
	public PurchaseContract save(final PurchaseContract purchaseContract) {
		return purchaseContractRepository.save(purchaseContract);
	}
	
	@Override
	@Transactional
    public String storeContract(MultipartFile file, final Apartment apartment) {

		if(purchaseContractRepository.existsByApartment(apartment)) {
			PurchaseContract pc = purchaseContractRepository.findByApartment(apartment);
			deleteContract(pc.getFileName());
		}
        // Normalize file name
        String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }

            PurchaseContract newContract = new PurchaseContract(null, fileName, file.getContentType(), new Date(), file.getBytes(), apartment);
            PurchaseContract contract = purchaseContractRepository.save(newContract);
            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
    }
	
	@Override
	public PurchaseContract loadContractAsResource(String fileName) {
		PurchaseContract oldContract = purchaseContractRepository.findByFileName(fileName).orElseThrow(() -> new IllegalArgumentException("Ne postoji ta datoteka"));
		if(oldContract != null) {
		    return oldContract;
		} else {
		    throw new IllegalStateException("Datoteka nije pronađena " + fileName);
		}
    }
	
    @Override
    @Transactional
    public void deleteContract(String fileName) {

		if(purchaseContractRepository.existsByFileName(fileName)) {
			PurchaseContract pc = purchaseContractRepository.findByFileName(fileName).get();
			//purchaseContractRepository.deleteFromApartment(pc.getId());
			purchaseContractRepository.deleteByFileName(fileName);
		}
    }

	@Override
	public void delete(final Long id) {
		purchaseContractRepository.deleteById(id);
	}
	
	public PurchaseContract getByApartment(Apartment apartment) {
		return purchaseContractRepository.findByApartment(apartment);
	}
}
