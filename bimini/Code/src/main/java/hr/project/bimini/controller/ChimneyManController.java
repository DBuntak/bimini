package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.project.bimini.model.ChimneyMan;
import hr.project.bimini.service.ChimneyManServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("chimney_man")
@AllArgsConstructor
@Slf4j
@RestController
public class ChimneyManController {
	@Autowired
	private ChimneyManServiceJpa chimneyManService;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<ChimneyMan>> getAll() {
		return ResponseEntity.ok(chimneyManService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<ChimneyMan> getById(@PathVariable("id") final Long id) {
		Optional<ChimneyMan> chimneyMan = chimneyManService.getById(id);
        if (!chimneyMan.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(chimneyMan.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<ChimneyMan> updateById(@PathVariable("id") final Long id, @RequestBody final ChimneyMan chimneyMan) {
        if (!chimneyManService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(chimneyManService.save(chimneyMan));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final ChimneyMan chimneyMan) {
		return ResponseEntity.ok(chimneyManService.save(chimneyMan));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!chimneyManService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        chimneyManService.delete(id);

        return ResponseEntity.ok().build();
	}
}