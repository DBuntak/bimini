package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import hr.project.bimini.controller.DTO.NewUserDTO;
import hr.project.bimini.controller.DTO.UserBuildingDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Role;
import hr.project.bimini.model.RoleName;
import hr.project.bimini.model.User;
import hr.project.bimini.repository.RoleRepository;
import hr.project.bimini.repository.UserRepository;

@Service
public class UserServiceJpa implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public List<User> getAll() {
		return userRepository.findAll();
	}

	@Override
	public Optional<User> getById(final Long id) {
		return userRepository.findById(id);
	}

	@Override
	public User update(final Long id, final User user) {

		User oldUser = userRepository.findById(id).get();
		if(user.getPassword() != null) {
			if(!user.getPassword().isBlank()) {
				oldUser.setPassword(passwordEncoder.encode(user.getPassword()));
			}
		}
		if(user.getUsername() != null) {
			if(!user.getUsername().isBlank()) {
				oldUser.setUsername(user.getUsername());
			}
		}
		return userRepository.save(oldUser);
	}

	@Override
	public User save(final User user) {
		return userRepository.save(user);
	}

	@Override
	public void delete(final Long id) {
		userRepository.deleteById(id);
	}

	@Override
	public Optional<User> getByUsername(final String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public Optional<List<User>> getByRole(Role role) {
		return userRepository.findByRole(role);
	}

	@Override
	public Optional<List<User>> getByApartment(Apartment apartment) {
		return userRepository.findByApartment(apartment);
	}

	@Override
	public Optional<Role> getRoleById(Long roleId) {
		return roleRepository.findById(roleId);
	}
	
	@Override
	public User createAdmin(NewUserDTO user) {
		Role role = roleRepository.findByName(RoleName.ROLE_ADMIN);
		User us = new User(null, user.getUsername(), passwordEncoder.encode(user.getPassword()), role, null, null);
		return save(us);
	}
	
	@Override
	public User createTenant(NewUserDTO user, Building building, Apartment apartment) {
		Role role = roleRepository.findByName(RoleName.ROLE_APARTMENT);
		User us = new User(null, user.getUsername(), passwordEncoder.encode(user.getPassword()), role, apartment, building);
		return save(us);
	}
	
	@Override
	public UserBuildingDTO getBuildingForUser(User user) {
		UserBuildingDTO building = new UserBuildingDTO(user.getBuilding().getId(), user.getBuilding().getName());
		return building;
	}
	
	@Override
	public List<User> getAdminsAndByBuilding(Building building){
		Role role = roleRepository.findByName(RoleName.ROLE_ADMIN);
		return userRepository.findByBuildingOrRole(building, role);
	}
	
	@Override
	public User updateApartment(final Long id, final NewUserDTO user, Apartment apartment, Building building) {
		User oldUser = userRepository.findById(id).get();
		if(user.getPassword() != null) {
			if(!user.getPassword().isBlank()) {
				oldUser.setPassword(passwordEncoder.encode(user.getPassword()));
			}
		}
		if(user.getUsername() != null) {
			if(!user.getUsername().isBlank()) {
				oldUser.setUsername(user.getUsername());
			}
		}
		if(apartment != null) {
			oldUser.setApartment(apartment);
		}
		if(building != null) {
			oldUser.setBuilding(building);
		}
		return userRepository.save(oldUser);
	}
}
