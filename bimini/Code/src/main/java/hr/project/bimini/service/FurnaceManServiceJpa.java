package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.model.FurnaceMan;
import hr.project.bimini.repository.FurnaceManRepository;

@Service
public class FurnaceManServiceJpa implements FurnaceManService {

	@Autowired
	private FurnaceManRepository furnaceManRepository;
	
	@Override
	public List<FurnaceMan> getAll() {
		return furnaceManRepository.findAll();
	}

	@Override
	public Optional<FurnaceMan> getById(final Long id) {
		return furnaceManRepository.findById(id);
	}

	@Override
	public FurnaceMan update(final Long id, final FurnaceMan furnaceMan) {
		return furnaceManRepository.save(furnaceMan);
	}

	@Override
	public FurnaceMan save(final FurnaceMan furnaceMan) {
		return furnaceManRepository.save(furnaceMan);
	}

	@Override
	public void delete(final Long id) {
		furnaceManRepository.deleteById(id);
	}
}
