package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Transaction;
import hr.project.bimini.model.TransactionScan;

public interface TransactionScanService {

	List<TransactionScan> getAll();
	Optional<TransactionScan> getById(final Long id);
	TransactionScan update(final Long id, final TransactionScan transactionScan);
	TransactionScan save(final TransactionScan transactionScan);
	void delete(final Long id);
	String storeContract(MultipartFile file, final Transaction transaction);
	TransactionScan loadContractAsResource(String fileName);
	void deleteContract(String fileName);
}
