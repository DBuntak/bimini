package hr.project.bimini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.model.EventType;
import hr.project.bimini.repository.EventTypeRepository;

@Service
public class EventTypeServiceJpa implements EventTypeService {

	@Autowired
	private EventTypeRepository eventTypeRepository;
	
	@Override
	public List<EventType> getAll() {
		return eventTypeRepository.findAll();
	}

	@Override
	public Optional<EventType> getById(final Long id) {
		return eventTypeRepository.findById(id);
	}

	@Override
	public EventType update(final Long id, final EventType eventType) {
		return eventTypeRepository.save(eventType);
	}

	@Override
	public EventType save(final EventType eventType) {
		return eventTypeRepository.save(eventType);
	}

	@Override
	public void delete(final Long id) {
		eventTypeRepository.deleteById(id);
	}
	
	@Override
	public Optional<EventType> getByName(final String name) {
		return eventTypeRepository.findByName(name);
	}
	
	@Override
	public boolean existsByName(String name) {
		return eventTypeRepository.existsByName(name);
	}
	
	@Override
	public boolean existsById(Long id) {
		return eventTypeRepository.existsById(id);
	}
}
