package hr.project.bimini.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.Finance;
import hr.project.bimini.model.FinanceType;

public interface FinanceRepository extends JpaRepository<Finance, Long> {

	List<Finance> findByBuildingAndType(Building building, FinanceType type);
	
	boolean existsByBuildingAndType(Building building, FinanceType type);
}
