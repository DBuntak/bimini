package hr.project.bimini.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.project.bimini.controller.DTO.ManagerDTO;
import hr.project.bimini.controller.DTO.NoticePositionDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Manager;
import hr.project.bimini.repository.ManagerRepository;

@Service
public class ManagerServiceJpa implements ManagerService {

	@Autowired
	private ManagerRepository managerRepository;
	
	@Override
	public List<Manager> getAll() {
		return managerRepository.findAll();
	}

	@Override
	public Optional<Manager> getById(final Long id) {
		return managerRepository.findById(id);
	}

	@Override
	public Manager update(final Long id, final ManagerDTO manager) {
		Manager ma = this.getById(id).get();
		ma.setFirm(manager.getFirm());
		ma.setFirmEmail(manager.getFirmEmail());
		ma.setIBAN(manager.getIBAN());
		ma.setIdentificationNumber(manager.getIdentificationNumber());
		ma.setAddress(manager.getAddress());
		
		ma.setName(manager.getName());
		ma.setSurname(manager.getSurname());
		ma.setMobilePhone(manager.getMobilePhone());
		ma.setEmail(manager.getEmail());
		return managerRepository.save(ma);
	}

	
	@Override
	public Manager save(final ManagerDTO manager, final Building building) {
		Manager ma = new Manager(null, building, manager.getFirm(), manager.getIdentificationNumber(), manager.getIBAN(), manager.getFirmEmail(),
				manager.getAddress(), manager.getName(), manager.getSurname(), manager.getEmail(), manager.getMobilePhone());
		return managerRepository.save(ma);
	}

	@Override
	public void delete(final Long id) {
		managerRepository.deleteById(id);
	}
	
	@Override
	public List<Manager> getByBuilding(Building building){
		return managerRepository.findByBuilding(building);
	}
}
