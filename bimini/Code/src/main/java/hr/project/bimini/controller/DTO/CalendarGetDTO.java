package hr.project.bimini.controller.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CalendarGetDTO {

	private Long id;
	private String title;
	private Date start;
	private Date end;
	private String resource;
	private String type;
	private String worker;
}
