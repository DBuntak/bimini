package hr.project.bimini.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import hr.project.bimini.controller.DTO.WatermeterDTO;
import hr.project.bimini.model.Apartment;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.BuildingContracts;
import hr.project.bimini.model.ContractType;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.model.TenantTypes;
import hr.project.bimini.model.Watermeter;
import hr.project.bimini.repository.WatermeterRepository;

@Service
public class WatermeterServiceJpa implements WatermeterService {

	@Autowired
	private WatermeterRepository watermeterRepository;
	
	@Autowired
	private TenantService tenantService;
	
	@Autowired
	private JavaMailSender emailSender;
	
	@Override
	public List<Watermeter> getAll() {
		return watermeterRepository.findAll();
	}

	@Override
	public Optional<Watermeter> getById(final Long id) {
		return watermeterRepository.findById(id);
	}

	@Override
	public Watermeter update(final Long id, final WatermeterDTO watermeter) {
		Watermeter meter = this.getById(id).get();
		if(meter.getNumber() != watermeter.getNumber() || meter.getSystemNumber() != watermeter.getSystemNumber()) {
			meter.setLastChange(new Date());
		}
		meter.setNumber(watermeter.getNumber());
		meter.setSystemNumber(watermeter.getSystemNumber());
		
		return watermeterRepository.save(meter);
	}
	
	@Override
	public Watermeter updateTenantsNumber(final Long id, final Integer tenantsNum) throws MessagingException, IOException, InvalidFormatException {
		Watermeter meter = this.getById(id).get();
		if(tenantsNum != meter.getTenantsNumber()) {
			meter.setTenantsNumber(tenantsNum);
			this.sendWaterChangeEmail("david.buntak998@gmail.com", "Voda", "Promjena broja ukućana", meter);
		}
		meter.setTenantsNumber(tenantsNum);
		return watermeterRepository.save(meter);
	}

	/*@Override
	public Watermeter save(final WatermeterDTO watermeter, final Apartment apartment) {
		Watermeter meter = new Watermeter(null, watermeter.getNumber(), watermeter.getSystemNumber(), watermeter.getTenantsNumber(), apartment, apartment.getBuilding());
		return watermeterRepository.save(meter);
	}*/
	
	@Override
	public Watermeter save(Watermeter watermeter) {
		return watermeterRepository.save(watermeter);
	}

	@Override
	public void delete(final Long id) {
		watermeterRepository.deleteById(id);
	}

	@Override
	public List<Watermeter> getByApartment(Apartment apartment) {
		return watermeterRepository.findByApartment(apartment);
	}

	@Override
	public List<Watermeter> getByBuilding(Building building) {
		return watermeterRepository.findByBuilding(building);
	}
	
	@Override
	public void sendWaterChangeEmail(String to, String subject, String text, Watermeter meter) throws MessagingException, IOException, InvalidFormatException {
		LocalDate date = LocalDate.now();
		List<TenantTypes> tt = new ArrayList<>();
    	tt.add(TenantTypes.vanjski_vlasnik);
    	tt.add(TenantTypes.vlasnik);
    	Tenant owner = tenantService.getByApartmentAndType(meter.getApartment(), tt).get(0);
		String output = owner.getSurname()+" promjena "+date.getDayOfMonth()+"."+date.getMonth().getValue()+"."+date.getYear()+".pdf";
		Path msWordPath = Paths.get("template-voda.docx");
		XWPFDocument doc = new XWPFDocument(Files.newInputStream(msWordPath));
		for (XWPFParagraph p : doc.getParagraphs()) {
		    List<XWPFRun> runs = p.getRuns();
		    if (runs != null) {
		        for (XWPFRun r : runs) {
		            String t = r.getText(0);
		            if (t != null && t.contains("zgr-001")) {
		                t = t.replace("zgr-001", meter.getApartment().getBuilding().getAddress());
		                r.setText(t, 0);
		            }
		            if (t != null && t.contains("vla-001")) {
		                t = t.replace("vla-001", owner.getSurname()+" "+owner.getName());
		                r.setText(t, 0);
		            }
		            if (t != null && t.contains("sis-001")) {
		                t = t.replace("sis-001", meter.getSystemNumber());
		                r.setText(t, 0);
		            }
		            if (t != null && t.contains("bro-001")) {
		                t = t.replace("bro-001", meter.getTenantsNumber().toString());
		                r.setText(t, 0);
		            }
		            if (t != null && t.contains("dat-001")) {
		                t = t.replace("dat-001", "1."+date.getMonth().getValue()+"."+date.getYear()+".");
		                r.setText(t, 0);
		            }
		            if (t != null && t.contains("zgr-002")) {
		                t = t.replace("zgr-002", meter.getApartment().getBuilding().getAddress());
		                r.setText(t, 0);
		            }
		            if (t != null && t.contains("dat-002")) {
		                t = t.replace("dat-002", date.getDayOfMonth()+"."+date.getMonth().getValue()+"."+date.getYear()+".");
		                r.setText(t, 0);
		            }
		        }
		    }
		}
		for (XWPFTable tbl : doc.getTables()) {
		   for (XWPFTableRow row : tbl.getRows()) {
		      for (XWPFTableCell cell : row.getTableCells()) {
		         for (XWPFParagraph p : cell.getParagraphs()) {
		            for (XWPFRun r : p.getRuns()) {
		              String t = r.getText(0);
		              if (t != null && t.contains("vla-001")) {
			                t = t.replace("vla-001", owner.getSurname()+" "+owner.getName());
			                r.setText(t, 0);
			            }
			            if (t != null && t.contains("sis-001")) {
			                t = t.replace("sis-001", meter.getSystemNumber());
			                r.setText(t, 0);
			            }
			            if (t != null && t.contains("bro-001")) {
			                t = t.replace("bro-001", meter.getTenantsNumber().toString());
			                r.setText(t, 0);
			            }
		            }
		         }
		      }
		   }
		}
		PdfOptions options = PdfOptions.create();
		options.fontEncoding("iso-8859-2");
        OutputStream out = new FileOutputStream(new File(output));
        PdfConverter.getInstance().convert(doc, out, options);
		//doc.write(new FileOutputStream(output));
		//doc.close();
		
		MimeMessage message = emailSender.createMimeMessage();
	     
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    
	    helper.setFrom("project.bimini10@gmail.com");
	    helper.setTo(to);
	    helper.setSubject(subject);
	    helper.setText("Postovani,\n\nkao predstavnik stanara stambene zgrade "+meter.getApartment().getBuilding().getAddress()+","
	    		+ " obavještavam vas da je došlo do nove promjene u broju clanova za obracun vode, te vas molim da ažurirate svoje podatke za obracun sa "+ "1."+date.getMonth().getValue()+"."+date.getYear()+"."+". U prilogu je ovjeren i potpisan zahtjev prema dosadašnjim uputama."
	    		+ "\n\nSrdačan pozdrav,\n\nMarin Komadina\n\n099 2525257\n\nPredstavnik stanara stambene zgrade "+meter.getApartment().getBuilding().getAddress()+".");
	    
	    File file = new File(output);
	    helper.addAttachment(output, file);
	    
	    emailSender.send(message);
	    
	    file.delete();
	}
}
