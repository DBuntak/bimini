package hr.project.bimini.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hr.project.bimini.model.Building;
import hr.project.bimini.model.BuildingContracts;
import hr.project.bimini.model.ChimneyInspection;
import hr.project.bimini.model.FurnaceInspection;
import hr.project.bimini.model.FurnaceReport;
import hr.project.bimini.repository.FurnaceInspectionRepository;

@Service
public class FurnaceInspectionServiceJpa implements FurnaceInspectionService {

	@Autowired
	private FurnaceInspectionRepository furnaceInspectionRepository;
	
	
	@Override
	public List<FurnaceInspection> getAll() {
		return furnaceInspectionRepository.findAll();
	}

	@Override
	public Optional<FurnaceInspection> getById(final Long id) {
		return furnaceInspectionRepository.findById(id);
	}

	@Override
	public FurnaceInspection update(final Long id, final FurnaceInspection furnaceInspection) {
		return furnaceInspectionRepository.save(furnaceInspection);
	}

	@Override
	public FurnaceInspection save(final FurnaceInspection furnaceInspection) {
		return furnaceInspectionRepository.save(furnaceInspection);
	}

	@Override
	public void delete(final Long id) {
		furnaceInspectionRepository.deleteById(id);
	}

	@Override
	public List<FurnaceInspection> getByBuilding(Building building) {
		return furnaceInspectionRepository.findByBuilding(building);
	}

	@Override
	public FurnaceReport getReport(FurnaceInspection furnaceInspection) {
		return furnaceInspection.getFurnaceReport();
	}

	@Override
	public String storeReport(MultipartFile file, FurnaceInspection fi) {
		// Normalize file name
        String fileName = StringUtils.cleanPath(UUID.randomUUID()+"."+file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1]);

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new IllegalArgumentException("Ime datoteke sadrži nedozvoljene znakove " + fileName);
            }

            FurnaceReport newReport = new FurnaceReport(null, fileName, file.getContentType(), new Date(), file.getBytes());
            fi.setFurnaceReport(newReport);
            furnaceInspectionRepository.save(fi);
            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Ne mogu spremiti datoteke " + fileName + ". Molim pokušajte ponovo!", ex);
        }
	}
	
	@Override
	public List<FurnaceInspection> getByBuildingAndDate(Building building){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1);
		return furnaceInspectionRepository.findByBuildingAndStartAfter(building, cal.getTime());
	}
}
