package hr.project.bimini.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.gson.Gson;

import hr.project.bimini.controller.DTO.LawDTO;
import hr.project.bimini.model.Building;
import hr.project.bimini.model.Law;
import hr.project.bimini.model.LawType;
import hr.project.bimini.model.Supplier;
import hr.project.bimini.model.Tenant;
import hr.project.bimini.service.LawServiceJpa;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("law")
@AllArgsConstructor
@Slf4j
@RestController
public class LawController {

	@Autowired
	private LawServiceJpa lawService;
	@Autowired
	private BuildingController buildingController;
	@Autowired
	private SupplierController supplierController;
	@Autowired
	private TenantController tenantController;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping
	public ResponseEntity<List<Law>> getAll() {
		return ResponseEntity.ok(lawService.getAll());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<Law> getById(@PathVariable("id") final Long id) {
		Optional<Law> law = lawService.getById(id);
        if (!law.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(law.get());
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<Law> updateById(@PathVariable("id") final Long id, @RequestBody final LawDTO law) {
        if (!lawService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Supplier supplier = null;
        Tenant tenant = null;
        if(law.getLawyerId() != null) {
        	ResponseEntity<Supplier> supplierResponse = supplierController.getById(law.getLawyerId());
    		if(supplierResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
    			ResponseEntity.badRequest().build();
    		}
    		supplier = supplierResponse.getBody();
        }
        if(law.getTenantId() != null) {
        	ResponseEntity<Tenant> tenantResponse = tenantController.getById(law.getTenantId());
    		if(tenantResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
    			ResponseEntity.badRequest().build();
    		}
    		tenant = tenantResponse.getBody();
        }

        return ResponseEntity.ok(lawService.update(id, law, supplier, tenant));
	}
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public ResponseEntity save(@RequestBody final Law law) {
		return ResponseEntity.ok(lawService.save(law));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable final Long id) {
		if (!lawService.getById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        lawService.delete(id);

        return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/building/{id}/type/{type}")
	public ResponseEntity<?> getLawsByBuildingAndType(@PathVariable("id") final Long id, @PathVariable("type") String typeName, HttpServletRequest request){
		ResponseEntity<Building> buildingResponse = buildingController.getById(id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		if(LawType.valueOf(typeName) == null) {
			ResponseEntity.badRequest().build();
		}
		LawType type = LawType.valueOf(typeName);
		List<Law> resource = lawService.getLawsByBuildingAndTypeDTO(building, type);

		if(resource == null) {
			return ResponseEntity.badRequest().build();
		}
        // Try to determine file's content type
        String contentType = null;
		//contentType = resource.getContentType();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
      
        return new ResponseEntity<>(resource, HttpStatus.OK);
		
	}
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PostMapping("/upload/building/{id}")
    public Law uploadFile(@RequestParam("card") MultipartFile card, @RequestParam("data") String json, @PathVariable("id") final Long building_id) {
		Gson gson = new Gson();
		LawDTO lawDTO = gson.fromJson(json, LawDTO.class);
		ResponseEntity<Building> buildingResponse = buildingController.getById(building_id);
		if(buildingResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
			ResponseEntity.badRequest().build();
		}
		Building building = buildingResponse.getBody();
		
		if(LawType.valueOf(lawDTO.getType()) == null) {
			ResponseEntity.badRequest().build();
		}
		LawType type = LawType.valueOf(lawDTO.getType());
		
		Supplier supplier = null;
        Tenant tenant = null;
        if(lawDTO.getLawyerId() != null) {
        	ResponseEntity<Supplier> supplierResponse = supplierController.getById(lawDTO.getLawyerId());
    		if(supplierResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
    			ResponseEntity.badRequest().build();
    		}
    		supplier = supplierResponse.getBody();
        }
        if(lawDTO.getTenantId() != null) {
        	ResponseEntity<Tenant> tenantResponse = tenantController.getById(lawDTO.getTenantId());
    		if(tenantResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
    			ResponseEntity.badRequest().build();
    		}
    		tenant = tenantResponse.getBody();
        }
		
		Law l = lawService.storeLaw(card, type, building, lawDTO, tenant, supplier);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/law/building/")
                .path(String.valueOf(building.getId()))
                .path("/type/")
                .path(type.name())
                .toUriString();

        return l;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	@PutMapping("/file/{id}")
    public Law updateFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long lawId) {
		 if (!lawService.getById(lawId).isPresent()) {
	            log.error("Id " + lawId + " is not existed");
	            ResponseEntity.badRequest().build();
	        }
		
		Law law = lawService.updateLaw(file, lawId);
        

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/law/download/")
                .path(lawId.toString())
                .toUriString();

        return law;
    }
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/download/{id}")
    public ResponseEntity<?> downloadLaw(@PathVariable("id") Long lawId, HttpServletRequest request) {
        // Load file as Resource
        Law resource = lawService.loadLawAsResource(lawId);

        // Try to determine file's content type
        String contentType = null;
        byte[] data = null;
        String name = null;
		contentType = resource.getCardType();
		data = resource.getCardScan();
		name = resource.getCardName();

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf(contentType));
        header.setContentLength(data.length);
        header.set("Content-Disposition", "attachment; filename=" + name);
      
        return new ResponseEntity<>(data, header, HttpStatus.OK);
     }
}
