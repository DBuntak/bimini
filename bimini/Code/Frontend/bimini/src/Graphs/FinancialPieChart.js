import { withStyles } from '@material-ui/core/styles';
import { ResponsivePie } from '@nivo/pie'
import axios from 'axios';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Progress } from 'reactstrap';
import { Button } from 'semantic-ui-react';
import cookie from 'react-cookies';
import { parseAnalyticDataSet } from '../Parser';


const styles = theme => ({
  
})

const data =  [
    {
      "id": "css",
      "label": "css",
      "value": 313
    },
    {
      "id": "elixir",
      "label": "elixir",
      "value": 540
    },
    {
      "id": "lisp",
      "label": "lisp",
      "value": 47
    },
    {
      "id": "ruby",
      "label": "ruby",
      "value": 14
    },
    {
      "id": "make",
      "label": "make",
      "value": 319
    }
]

class FinancialPieChart extends Component{


  constructor(props) {
    super(props);
  
    this.state={
      token: '',
      buildingId: '',
      data : [],
      unavailable: true,
      fileUploadState:"",
      uploadPercentage: 0,
      progressBarHidden : "none",
      fileDownloadState:"",
      
    };
    this.inputReference = React.createRef();
      
  };




    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
      };


      componentDidMount(){

        var buildingId = cookie.load("buildingId");
        this.setState({buildingId:buildingId});
        const token = cookie.load('token');
        this.setState({token: token});

        

        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
       
          
          
        
      };

    parseFetchResponse = response => response.json().then(text => ({
      json: text,
      meta: response,
    }));


  
   

    render(){
        const { classes } = this.props;
       
       
      if(!this.props.loading){
          return (
           
            <ResponsivePie
            data={data}
            margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
            sortByValue={true}
            innerRadius={0.35}
            padAngle={2}
            cornerRadius={1}
            colors={{ scheme: 'pastel1' }}
            borderWidth={4}
            borderColor={{ from: 'color', modifiers: [ [ 'darker', '0.1' ] ] }}
            radialLabelsSkipAngle={0}
            radialLabelsTextXOffset={6}
            radialLabelsTextColor="#333333"
            radialLabelsLinkOffset={0}
            radialLabelsLinkDiagonalLength={16}
            radialLabelsLinkHorizontalLength={24}
            radialLabelsLinkStrokeWidth={1}
            radialLabelsLinkColor={{ from: 'color' }}
            slicesLabelsSkipAngle={10}
            slicesLabelsTextColor="#333333"
            animate={true}
            motionStiffness={90}
            motionDamping={15}
            legends={[
                {
                    anchor: 'bottom',
                    direction: 'row',
                    translateY: 56,
                    itemWidth: 100,
                    itemHeight: 18,
                    itemTextColor: '#999',
                    symbolSize: 18,
                    symbolShape: 'circle',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemTextColor: '#000'
                            }
                        }
                    ]
                }
            ]}
            />
                

            ); 

        }else{
          return(<div></div>)
        }
    }
  }
    


  FinancialPieChart.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FinancialPieChart);