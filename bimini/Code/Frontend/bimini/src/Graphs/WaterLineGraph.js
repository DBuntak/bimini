import { withStyles } from '@material-ui/core/styles';
import { ResponsiveLine } from '@nivo/line';
import axios from 'axios';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Progress } from 'reactstrap';
import { Button , Segment} from 'semantic-ui-react';
import cookie from 'react-cookies';
import { parseAnalyticDataSet } from '../Parser';

const styles = theme => ({
  
})



class WaterLineGraph extends Component{

  constructor(props) {
    super(props);
    
    this.state={
      token: '',
      buildingId: '',
      data : [],
      unavailable: true,
      fileUploadState:"",
      uploadPercentage: 0,
      progressBarHidden : "none",
      fileDownloadState:"",
      
    };
    this.inputReference = React.createRef();
  };

    download=()=>{
      const options={
        method:'GET',
        headers: {
          'Authorization': 'Bearer ' + this.state.token,
      }
      };
      fetch('file/building/'+this.state.buildingId+"/type/WATER")
          .then(response => {
            const filename =  response.headers.get('Content-Disposition').split('filename=')[1];
            response.blob().then(blob => {
              let url = window.URL.createObjectURL(blob);
              let a = document.createElement('a');
              a.href = url;
              a.download = filename;
              a.click();
          });
        });

    }

   
    trash=()=>{
      this.setState({data:[]})
      this.setState({unavailable: true})
    }

    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
      };

      componentDidMount(){

        var buildingId = cookie.load("buildingId");
        this.setState({buildingId:buildingId});
        const token = cookie.load('token');
        this.setState({token: token});

        this.props.enableLoading("WATER")


        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/analytics/'+buildingId+"/WATER", options)
        .then(data=>data.json())
        .then(data=>{
          if(data.length > 0){
            this.setState({data:parseAnalyticDataSet(data)})
            this.setState({unavailable: false});
            this.props.enableButtons("WATER")
          }else{
            this.setState({unavailable: true});
            this.props.disableButtons("WATER")
          }      
          }) 
          .then(
            setTimeout(() => {
              this.props.disableLoading("WATER") 
            }, 500)
          )
             
      };
      
      parseFetchResponse = response => response.json().then(text => ({
        json: text,
        meta: response,
      }));
  

  fileUploadAction = () =>{
    this.setState({progressBarHidden: ""})
    this.inputReference.current.click();
  }

  uploadFile=({target: {files} })=>{
    if(files[0].size > 2000000){
      this.setState({fileUploadState:"Datoteka je prevelika! (Maksimalna veličina 2MB)"})
      return
    }
    this.setState({fileUploadState:"Učitavam..."})
    let data = new FormData();
    let type="WATER";
    data.append("type", type);
    data.append("file", files[0]);
    const options={
      onUploadProgress: (progressEvent) =>{
        const {loaded, total} = progressEvent;
        let percent = Math.floor((loaded*100)/total)
        console.log(loaded+" kb of "+total+" kb | "+percent+" %")

        if(percent < 100){
          this.setState({uploadPercentage: percent})
        }
      },
      headers:{
        'Authorization': 'Bearer ' + this.state.token,
      }
    }
    axios.post('analytics/upload/'+this.state.buildingId, data, options).then(res=>{
      
      if(res.status != 200){
        this.setState({fileUploadState:"Neuspješno prenesena datoteka!"})
        this.setState({uploadPercentage : 0});
       
      }else{
        this.setState({fileUploadState:"Upješno prenesena datoteka!"})
        this.setState({uploadPercentage : 100})
        this.setState({unavailable: false})
        this.componentDidMount();
        this.props.enableButtons("WATER");
        this.setState({fileUploadState:""})
        this.setState({uploadPercentage : 0})
        this.setState({progressBarHidden:"none"})
      }
      
    })

  }
   
    render(){
        const { classes } = this.props;

        if(this.state.unavailable && !this.props.loading){
          return(
            <div>
              <center><h5>Učitajte datoteku sa podacima o potrošnji vode!</h5></center>
              <br></br>
              <div>
                <input type="file" hidden ref={this.inputReference} onChange={this.uploadFile} />
                <Button primary content='Prenesi' icon="upload" className="ui button" onClick={this.fileUploadAction}  size='huge'  style={{borderRadius: 30}} />
                </div>

                <br></br>
                <div  style={{display: this.state.progressBarHidden}}>
                <Progress color="success"   animated  value={this.state.uploadPercentage} />
                <center>{this.state.fileUploadState}</center>
              </div>

            </div>
          );

        }else if(!this.props.loading){
          return (
            
              <ResponsiveLine
                margin= {{ top: 20, right: 20, bottom: 60, left: 80 }}
                animate= {true}
                data={this.state.data}
                xScale={{
                    type: 'time',
                    format: '%d.%m.%Y.',
                    useUTC: false,
                    precision: 'day',
                }}
                xFormat="time:%d.%m.%Y."
                yScale={{
                    type: 'linear',
                    stacked: false,
                }}
                axisLeft={{
                  orient: 'left',
                  tickSize: 5,
                  tickPadding: 5,
                  tickRotation: 0,
                  legend: 'Vrijednost',
                  legendOffset: -40,
                  legendPosition: 'middle'
                }}
                axisBottom={{
                    format: '%d %b',
                    tickValues: 'every day',
                    legend: 'Vrijeme',
                    orient: 'bottom',
                    legendOffset: 36,
                    legendPosition: 'middle',
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                }}
                
                enableArea={true}
                pointSize={10}
                pointColor={{ theme: 'background' }}
                pointBorderWidth={2}
                pointBorderColor={{ from: 'serieColor' }}
                colors={{ scheme: 'category10' }}
                enableCrosshair={false}
                useMesh={true}
                enableSlices={false}
                curve={"monotoneX"}
            
                />
                

            ); 

        }else{
          return(<div ></div>)
        }
    }
  }
    


  WaterLineGraph.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(WaterLineGraph);