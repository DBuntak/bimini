import { withStyles } from '@material-ui/core/styles';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import InitializerProgress from '../Initializer/InitializerProgress'
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Form, TextArea, Label, Card } from 'semantic-ui-react';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import React, { useState, useEffect, forwardRef } from "react";
import Draggable from "react-draggable";
import { v4 as uuidv4 } from "uuid";
import { parseDateJS, parseNotes } from '../Parser';
import background1 from "../Assets/Board1.jpg";
import cookie from 'react-cookies';
import MaterialTable from 'material-table';

var randomColor = require("randomcolor");

const columns = () => [
    { title: 'Identifikator', field: 'id', editable: "never", width: 100, hidden: true },
    { title: 'Naslov', field: 'title', width: 100 },
    { title: 'Sadržaj', field: 'message', width: 100 },
    { title: 'Datum', field: 'date', width: 100 },
]

class NoticeBoard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            token: '',
            role: '',
            notes: [],
            addPopup: false,
            editPopup: false,
            deletePopup: false,
            archivePopup: false,

            newNoteTitle: '',
            newNoteMessage: '',

            noteId: '',

            messageText: '',
            messageAlert: false,
            messageColor: "danger",

        };

    };

    clearTextFields = () => {
        this.setState({ newNoteTitle: '' })
        this.setState({ newNoteMessage: '' })
    }


    toggleAddPopup = () => {
        this.setState({ addPopup: true })
    }

    cancelAddPopup = () => {
        this.setState({ addPopup: false })
        this.clearTextFields()

    }

    toggleDeletePopup = () => {
        this.setState({ deletePopup: true })
    }

    cancelDeletePopup = () => {
        this.setState({ deletePopup: false })
        this.clearTextFields()
        this.componentDidMount()
    }

    toggleEditPopup = () => {
        this.setState({ editPopup: true })
    }

    cancelEditPopup = () => {
        this.setState({ editPopup: false })
        this.clearTextFields()
        this.componentDidMount()
    }

    toggleArchivePopup = () => {
        this.setState({ archivePopup: true })

    }

    cancelArchivePopup = () => {
        this.setState({ archivePopup: false })
        this.clearTextFields()

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    parseFetchResponse = response => response.json().then(text => ({
        json: text,
        meta: response,
    }));

    componentDidMount() {

        const token = cookie.load('token');
        this.setState({ token: token });
        const role = cookie.load('role');
        this.setState({ role: role });
        const buildingId = cookie.load('buildingId');
        this.setState({ buildingId: buildingId });

        this.setState({ loading: true })
        const options = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
            },
        };
        fetch('/notice-board/building/' + buildingId, options)
            .then(data => data.json())
            .then(notes => {
                this.setState({ notes: parseNotes(notes) })

            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )
    };

    addNote = () => {
        const data = {
            title: this.state.newNoteTitle,
            message: this.state.newNoteMessage,
            buildingId: this.state.buildingId,
            x: 250,
            y: 250
        }
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        fetch('/notice-board', options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
                if (meta.status !== 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno stvorena obavijest!" })
                    this.cancelAddPopup();
                    this.clearTextFields();
                    this.componentDidMount();

                }
                else {
                    this.cancelAddPopup();
                    this.clearTextFields();
                    this.componentDidMount();


                }
            })

    }



    deleteNote = () => {
        const options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            }
        };
        fetch('/notice-board/' + this.state.noteId, options)
            .then(response => {
                if (response.status === 200) {
                    this.cancelDeletePopup();
                    this.componentDidMount();

                }
                else {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno obrisana obavijest!" })
                    this.cancelDeletePopup();
                    this.componentDidMount();
                }
            })
    }

    editNote = () => {
        const data = {
            title: this.state.newNoteTitle,
            message: this.state.newNoteMessage,
            buildingId: this.state.buildingId
        }


        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        fetch('/notice-board/' + this.state.noteId, options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
                if (meta.status !== 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno izmijenjena obavijest!" })
                    this.cancelEditPopup();
                    this.clearTextFields();
                    this.componentDidMount();

                }
                else {
                    this.cancelEditPopup();
                    this.clearTextFields();
                    this.componentDidMount();
                }
            })
    }

    savePositions = () => {
        var error = false
        for (var i = 0; i < this.state.notes.length; ++i) {
            const data = {
                x: this.state.notes[i].x,
                y: this.state.notes[i].y,
            }
            const options = {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + this.state.token,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            };
            fetch('/notice-board/position/' + this.state.notes[i].id, options)
                .then(this.parseFetchResponse)
                .then(({ json, meta }) => {
                    if (meta.status !== 200) {
                        error = true
                    }
                })
        }
        if (error === true) {
            this.setState({ messageAlert: true }, () => {
                window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                }, 6000)
            });
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno spremljene obavijesti!" })
        }

    }

    updatePosition = (data, index, noteId) => {

        for (var i = 0; i < this.state.notes.length; ++i) {
            if (this.state.notes[i].id === noteId) {
                this.state.notes[i].x = data.x
                this.state.notes[i].y = data.y
            }
        }
    }

    dismissAlert = () => {
        this.setState({ messageAlert: false })
    }


    tableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
        body: {
            emptyDataSourceMessage: 'Nema podataka za prikazati',
            editRow: {
                deleteText: 'Jeste li sigurni?',
                saveTooltip: 'Potvrdi',
                cancelTooltip: 'Odustani'
            },
            addTooltip: 'Dodaj',
            deleteTooltip: 'Obriši',
            editTooltip: 'Izmijeni',
        },
        header: {
            actions: 'Akcije'
        },
        pagination: {
            firstTooltip: 'Prva stranica',
            previousTooltip: 'Prethodna stranica',
            nextTooltip: 'Iduća stranica',
            lastTooltip: 'Posljednja stranica',
            labelRowsSelect: 'redaka',
        },
        toolbar: {
            searchTooltip: 'Pretraži',
            searchPlaceholder: 'Pretraži',
            nRowsSelected: '{0} redaka odabrano',
            exportTitle: 'Izvezi',
            exportName: 'Izvezi kao .csv',
        }
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <div >
                    <Alert style={{ position: 'absolute', width: 100 + '%' }} color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                        <h5>{this.state.messageText}</h5>
                    </Alert>

                    <div>
                    <Modal isOpen={this.state.archivePopup}  >
                        <ModalHeader toggle={this.cancelArchivePopup}><h4>Arhiva obavijesti</h4></ModalHeader>
                        <ModalBody>
                            <MaterialTable

                                icons={this.tableIcons}
                                title="Obavijesti"
                                localization={this.tableTextLocalization}
                                columns={columns()}
                                options={{
                                    actionsColumnIndex: -1,
                                    exportButton: { pdf: false, csv: true },
                                    headerStyle: {
                                        backgroundColor: '#E86A22',
                                        color: '#FFF'
                                    },
                                    maxBodyHeight: 600,
                                    pageSize: 10,
                                    pageSizeOptions: [10, 20, 50, 100]
                                }}
                                data={this.state.notes}
                                actions={[
                                    {
                                        icon: () => {
                                            return (
                                                <Icon size="small" name='add' />
                                            );
                                        },
                                        tooltip: 'Dodaj obavijest',
                                        isFreeAction: true,
                                        onClick: () => this.toggleAddPopup(),

                                    },

                                    {
                                        icon: () => <Icon name="pencil" style={{ color: '#E86A22' }}></Icon>,
                                        tooltip: 'Uredi obavijest',
                                        onClick: (event, rowData) => {
                                            this.setState({ noteId: rowData.id })
                                            this.setState({ newNoteTitle: rowData.title });
                                            this.setState({ newNoteMessage: rowData.message });
                                            this.toggleEditPopup();

                                        }
                                    },
                                    {
                                        icon: () => <Icon name="trash" style={{ color: '#E86A22' }}></Icon>,
                                        tooltip: 'Obriši obavijest',
                                        onClick: (event, rowData) => {
                                            this.setState({ noteId: rowData.id })
                                            this.toggleDeletePopup();

                                        }
                                    },

                                ]}
                            />

                        </ModalBody>

                    </Modal>
                    </div>

                    <Modal isOpen={this.state.deletePopup}  >
                        <ModalBody>
                            Jeste li sigurni da želite obrisati obavijest?
                         </ModalBody>
                        <ModalFooter>
                            <Button size="small" positive onClick={this.deleteNote}>Izbriši</Button>{' '}
                            <Button size="small" negative onClick={this.cancelDeletePopup}>Odustani</Button>
                        </ModalFooter>
                    </Modal>

                    <Modal isOpen={this.state.editPopup} centered={true} >
                        <ModalHeader toggle={this.cancelEditPopup}><h4>Unesite nove podatke o obavijesti</h4></ModalHeader>
                        <ModalBody>
                            <h6>Naslov obavijesti:</h6>
                            <Input placeholder='Naslov' fluid value={this.state.newNoteTitle} onChange={this.handleChange('newNoteTitle')} /><br></br>
                            <h6>Sadržaj obavijesti:</h6>
                            <Form>
                                <TextArea placeholder='Sadržaj' value={this.state.newNoteMessage}
                                    onChange={this.handleChange('newNoteMessage')} />
                            </Form>

                        </ModalBody>
                        <ModalFooter>
                            <Button size="small" positive onClick={this.editNote}>Izmijeni</Button>

                        </ModalFooter>
                    </Modal>

                    <Modal isOpen={this.state.addPopup} centered={true} >
                        <ModalHeader toggle={this.cancelAddPopup}><h4>Unesite podatke o obavijesti</h4></ModalHeader>
                        <ModalBody>

                            <Input placeholder='Naslov' fluid value={this.state.newNoteTitle} onChange={this.handleChange('newNoteTitle')} /><br></br>
                            <Form>
                                <TextArea placeholder='Sadržaj' value={this.state.newNoteMessage}
                                    onChange={this.handleChange('newNoteMessage')} />
                            </Form>

                        </ModalBody>
                        <ModalFooter>
                            <Button size="small" positive onClick={this.addNote}>Dodaj</Button>

                        </ModalFooter>
                    </Modal>

                    <br></br>
                    <h1 style={{ marginLeft: 3 + 'vh', marginTop: 3 + 'vh', color: 'white', position: 'absolute' }}><b>Oglasna ploča</b></h1>
                    <Button style={{ marginTop: 4 + 'vh', marginLeft: 40 + 'vh', backgroundColor: '#ffdead' }} icon labelPosition='left' onClick={this.toggleAddPopup} circular>
                        <Icon name='add' />
                        Dodaj novu obavijest
                    </Button>
                    <Button style={{ marginTop: 4 + 'vh', marginLeft: 2 + 'vh', backgroundColor: '#ffdead' }} icon labelPosition='left' onClick={this.savePositions} circular>
                        <Icon name='save' />
                        Spremi stanje ploče
                    </Button>
                    <Button style={{ marginTop: 4 + 'vh', marginLeft: 2 + 'vh', backgroundColor: '#ffdead' }} icon labelPosition='left' onClick={this.toggleArchivePopup} circular>
                        <Icon name='archive' />
                        Arhiva obavijesti
                    </Button>

                    {this.state.notes.map((note, index) => {
                        return (
                            <Draggable
                                key={note.id}
                                defaultPosition={note.defaultPos}
                                onStop={(e, data) => {
                                    this.updatePosition(data, index, note.id);
                                }}
                            >
                                <Card style={{ position: 'absolute', left: note.x + 'px', top: note.y + 'px', backgroundColor: '#ffdead', width: '20%' }} >
                                    <Card.Content>
                                        <Card.Header>
                                            <div>
                                                {note.title}
                                                <Button style={{ backgroundColor: '#ffdead' }} onClick={() => { this.setState({ noteId: note.id }); this.toggleDeletePopup() }} icon='x' size="mini" />
                                                <Button style={{ backgroundColor: '#ffdead' }} onClick={() => { this.setState({ noteId: note.id }); this.setState({ newNoteTitle: note.title }); this.setState({ newNoteMessage: note.message }); this.toggleEditPopup() }} icon='pencil' size="mini" />
                                            </div>
                                        </Card.Header>
                                        <Card.Meta>
                                            {note.date}
                                        </Card.Meta>
                                        <Card.Description>
                                            {note.message}
                                        </Card.Description>
                                    </Card.Content>
                                </Card>

                            </Draggable>
                        );
                    })}


                </div>
                <br></br>
                <div style={{ marginTop: -13 + 'vh', height: 100 + 'vh' }}>
                    <img src={background1} />
                </div>
            </div>

        );
    }
}
export default withStyles()(NoticeBoard);