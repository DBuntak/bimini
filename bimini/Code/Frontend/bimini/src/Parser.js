
export function parseDate(oldDate){
    if(oldDate === null){
      return ''
    }
    let parts1 = oldDate.split("T");
    let parts2 = parts1[0].split("-");
    let newDate=parts2[2]+'.'+parts2[1]+'.'+parts2[0];
    return newDate;
};

export function parseDateTime(oldDate){
  if(oldDate === null){
    return ''
  }
  let parts1 = oldDate.split("T");
  let parts2 = parts1[0].split("-");
  let parts3 = parts1[1].split(":");
  let newDate=parts2[2]+'.'+parts2[1]+'.'+parts2[0]+' '+parts3[0]+':'+parts3[1];
  return newDate;
};





export function parseBoolean(boolean){
    if(boolean){
      return 'DA';
    }else{
      return 'NE'
    }
};
export function parseGender(gender){
    if(gender==='male'){
      return 'Muški';
    }else if(gender === 'female'){
      return 'Ženski';
    }else{
      return 'Ostalo';
    }
};





export function parseNullString(detail){
  if(detail=== null){
    return 'NIJE DOSTUPNO'
  }
  else return detail;

}

export function lessEqualDate(date1, date2){
  let date1Parts = date1.split('.');
  let date2Parts = date2.split('.');

  let month1 = date1Parts[1];
  let day1 = date1Parts[0];
  let year1 = date1Parts[2];

  let month2 =date2Parts[1];
  let day2 = date2Parts[0];
  let year2 = date2Parts[2];

  if(year1 < year2){
    return true;
    
  }else if(year1 === year2){
    if(month1 < month2){
      return true;
    }else  if(month1 === month2){
      if(day1 < day2){
        return true;
      }
    }
  }

  if(year1 === year2 && month1 === month2 && day1 === day2){
    return true;
  }

  return false;
  
}

export function greaterEqualDate(date1, date2){
 

  let date1Parts = date1.split('.');
  let date2Parts = date2.split('.');

  let month1 = date1Parts[1];
  let day1 = date1Parts[0];
  let year1 = date1Parts[2];

  let month2 =date2Parts[1];
  let day2 = date2Parts[0];
  let year2 = date2Parts[2];

  if(year1 > year2){
    return true;
    
  }else if(year1 === year2){
    if(month1 > month2){
      return true;
    }else  if(month1 === month2){
      if(day1> day2){
        return true;
      }
    }
  }
  if(year1 === year2 && month1=== month2 && day1 === day2){
    return true;
  }

  return false;
  
}

export function parseDateJS(dateString){
  let month1 = dateString.getMonth() +1;
  let day1 = dateString.getDate();
  let year1 = dateString.getFullYear();
  if(day1 <= 9){
    day1 = '0'+dateString.getDate();
  }
  if(month1 <= 9){
    month1 = '0'+(dateString.getMonth()+1);
  }
  return day1+'.'+month1+'.'+year1;

}

export function parseDateTimeJS(dateString){
  let month = dateString.getMonth() +1;
  let day = dateString.getDate();
  let year = dateString.getFullYear();
  let hours = dateString.getHours();
  let minutes = dateString.getMinutes();
  if(day <= 9){
    day = '0'+dateString.getDate();
  }
  if(month <= 9){
    month = '0'+(dateString.getMonth()+1);
  }
  if(hours <= 9){
    hours = '0'+(dateString.getHours());
  }
  if(minutes <= 9){
    minutes = '0'+(dateString.getMinutes());
  }
  return day+'.'+month+'.'+year+"  "+hours+":"+minutes;

}



export function parseUplaodFileName(fileName){
  var name = '';
  let parts = fileName.split("\\");
  return parts[parts.length-1];
}


export function parseResource(resource){
  if(resource === undefined) return "Nedostupno"
  else return resource
}

export function sortData(data){
  return data.sort((a, b) => (a.id > b.id) ? 1 : -1)
}

export function addOneHour(date) {
  let h = date.getHours();
  
  date.setHours(12);
  return date;
}

export function convertDates(dateJava){
  if(dateJava === null){
    return ''
  }
  let parts1 = dateJava.split("T");
  let parts2 = parts1[0].split("-");
  let parts3 = parts1[1].split(":");
  
  let days = parts2[2]
  let month = parts2[1]
  let years = parts2[0]
  let hours = parts3[0]
  let minutes = parts3[1]
  let newDate = new Date(years, month-1, days, hours, minutes, 0)
  return newDate; 
  
}

export function parseAnalyticDataSet(dataSet){
  var newDataSet = []
  for(let i = 0; i<dataSet.length; ++i){
    var dataEntry = {}
    dataEntry.x = dataSet[i].date
    dataEntry.y = dataSet[i].value
    newDataSet[i] = dataEntry
  }
  const dataArray = []
  let data = {}
  data.id = dataSet[0].type
  data.data = newDataSet
  dataArray.push(data)
  return dataArray

}

export function parseWorkers(workers){
  let workerList = []
  for(let i = 0; i<workers.length; ++i){
    let worker = workers[i]
    let workerOption = {}
    workerOption.value = worker.id;
    workerOption.text = worker.name+" "+worker.surname
    workerList.push(workerOption)
  }
  let nonOption = {}
  nonOption.value = 99999;
  nonOption.text = "Ništa od navedenog"
  workerList.push(nonOption)
  return workerList
}

export function parseApartments(apartments){
  let apartmentList = []
  for(let i = 0; i<apartments.length; ++i){
    let apartment = apartments[i]
    let apartmentOption = {}
    apartmentOption.value = apartment.id;
    apartmentOption.text = apartment.name
    apartmentList.push(apartmentOption)
  }
  let nonOption = {}
  nonOption.value = 99999;
  nonOption.text = "Ništa od navedenog"
  apartmentList.push(nonOption)
  return apartmentList
}

export function parseTenants(tenants){
  for(var i = 0; i<tenants.length; ++i){
    if(tenants[i].type ==="stanar"){
      tenants[i].type = "STANAR"
    }
    else if(tenants[i].type ==="vlasnik"){
      tenants[i].type = "VLASNIK"
    }
    else if(tenants[i].type ==="podstanar"){
      tenants[i].type = "PODSTANAR"
    }
    else if(tenants[i].type ==="glavni_podstanar"){
      tenants[i].type = "GLAVNI PODSTANAR"
    }
    else if(tenants[i].type ==="vanjski_vlasnik"){
      tenants[i].type = "VANJSKI VLASNIK"
    }
    if(tenants[i].role === null){
      tenants[i].role = "NIŠTA"
    }
    else if(tenants[i].role === "stambeno_vijeće") {
      tenants[i].role = "ČLAN STAMBENOG VIJEĆA";
    }
    else{
      tenants[i].role =  tenants[i].role.toUpperCase();
    }
  }
}

export function deRefType(type){
  if(type ==="STANAR"){
    return "stanar"
  }
  else if(type ==="VLASNIK"){
    return "vlasnik"
  }
  else if(type ==="PODSTANAR"){
    return "podstanar"
  }
  else if(type ==="GLAVNI PODSTANAR"){
    return "glavni_podstanar"
  }
  else if(type ==="VANJSKI VLASNIK"){
    return "vanjski_vlasnik"
  }

 
}

export function deRefRole(role){
  if(role ==="NIŠTA"){
    return null
  }
  else if(role ==="PREDSTAVNIK"){
    return "predstavnik"
  }
  else if(role ==="ČLAN STAMBENOG VIJEĆA"){
    return "stambeno_vijeće"
  }
  
}

export function deRefManagementType(type){
  if(type ==="NIŠTA"){
    return null
  }
  else if(type ==="PREDSTAVNIK"){
    return "predstavnik"
  }
  else if(type ==="STAMBENO VIJEĆE"){
    return "stambeno_vijeće"
  }
}

export function parseBuildings(buildings){
  for(var i = 0; i<buildings.length; ++i){
    if(buildings[i].managamentType ==="null"){
      buildings[i].managamentType = "NIŠTA"
    }
    else if(buildings[i].managamentType ==="predstavnik"){
      buildings[i].managamentType = "PREDSTAVNIK"
    }
    else if(buildings[i].managamentType ==="podstanar"){
      buildings[i].managamentType = "PODSTANAR"
    }
    else if(buildings[i].managamentType ==="stambeno_vijeće"){
      buildings[i].managamentType = "STAMBENO VIJEĆE"
    }
    
  }
}

export function parseContracts(contracts){
  for(var i = 0; i<contracts.length; ++i){
    if(contracts[i].conclusionDate !== null){
      contracts[i].conclusionDate = parseDate(contracts[i].conclusionDate)
    }
    else{
      contracts[i].conclusionDate = "NIJE NAVEDENO"
    }
    if(contracts[i].expirationDate !== null){
     contracts[i].expirationDate = parseDate(contracts[i].expirationDate)
    }
    else{
      contracts[i].expirationDate = "NIJE NAVEDENO"
    }
    
  }
  return contracts
}

export function parseLaws(laws){
  for(var i = 0; i<laws.length; ++i){
    if(laws[i].periodFrom !== null){
      laws[i].periodFrom = parseDate(laws[i].periodFrom)
    }
    else{
      laws[i].periodFrom = "NIJE NAVEDENO"
    }
    if(laws[i].periodTo !== null){
     laws[i].periodTo = parseDate(laws[i].periodTo)
    }
    else{
      laws[i].periodTo = "NIJE NAVEDENO"
    }
    if(laws[i].tenant !== null){
      var ten = laws[i].tenant.name + " " + laws[i].tenant.surname;
      laws[i].ten = ten;
    } 
    if(laws[i].lawyer !== null){
      var lawy = laws[i].lawyer.firm;
      laws[i].lawy = lawy;
    }
  }
  return laws
}


export function parseInterventions(interventions){
  for(var i = 0; i<interventions.length; ++i){
    if(interventions[i].date !== null){
      interventions[i].date = parseDate(interventions[i].date)
    }
    else{
      interventions[i].date = "NIJE NAVEDENO"
    }
  }
  return interventions
}

export function parseWaterApartments(apartments){
  for(var i = 0; i<apartments.length; ++i){
    if(apartments[i].number === null){
      apartments[i].number="NIJE UNESENO"
    }
    if(apartments[i].systemNumber === null){
      apartments[i].systemNumber="NIJE UNESENO"
    }
    if(apartments[i].tenantsNumber === null){
      apartments[i].tenantsNumber="NIJE UNESENO"
    }
    if(apartments[i].lastChange !== null && apartments[i].lastChange !== undefined ){
      apartments[i].lastChange  = parseDate(apartments[i].lastChange )
    }
    else{
      apartments[i].lastChange = "NIJE MIJENJANO"
    }
  }
  return apartments

}

export function parseNotes(notes){
  for(var i = 0; i<notes.length; ++i){
    if(notes[i].date !== null){
      notes[i].date=parseDate(notes[i].date)
    }
   
  }
  return notes

}

export function makeJSDate(date){
  let splits = date.split(".")
  let d = splits[0]
  let m = splits[1]
  let y = splits[2]
  let newDate = new Date(y, m-1, d, 0, 0, 0)
  return newDate
}