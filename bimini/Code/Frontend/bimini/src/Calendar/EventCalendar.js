import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';
import 'moment/locale/hr';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Loader, Segment, Form, TextArea, Select, Input, Dropdown } from 'semantic-ui-react';
import { parseBoolean, parseDateJS, parseDateTimeJS, parseResource, parseDate, parseDateTime, addOneHour, convertDates, parseWorkers } from '../Parser';
import cookie from 'react-cookies';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'
import 'react-big-calendar/lib/addons/dragAndDrop/styles.scss'

const DragAndDropCalendar = withDragAndDrop(Calendar)


const styles = theme => ({

})


const localizer = momentLocalizer(moment)


function Event({ event }) {
  return (
    <span>
      <strong>[{event.type}] </strong>
      <font size="2">{event.title}</font>
    </span>
  )
}


class EventCalendar extends Component {


  constructor(props) {
    super(props);

    this.state = {
      token: '',
      role: '',
      buildingId: '',
      events: [],
      eventDetailsPopup: false,
      eventAddPopup: false,
      eventTypes: [],

      eventId: '',
      eventName: '',
      eventType: '',
      eventStart: '',
      eventEnd: '',
      eventAllDay: '',
      eventResource: '',
      eventWorker: '',

      newEventStart: '',
      newEventEnd: '',
      newEventName: '',
      newEventResource: '',
      newEventRequestStart: '',
      newEventRequestEnd: '',
      newEventType: "",
      newEventWorker: '',


      loading: false,
      messageText: '',
      messageAlert: false,
      messageColor: "danger",
      deletePopup: false,




    };

  };

  componentDidMount() {


    var buildingId = cookie.load("buildingId");
    this.setState({ buildingId: buildingId });
    this.setState({ loading: true })
    const token = cookie.load('token');
    this.setState({ token: token });
    const role = cookie.load('role');
    this.setState({ role: role });

    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/calendar/' + buildingId, options)
      .then(data => data.json())
      .then(events => { this.setState({ events: this.convertDatesInEvents(events) }) })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  };

  toogleDetailsPopUp = (event) => {
    this.setState({ eventId: event.id })
    this.setState({ eventName: event.title })
    this.setState({ eventType: event.type })
    let parsedAllDay = parseBoolean(event.allDay)
    this.setState({ eventAllDay: parsedAllDay })
    if (event.allDay) {
      let startDate = parseDateJS(event.start)
      this.setState({ eventStart: startDate })
      let endDate = parseDateJS(event.end)
      this.setState({ eventEnd: endDate })
    }
    else {
      let startDate = parseDateTimeJS(event.start)
      this.setState({ eventStart: startDate })
      let endDate = parseDateTimeJS(event.end)
      this.setState({ eventEnd: endDate })
    }


    let parsedResource = parseResource(event.resource)
    this.setState({ eventResource: parsedResource })
    this.setState({ eventDetailsPopup: true })

    let worker = event.worker;
    if (worker === null) {
      this.setState({ eventWorker: "Nije dostupno" })
    } else {
      this.setState({ eventWorker: worker })
    }

  }

  toogleDeletePopUp = () => {
    this.setState({ deletePopup: true })
  }

  cancelDeletePopUp = () => {
    this.setState({ deletePopup: false })
  }

  cancelDetailsPopUp = () => {
    this.setState({ eventDetailsPopup: false })
  }

  toogleAddPopUp = () => {
    this.getEventTypes()
    this.setState({ eventAddPopup: true })
  }

  cancelAddPopUp = () => {
    this.setState({ eventAddPopup: false })
    this.clearTextFields()
  }

  deleteEvent = () => {

    const options = {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      }
    };
    return fetch('/event/' + this.state.eventId, options)
      .then(response => {
        if (response.status === 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno obrisan događaj!" })
          this.cancelDeletePopUp();
          this.cancelDetailsPopUp();
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno obrisan događaj!" })
          this.cancelDeletePopUp();
          this.cancelDetailsPopUp();

        }
      })
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSelect = ({ start, end }) => {
    let eventStart = parseDateTimeJS(start)
    this.setState({ newEventStart: eventStart })
    this.setState({ newEventRequestStart: start })
    let eventEnd = parseDateTimeJS(end)
    this.setState({ newEventEnd: eventEnd })
    this.setState({ newEventRequestEnd: end })
    this.toogleAddPopUp();

  }

  handleDropdownAddition = (e, { value }) => {
    this.addNewEventType(value)
  }

  handleChangeDropdown = (e, { value }) => {
    this.setState({ newEventType: value });
  }

  convertDatesInEvents = (events) => {
    var newEvents = []
    for (let i = 0; i < events.length; ++i) {

      var event = events[i]
      event.start = convertDates(event.start)
      event.end = convertDates(event.end)
      newEvents.push(event);
    }
    return newEvents;
  }

  addNewEventType = (value) => {
    const data = {
      name: value
    };
    const options = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    };
    return fetch('/event-type', options)
      .then(this.parseFetchResponse)
      .then(({ json, meta }) => {
        if (meta.status !== 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno stvoren događaj!" })
          this.cancelAddPopUp();
          this.clearTextFields();
        } else {
          this.state.eventTypes.push({ key: json.id, value: json.id, text: json.name })
        }
      })

  }


  getEventTypes = () => {
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/event-type', options)
      .then(data => data.json())
      .then(events => {
        var evTypes = []
        for (var i = 0; i < events.length; ++i) {
          evTypes.push({ key: events[i].id, value: events[i].id, text: events[i].name })
        }
        this.setState({ eventTypes: evTypes })
      })
  }

  resizeEvent = ({ event, start, end }) => {
    this.updateEvent(start, end, event.id)
  }

  updateEvent = (start, end, eventId) => {
    const data = {
      end: addOneHour(end),
      start: addOneHour(start),
    };
    const options = {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    };

    fetch('/event/dates/' + eventId, options)
      .then(this.parseFetchResponse)
      .then(({ json, meta }) => {
        if (meta.status !== 200) {


        }
        else {
          this.componentDidMount()

        }
      })

  }

  moveEvent = ({ event, start, end }) => {
    this.updateEvent(start, end, event.id)
  }

  addNewEvent = () => {

    this.setState({ loading: true })

    const data = {
      title: this.state.newEventName,
      start: addOneHour(this.state.newEventRequestStart),
      end: addOneHour(this.state.newEventRequestEnd),
      resource: this.state.newEventResource,
      typeId: this.state.newEventType,
      worker: this.state.newEventWorker
    };
    const options = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    };
    return fetch('/calendar/building/' + this.state.buildingId, options)
      .then(this.parseFetchResponse)
      .then(({ json, meta }) => {
        if (meta.status !== 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno stvoren događaj!" })
          this.cancelAddPopUp();
          this.clearTextFields();


        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno stvoren događaj!" })
          this.cancelAddPopUp();
          this.componentDidMount();
          this.clearTextFields();


        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )

  };



  parseFetchResponse = response => response.json().then(text => ({
    json: text,
    meta: response,
  }));

  clearTextFields = () => {
    this.setState({ newEventName: '' })
    this.setState({ newEventResource: '' })
    this.setState({ newEventEnd: '' })
    this.setState({ newEventStart: '' })
    this.setState({ newEventType: '' })
    this.setState({ newEventWorker: '' })


  }


  dismissAlert = () => {
    this.setState({ messageAlert: false })
  }


  render() {

    const { classes } = this.props;

    return (

      <div>
        <Modal isOpen={this.state.deletePopup}  >
          <ModalBody>
            Jeste li sigurni da želite obrisati događaj?
              </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.deleteEvent}>Izbriši</Button>
            <Button size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.eventDetailsPopup} centered={true} >
          <ModalHeader toggle={this.cancelDetailsPopUp}><h4>Detalji o događaju</h4></ModalHeader>
          <ModalBody>
            <p>
              <b>Naziv događaja:</b> {this.state.eventName}<br></br><br></br>
              <b> Tip događaja:</b> {this.state.eventType}<br></br><br></br>
              <b>  Početak događaja:</b> {this.state.eventStart}<br></br><br></br>
              <b>  Završetak događaja:</b> {this.state.eventEnd}<br></br><br></br>
              <b>  Opis:</b> {this.state.eventResource}<br></br><br></br>
              <b>  Izvođač:</b> {this.state.eventWorker}<br></br><br></br>
            </p>

          </ModalBody>
          <ModalFooter>
            <Button size="small" negative onClick={this.toogleDeletePopUp}>Izbriši događaj</Button>

          </ModalFooter>

        </Modal>

        <Modal isOpen={this.state.eventAddPopup} centered={true} >
          <ModalHeader toggle={this.cancelAddPopUp} ><h4>Unesite podatke o događaju</h4></ModalHeader>
          <ModalBody>
            <div>

              <Input fluid placeholder='Ime događaja' value={this.state.newEventName} onChange={this.handleChange('newEventName')} />
              <br></br>
              <Dropdown
                options={this.state.eventTypes}
                placeholder='Odaberi tip događaja'
                search
                selection
                fluid
                allowAdditions
                additionLabel='Novi tip događaja: '
                value={this.state.newEventType}
                onAddItem={this.handleDropdownAddition}
                onChange={this.handleChangeDropdown}
              />
              <br></br>
              <Form>
                <TextArea placeholder='Opis događaja' value={this.state.newEventResource}
                  onChange={this.handleChange('newEventResource')} />
              </Form>
              <br></br>
              <Input fluid placeholder='Naziv izvođača' value={this.state.newEventWorker} onChange={this.handleChange('newEventWorker')} />
              <br></br>

              <p>Početak događaja: {this.state.newEventStart}</p>
              <p>Završetak događaja: {this.state.newEventEnd}</p>

            </div>

          </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.addNewEvent}>Dodaj događaj</Button>

          </ModalFooter>
        </Modal>


        <Segment>
          <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
            <h5>{this.state.messageText}</h5>
          </Alert>
          <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
          </Dimmer>
          <DragAndDropCalendar
            selectable
            localizer={localizer}
            events={this.state.events}
            defaultView={Views.MONTH}
            defaultDate={new Date()}
            onSelectEvent={event => this.toogleDetailsPopUp(event)}
            resizable
            onEventResize={this.state.role === 'ROLE_ADMIN' ? this.resizeEvent : null}
            onEventDrop={this.state.role === 'ROLE_ADMIN' ? this.moveEvent : null}
            onSelectSlot={this.state.role === 'ROLE_ADMIN' ? this.handleSelect : null}
            style={{ height: 800, width: '100%' }}
            eventPropGetter={
              (event, start, end, isSelected) => {
                let newStyle = {
                  backgroundColor: "#E86A22",
                  color: 'white',
                  borderRadius: "6px",
                  border: "none"
                };
                return {
                  className: "",
                  style: newStyle
                };
              }
            }
            components={{
              event: Event,

            }}


          />

        </Segment>


      </div>



    );
  }
}



EventCalendar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EventCalendar);