import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import RepHomePage from './Pages/RepHomepage';
import RepBuildingsPage from './Pages/BuildingListPage';
import RepTenantsPage from './Pages/TenantsListPage';
import RepContractsPage from './Pages/ContractsPage';
import RepAnalyticsPage from './Pages/AnalyticsPage';
import RepApartmentsPage from './Pages/ApartmentsListPage';
import RepReportPage from './Pages/ReportPage';
import RepSnowPage from './Pages/SnowSchedulePage';
import ProfilePage from './Pages/ProfilePage';
import CalendarPage from './Pages/CalendarPage';
import UsersPage from './Pages/UsersPage';
import RepParkingPage from './Pages/RepParkingPage'
import ErrorPage from './Pages/ErrorPage';
import InterventionPage from './Pages/InterventionPage';
import SmallBillPage from './Pages/SmallBillPage';
import PersonalBillPage from './Pages/PersonalBillPage';
import YearPlanPage from './Pages/YearPlanPage';
import YearlyFinanceReportPage from './Pages/YearlyFinanceReportPage';
import FormPage from './Pages/FormPage';
import WarrantyPage from './Pages/WarrantyPage';
import TenantHome from './Pages/TenantHome';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import WatermeterPage from './Pages/WatermeterPage';
import LawsPage from './Pages/LawsPage';
import SupplierPage from './Pages/SupplierPage';

var ReactRouter = require("react-router")
var BrowserHistory = ReactRouter.Browserhistory

const routing = (
  <Router history={BrowserHistory}> 
    <div>
      
      <Switch>
          <Route exact path="/" component={App}  />    
          <Route exact path="/repHome" component={RepHomePage}  />
          <Route exact path="/repBuildings" component={RepBuildingsPage}  />     
          <Route exact path="/repApartments" component={RepApartmentsPage}  />     
          <Route exact path="/repTenants" component={RepTenantsPage}  /> 
          <Route exact path="/repParking" component={RepParkingPage}  />   
          <Route exact path="/repContracts" component={RepContractsPage}  />    
          <Route exact path="/repAnalytics" component={RepAnalyticsPage}  />    
          <Route exact path="/repReport" component={RepReportPage}  /> 
          <Route exact path="/repSnow" component={RepSnowPage}  />    
          <Route exact path="/profile" component={ProfilePage}  />
          <Route exact path="/repCalendar" component={CalendarPage}  /> 
          <Route exact path="/repUsers" component={UsersPage}  />  
          <Route exact path="/repForms" component={FormPage}  />   
          <Route exact path="/repInterventions" component={InterventionPage}  />   
          <Route exact path="/repSmallBills" component={SmallBillPage}  />   
          <Route exact path="/repPersonalBills" component={PersonalBillPage}  />  
          <Route exact path="/repYearPlan" component={YearPlanPage}  />  
          <Route exact path="/repYearlyFinanceReport" component={YearlyFinanceReportPage}  />  
          <Route exact path="/repParking" component={RepParkingPage}  /> 
          <Route exact path="/repWarranty" component={WarrantyPage}  />  
          <Route exact path="/repWatermeter" component={WatermeterPage}  />  
          <Route exact path="/repLaw" component={LawsPage}  /> 
          <Route exact path="/repSupplier" component={SupplierPage}  />   

          <Route exact path="/tenHome" component={TenantHome}  />  
          <Route exact path="/error" component={ErrorPage}  />   

      </Switch>
    </div>
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
