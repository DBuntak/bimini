import { withStyles } from '@material-ui/core';
import React from 'react';
import cookie from 'react-cookies';
import { Segment, Step, Divider, Button, Icon } from 'semantic-ui-react'
import PropTypes from 'prop-types';
import axios from 'axios'
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';

const styles = theme => ({
  
})
  
class InitializerProgress extends React.Component{


    constructor(props) {
        super(props);
    
        this.state={
           step1Compl : false,
           step2Compl : false,
           step3Compl : false,
           step4Compl : false,

           step1Active : true,
           step2Active : false,
           step3Active : false,
           step4Actove : false,

           fileUploadState:"",
           uploadPercentage: 0,
           progressBarHidden : "none",
        };

        this.inputReference = React.createRef();
     
      };


      componentDidMount=()=>{
        let state = cookie.load("initializerState")
      }

      clickNext=()=>{
          if(this.state.step1Compl){
              if(this.state.step2Compl){
                  if(this.state.step3Compl){ 
                      

                  }
                  else{
                      this.setState({step3Compl: true})
                      this.setState({step3Active: false})
                     
              
                  }
              }
              else{
                  this.setState({step2Compl: true})
                  this.setState({step2Active: false})
                  this.setState({step3Active: true})
              }
          }
          else{
              this.setState({step1Compl: true})
              this.setState({step1Active: false})
              this.setState({step2Active: true})
              
          }
          
      }

      fileUploadAction = () =>{
        this.setState({progressBarHidden: ""})
        this.inputReference.current.click();
      }
  
      uploadFile=({target: {files} })=>{
        //console.log(files[0]);
        if(files[0].size > 2000000){
          this.setState({fileUploadState:"Datoteka je prevelika! (Maksimalna veličina 2MB)"})
          return
        }
        this.setState({fileUploadState:"Učitavam..."})
        let data = new FormData();
        data.append("file", files[0]);
        const options={
          onUploadProgress: (progressEvent) =>{
            const {loaded, total} = progressEvent;
            let percent = Math.floor((loaded*100)/total)
            console.log(loaded+" kb of "+total+" kb | "+percent+" %")
  
            if(percent < 100){
              this.setState({uploadPercentage: percent})
            }
          },
          headers:{
            'Authorization': 'Bearer ' + this.state.token,
          }
  
        }
        axios.post('purchase_contract/upload/apartment/1', data, options).then(res=>{
          
          if(res.status != 200){
            this.setState({fileUploadState:"Neuspješno prenesena datoteka!"})
            this.setState({uploadPercentage : 0});
           
          }else{
            this.setState({fileUploadState:"Upješno prenesena datoteka!"})
            this.setState({uploadPercentage : 100}, ()=>{
              setTimeout(()=>{
                this.setState({uploadPercentage: 0})
                this.setState({fileUploadState: ''})
                this.setState({progressBarHidden: "none"})
              }, 4000)
            })
          }
          
        })
  
      }


    render(){
        const { classes } = this.props;
       
        return ( 
            <div style={{marginTop:5+'vh'}} >
            <center>
                <Segment style={{width:100+'vh'}}>
                <Step.Group >
                    <Step active={this.state.step1Active} completed={this.state.step1Compl}>
                    <Icon name='info' style={{color:"orange"}} />
                    <Step.Content>
                        <Step.Title style={{color:"orange"}}>Informacije</Step.Title>
                    </Step.Content>
                    </Step>
                
                    <Step active={this.state.step2Active} completed={this.state.step2Compl}>
                    <Icon name='building' style={{color:"orange"}}/>
                    <Step.Content>
                        <Step.Title style={{color:"orange"}}>Zgrade</Step.Title>
                        <Step.Description>Podaci o zgradama, stanovima i stanarima</Step.Description>
                    </Step.Content>
                    </Step>
                
                    <Step active={this.state.step3Active} completed={this.state.step3Compl}>
                    <Icon name='check circle' style={{color:"orange"}}/>
                    <Step.Content>
                        <Step.Title style={{color:"orange"}}>Završetak</Step.Title>
                    </Step.Content>
                    </Step>
                </Step.Group>

                {this.state.step1Active ? 
                <p style={{marginTop:3+'vh'}}> U nastavku biti ćete vođeni kroz sustav prenošenja podataka iz datoteka u našu bazu podataka!</p> :
                <div></div>}

                {this.state.step2Active ? 
                <div style={{marginTop:3+'vh'}}>
                    <p>Ovdje prenesite datoteku koja sadržava podatke s popisom zgrada, popisom stanova i popisom stanara!</p> 
                    <div>
                    <input type="file" hidden ref={this.inputReference} onChange={this.uploadFile} />
                    <Button content='Prenesi' icon="upload" className="ui button" onClick={this.fileUploadAction} color="orange" size='huge'  style={{borderRadius: 30}} />
                    </div>

                    <br></br>
                    <div  style={{display: this.state.progressBarHidden}}>
                    <Progress color="success"   animated  value={this.state.uploadPercentage} />
                    <center>{this.state.fileUploadState}</center>
                    </div>
                    
                    
                
                </div>:
                <div></div>}

                {this.state.step3Active ? 
                <p style={{marginTop:3+'vh'}}>Uspješno ste završili s inicijalizacijom baze podataka!</p> :
                <div></div>}


                <Divider></Divider>
                <Button size="big" animated color="orange" style={{marginBottom:2+'vh', maxWidth:50+"%",marginLeft:50+'vh'}} onClick={this.clickNext}>
                    <Button.Content visible>Dalje</Button.Content>
                    <Button.Content hidden>
                        <Icon name='arrow right' />
                    </Button.Content>
                </Button>
                </Segment>
            </center>
            </div>
    
        );
    }
}

InitializerProgress.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles()(InitializerProgress);