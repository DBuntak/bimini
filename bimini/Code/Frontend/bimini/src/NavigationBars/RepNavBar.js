import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import BuildIcon from '@material-ui/icons/BuildTwoTone';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';
import DescriptionTwoToneIcon from '@material-ui/icons/DescriptionTwoTone';
import EuroTwoToneIcon from '@material-ui/icons/EuroTwoTone';
import EventNoteTwoToneIcon from '@material-ui/icons/EventNoteTwoTone';
import HomeWorkTwoToneIcon from '@material-ui/icons/HomeWorkTwoTone';
import PeopleAltTwoToneIcon from '@material-ui/icons/PeopleAltTwoTone';
import TimelineTwoToneIcon from '@material-ui/icons/TimelineTwoTone';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import LocalParkingIcon from '@material-ui/icons/LocalParking';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import NotificationImportantIcon from '@material-ui/icons/NotificationImportant';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import EventIcon from '@material-ui/icons/Event';
import PropTypes from 'prop-types';
import React from 'react';
import cookie from 'react-cookies';
import { withRouter } from "react-router-dom";
import { Dropdown } from 'semantic-ui-react';


const styles = {

}



let buttonStyle = {
  color: "white", borderColor: 'white', marginRight: 4 + 'vh'
};

let iconStyle = {
  color: 'white',
}


const optionsReports = [
  { key: 1, text: 'Grijanje', value: 1, icon: 'gripfire' },
  { key: 2, text: 'Dimnjak', value: 2, icon: 'gripfire' },
]



class RepNavBar extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      buildingId: '',
      buildingName: '',
      menuLocked: true,
      reportDropDown: false,

      infoDropdown: false,
      eventsDropdown: false,
      filesDropdown: false,
      financesDropdown: false,

    };



  }

  clickBuilding = () => {
    this.props.history.push({ pathname: '/repBuildings' });

  }

  clickApartment = () => {
    this.props.history.push({ pathname: '/repApartments' });
  }

  clickTenant = () => {
    this.props.history.push({ pathname: '/repTenants' });

  }

  clickContracts = () => {
    this.props.history.push({ pathname: '/repContracts' });

  }

  clickParking = () => {
    this.props.history.push({ pathname: '/repParking' });

  }

  clickCalendar = () => {
    this.props.history.push({ pathname: '/repCalendar' });

  }


  clickAnalytics = () => {
    this.props.history.push({ pathname: '/repAnalytics' });
  }


  clickSnow = () => {
    this.props.history.push({ pathname: '/repSnow' });
  }

  clickReport = () => {
    this.props.history.push({ pathname: '/repReport' });
  }

  
  clickSnow = () => {
    this.props.history.push({ pathname: '/repSnow' });
  }

  clickUser = () => {
    this.props.history.push({ pathname: '/repUsers' });
  }

  clickNoticeBoard = () => {
    this.props.history.push({ pathname: '/repHome' });
  }

  clickIntervention = () => {
    this.props.history.push({ pathname: '/repInterventions' });
  }

  clickForm = () => {
    this.props.history.push({ pathname: '/repForms' });
  }

  clickSmallBill = () => {
    this.props.history.push({ pathname: '/repSmallBills' });
  }

  clickPersonalBill = () => {
    this.props.history.push({ pathname: '/repPersonalBills' });
  }

  clickWarranty = () => {
    this.props.history.push({ pathname: '/repWarranty' });
  }

  clickWatermeter = () => {
    this.props.history.push({ pathname: '/repWatermeter' });
  }

  clickFinanceReport=()=>{
    this.props.history.push({ pathname: '/repYearlyFinanceReport' });
  }

  clickPlan=()=>{
    this.props.history.push({ pathname: '/repYearPlan' });
  }

  clickLaw=()=>{
    this.props.history.push({ pathname: '/repLaw' });
  }

  clickSupplier=()=>{
    this.props.history.push({ pathname: '/repSupplier' });
  }

  handleToggleInfoDropDown = () => {
    this.setState({ infoDropdown: !this.state.infoDropdown })
    this.setState({ eventsDropdown: false })
    this.setState({ filesDropdown: false })
    this.setState({ financesDropdown: false })

  }

  handleToggleEventsDropDown = () => {
    this.setState({ eventsDropdown: !this.state.eventsDropdown })
    this.setState({ infoDropdown: false })
    this.setState({ filesDropdown: false })
    this.setState({ financesDropdown: false })

  }
  handleToggleFilesDropDown = () => {
    this.setState({ filesDropdown: !this.state.filesDropdown })
    this.setState({ infoDropdown: false })
    this.setState({ eventsDropdown: false })
    this.setState({ financesDropdown: false })

  }
  handleToggleFinancesDropDown = () => {
    this.setState({ financesDropdown: !this.state.financesDropdown })
    this.setState({ infoDropdown: false })
    this.setState({ filesDropdown: false })
    this.setState({ eventsDropdown: false })

  }

  handleToggleReportDropDown = () => {
    this.setState({ reportDropDown: !this.state.reportDropDown })

  }

  componentDidMount = () => {
    var buildingId = cookie.load('buildingId');
    var buildingName = cookie.load("buildingName")
    if (buildingId === undefined) {
      this.setState({ menuLocked: true });
      buttonStyle = { color: "white", marginRight: 4 + 'vh', borderColor: 'white' }
      iconStyle = {}
    }
    else {
      this.setState({ menuLocked: false });
      this.setState({ buildingId: buildingId })
      this.setState({ buildingName: buildingName })
      buttonStyle = {
        color: "white", borderColor: 'white', marginRight: 3 + 'vh'
      };
      iconStyle = {
        color: 'white',
      }
    }

  }


  render() {


    return (
      <AppBar position="static" style={{ background: '#E86A22' }}>
        <Toolbar variant="dense">

          <div style={{ color: "white", marginRight: 4 + 'vh' }}><h4>{this.state.buildingName}</h4></div>

          <Button variant="outlined" size="large" onClick={this.clickBuilding} style={buttonStyle}>
            <BusinessTwoToneIcon style={{ color: "white" }}> </BusinessTwoToneIcon>
            &nbsp;Zgrade
            </Button>

          <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.clickNoticeBoard} style={buttonStyle}><NotificationImportantIcon style={iconStyle}>  </NotificationImportantIcon>&nbsp;&nbsp;&nbsp;Obavijesti</Button>

          <div ref={node => this.node = node} style={{ marginRight: 4 + 'vh', marginLeft: "auto" }}>
            <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.handleToggleInfoDropDown} style={{ color: "white", borderColor: 'white' }}><ContactMailIcon style={{ color: "white" }}>  </ContactMailIcon>
            &nbsp;Matični podaci
              <Dropdown open={this.state.infoDropdown} pointing="top" text="&nbsp;" direction="left" onClick={this.handleToggleInfoDropDown}  >
                <Dropdown.Menu hidden={!this.state.infoDropdown}>
                  <Dropdown.Item text='Stanovi' onClick={this.clickApartment} icon="home" />
                  <Dropdown.Item text='Stanari' onClick={this.clickTenant} icon="users" />
                  <Dropdown.Item text='Vodomjeri' onClick={this.clickWatermeter} icon="tint" />
                  <Dropdown.Item text='Parking' onClick={this.clickParking} icon="car" />
                </Dropdown.Menu>
              </Dropdown>
            </Button>
          </div>

          <div ref={node => this.node = node} style={{ marginRight: 4 + 'vh', marginLeft: "auto" }}>
            <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.handleToggleEventsDropDown} style={{ color: "white", borderColor: 'white' }}><EventIcon style={{ color: "white" }}>  </EventIcon>
            &nbsp;Događaji
              <Dropdown open={this.state.eventsDropdown} pointing="top" text="&nbsp;" direction="left" onClick={this.handleToggleEventsDropDown}  >
                <Dropdown.Menu hidden={!this.state.eventsDropdown}>
                  <Dropdown.Item text='Kalendar' onClick={this.clickCalendar} icon="calendar alternate outline" />
                  <Dropdown.Item text='Intervencije' onClick={this.clickIntervention} icon="exclamation triangle" />
                  <Dropdown.Item text='Pregledi' onClick={this.clickReport} icon="wrench" />
                  <Dropdown.Item text='Snijeg' onClick={this.clickSnow} icon="snowflake" />
                </Dropdown.Menu>
              </Dropdown>
            </Button>
          </div>

          <div ref={node => this.node = node} style={{ marginRight: 4 + 'vh', marginLeft: "auto" }}>
            <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.handleToggleFilesDropDown} style={{ color: "white", borderColor: 'white' }}><FileCopyIcon style={{ color: "white" }}>  </FileCopyIcon>
            &nbsp; Datoteke
              <Dropdown open={this.state.filesDropdown} pointing="top" text="&nbsp;" direction="left" onClick={this.handleToggleFilesDropDown}  >
                <Dropdown.Menu hidden={!this.state.filesDropdown}>
                  <Dropdown.Item text='Ugovori' onClick={this.clickContracts} icon="file" />
                  <Dropdown.Item text='Garancije' onClick={this.clickWarranty} icon="certificate" />
                  <Dropdown.Item text='Obrasci' onClick={this.clickForm} icon="file word outline" />
                </Dropdown.Menu>
              </Dropdown>
            </Button>
          </div>

          <div ref={node => this.node = node} style={{ marginRight: 4 + 'vh', marginLeft: "auto" }}>
            <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.handleToggleFinancesDropDown} style={{ color: "white", borderColor: 'white' }}><AccountBalanceWalletIcon style={{ color: "white" }}>  </AccountBalanceWalletIcon>
            &nbsp;Financije
              <Dropdown open={this.state.financesDropdown} pointing="top" text="&nbsp;" direction="left" onClick={this.handleToggleFinancesDropDown}  >
                <Dropdown.Menu hidden={!this.state.financesDropdown}>
                  <Dropdown.Item text='Sitni računi' onClick={this.clickSmallBill} icon="money bill alternate outline" />
                  <Dropdown.Item text='Osobni računi' onClick={this.clickPersonalBill} icon="money bill alternate" />
                  <Dropdown.Item text='Godišnji financijski izvještaj' onClick={this.clickFinanceReport} icon="file" />
                  <Dropdown.Item text='Godišnji plan' onClick={this.clickPlan} icon="clipboard list" />
                </Dropdown.Menu>
              </Dropdown>
            </Button>
          </div>



          <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.clickAnalytics} style={buttonStyle}><TimelineTwoToneIcon style={iconStyle}>  </TimelineTwoToneIcon>
        &nbsp;Analitika
          </Button>

          
          <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.clickLaw} style={buttonStyle}><TimelineTwoToneIcon style={iconStyle}>  </TimelineTwoToneIcon>
        &nbsp;Pravo
          </Button>

          <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.clickSupplier} style={buttonStyle}><TimelineTwoToneIcon style={iconStyle}>  </TimelineTwoToneIcon>
        &nbsp;Dobavljači
          </Button>


        </Toolbar>
      </AppBar>



    )

  }
}

RepNavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withRouter(withStyles(styles)(RepNavBar))

