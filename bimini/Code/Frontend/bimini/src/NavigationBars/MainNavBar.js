import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AccountCircleTwoToneIcon from '@material-ui/icons/AccountCircleTwoTone';
import PropTypes from 'prop-types';
import React from 'react';
import cookie from 'react-cookies';
import { withRouter } from "react-router-dom";
import { Dropdown } from 'semantic-ui-react';



const optionsProfile = [
    { key: 1, text: 'Početna', value: 1 ,  icon: 'home'},
    { key: 2, text: 'Uredi profil', value: 2 ,  icon: 'edit'},
    { key: 3, text: 'Odjavi se', value: 3 , icon:'log out'},
  ]

class RepNavBar extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            profileDropDown: false,
            userName: '',
            role: ''
        };
    
       
  
    }

    componentDidMount(){
        var userName = cookie.load('userName');
        this.setState({userName: userName})
        const role = cookie.load('role');
        this.setState({role: role});
     }

     handleToggleProfileDropDown=()=>{
        this.setState({profileDropDown:!this.state.profileDropDown})
  
      }

  
      clickProfile=()=>{
        this.props.history.push({pathname: '/profile'}); 
      }
      clickExit=()=>{
        cookie.remove('userName', { path: '/' });
          cookie.remove('buildingId', { path: '/' });
          cookie.remove('token', { path: '/' });
          cookie.remove('role', { path: '/' });
          this.props.history.push({pathname: '/'});   
      }

      clickUsers = () => {
        this.props.history.push({ pathname: '/repUsers' });
      }

      componentWillMount(){
        document.addEventListener('mousedown', this.handleClick, false)
      }
      componentWillUnmount(){
        document.addEventListener('mousedown', this.handleClick, false)
      }
  
      handleClick=(e)=>{
        
        if(this.node===null) return
        if(this.node.contains(e.target)){
          
          return;
        }
  
        this.handleClickOutside();
      }
      handleClickOutside=()=>{
        this.setState({profileDropDown: false})
      }
 

    render(){
    
       
        return (
            <div>
            <AppBar style={{ background: '#E86A22' }} position="static">
                <Toolbar >
                <Typography variant="h2" >
                    Project Bimini
                </Typography>

                <div ref={node=>this.node=node} style={{ marginRight:4+'vh', marginLeft: "auto"}}>
                <Button  variant="outlined" size="large" onClick={this.handleToggleProfileDropDown} style={{color:"white",borderColor: 'white'}}><AccountCircleTwoToneIcon style={{color:"white"}}>  </AccountCircleTwoToneIcon>
                &nbsp;&nbsp;&nbsp;{this.state.userName}
                <Dropdown  open={this.state.profileDropDown}  pointing="top" text="&nbsp;" direction="left" onClick={this.handleToggleProfileDropDown} onChange={this.handleChangeProfileDropDown} >
                  <Dropdown.Menu hidden={!this.state.profileDropDown}>
                    <Dropdown.Item text='Korisnici' onClick={this.clickUsers} icon="users"/>
                    <Dropdown.Item text='Uredi profil' onClick={this.clickProfile} icon="edit"/>
                    <Dropdown.Item text='Odjavi se' onClick={this.clickExit} icon="log out"/>
                  </Dropdown.Menu>
                </Dropdown>
                </Button>
                </div>

                </Toolbar>
            </AppBar>
           
            </div>
          )
       
    }
}

RepNavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withRouter(withStyles()(RepNavBar))

