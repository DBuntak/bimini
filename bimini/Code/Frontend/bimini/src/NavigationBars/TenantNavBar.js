import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import BuildIcon from '@material-ui/icons/BuildTwoTone';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';
import DescriptionTwoToneIcon from '@material-ui/icons/DescriptionTwoTone';
import EuroTwoToneIcon from '@material-ui/icons/EuroTwoTone';
import EventNoteTwoToneIcon from '@material-ui/icons/EventNoteTwoTone';
import HomeWorkTwoToneIcon from '@material-ui/icons/HomeWorkTwoTone';
import PeopleAltTwoToneIcon from '@material-ui/icons/PeopleAltTwoTone';
import TimelineTwoToneIcon from '@material-ui/icons/TimelineTwoTone';
import PropTypes from 'prop-types';
import React from 'react';
import cookie from 'react-cookies';
import { withRouter } from "react-router-dom";
import { Dropdown } from 'semantic-ui-react';


const styles = {
    
}



let buttonStyle = {
  color:"white",borderColor: 'white', borderRadius: 30, marginRight:4+'vh'
};

let iconStyle ={
  color:'white',
}


const optionsReports = [
  { key: 1, text: 'Grijanje', value: 1 ,  icon: 'gripfire'},
  { key: 2, text: 'Dimnjak', value: 2 ,  icon: 'gripfire'},
]



class TenantNavBar extends React.Component{
  

    constructor(props) {
        super(props);

        this.state = {
            buildingId: '',
            buildingName: '',
            menuLocked: true,
            reportDropDown: false,
            
        };
    
       
  
    }

    clickBuilding=()=>{
      this.props.history.push({pathname: '/repBuildings'}); 

    }

    clickApartment=()=>{
      this.props.history.push({pathname: '/repApartments'}); 
    }

    clickTenant=()=>{
      this.props.history.push({pathname: '/repTenants'}); 

    }

    clickContracts=()=>{
      this.props.history.push({pathname: '/repContracts'}); 

    }
    clickCalendar=()=>{
      this.props.history.push({pathname: '/repCalendar'}); 

    }

    clickFinance=()=>{
      this.props.history.push({pathname: '/repFinance'}); 
    }

    clickAnalytics=()=>{
      this.props.history.push({pathname: '/repAnalytics'}); 
    }

   
    clickSnow=()=>{
      this.props.history.push({pathname: '/repSnow'}); 
    }


    handleToggleReportDropDown=()=>{
      this.setState({reportDropDown:!this.state.reportDropDown})

    }

    
    clikcHeating=()=>{
      this.props.history.push({pathname: '/repHeating'});  
    }

    clickChimney=()=>{
      this.props.history.push({pathname: '/repChimney'}); 
    }
    clickSnow=()=>{
      this.props.history.push({pathname: '/repSnow'}); 
    }

    componentWillMount(){
      document.addEventListener('mousedown', this.handleClick, false)
    }
    componentWillUnmount(){
      document.addEventListener('mousedown', this.handleClick, false)
    }

    handleClick=(e)=>{
      
      if(this.node===null) return
      if(this.node.contains(e.target)){
        
        return;
      }

      this.handleClickOutside();
    }
    handleClickOutside=()=>{
      this.setState({reportDropDown: false})
    }
   

    componentDidMount=()=>{
      var buildingId = cookie.load('buildingId');
      var buildingName = cookie.load("buildingName")
      if(buildingId === undefined){
        this.setState({menuLocked:true});
        buttonStyle={borderRadius: 30, marginRight:4+'vh', borderColor: 'white'}
        iconStyle={}
      }
      else{
        this.setState({menuLocked:false});
        this.setState({buildingId: buildingId})
        this.setState({buildingName: buildingName})
        buttonStyle = {
          color:"white",borderColor: 'white', borderRadius: 30, marginRight:3+'vh'
        };
        iconStyle ={
          color:'white',
        }
      }
     
    }

    render(){
    
        
        return (
          <AppBar position="static" style={{ background: '#E86A22' }}>
          <Toolbar variant="dense">
            
            <div style={{color:"white", marginRight:4+'vh'}}><h4>{this.state.buildingName}</h4></div>

            <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.clickCalendar} style={buttonStyle}><EventNoteTwoToneIcon style={iconStyle}>  </EventNoteTwoToneIcon>&nbsp;&nbsp;&nbsp;Kalendar</Button>
            <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.clickFinance} style={buttonStyle}><EuroTwoToneIcon style={iconStyle}>  </EuroTwoToneIcon>&nbsp;&nbsp;&nbsp;Financije</Button>
           
            <Button hidden={this.state.menuLocked} variant="outlined" size="large" onClick={this.clickSnow} style={buttonStyle}><AcUnitIcon style={iconStyle}>  </AcUnitIcon>&nbsp;&nbsp;&nbsp;Snijeg</Button>

            <div ref={node=>this.node=node}>
            <Button  hidden={this.state.menuLocked} variant="outlined" size="large"  style={buttonStyle} onClick={this.handleToggleReportDropDown}>  <BuildIcon style={iconStyle}></BuildIcon>
                &nbsp;&nbsp;Pregledi
               
                <Dropdown pointing="top right"  open={this.state.reportDropDown} onClick={this.handleToggleReportDropDown} >
                  <Dropdown.Menu hidden={!this.state.reportDropDown}>
                    <Dropdown.Item text='Grijanje' onClick={this.clikcHeating} />
                    <Dropdown.Item text='Dimnjak' onClick={this.clickChimney} />
                  </Dropdown.Menu>
                </Dropdown>
                

            </Button>
            </div>

           
            
            
            
          </Toolbar>
        </AppBar>
      
            
           
          )
       
    }
}

TenantNavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withRouter(withStyles(styles)(TenantNavBar))

