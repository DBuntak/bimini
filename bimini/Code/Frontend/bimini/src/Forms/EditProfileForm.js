import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AccountCircleTwoToneIcon from '@material-ui/icons/AccountCircleTwoTone';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOffTwoTone';
import VisibilityIcon from '@material-ui/icons/VisibilityTwoTone';
import PropTypes from 'prop-types';
import React from 'react';
import cookie from 'react-cookies';
import { withRouter } from "react-router-dom";
import { Button, Divider, Icon, Segment, Grid, Input } from 'semantic-ui-react';



const styles = theme => ({

  label: {
    '&$focused': {
      color: '#E86A22'
    },
  },
  focused: {},
  outlinedInput: {
    '&$focused $notchedOutline': {
      border: '3px solid #E86A22'
    },
  },
  notchedOutline: {},
})

class EditProfileForm extends React.Component{
  

    constructor(props) {
        super(props);
    
        this.state = {
            token: '',
            role: '',
            userName: '',
            userId: '',
            newPassword: '',
            repeatPassword: '',
            changePasswordDisabled: true,
            changeUsernameDisabled: false,
  
        };
  
    }

  
    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
        if( name === "userName"){
          if(event.target.value === ""){
            this.setState({changeUsernameDisabled: true})
            return
          }
          else{
            this.setState({changeUsernameDisabled: false})
            return
          }
        }
        if(name === "newPassword" || name==="repeatPassword") {
          if(event.target.value === ""){
            this.setState({changePasswordDisabled: true})
            return
          }
        }
        if(name === "newPassword"){
          if(event.target.value !== this.state.repeatPassword){
            this.setState({changePasswordDisabled: true})
          }
          else{
            this.setState({changePasswordDisabled: false})
          }
        }
        else if(name === "repeatPassword"){
          if(event.target.value !== this.state.newPassword){
            this.setState({changePasswordDisabled: true})
          }
          else{
            this.setState({changePasswordDisabled: false})
          }

        }
      

        
      

    };


    handleClickShowPassword = () => {
        this.setState({
          showPassword:!this.state.showPassword
        })
        
      };
    
      handleMouseDownPassword = event => {
        event.preventDefault();
      };

      componentDidMount=()=>{
        var userName = cookie.load('userName');
        this.setState({userName: userName});
        const token = cookie.load('token');
        this.setState({token: token});
        const role = cookie.load('role');
        this.setState({role: role});

        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/user/username/'+userName, options)
        .then(data=>data.json())
        .then(user=>{
          this.setState({userName: user.username});
          this.setState({passWord: user.password});
          this.setState({userId: user.id})
        }
        )
      }

      changeProfile=()=>{
        const data={
          username: this.state.userName,
        };
        const options={
            method:'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        if(this.state.role === 'ROLE_ADMIN'){
          fetch('/user/'+this.state.userId,options)
        }
        if(this.state.role === 'ROLE_APARTMENT'){
          fetch('/user/apartment/'+this.state.userId,options)
        }

        cookie.remove('userName', { path: '/' });
        cookie.remove('buildingId', { path: '/' });
        this.props.history.push({pathname: '/'});  
      }

      changePassword=()=>{
        const data={
          password: this.state.newPassword,
        };
        const options={
            method:'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        if(this.state.role === 'ROLE_ADMIN'){
          fetch('/user/'+this.state.userId,options)
        }
        if(this.state.role === 'ROLE_APARTMENT'){
          fetch('/user/apartment/'+this.state.userId,options)
        }

        cookie.remove('userName', { path: '/' });
        cookie.remove('buildingId', { path: '/' });
        this.props.history.push({pathname: '/'});  
      }

    
      
    render(){
      const { classes } = this.props;
        
         return(
          <div>    
            
              <Segment style={{maxWidth:50+"%", left:40+'vh',  top:30+'vh'}} >
                    <Grid columns={2} relaxed='very' stackable>
                    <Grid.Column>

                          <h4>Unesite novo korisničko ime</h4>
                          <Divider fitted />
                          <br></br><br></br>
                          <Input placeholder='Korisničko ime/Email' value={this.state.userName} onChange={this.handleChange('userName')} />
                          <br></br><br></br><br></br><br></br><br></br>
                          
                          <Button animated color="orange" size="medium" onClick={this.changeProfile}  disabled={this.state.changeUsernameDisabled}>
                            <Button.Content visible>Promijeni korisničko ime</Button.Content>
                            <Button.Content hidden>
                              <Icon name='arrow right' />
                            </Button.Content>
                          </Button>

                    </Grid.Column>

                    <Grid.Column verticalAlign='middle'>

                        <h4>Unesite novu lozinku</h4>
                        <Divider fitted />
                        <br></br><br></br>
                        <Input placeholder='Nova lozinka' value={this.state.newPassword} onChange={this.handleChange('newPassword')} /><br></br><br></br>
                        <Input placeholder='Ponovi lozinku' value={this.state.repeatPassword} onChange={this.handleChange('repeatPassword')} />
                        <br></br><br></br>

                        <Button animated color="orange" size="medium" onClick={this.changePassword}  disabled={this.state.changePasswordDisabled}>
                            <Button.Content visible>Promijeni lozinku</Button.Content>
                            <Button.Content hidden>
                              <Icon name='arrow right' />
                            </Button.Content>
                          </Button>
                       

                     
                    
                   
              
                    </Grid.Column>
                    </Grid>

                    <Divider vertical>ILI</Divider>
    
            </Segment>
           
            
           
          </div>

         );
       
    }
}

EditProfileForm.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withRouter(withStyles(styles)(EditProfileForm))

