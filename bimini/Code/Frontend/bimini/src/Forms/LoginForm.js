import { green, red } from '@material-ui/core/colors';

import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import React from 'react';
import cookie from 'react-cookies';
import { withRouter } from "react-router-dom";
import { Segment } from 'semantic-ui-react'
import { Button, Checkbox, Form, Icon, Card } from 'semantic-ui-react'

import TextField from '@material-ui/core/TextField';
import VisibilityIcon from '@material-ui/icons/VisibilityTwoTone';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOffTwoTone';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircleTwoToneIcon from '@material-ui/icons/AccountCircleTwoTone';
import { Alert } from 'reactstrap';

const styles = theme => ({

  label: {
    '&$focused': {
      color: '#E86A22'
    },
  },
  focused: {},
  outlinedInput: {
    '&$focused $notchedOutline': {
      border: '3px solid #E86A22'
    },
  },
  notchedOutline: {},
})

class LoginForm extends React.Component{
  

    constructor(props) {
        super(props);
    
        this.state = {
            usernameOrEmail:'',
            passWord:'',
            sumbitDisabled:true,
            showPassword:false,
            popupVisible: false,
  
        };
  
    }

  
    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
        
        if(this.state.usernameOrEmail.length <= 0 ||
        this.state.passWord.length <= 0){
          this.setState({ sumbitDisabled: true});
        }
        else{
          this.setState({ sumbitDisabled: false});
        }

    };

    isSubmitDisabled = () => {
      
      
    };
    popUpDismiss = () => this.setState({popupVisible:false});

    handleClickShowPassword = () => {
        this.setState({
          showPassword:!this.state.showPassword
        })
        
      };
    
      handleMouseDownPassword = event => {
        event.preventDefault();
      };

    login=(e)=>{
        e.preventDefault();
        const data={
          username:this.state.usernameOrEmail,
          password:this.state.passWord     
      };
      const options={
          method:'POST',
          headers:{
              'Content-Type':'application/json'
          },
          body: JSON.stringify(data)
      };
      return fetch('/authenticate',options)
      .then(response => {
              response.json().then(
                  body =>{
                      if(response.status === 200){  
                        this.setState({popupVisible:false});
                        cookie.save('userName', this.state.usernameOrEmail, { path: '/' });
                        cookie.save('token', body.jwt);
                        cookie.save('role', body.role);
                        if(body.role === 'ROLE_ADMIN'){
                          this.props.history.push({pathname: '/repHome'});
                        }
                        else if(body.role === 'ROLE_APARTMENT'){
                          const options={
                            method:'GET',
                            headers:{
                                'Content-Type':'application/json',
                                'Authorization': 'Bearer ' + body.jwt,
                            },
                          };
                          return fetch('/user/'+this.state.usernameOrEmail+'/building',options)
                          .then(response => {
                            response.json().then(
                                body =>{
                                  cookie.save('buildingId', body.buildingId);
                                  cookie.save('buildingName', body.buildingName);
                                  this.props.history.push({pathname: '/tenHome'});
                                });
                              }
                            );
                        }

                      }
                      else{
                        this.setState({popupVisible:true});
                        this.setState({usernameOrEmail:''});
                        this.setState({passWord:''});
                      }
                  }  
              );  
      })
      
      };
      
    render(){
      const { classes } = this.props;
        
         return(
          <div>
            
              <Segment style={{maxWidth:35+"%",  minWidth:35+"%",  top:30+'vh', left:60+'vh'}} >

                    <Alert color="danger" isOpen={this.state.popupVisible} toggle={this.popUpDismiss}>
                      <h5>Neispravno uneseni podaci za prijavu!</h5>
                    </Alert>
             
                    <div style={{marginBottom:2+'vh', marginTop:1+'vh'}}>
                    <h1 style={{color:'#E86A22'}}><b>Prijava</b></h1>
                    </div>
                   
                    <div style={{marginBottom:2+'vh', marginTop:1+'vh'}}>
                       
                    <TextField id="outlined-basic" label="Korisničko ime/Email" variant="outlined" 
                        value={this.state.usernameOrEmail}
                        onChange={this.handleChange('usernameOrEmail')}
                        style = {{width: 50+'%'}}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment >
                              <IconButton   disabled={true}>
                                <AccountCircleTwoToneIcon></AccountCircleTwoToneIcon>
                              </IconButton> 
                            </InputAdornment>
                          ),
                        }}
                        InputLabelProps={{
                          classes: {
                            root: classes.label,
                            focused: classes.focused,
                          },
                        }}
                        InputProps={{
                          classes: {
                            root: classes.outlinedInput,
                            focused: classes.focused,
                            notchedOutline: classes.notchedOutline,
                          },
                        }}
                        
                        />
                    </div>

                    <div  style={{marginBottom:2+'vh', marginTop:2+'vh'}}>
                       
                        <TextField id="outlined-basic" label="Lozinka" variant="outlined" 
                        value={this.state.passWord}
                        onChange={this.handleChange('passWord')}
                        type={this.state.showPassword ? 'text' : 'password'}
                        style = {{width: 50+'%'}}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment >
                              <IconButton
                                
                                aria-label="toggle password visibility"
                                onClick={this.handleClickShowPassword}
                                onMouseDown={this.handleMouseDownPassword}
                              >
                                {this.state.showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        InputLabelProps={{
                          classes: {
                            root: classes.label,
                            focused: classes.focused,
                          },
                        }}
                        InputProps={{
                          classes: {
                            root: classes.outlinedInput,
                            focused: classes.focused,
                            notchedOutline: classes.notchedOutline,
                          },
                        }}
                        />
                    </div>
        
                    <center>
                    <Button animated color="orange" size="big" onClick={this.login} style={{marginBottom:2+'vh', maxWidth:30+"%",marginLeft:25+'vh'}} disabled={this.state.sumbitDisabled}>
                        <Button.Content visible>Prijavi se</Button.Content>
                        <Button.Content hidden>
                            <Icon name='arrow right' />
                        </Button.Content>
                    </Button>
                    </center>
                    <p></p><p></p><p></p>
                    
                    
                
            </Segment>
            
           
          </div>

         );
       
    }
}

LoginForm.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withRouter(withStyles(styles)(LoginForm))

