import React from 'react';
import './Footer.css';

function Footer() {
  return (
     <div className="footer">    
            <p style={{textAlign:'center', color:"white"}} >
            <font >
              © 2020 – 2021 ProjectBimini.hr – Sva prava pridržana.<br></br>
              Komadina & Co. d.o.o.<br></br>
              Marije Jambrišak 2A, Zagreb<br></br>
              <a href={"mailto:Lingue.mundi19@gmail.com"} style={{textAlign:'center', color:"white"}} >marin.komadina@pan-net.eu</a>           
            </font>
            </p>      
      </div>
  );
}

export default Footer;
