import React from 'react';
import './App.css';
import LoginForm from './Forms/LoginForm.js'
import 'semantic-ui-css/semantic.min.css'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';
import LoginPage from './Pages/LoginPage';
import './Footer.css';
import Footer from './Footer.js'

function App() {
  return (
    <div className="App">
      <AppBar style={{ background: '#E86A22' }} position="static">
        <Toolbar >
          <Typography variant="h2" >
            Project Bimini
          </Typography>
        </Toolbar>
      </AppBar>
      <LoginPage></LoginPage>
      <Footer></Footer>

    </div>
  );
}

export default App;
