import { withStyles } from '@material-ui/core';
import axios from 'axios';
import React from 'react';
import cookie from 'react-cookies';
import { Progress } from 'reactstrap';
import { Button, Divider, Grid, Header, Icon, Segment } from 'semantic-ui-react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import ContractListTable from '../Tables/ContractListTable';



class ContractsPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      buildingId: '',
      buildingName: '',
      token: '',

    };

  };

  componentDidMount() {
    var buildingId = parseInt(cookie.load('buildingId'));
    var buildingName = cookie.load("buildingName")
    this.setState({ buildingId: buildingId })
    this.setState({ buildingName: buildingName })
    const token = cookie.load('token');
    this.setState({ token: token });
  };



  render() {

    return (
      <div>

        <MainNavBar></MainNavBar>
        <RepNavBar></RepNavBar>
        <br></br><br></br>


        <Divider horizontal>
          <Header as='h3'>
            <Icon name='users' />
                Međuvlasnički ugovori
            </Header>
        </Divider>
        <Segment placeholder>
          <ContractListTable fileType={"međuvlasnički"}></ContractListTable>
        </Segment>
        <br></br>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='cogs' />
                Ugovori o upravljanju
            </Header>
        </Divider>
        <Segment placeholder>
          <ContractListTable fileType={"upravljanje"}></ContractListTable>
        </Segment>
        <br></br>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='shield' />
                Ugovori o osiguranju
            </Header>
        </Divider>
        <Segment placeholder>
          <ContractListTable fileType={"osiguranje"}></ContractListTable>
        </Segment>
        <br></br>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='leaf' />
                Ugovori o čišćenju
            </Header>
        </Divider>
        <Segment placeholder>
          <ContractListTable fileType={"čišćenje"}></ContractListTable>
        </Segment>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='wrench' />
                Ugovori za održavanje
            </Header>
        </Divider>
        <Segment placeholder>
          <ContractListTable fileType={"održavanje"}></ContractListTable>
        </Segment>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='fire extinguisher' />
                Ugovori za protupožarne aparate
            </Header>
        </Divider>
        <Segment placeholder>
          <ContractListTable fileType={"protupožarni_aparati"}></ContractListTable>
        </Segment>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='list' />
              Ostali ugovori
            </Header>
        </Divider>
        <Segment placeholder>
          <ContractListTable fileType={"ostalo"}></ContractListTable>
        </Segment>
      </div>

    );
  }
}
export default withStyles()(ContractsPage);