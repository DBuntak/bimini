import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import SupplierTable from '../Tables/SupplierListTable';

      
  
class SupplierPage extends React.Component{
    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            <RepNavBar></RepNavBar>
            <SupplierTable></SupplierTable>

        </div>
    
        );
    }
}
export default withStyles()(SupplierPage);