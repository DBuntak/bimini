import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import DeviceTable from '../Tables/WarrantyDeviceListTable';
import WorkTable from '../Tables/WarrantyWorkListTable';
import TenantNavBar from '../NavigationBars/TenantNavBar';
import cookie from 'react-cookies';
import { Divider, Header, Icon, Segment, Button, Loader, Dimmer } from 'semantic-ui-react';

class WarrantyPage extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
        };
    };


    componentDidMount() {
    };

    render() {
        return (
            <div>
                <MainNavBar></MainNavBar>
                <RepNavBar></RepNavBar>
                <br></br>

                <Divider horizontal>
                    <Header as='h3'>
                        <Icon name='cogs' />
                    Aparati
                </Header>
                </Divider>
                <DeviceTable></DeviceTable>
                <br></br>
                <Divider horizontal>
                    <Header as='h3'>
                        <Icon name='dolly' />
                    Izvedeni radovi
                </Header>
                </Divider>
                <WorkTable></WorkTable>

            </div>

        );
    }
}
export default withStyles()(WarrantyPage);