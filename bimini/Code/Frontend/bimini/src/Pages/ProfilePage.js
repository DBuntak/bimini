import { withStyles } from '@material-ui/core';
import React from 'react';
import EditProfileForm from '../Forms/EditProfileForm';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import TenantNavBar from '../NavigationBars/TenantNavBar';
import cookie from 'react-cookies';
      
  
class ProfilePage extends React.Component{
    navBar = () => {
        const role = cookie.load('role');
        if(role === 'ROLE_ADMIN'){
            return <RepNavBar></RepNavBar>
        }
        else if(role === 'ROLE_APARTMENT'){
            return <TenantNavBar></TenantNavBar>
        }
    }
    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            {this.navBar()}
            <EditProfileForm></EditProfileForm>
           
        </div>
    
        );
    }
}
export default withStyles()(ProfilePage);