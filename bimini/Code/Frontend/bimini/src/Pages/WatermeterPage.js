import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import WaterApartmentListTable from '../Tables/WaterApartmentListTable';



class WatermeterPage extends React.Component {

    render() {
        return (
            <div>

                <MainNavBar></MainNavBar>
                <RepNavBar></RepNavBar>
                
                <WaterApartmentListTable></WaterApartmentListTable>
            </div>

        );
    }
}
export default withStyles()(WatermeterPage);