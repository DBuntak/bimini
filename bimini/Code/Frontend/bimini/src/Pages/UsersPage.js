import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import UsersListTable from '../Tables/UsersListTable';
      
  
class UsersPage extends React.Component{
    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            <RepNavBar></RepNavBar>
           <UsersListTable></UsersListTable>
        </div>
    
        );
    }
}
export default withStyles()(UsersPage);