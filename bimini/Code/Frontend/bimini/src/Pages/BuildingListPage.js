import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import BuildingsListTable from '../Tables/BuildingsListTable';
      
  
class BuildingListPage extends React.Component{
    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            <RepNavBar></RepNavBar>
           <BuildingsListTable></BuildingsListTable>
        </div>
    
        );
    }
}
export default withStyles()(BuildingListPage);