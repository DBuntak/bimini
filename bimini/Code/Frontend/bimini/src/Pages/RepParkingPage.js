import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import ParkingTable from '../Tables/ParkingListTable';

      
  
class RepParkingPage extends React.Component{
    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            <RepNavBar></RepNavBar>
            <ParkingTable></ParkingTable>

        </div>
    
        );
    }
}
export default withStyles()(RepParkingPage);