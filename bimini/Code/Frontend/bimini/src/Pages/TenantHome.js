import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import TenantNavBar from '../NavigationBars/TenantNavBar';
import InitializerProgress from '../Initializer/InitializerProgress'
import {  Divider, Header, Icon } from 'semantic-ui-react'



class TenantHome extends React.Component{    

    render(){
        const { classes } = this.props;
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            <TenantNavBar></TenantNavBar>
            
            
        </div>
    
        );
    }
}
export default withStyles()(TenantHome);