import { withStyles } from '@material-ui/core';
import React from 'react';
import { Divider, Header, Icon, Segment, Button, Loader, Dimmer } from 'semantic-ui-react';
import ElectricityLineGraph from '../Graphs/ElectricityLineGraph';
import GasLineGraph from '../Graphs/GasLineGraph';
import WaterLineGraph from '../Graphs/WaterLineGraph';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import WaterApartmentListTable from '../Tables/WaterApartmentListTable';



class AnalyticsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            WaterButton: false,
            GasButton: false,
            ElecButton: false,

            waterLoading: true,
            gasLoading: true,
            elecLoading: true,
        };


        this.childWater = React.createRef();
        this.childGas = React.createRef();
        this.childElec = React.createRef();
    }

    downloadWater = () => {
        this.childWater.current.download();
    };

    trashWater = () => {
        this.childWater.current.trash();
        this.setState({ WaterButton: false })
    };

    downloadGas = () => {
        this.childGas.current.download();
    };

    trashGas = () => {
        this.childGas.current.trash();
        this.setState({ GasButton: false })
    };

    downloadElec = () => {
        this.childElec.current.download();
    };

    trashElec = () => {
        this.childElec.current.trash();
        this.setState({ ElecButton: false })
    };

    enableButtons = (type) => {
        if (type === "WATER") {
            this.setState({ WaterButton: true })
        }
        else if (type === "GAS") {
            this.setState({ GasButton: true })
        }
        else if (type === "ELECTRICITY") {
            this.setState({ ElecButton: true })
        }
    }

    disableButtons = (type) => {
        if (type === "WATER") {
            this.setState({ WaterButton: false })
        }
        else if (type === "GAS") {
            this.setState({ GasButton: false })
        }
        else if (type === "ELECTRICITY") {
            this.setState({ ElecButton: false })
        }
    }

    enableLoading = (type) => {
        if (type === "WATER") {
            this.setState({ waterLoading: true })
        }
        else if (type === "GAS") {
            this.setState({ gasLoading: true })
        }
        else if (type === "ELECTRICITY") {
            this.setState({ elecLoading: true })
        }
    }

    disableLoading = (type) => {
        if (type === "WATER") {
            this.setState({ waterLoading: false })
        }
        else if (type === "GAS") {
            this.setState({ gasLoading: false })
        }
        else if (type === "ELECTRICITY") {
            this.setState({ elecLoading: false })
        }
    }




    render() {




        return (
            <div>

                <MainNavBar></MainNavBar>
                <RepNavBar></RepNavBar>
                <br></br><br></br>

                <Divider horizontal>
                    <Header as='h3'>
                        <Icon name='tint' style={{ color: "#00A6FF" }} />
                    Voda
                </Header>
                </Divider>
                {this.state.WaterButton ?
                    <center>
                        <Button circular animated='vertical' primary size="medium" style={{ width: 10 + 'vh' }} onClick={this.downloadWater}>
                            <Button.Content hidden>Preuzmi</Button.Content>
                            <Button.Content visible>
                                <Icon name='download' />
                            </Button.Content>
                        </Button>
                        <Button circular animated='vertical' primary size="medium" style={{ width: 12 + 'vh' }} onClick={this.trashWater}>
                            <Button.Content hidden>Izbriši podatke</Button.Content>
                            <Button.Content visible>
                                <Icon name='trash alternate' />
                            </Button.Content>
                        </Button>
                    </center>
                    : <div></div>
                }

                <Segment placeholder style={{ height: 50 + 'vh' }}>
                    <Dimmer active={this.state.waterLoading} inverted>
                        <Loader active={this.state.waterLoading} inverted size="massive" >Učitavam</Loader>
                    </Dimmer>
                    <WaterLineGraph loading={this.state.waterLoading} ref={this.childWater} enableButtons={this.enableButtons} disableButtons={this.disableButtons} enableLoading={this.enableLoading} disableLoading={this.disableLoading}></WaterLineGraph>

                </Segment>
                <br></br>

                <Divider horizontal>
                    <Header as='h3'>
                        <Icon name='gripfire' style={{ color: "red" }} />
                    Plin
                </Header>
                </Divider>
                {this.state.GasButton ?
                    <center>
                        <Button circular animated='vertical' color="red" size="medium" style={{ width: 10 + 'vh' }} onClick={this.downloadGas}>
                            <Button.Content hidden>Preuzmi</Button.Content>
                            <Button.Content visible>
                                <Icon name='download' />
                            </Button.Content>
                        </Button>
                        <Button circular animated='vertical' color="red" size="medium" style={{ width: 12 + 'vh' }} onClick={this.trashGas}>
                            <Button.Content hidden>Izbriši podatke</Button.Content>
                            <Button.Content visible>
                                <Icon name='trash alternate' />
                            </Button.Content>
                        </Button>
                    </center>
                    : <div></div>
                }
                <Segment placeholder style={{ height: 50 + 'vh' }}>
                    <Dimmer active={this.state.gasLoading} inverted>
                        <Loader active={this.state.gasLoading} inverted size="massive" >Učitavam</Loader>
                    </Dimmer>
                    <GasLineGraph loading={this.state.gasLoading} ref={this.childGas} enableButtons={this.enableButtons} disableButtons={this.disableButtons} enableLoading={this.enableLoading} disableLoading={this.disableLoading}></GasLineGraph>

                </Segment>
                <br></br>


                <Divider horizontal>
                    <Header as='h3'>
                        <Icon name='lightbulb' style={{ color: "#FFE000" }} />
                    Struja
                </Header>
                </Divider>
                {this.state.ElecButton ?
                    <center>
                        <Button circular animated='vertical' color="yellow" size="medium" style={{ width: 10 + 'vh' }} onClick={this.downloadElec}>
                            <Button.Content hidden>Preuzmi</Button.Content>
                            <Button.Content visible>
                                <Icon name='download' />
                            </Button.Content>
                        </Button>

                        <Button circular animated='vertical' color="yellow" size="medium" style={{ width: 12 + 'vh' }} onClick={this.trashElec}>
                            <Button.Content hidden>Izbriši podatke</Button.Content>
                            <Button.Content visible>
                                <Icon name='trash alternate' />
                            </Button.Content>
                        </Button>
                    </center>
                    : <div></div>
                }
                <Segment placeholder style={{ height: 50 + 'vh' }}>
                    <Dimmer active={this.state.elecLoading} inverted>
                        <Loader active={this.state.elecLoading} inverted size="massive" >Učitavam</Loader>
                    </Dimmer>
                    <ElectricityLineGraph loading={this.state.elecLoading} ref={this.childElec} enableButtons={this.enableButtons} disableButtons={this.disableButtons} enableLoading={this.enableLoading} disableLoading={this.disableLoading}></ElectricityLineGraph>

                </Segment>

            </div>

        );
    }
}
export default withStyles()(AnalyticsPage);