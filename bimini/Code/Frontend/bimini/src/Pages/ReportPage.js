import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import ReportTable from '../Tables/ReportListTable';
import TenantNavBar from '../NavigationBars/TenantNavBar';
import cookie from 'react-cookies';
import { Button, Dimmer, Icon, Loader, Segment, Input, Dropdown, Item, Divider, Checkbox, TextArea, Form, Select } from 'semantic-ui-react';


class ReportPage extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            token: '',
            buildingId: '',
            typeOptions: [],
            selectedType: '',
        };
    };


    componentDidMount() {

        var buildingId = parseInt(cookie.load("buildingId"));
        this.setState({ buildingId: buildingId });
        const token = cookie.load('token');
        this.setState({ token: token });

        const options = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
            },
        };
        fetch('/event-type', options)
            .then(data => data.json())
            .then(eventTypes => {
                for (var i = 0; i < eventTypes.length; ++i) {
                    this.state.typeOptions.push({ key: eventTypes[i].id, value: eventTypes[i].id, text: eventTypes[i].name })
                }

            })
    };

    handleSelectEventType = (e, { value }) => {
        this.setState({ selectedType: value })
    }


    navBar = () => {
        const role = cookie.load('role');
        if (role === 'ROLE_ADMIN') {
            return <RepNavBar></RepNavBar>
        }
        else if (role === 'ROLE_APARTMENT') {
            return <TenantNavBar></TenantNavBar>
        }
    }
    render() {
        return (
            <div>
                <MainNavBar></MainNavBar>
                {this.navBar()}
                <br></br>
                <h5 style={{ marginLeft: 5 + 'vh' }}>Odaberite tip događaja:</h5>
                <div style={{ marginLeft: 5 + 'vh' }}>
                    <Select placeholder='Tip događaja' options={this.state.typeOptions} value={this.state.selectedType} onChange={this.handleSelectEventType} /><br></br><br></br>
                </div>
                {this.state.selectedType !== "" ?
                <ReportTable eventType={this.state.selectedType}></ReportTable>
                :
                <div></div>
                }
                

            </div>

        );
    }
}
export default withStyles()(ReportPage);