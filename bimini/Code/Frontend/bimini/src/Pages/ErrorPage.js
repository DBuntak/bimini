import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import BuildingsListTable from '../Tables/BuildingsListTable';
      
  
class ErrorPage extends React.Component{
    render(){
        return ( 
        <div>
            Nešto je pošlo po zlu ili nemate dovoljnu razinu prava za pristup stranici.
        </div>
    
        );
    }
}
export default withStyles()(ErrorPage);