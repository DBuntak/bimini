import { withStyles } from '@material-ui/core';
import React from 'react';
import EventCalendar from '../Calendar/EventCalendar';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import TenantNavBar from '../NavigationBars/TenantNavBar';
import cookie from 'react-cookies';
      
  
class CalendarPage extends React.Component{
    navBar = () => {
        const role = cookie.load('role');
        if(role === 'ROLE_ADMIN'){
            return <RepNavBar></RepNavBar>
        }
        else if(role === 'ROLE_APARTMENT'){
            return <TenantNavBar></TenantNavBar>
        }
    }

    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            {this.navBar()}
            <EventCalendar></EventCalendar>
           
        </div>
    
        );
    }
}
export default withStyles()(CalendarPage);