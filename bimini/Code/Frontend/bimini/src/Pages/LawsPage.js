import { withStyles } from '@material-ui/core';
import axios from 'axios';
import React from 'react';
import cookie from 'react-cookies';
import { Progress } from 'reactstrap';
import { Button, Divider, Grid, Header, Icon, Segment } from 'semantic-ui-react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import LawListTable from '../Tables/LawListTable';



class LawsPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      buildingId: '',
      buildingName: '',
      token: '',

    };

  };

  componentDidMount() {
    var buildingId = parseInt(cookie.load('buildingId'));
    var buildingName = cookie.load("buildingName")
    this.setState({ buildingId: buildingId })
    this.setState({ buildingName: buildingName })
    const token = cookie.load('token');
    this.setState({ token: token });
  };



  render() {

    return (
      <div>

        <MainNavBar></MainNavBar>
        <RepNavBar></RepNavBar>
        <br></br><br></br>


        <Divider horizontal>
          <Header as='h3'>
            <Icon name='users' />
                Utuženja
            </Header>
        </Divider>
        <Segment placeholder>
          <LawListTable fileType={"utuzenja"}></LawListTable>
        </Segment>
        <br></br>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='cogs' />
                Sudski postupci
            </Header>
        </Divider>
        <Segment placeholder>
          <LawListTable fileType={"sudski_postupci"}></LawListTable>
        </Segment>
        <br></br>

        <Divider horizontal>
          <Header as='h3'>
            <Icon name='shield' />
                Presude
            </Header>
        </Divider>
        <Segment placeholder>
          <LawListTable fileType={"presude"}></LawListTable>
        </Segment>
      </div>

    );
  }
}
export default withStyles()(LawsPage);