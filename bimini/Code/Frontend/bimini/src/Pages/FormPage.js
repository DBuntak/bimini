import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import FormList from '../Tables/FormList';
import TenantNavBar from '../NavigationBars/TenantNavBar';
import cookie from 'react-cookies';
import { Button, Dimmer, Icon, Loader, Segment, Input, Dropdown, Item, Divider, Checkbox, TextArea, Form, Select } from 'semantic-ui-react';


class FormPage extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
        };
    };


    componentDidMount() {
    };

    render() {
        return (
            <div>
                <MainNavBar></MainNavBar>
                <RepNavBar></RepNavBar>
                <FormList></FormList>
            </div>

        );
    }
}
export default withStyles()(FormPage);