import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import TenantNavBar from '../NavigationBars/TenantNavBar';
import SnowScheduleTable from '../Tables/SnowScheduleTable';
import cookie from 'react-cookies';
      
  
class SnowSchedulePage extends React.Component{
    navBar = () => {
        const role = cookie.load('role');
        if(role === 'ROLE_ADMIN'){
            return <RepNavBar></RepNavBar>
        }
        else if(role === 'ROLE_APARTMENT'){
            return <TenantNavBar></TenantNavBar>
        }
    }
    render(){
        return ( 
        <div>

            <MainNavBar></MainNavBar>
            {this.navBar()}
            <SnowScheduleTable></SnowScheduleTable>
           
        </div>
    
        );
    }
}
export default withStyles()(SnowSchedulePage);