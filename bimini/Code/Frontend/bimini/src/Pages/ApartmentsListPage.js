import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import ApartmentsListTable from '../Tables/ApartmentsListTable';
      
  
class ApartmentsListPage extends React.Component{
    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            <RepNavBar></RepNavBar>
            <ApartmentsListTable></ApartmentsListTable>
           
        </div>
    
        );
    }
}
export default withStyles()(ApartmentsListPage);