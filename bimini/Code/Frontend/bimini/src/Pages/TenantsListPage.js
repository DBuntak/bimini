import { withStyles } from '@material-ui/core';
import React from 'react';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import TenantListTable from '../Tables/TenantListTable';
      
  
class TenantsListPage extends React.Component{
    render(){
        return ( 
        <div>
            <MainNavBar></MainNavBar>
            <RepNavBar></RepNavBar>
           <TenantListTable></TenantListTable>
        </div>
    
        );
    }
}
export default withStyles()(TenantsListPage);