import { withStyles } from '@material-ui/core';
import MainNavBar from '../NavigationBars/MainNavBar';
import RepNavBar from '../NavigationBars/RepNavBar';
import NoticeBoard from '../NoticeBoard/NoticeBoard';
import React, { useState, useEffect } from "react";
import cookie from 'react-cookies';
import background1 from "../Assets/Board1.jpg";


class RepHomepagePage extends React.Component{ 

    constructor(props) {
        super(props);

        this.state = {
            buildingSelected: false
        };

    };

    
    componentDidMount() {

   
        const buildingId = cookie.load('buildingId');
        this.setState({ buildingId: buildingId });
    
        if(buildingId === undefined){
            this.setState({buildingSelected: false})
        }
        else{
            this.setState({buildingSelected: true})
        }
        
      };


    render(){
        const { classes } = this.props;
        return ( 
        <div >
            <MainNavBar></MainNavBar>
            <RepNavBar></RepNavBar>
            {this.state.buildingSelected 
            ? <NoticeBoard ></NoticeBoard>
            :<div style={{ marginTop:1+'vh',height: 100 + 'vh' }}>
            <h1 style={{marginLeft: 3+ 'vh',color:'white', position: 'absolute'}}><b>Oglasna ploča</b></h1>
            <h2 style={{marginLeft: 3+ 'vh',marginTop: 5+ 'vh',color:'white', position: 'absolute'}}>Nije odabrana zgrada</h2>
            <img src={background1} />
             </div>}
            
        </div>
    
        );
    }
}
export default withStyles()(RepHomepagePage);