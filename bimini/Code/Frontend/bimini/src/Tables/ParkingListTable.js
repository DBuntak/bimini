import { green, red } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Icon, Loader, Segment, Input, Dropdown, Item, Divider, Checkbox, TextArea, Form, Select } from 'semantic-ui-react';
import { sortData, parseTenants, parseDate,addOneHour } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";


registerLocale("hr", hr);



const styles = theme => ({
  success: {
    backgroundColor: green[600],
  },
  fail: {
    backgroundColor: red[600],
  },
});

const typeOptions = [
  { key: 'unutarnje', value: 'unutarnje', text: 'Unutarnje' },
  { key: 'vanjsko', value: 'vanjsko', text: 'Vanjsko' },
]

const rentOptions = [
  { key: 'mjesečni', value: 'mjesečni', text: 'Mjesečni' },
  { key: 'godišnji', value: 'godišnji', text: 'Godišnji' },
]

const columns = () => [
  { title: 'Identifikator', field: 'id', editable: "never", hidden: true },
  { title: 'Broj mjesta', field: 'number' },
  { title: 'Tip', field: 'type' },
  { title: 'Vlasnik', field: 'ownerDetails' },
  { title: 'Najamnik', field: 'rentedDetails' },
  { title: 'Početak najma', field: 'rentStart' },
  { title: 'Završetak najma', field: 'rentEnd' },
  { title: 'Vrsta najma', field: 'rentType' },
]

class ParkingListTable extends Component {


  constructor(props) {
    super(props);

    this.state = {
      token: '',
      buildingId: '',
      parkings: [],
      tenants: [],
      parkingId: '',
      deletePopup: false,
      newParkingNumber: '',
      newParkingType: '',
      newParkingOwner: '',
      newParkingRented: '',
      newParkingRentedChecked: false,
      newParkingRentedTenantChecked: true,
      newParkingRentedName: '',
      newParkingRentedSurname: '',
      newParkingRentedPhone: '',
      newParkingRentedEmail: '',
      newRentType: '',
      newStartRent: '',
      newEndRent: '',
      newTenantChecked: false,
      newParkingOwnerName: '',
      newParkingOwnerSurname: '',
      newParkingOwnerPhone: '',
      newParkingOwnerEmail: '',
      ownerOptions: [],
      editParkingNumber: '',
      editParkingType: '',
      editParkingOwner: '',
      editParkingOwnerDetails: '',
      editParkingRented: '',
      editParkingRentedDetails: '',
      editPopup: '',
      editParkingRentedChecked: false,
      editParkingRentedTenantChecked: true,
      editTenantChecked: false,
      loading: true,
      messageText: '',
      messageAlert: false,
      messageColor: "danger",
    };
  };


  componentDidMount() {

    var buildingId = parseInt(cookie.load("buildingId"));
    this.setState({ buildingId: buildingId });
    const token = cookie.load('token');
    this.setState({ token: token });

    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/parking/building/' + buildingId, options)
      .then(data => data.json())
      .then(parkings => {
        for (var i = 0; i < parkings.length; ++i) {
          var ownerDetails = ''
          if (parkings[i].owner !== null) {
            ownerDetails = parkings[i].owner.name + " " + parkings[i].owner.surname
          }
          else {
            ownerDetails = "NEMA VLASNIKA"
          }

          var rentedDetails = ""
          if (parkings[i].rented !== null) {
            rentedDetails = parkings[i].rented.name + " " + parkings[i].rented.surname
          }
          else {
            rentedDetails = "NIJE U NAJMU"
          }

          if (parkings[i].rentStart !== null) {
            parkings[i].rentStart = parseDate(parkings[i].rentStart)
          }
          if (parkings[i].rentEnd !== null) {
            parkings[i].rentEnd = parseDate(parkings[i].rentEnd)
          }
          parkings[i].ownerDetails = ownerDetails
          parkings[i].rentedDetails = rentedDetails
        }

        this.setState({ parkings: sortData(parkings) })


      })
    fetch('/building/' + buildingId + "/tenants", options)
      .then(data => data.json())
      .then(tenants => {
        parseTenants(tenants)
        var ownersOptions = []
        for (var i = 0; i < tenants.length; ++i) {
          var option = { key: tenants[i].id, value: tenants[i].id, text: tenants[i].name + " " + tenants[i].surname }
          ownersOptions.push(option)
        }
        this.setState({ ownerOptions: ownersOptions })
        this.setState({ tenants: sortData(tenants) })
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )

  };

  parseFetchResponse = response => response.json().then(text => ({
    json: text,
    meta: response,
  }));

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };


  cleanTextFields = () => {
    this.setState({ newParkingNumber: '' });
    this.setState({ newParkingType: '' });
    this.setState({ newParkingOwner: '' });
    this.setState({ newParkingRented: '' });

    this.setState({ newParkingOwnerName: '' });
    this.setState({ newParkingOwnerSurname: '' });
    this.setState({ newParkingOwnerEmail: '' });
    this.setState({ newParkingOwnerPhone: '' });

    this.setState({ newParkingRentedName: '' });
    this.setState({ newParkingRentedSurname: '' });
    this.setState({ newParkingRentedEmail: '' });
    this.setState({ newParkingRentedPhone: '' });

    this.setState({ newParkingRentedChecked: false });
    this.setState({ newParkingRentedTenantChecked: true });
    this.setState({ newTenantChecked: false });

    this.setState({ editParkingNumber: '' });
    this.setState({ editParkingType: '' });
    this.setState({ editParkingOwner: '' });
    this.setState({ editParkingRented: '' });
    this.setState({ parkingId: '' });

    this.setState({newStartRent: ''})
    this.setState({newEndRent: ''})
    this.setState({newRentType: ''})

  }

  createRemoteOwner() {

    const data = {
      name: this.state.newParkingOwnerName,
      surname: this.state.newParkingOwnerSurname,
      email: this.state.newParkingOwnerEmail,
      mobilePhone: this.state.newParkingOwnerPhone,
    };
    const options = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    };
    fetch('/tenant/building/' + this.state.buildingId + '/remote', options)
      .then(response => {
        console.log(response.json())
        response.json()
      })

  }

  createRemoteRented() {

  }

  addNewParking = () => {
    this.setState({ loading: true })
    if (this.state.newTenantChecked) {
      const data = {
        name: this.state.newParkingOwnerName,
        surname: this.state.newParkingOwnerSurname,
        email: this.state.newParkingOwnerEmail,
        mobilePhone: this.state.newParkingOwnerPhone,
      };
      const options = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      };
      fetch('/tenant/building/' + this.state.buildingId + '/remote', options)
        .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          var newOwner = json.id
          if (meta.status !== 200) {
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno dodan parking!" })
            this.cleanTextFields()
            this.cancelEditPopUp()
          }

          const data = {
            buildingId: this.state.buildingId,
            number: this.state.newParkingNumber,
            type: this.state.newParkingType,
            ownerId: newOwner,
            rentedId: null,
          };
          const options = {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
          };


          fetch('/parking', options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
              if (meta.status !== 200) {
                this.setState({ messageAlert: true }, () => {
                  window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                  }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno dodan parking!" })
                this.cancelAddParkingPopUp();
                this.cleanTextFields();
                this.componentDidMount();

              }
              else {
                this.setState({ messageAlert: true }, () => {
                  window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                  }, 6000)
                });
                this.setState({ messageColor: "success" });
                this.setState({ messageText: "Uspješno dodan parking!" })
                this.cancelAddParkingPopUp();
                this.cleanTextFields();
                this.componentDidMount();
              }
            })
            .then(
              setTimeout(() => {
                this.setState({
                  loading: false
                })
              }, 500)
            )
        })
    }

    else if (!this.state.newTenantChecked) {
      const data = {
        buildingId: this.state.buildingId,
        number: this.state.newParkingNumber,
        type: this.state.newParkingType,
        ownerId: this.state.newParkingOwner,
        rentedId: null,
      };
      const options = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      };
      fetch('/parking', options)
        .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          if (meta.status !== 200) {
            this.setState({ messageAlert: true }, () => {
              window.setTimeout(() => {
                this.setState({ messageAlert: false })
              }, 6000)
            });
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno dodan parking!" })
            this.cancelAddParkingPopUp();
            this.cleanTextFields();
            this.componentDidMount();

          }
          else {
            this.setState({ messageAlert: true }, () => {
              window.setTimeout(() => {
                this.setState({ messageAlert: false })
              }, 6000)
            });
            this.setState({ messageColor: "success" });
            this.setState({ messageText: "Uspješno dodan parking!" })
            this.cancelAddParkingPopUp();
            this.cleanTextFields();
            this.componentDidMount();
          }
        })
        .then(
          setTimeout(() => {
            this.setState({
              loading: false
            })
          }, 500)
        )

    }

  };

  editParkingBasic = () => {
    this.setState({ loading: true })
    const data = {
      number: this.state.editParkingNumber,
      type: this.state.editParkingType
    };
    const options = {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    };
    fetch('/parking/' + this.state.parkingId, options)
      .then(this.parseFetchResponse)
      .then(({ json, meta }) => {
        if (meta.status !== 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno izmijenjeni podaci o parkingu!" })
          this.cancelEditPopUp();
          this.cleanTextFields();
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno izmijenjeni podaci o parkingu!" })
          this.cancelEditPopUp();
          this.cleanTextFields();
          this.componentDidMount();
        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  };


  editParkingOwner = () => {
    this.setState({ loading: true })
    var newOwner = ''
    if (this.state.newTenantChecked) {
      const data = {
        name: this.state.newParkingOwnerName,
        surname: this.state.newParkingOwnerSurname,
        email: this.state.newParkingOwnerEmail,
        mobilePhone: this.state.newParkingOwnerPhone,
      };
      const options = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      };
      fetch('/tenant/building/' + this.state.buildingId + '/remote', options)
        .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          newOwner = json.id
          if (meta.status !== 200) {
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno izmijenjen vlasnik parkinga!" })
            this.cleanTextFields()
            this.cancelEditPopUp()
          }
          const options = {
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
            },
          };
          fetch('/parking/' + this.state.parkingId + '/owner/' + newOwner, options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
              if (meta.status !== 200) {
                this.setState({ messageAlert: true }, () => {
                  window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                  }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno izmijenjen vlasnik parkinga!" })
                this.cancelEditPopUp();
                this.cleanTextFields();
                this.componentDidMount();

              }
              else {
                this.setState({ messageAlert: true }, () => {
                  window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                  }, 6000)
                });
                this.setState({ messageColor: "success" });
                this.setState({ messageText: "Uspješno izmijenjen vlasnik parkinga!" })
                this.cancelEditPopUp();
                this.cleanTextFields();
                this.componentDidMount();
              }
            })
            .then(
              setTimeout(() => {
                this.setState({
                  loading: false
                })
              }, 500)
            )
        })
    }
    else {
      newOwner = this.state.editParkingOwner
      const options = {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
        },
      };
      fetch('/parking/' + this.state.parkingId + '/owner/' + newOwner, options)
        .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          if (meta.status !== 200) {
            this.setState({ messageAlert: true }, () => {
              window.setTimeout(() => {
                this.setState({ messageAlert: false })
              }, 6000)
            });
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno izmijenjen vlasnik parkinga!" })
            this.cancelEditPopUp();
            this.cleanTextFields();
            this.componentDidMount();

          }
          else {
            this.setState({ messageAlert: true }, () => {
              window.setTimeout(() => {
                this.setState({ messageAlert: false })
              }, 6000)
            });
            this.setState({ messageColor: "success" });
            this.setState({ messageText: "Uspješno izmijenjen vlasnik parkinga!" })
            this.cancelEditPopUp();
            this.cleanTextFields();
            this.componentDidMount();
          }
        })
        .then(
          setTimeout(() => {
            this.setState({
              loading: false
            })
          }, 500)
        )
    }
  };


  editParkingRented = () => {
    this.setState({ loading: true })
    var newRented = ''
    if (this.state.newParkingRentedChecked) {
      const data = {
        name: this.state.newParkingRentedName,
        surname: this.state.newParkingRentedSurname,
        email: this.state.newParkingRentedEmail,
        mobilePhone: this.state.newParkingRentedPhone,

      };
      const options = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      };
      fetch('/tenant/building/' + this.state.buildingId + '/remote', options)
        .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          if (meta.status !== 200) {
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno izmijenjen najamnik parkinga!" })
            this.cleanTextFields()
            this.cancelEditPopUp()
          }
          newRented = json.id
          const data = {
            rentStart: addOneHour(this.state.newStartRent),
            rentEnd: addOneHour( this.state.newEndRent),
            rentType: this.state.newRentType
          };
          const options = {
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
          };
          fetch('/parking/' + this.state.parkingId + '/rented/' + newRented, options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
              if (meta.status !== 200) {
                this.setState({ messageAlert: true }, () => {
                  window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                  }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno izmijenjen najamnik parkinga!" })
                this.cancelEditPopUp();
                this.cleanTextFields();
                this.componentDidMount();

              }
              else {
                this.setState({ messageAlert: true }, () => {
                  window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                  }, 6000)
                });
                this.setState({ messageColor: "success" });
                this.setState({ messageText: "Uspješno izmijenjen najamnik parkinga!" })
                this.cancelEditPopUp();
                this.cleanTextFields();
                this.componentDidMount();
              }
            })
            .then(
              setTimeout(() => {
                this.setState({
                  loading: false
                })
              }, 500)
            )
        })
    }
    else {
      newRented = this.state.editParkingRented
      const data = {
        rentStart: addOneHour(this.state.newStartRent),
        rentEnd: addOneHour(this.state.newEndRent),
        rentType: this.state.newRentType
      };
      const options = {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      };
      fetch('/parking/' + this.state.parkingId + '/rented/' + newRented, options)
        .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          if (meta.status !== 200) {
            this.setState({ messageAlert: true }, () => {
              window.setTimeout(() => {
                this.setState({ messageAlert: false })
              }, 6000)
            });
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno izmijenjen najamnik parkinga!" })
            this.cancelEditPopUp();
            this.cleanTextFields();
            this.componentDidMount();

          }
          else {
            this.setState({ messageAlert: true }, () => {
              window.setTimeout(() => {
                this.setState({ messageAlert: false })
              }, 6000)
            });
            this.setState({ messageColor: "success" });
            this.setState({ messageText: "Uspješno izmijenjen najamnik parkinga!" })
            this.cancelEditPopUp();
            this.cleanTextFields();
            this.componentDidMount();
          }
        })
        .then(
          setTimeout(() => {
            this.setState({
              loading: false
            })
          }, 500)
        )
    }

  };

  deleteParking = () => {
    this.setState({ loading: true })
    const options = {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      }
    };
    return fetch('/parking/' + this.state.parkingId, options)
      .then(response => {
        if (response.status === 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno obrisan parking!" })
          this.cancelDeletePopUp();
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno obrisan parking!" })
          this.cancelDeletePopUp();
          this.componentDidMount();

        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  }

  toggleDeletePopUp = () => {
    this.setState({ deletePopup: true })
  }

  cancelDeletePopUp = () => {
    this.setState({ deletePopup: false })
  }

  toggleEditPopUp = () => {
    this.setState({ editPopup: true })
  }

  cancelEditPopUp = () => {
    this.setState({ editPopup: false })
    this.cleanTextFields()
  }

  dismissAlert = () => {
    this.setState({ messageAlert: false })
  }

  toogleAddParkingPopUp = () => {
    this.setState({ addParkingPopUp: true })
  }

  cancelAddParkingPopUp = () => {
    this.setState({ addParkingPopUp: false })
    this.cleanTextFields();
  }

  handleChecked = (name, old) => {
    var newOld = !old;
    this.setState({
      [name]: newOld,
    });
  };

  handleSelectParkingType = (e, { value }) => {
    this.setState({ newParkingType: value })
  }

  handleSelectRentType = (e, { value }) => {
    this.setState({ newRentType: value })
  }

  handleSelectEditParkingType = (e, { value }) => {
    this.setState({ editParkingType: value })
  }

  handleSelectParkingOwner = (e, { value }) => {
    this.setState({ newParkingOwner: value })
  }

  handleSelectParkingRented = (e, { value }) => {
    this.setState({ newParkingRented: value })
  }

  handleSelectEditParkingOwner = (e, { value }) => {
    this.setState({ editParkingOwner: value })
  }

  handleSelectEditParkingRented = (e, { value }) => {
    this.setState({ editParkingRented: value })
  }


  tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  tableTextLocalization = {
    body: {
      emptyDataSourceMessage: 'Nema podataka za prikazati',
      editRow: {
        deleteText: 'Jeste li sigurni?',
        saveTooltip: 'Potvrdi',
        cancelTooltip: 'Odustani'
      },
      addTooltip: 'Dodaj',
      deleteTooltip: 'Obriši',
      editTooltip: 'Izmijeni',
    },
    header: {
      actions: 'Akcije'
    },
    pagination: {
      firstTooltip: 'Prva stranica',
      previousTooltip: 'Prethodna stranica',
      nextTooltip: 'Iduća stranica',
      lastTooltip: 'Posljednja stranica',
      labelRowsSelect: 'redaka',
    },
    toolbar: {
      searchTooltip: 'Pretraži',
      searchPlaceholder: 'Pretraži',
      nRowsSelected: '{0} redaka odabrano',
      exportTitle: 'Izvezi',
      exportName: 'Izvezi kao .csv',
    }
  }


  render() {
    const { classes } = this.props;

    return (
      <div  >

        <Modal isOpen={this.state.deletePopup}  >
          <ModalBody>
            Jeste li sigurni da želite obrisati parkirno mjesto?
              </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.deleteParking}>Izbriši</Button>{' '}
            <Button size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
          </ModalFooter>
        </Modal>


        <Modal isOpen={this.state.addParkingPopUp} centered={true} >
          <ModalHeader toggle={this.cancelAddParkingPopUp}><h4>Unesite podatke o parkirnom mjestu</h4></ModalHeader>
          <ModalBody>
            <div>
              <Input placeholder='Broj parkirnog mjesta' value={this.state.newParkingNumber} onChange={this.handleChange('newParkingNumber')} /><br></br><br></br>
            </div>
            <div>
              <Select placeholder='Tip parkirnog mjesta' options={typeOptions} value={this.state.newParkingType} onChange={this.handleSelectParkingType} /><br></br><br></br>
            </div>
            <div>
              <Checkbox toggle label='Vlasnik nije stanar?' checked={this.state.newTenantChecked} onChange={() => this.handleChecked('newTenantChecked', this.state.newTenantChecked)} /><br></br><br></br>
            </div>
            {!this.state.newTenantChecked ?
              <div>
                <Select placeholder='Vlasnik' options={this.state.ownerOptions} value={this.state.newParkingOwner} onChange={this.handleSelectParkingOwner} /><br></br><br></br>
              </div>
              :
              <div>
                <Input placeholder='Ime vlasnika' value={this.state.newParkingOwnerName} onChange={this.handleChange('newParkingOwnerName')} /><br></br><br></br>
                <Input placeholder='Prezime vlasnika' value={this.state.newParkingOwnerSurname} onChange={this.handleChange('newParkingOwnerSurname')} /><br></br><br></br>
                <Input placeholder='Broj telefona vlasnika' value={this.state.newParkingOwnerPhone} onChange={this.handleChange('newParkingOwnerPhone')} /><br></br><br></br>
                <Input placeholder='Email vlasnika' value={this.state.newParkingOwnerEmail} onChange={this.handleChange('newParkingOwnerEmail')} /> <br></br><br></br>
              </div>
            }
          </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.addNewParking}>Dodaj</Button>{' '}

          </ModalFooter>
        </Modal>


        <Modal isOpen={this.state.editPopup} centered={true} >
          <ModalHeader toggle={this.cancelEditPopUp}><h5>Promijeni osnovne podatke o parkirnom mjestu</h5></ModalHeader>
          <ModalBody>
            <div>
              <h6>Broj parkirnog mjesta:</h6>
              <Input placeholder='Broj parkirnog mjesta' value={this.state.editParkingNumber} onChange={this.handleChange('editParkingNumber')} /><br></br><br></br>
            </div>
            <h6>Tip parkirnog mjesta:</h6>
            <div>
              <Select placeholder='Tip parkirnog mjesta' options={typeOptions} value={this.state.editParkingType} onChange={this.handleSelectEditParkingType} /><br></br><br></br>
            </div>
            <div>
              <Button size="small" positive onClick={this.editParkingBasic}>Izmijeni podatke</Button>
            </div>
          </ModalBody>
          <br></br>

          <ModalHeader ><h5>Promijeni vlasnika parkirnog mjesta</h5></ModalHeader>
          <ModalBody>
            <Checkbox toggle label='Vlasnik nije stanar?' checked={this.state.newTenantChecked} onChange={() => this.handleChecked('newTenantChecked', this.state.newTenantChecked)} /><br></br><br></br>
            <h6>Trenutni vlasnik: {this.state.editParkingOwnerDetails}</h6>
            {!this.state.newTenantChecked ?
              <div>
                <Select placeholder='Novi vlasnik' options={this.state.ownerOptions} value={this.state.editParkingOwner} onChange={this.handleSelectEditParkingOwner} /><br></br><br></br>
              </div>
              :
              <div>
                <Input placeholder='Ime vlasnika' value={this.state.newParkingOwnerName} onChange={this.handleChange('newParkingOwnerName')} /><br></br><br></br>
                <Input placeholder='Prezime vlasnika' value={this.state.newParkingOwnerSurname} onChange={this.handleChange('newParkingOwnerSurname')} /><br></br><br></br>
                <Input placeholder='Broj telefona vlasnika' value={this.state.newParkingOwnerPhone} onChange={this.handleChange('newParkingOwnerPhone')} /><br></br><br></br>
                <Input placeholder='Email vlasnika' value={this.state.newParkingOwnerEmail} onChange={this.handleChange('newParkingOwnerEmail')} /> <br></br><br></br>
              </div>
            }
            <div>
              <Button size="small" positive onClick={this.editParkingOwner}>Promijeni vlasnika</Button>
            </div>
          </ModalBody>
          <br></br>

          <ModalHeader ><h5>Promijeni najamnika parkirnog mjesta</h5></ModalHeader>
          <ModalBody>
            <Checkbox toggle label='Najamnik nije stanar?' checked={this.state.newParkingRentedChecked} onChange={() => this.handleChecked('newParkingRentedChecked', this.state.newParkingRentedChecked)} /><br></br><br></br>
            <h6>Trenutni najamnik: {this.state.editParkingRentedDetails}</h6>
            {!this.state.newParkingRentedChecked ?
              <div>
                <Select placeholder='Novi najamnik' options={this.state.ownerOptions} value={this.state.editParkingRented} onChange={this.handleSelectEditParkingRented} /><br></br><br></br>
              </div>
              :
              <div>
                <Input placeholder='Ime najamnika' value={this.state.newParkingRentedName} onChange={this.handleChange('newParkingRentedName')} /><br></br><br></br>
                <Input placeholder='Prezime najamnika' value={this.state.newParkingRentedSurname} onChange={this.handleChange('newParkingRentedSurname')} /><br></br><br></br>
                <Input placeholder='Broj telefona najamnika' value={this.state.newParkingRentedPhone} onChange={this.handleChange('newParkingRentedPhone')} /><br></br><br></br>
                <Input placeholder='Email najamnika' value={this.state.newParkingRentedEmail} onChange={this.handleChange('newParkingRentedEmail')} /> <br></br><br></br>
              </div>
            }
            <div>
              <Select placeholder='Vrsta najma' options={rentOptions} value={this.state.newRentType} onChange={this.handleSelectRentType} /><br></br><br></br>
            </div>
            <Form style={{ width: 50 + '%' }}>
              <DatePicker
                withPortal
                placeholderText="Datum početka najma"
                minDate={new Date()}
                isClearable
                selected={this.state.newStartRent}
                todayButton="Danas"
                dateFormat="dd.MM.yyyy"
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                onChange={date => {
                  this.setState({ newStartRent: date })
                }}
                locale="hr"
              />
            </Form><br></br>
            <Form style={{ width: 50 + '%' }}>
              <DatePicker
                withPortal
                placeholderText="Datum završetka najma"
                minDate={new Date()}
                isClearable
                selected={this.state.newEndRent}
                todayButton="Danas"
                dateFormat="dd.MM.yyyy"
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                onChange={date => {
                  this.setState({ newEndRent: date })
                }}
                locale="hr"
              />
            </Form><br></br>
            <div>
              <Button size="small" positive onClick={this.editParkingRented}>Promijeni najamnika</Button>
            </div>
          </ModalBody>

        </Modal>

        <Segment  >

          <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
          </Dimmer>

          <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
            <h5>{this.state.messageText}</h5>
          </Alert>


          <MaterialTable
            icons={this.tableIcons}
            title="Popis parkirnih mjesta"
            localization={this.tableTextLocalization}
            columns={columns()}
            options={{
              actionsColumnIndex: -1,
              exportButton: { pdf: false, csv: true },
              headerStyle: {
                backgroundColor: '#E86A22',
                color: '#FFF'
              },
              maxBodyHeight: 600,
              pageSize: 10,
              pageSizeOptions: [10, 20, 50, 100]
            }}
            data={this.state.parkings}
            actions={[
              {
                icon: () => {
                  return (
                    <Icon size="small" name='add' />
                  );
                },
                tooltip: 'Dodaj parkirno mjesto',
                isFreeAction: true,
                onClick: () => this.toogleAddParkingPopUp(),

              },
              {
                icon: () => <Icon name="pencil" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Izmjeni podatke',
                onClick: (event, rowData) => {
                  this.setState({ parkingId: rowData.id });
                  this.setState({ editParkingNumber: rowData.number });
                  this.setState({ editParkingType: rowData.type });
                  this.setState({ editParkingOwnerDetails: rowData.ownerDetails });
                  this.setState({ editParkingRentedDetails: rowData.rentedDetails });
                  if (rowData.owner === null) {
                    this.setState({ editTenantChecked: true })
                  } else {
                    this.setState({ editTenantChecked: false })
                  }
                  if (rowData.rented !== null) {
                    this.setState({ editParkingRentedChecked: true })
                  }
                  else {
                    this.setState({ editParkingRentedChecked: false })
                  }
                  this.setState({ editPopup: true });
                }
              },

              rowData => ({
                icon: () => <Icon name="trash" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Obriši parkirno mjesto',
                onClick: () => {
                  this.setState({ parkingId: rowData.id });
                  this.toggleDeletePopUp();
                }
              })

            ]}

          />
        </Segment>

      </div>


    );

  }

}

ParkingListTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ParkingListTable);