import { green, red } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import axios from 'axios';
import MaterialTable from 'material-table';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';
import { Button, Dimmer, Icon, Loader, Segment, Input } from 'semantic-ui-react';
import { parseDate, parseDateTime } from '../Parser.js';
import {sortData} from '../Parser'



const styles = theme => ({
  success: {
    backgroundColor: green[600],
  },
  fail: {
    backgroundColor: red[600],
  },
  });

class ReportListTable extends Component{


  constructor(props) {
    super(props);

    this.state={
        token: '',
        role: '',
        eventTypeOptions : [],
        reviews:[],
        reviewId:'',
        loading:true,
        messageText: '',
        messageAlert: false,
        messageColor: "danger",
        uploadPopup: false,
        messageText2: '',
        messageAlert2: false,
        messageColor2: "danger",
        uploadState: '',
        uploadPercentage: 0,
        progressBarHidden : "none",
        
        
    };
    this.inputReference = React.createRef();
  };

 


    componentDidMount(){

      
    };

    parseFetchResponse = response => response.json().then(text => ({
      json: text,
      meta: response,
    }));
    
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };

    toogleDeletePopUp=()=>{
      this.setState({deletePopup: true})
    }

    cancelDeletePopUp=()=>{
      this.setState({deletePopup: false})
    }

    toogleDeletePopUp2=()=>{
      this.setState({deletePopup2: true})
    }

    cancelDeletePopUp2=()=>{
      this.setState({deletePopup2: false})
    }


    toogleEditPopUp=()=>{
      this.setState({editPopup: true})
    }

    cancelEditPopUp=()=>{
      this.setState({editPopup: false})
    }

    toogleEditFurnaceManPopUp=()=>{
      this.setState({furnaceManEditPopup: true})
    }

    cancelEditFurnaceManPopUp=()=>{
      this.setState({furnaceManEditPopup: false})
      this.cleanTextFields()
    }
    
    toogleServiceManPopup=()=>{
      this.getAllFurnaceMen();
      this.setState({serviceManPopup: true})
    }

    dismissAlert2=()=>{
      this.setState({messageAlert2: false})
    }  
    

 

    cleanTextFields=()=>{
      this.setState({furnaceManId: ''})
      this.setState({newFurnaceManName: ''})
      this.setState({newFurnaceManSurname: ''})
      this.setState({newFurnaceManEmail: ''})
      this.setState({newFurnaceManMobilePhone: ''})
      this.setState({newFurnaceManFirmName: ''})
    }


    dismissAlert=()=>{
      this.setState({messageAlert: false})
    }   

    
    toogleUploadPopup=()=>{
      this.setState({uploadPopup: !this.state.uploadPopup})
    }

    cancelUploadPopup=()=>{
      this.setState({uploadPopup: false})
      this.setState({uploadPercentage: 0})
      this.setState({uploadState: ''})
      this.setState({progressBarHidden: 'none'})
    }

 
    tableIcons = {
      Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}  />),
      Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
      Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Delete: forwardRef((props, ref) => <Icon name="trash" style={{color: '#E86A22'}} />),
      DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
      Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
      Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
      ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
      ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
      body: {
          emptyDataSourceMessage: 'Nema podataka za prikazati',
          editRow: {
            deleteText: 'Jeste li sigurni?',
            saveTooltip: 'Potvrdi',
            cancelTooltip: 'Odustani'
          },
          addTooltip:'Dodaj',
          deleteTooltip: 'Obriši',
          editTooltip: 'Izmijeni',
      },
      header:{
        actions: 'Akcije'
      },
      pagination:{
        firstTooltip: 'Prva stranica',
        previousTooltip: 'Prethodna stranica',
        nextTooltip:'Iduća stranica',
        lastTooltip:'Posljednja stranica',
        labelRowsSelect:'redaka',
      },
      toolbar:{
        searchTooltip:'Pretraži',
        searchPlaceholder:'Pretraži',
        nRowsSelected:'{0} redaka odabrano',
        exportTitle:'Izvezi',
        exportName:"Izvezi kao .csv",
      }
  } 

    render(){
        const { classes } = this.props;
        
        return (
          <div  >
   
            <Segment  >

              <Dimmer active={this.state.loading} inverted>
              <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
              </Dimmer>

              <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                <h5>{this.state.messageText}</h5>
              </Alert>


              <MaterialTable  
                icons={this.tableIcons}
                title={"Popis pregleda "+this.props.eventType}
                localization={this.tableTextLocalization}
                columns={this.state.columns}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: { pdf: false, csv: true} ,
                  headerStyle: {
                    backgroundColor: '#E86A22',
                    color: '#FFF'
                  },
                  maxBodyHeight:600,
                  pageSize:10,
                    pageSizeOptions:[10,20,50,100]
                }}
                data={this.state.reviews}
                actions={[     

                  {
                    icon:() => <Icon name="download" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Preuzmi izvještaj',
                    onClick: (event, rowData) =>  {
                      this.downloadReport(rowData.id);
                    }
                  },

                  {
                    icon:() => <Icon name="upload" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Prenesi izvještaj',
                    hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                    onClick: (event, rowData) =>  {
                      this.toogleUploadPopup();
                      this.setState({reviewId: rowData.id})
                    }
                  },

                  rowData => ({
                    icon: () => <Icon name="trash" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Obriši pregled',
                    hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                    onClick: () => { 
                      this.setState({reviewId : rowData.id});
                      this.toogleDeletePopUp();
                  }
                    
                  })
                

                
                ]}
             
              />
              </Segment>
         
          </div>
            
          
          ); 
    
  }
    
}

ReportListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ReportListTable);