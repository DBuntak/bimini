import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Label } from 'semantic-ui-react';
import {sortData} from '../Parser'

const styles = theme => ({
  
})
  

class DebtorListTable extends Component{


  constructor(props) {
    super(props);

    this.state={
        token: '',
        debtors:[],

        debtorId: '',
        editDebtorName: '',
        editDebtorDebt: '',

        editPopup: false,
        loading: true,
        messageText: '',
        messageAlert: false,
        messageColor: "danger",

        columns: [
          { title: 'Identifikator', field: 'id', editable:"never" },
          { title: 'Ime', field: 'name'},
          { title: 'Dug (HRK)', field: 'debt' },
         
        ]
    };
      
  };

    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
      };


    componentDidMount(){
      this.setState({loading:true})
      var buildingId = cookie.load("buildingId");
      const token = cookie.load('token');
      this.setState({token: token});
      
        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/apartment/debt/'+buildingId, options)
        .then(data=>data.json())
        .then(debtors=>this.setState({debtors:sortData(debtors)}))
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
        
        
      
    };

    parseFetchResponse = response => response.json().then(text => ({
      json: text,
      meta: response,
    }));

   

    editDebt=()=>{

        this.setState({loading:true})

        const data={
          debt: this.state.editDebtorDebt,
        };
        const options={
            method:'POST',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        fetch('/apartment/update/debt/'+this.state.debtorId,options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno izmjenjen dug!"})
              this.cancelEditPopUp();
              this.clearTextFields();
              this.componentDidMount();
            
            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno izmjenjen dug!"})
              this.cancelEditPopUp();
              this.clearTextFields();  
              this.componentDidMount();
            }
          })
          


    }

   
    

    clearTextFields=()=>{
     
    }


    toogleEditPopUp=()=>{
      this.setState({editPopup: true})
    }

    cancelEditPopUp=()=>{
      this.setState({editPopup: false})
    }

    dismissAlert=()=>{
      this.setState({messageAlert: false})
    }



 
    tableIcons = {
      Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}  />),
      Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
      Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Delete: forwardRef((props, ref) => <Icon name="trash" style={{color: '#E86A22'}} />),
      DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
      Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
      Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
      ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
      ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
      body: {
          emptyDataSourceMessage: 'Nema podataka za prikazati',
          editRow: {
            deleteText: 'Jeste li sigurni?',
            saveTooltip: 'Potvrdi',
            cancelTooltip: 'Odustani'
          },
          addTooltip:'Dodaj',
          deleteTooltip: 'Obriši',
          editTooltip: 'Izmijeni',
      },
      header:{
        actions: 'Akcije'
      },
      pagination:{
        firstTooltip: 'Prva stranica',
        previousTooltip: 'Prethodna stranica',
        nextTooltip:'Iduća stranica',
        lastTooltip:'Posljednja stranica',
        labelRowsSelect:'redaka',
      },
      toolbar:{
        searchTooltip:'Pretraži',
        searchPlaceholder:'Pretraži',
        nRowsSelected:'{0} redaka odabrano',
        exportTitle:'Izvezi',
        exportName:'Izvezi kao .csv',
      }
  }
    


    render(){
        const { classes } = this.props;
       
        return (
          
          
          <div  >
            

            <Modal isOpen={this.state.editPopup} centered={true} >
              <ModalHeader toggle={this.cancelEditPopUp}><h4>Unesite nove podatke o dugu</h4></ModalHeader>
              <ModalBody>

                        <h6>Dug za stan: {this.state.editDebtorName}</h6><br></br>
                        
                        <div>
                        <Input labelPosition='right' type='text' placeholder='Dug stana' value={this.state.editDebtorDebt} onChange={this.handleChange('editDebtorDebt')}>
                          <Label basic>HRK</Label>
                          <input />
                          <Label>.00</Label>
                        </Input>
                      
                        </div>
                      
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.editDebt}>Izmijeni</Button>
                
              </ModalFooter>
            </Modal>

          

            <Segment  >
            <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
            </Dimmer>

              <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                <h5>{this.state.messageText}</h5>
              </Alert>

              <MaterialTable  
                icons={this.tableIcons}
                title="Popis dužnika"
                localization={this.tableTextLocalization}
                columns={this.state.columns}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: { pdf: false, csv: true} ,
                  headerStyle: {
                    backgroundColor: '#E86A22',
                    color: '#FFF'
                  },
                  maxBodyHeight:600,
                  pageSize:10,
                    pageSizeOptions:[10,20,50,100]
                }}
                data={this.state.debtors}
                actions={[

                             

                  {
                    icon:() => <Icon name="pencil" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Promijeni iznos duga',
                    onClick: (event, rowData) =>  {
                      this.setState({debtorId: rowData.id});
                      this.setState({editDebtorName: rowData.name});
                      this.setState({editDebtorDebt: rowData.debt});
                      this.setState({editPopup:true});
                    }
                  },
                  
               
                ]}
             
              />
         
              </Segment>
         
          </div>
          
            
          
          ); 
    }
  }
    


  DebtorListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DebtorListTable);