import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Form, TextArea, Label, Select } from 'semantic-ui-react';
import { sortData, parseLaws, makeJSDate } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";
import axios from 'axios';


registerLocale("hr", hr);

const styles = theme => ({

})

const columns = () => [
    { title: 'Identifikator', field: 'id', editable: "never", width: 200, hidden: true },
    { title: 'Stanar', field: 'ten', width: 50 + 'vh' },
    { title: "Odvjetnik", field: 'lawy', width: 50 + 'vh' },
    { title: "Dug", field: 'debt', width: 50 + 'vh' },
    { title: "Period od", field: 'periodFrom', width: 50 + 'vh' },
    { title: "Period do", field: 'periodTo', width: 50 + 'vh' },
];

class LawListTable extends Component {


    constructor(props) {
        super(props);

        this.state = {
            token: '',
            role: '',
            laws: [],
            deletePopup: false,
            addPopup: false,
            editPopup: false,
            detailsPopup: false,
            downloadPopup: false,
            uploadPopup: false,
            loading: true,
            messageText: '',
            messageAlert: false,
            messageColor: "danger",

            tenantOptions: [],
            lawyerOptions: [],

            newLawTenantId: '',
            newLawLawyerId: '',
            newLawDebt: '',
            newLawPeriodFrom: '',
            newLawPeriodTo: '',

            detailsLawTenantId: '',
            detailsLawLawyerId: '',
            detailsLawDebt: '',
            detailsLawPeriodFrom: '',
            detailsLawPeriodTo: '',

            detailsLawTenant: '',
            detailsLawLawyer: '',

            lawId: '',

            fileUploadState: "",
            uploadPercentage: 0,
            progressBarHidden: "none",
            fileDownloadState: "",
            newFile: null,
            file: null,
            fileSizeExceeded: false,


        };
        this.inputReference = React.createRef();

    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };



    componentDidMount() {
        var type = "type"
        const token = cookie.load('token');
        this.setState({ token: token });
        const role = cookie.load('role');
        this.setState({ role: role });
        const buildingId = parseInt(cookie.load('buildingId'));
        this.setState({ buildingId: buildingId });

        this.setState({ loading: true })
        const options = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
            },
        };
        fetch('/law/building/' + buildingId + '/type/' + this.props.fileType, options)
            .then(data => data.json())
            .then(laws => {
                this.setState({ laws: parseLaws(laws) })

            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )

            fetch('/building/' + buildingId + "/tenants", options)
            .then(data => data.json())
            .then(tenants => {
                var tenantOptions = []
                for (var i = 0; i < tenants.length; ++i) {
                    var option = { key: tenants[i].id, value: tenants[i].id, text: tenants[i].name + " " + tenants[i].surname }
                    tenantOptions.push(option)
                }
                this.setState({ tenantOptions: tenantOptions })
            })
            fetch('/supplier/building/' + buildingId, options)
            .then(data => data.json())
            .then(lawyers => {
                var lawyerOptions = []
                for (var i = 0; i < lawyers.length; ++i) {
                    var option = { key: lawyers[i].id, value: lawyers[i].id, text: lawyers[i].firm }
                    lawyerOptions.push(option)
                }
                this.setState({ lawyerOptions: lawyerOptions })
            })
    };

    fileUploadAction = () =>{
        this.setState({progressBarHidden: ""})
        this.inputReference.current.click();
      }


    parseFetchResponse = response => response.json().then(text => ({
        json: text,
        meta: response,
    }));


    setUploadState = (newState) => {
        this.setState({ fileUploadState: newState });
    }

    setPercentage = (newPercentage) => {
        this.setState({ uploadPercentage: newPercentage });
    }



    uploadFile = ({ target: { files } }) => {
        if (files[0].size > 2000000) {
            this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
            return
        }
        this.setState({ fileUploadState: "Učitavam..." })
        let data = new FormData();
        data.append("file", files[0]);
        const options = {
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor((loaded * 100) / total)
                console.log(loaded + " kb of " + total + " kb | " + percent + " %")

                if (percent < 100) {
                    this.setState({ uploadPercentage: percent })
                }
            },
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        }
        axios.put('law/file/' + this.state.lawId, data, options).then(res => {

            if (res.status != 200) {
                this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 0 });
                this.setState({ messageAlert: true }, () => {
                    window.setTimeout(() => {
                        this.setState({ messageAlert: false })
                    }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Prijenos neuspješan!" })
                this.clearTextFields()
                this.cancelLawUploadPopUp()
                this.componentDidMount()

            } else {
                this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 100 }, () => {
                    setTimeout(() => {
                        this.setState({ messageAlert: true }, () => {
                            window.setTimeout(() => {
                                this.setState({ messageAlert: false })
                            }, 6000)
                        });
                        this.setState({ messageColor: "success" });
                        this.setState({ messageText: "Prijenos uspješan!" })
                       this.clearTextFields()
                       this.cancelLawUploadPopUp()
                       this.componentDidMount()
                       
                    }, 4000)
                })
            }

        })

    }


    downloadLaw = (lawId) => {
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        };
        fetch('law/download/' + lawId, options)
            .then(response => {
                if (response.headers.get('Content-Disposition') === null) {
                    this.setState({ fileDownloadState: "Datoteka nije pronađena!" })
                    return
                }
                const filename = response.headers.get('Content-Disposition').split('filename=')[1];
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = filename;
                    a.click();
                });
            });
    }




    addLaw = () => {
        this.setState({progressBarHidden: ''})
        this.setState({ loading: true })
        const data = {
            tenantId: this.state.newLawTenantId,
            lawyerId: this.state.newLawLawyerId,
            debt: this.state.newLawDebt,
            periodFrom: this.state.newLawPeriodFrom,
            periodTo: this.state.newLawPeriodTo,
            type: this.props.fileType
        };

        let postData = new FormData();
        postData.append("card", this.state.newFile);
        postData.append("data", JSON.stringify(data))
        if (this.state.newFile.size > 2000000) {
            this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
            return
        }
        this.setState({ fileUploadState: "Učitavam..." })
        const options = {
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor((loaded * 100) / total)
                console.log(loaded + " kb of " + total + " kb | " + percent + " %")

                if (percent < 100) {
                    this.setState({ uploadPercentage: percent })
                }
            },
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        }
        axios.post('/law/upload/building/'+this.state.buildingId, postData, options).then(res => {

            if (res.status != 200) {
                this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 0 });
                this.setState({ messageAlert: true }, () => {
                    window.setTimeout(() => {
                        this.setState({ messageAlert: false })
                    }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno stvorenje!" })
                this.cancelAddPopUp()
                this.setState({ loading: false })
                this.componentDidMount()

            } else {
                this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 100 }, () => {
                    setTimeout(() => {
                        this.setState({ messageAlert: true }, () => {
                            window.setTimeout(() => {
                                this.setState({ messageAlert: false })
                            }, 6000)
                        });
                        this.setState({ messageColor: "success" });
                        this.setState({ messageText: "Uspješno stvaranje!" })
                        this.clearTextFields()
                        this.cancelAddPopUp()
                        this.setState({ loading: false })
                        this.componentDidMount()
                    }, 4000)
                })
            }

        })
    };


    editLaw = () => {
        this.setState({ loading: true })
        const data = {
            tenantId: this.state.detailsLawTenantId,
            lawyerId: this.state.detailsLawLawyerId,
            debt: this.state.detailsLawDebt,
            periodFrom: this.state.detailsLawPeriodFrom,
            periodTo: this.state.detailsLawPeriodTo,
            type: this.props.fileType
        };
        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        fetch('/law/' + this.state.lawId, options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
                if (meta.status !== 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješna izmjena!" })
                    this.cancelEditPopUp();
                    this.clearTextFields();
                    this.componentDidMount();

                }
                else {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješna izmjena!" })
                    this.cancelEditPopUp();
                    this.clearTextFields();
                    this.componentDidMount();
                }
            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )

    };


    deleteLaw = () => {

        this.setState({ loading: true })
        const options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            }
        };
        return fetch('/law/' + this.state.lawId, options)
            .then(response => {
                if (response.status === 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješno brisanje!" })
                    this.cancelDeletePopUp();
                    this.componentDidMount();

                }
                else {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno brisanje!" })
                    this.cancelDeletePopUp();

                }
            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )
    };

    clearTextFields = () => {
        this.setState({ lawId: '' })
        this.setState({ newLawDebt: '' })
        this.setState({ newLawTenantId: '' })
        this.setState({ newLawLawyerId: '' })
        this.setState({ newLawPeriodFrom: '' })
        this.setState({ newLawPeriodTo: '' })
        this.setState({file: null})
        this.setState({newFile: null})
        this.setState({fileUploadState: ''})
        this.setState({uploadPercentage: 0})
        this.setState({progressBarHidden: "none"})
        this.setState({fileDownloadState:""})
    }

    handleSelectTenant = (e, { value }) => {
        this.setState({ newLawTenantId: value })
    }
    handleSelectLawyer = (e, { value }) => {
        this.setState({ newLawLawyerId: value })
    }

    handleSelectEditTenant = (e, { value }) => {
        this.setState({ detailsLawTenantId: value })
    }
    handleSelectEditLawyer = (e, { value }) => {
        this.setState({ detailsLawLawyerId: value })
    }

    toogleDeletePopUp = () => {
        this.setState({ deletePopup: true })
    }

    cancelDeletePopUp = () => {
        this.setState({ deletePopup: false })
    }

    toogleAddPopUp = () => {
        this.setState({ addPopup: true })
    }

    cancelAddPopUp = () => {
        this.setState({ addPopup: false })
        this.clearTextFields();
    }

    toogleEditPopUp = () => {
        this.setState({ editPopup: true })
    }

    cancelEditPopUp = () => {
        this.setState({ editPopup: false })
        this.clearTextFields();
    }

    toogleDetailsPopUp = () => {
        this.setState({ detailsPopup: true })
    }

    cancelDetailsPopUp = () => {
        this.setState({ detailsPopup: false })
        this.clearTextFields();
    }

    toogleLawUploadPopUp = () => {
        this.setState({ uploadPopup: true })
    }

    cancelLawUploadPopUp = () => {
        this.setState({ uploadPopup: false })
        this.clearTextFields();
    }

    dismissAlert = () => {
        this.setState({ messageAlert: false })
    }

    tableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
        body: {
            emptyDataSourceMessage: 'Nema podataka za prikazati',
            editRow: {
                deleteText: 'Jeste li sigurni?',
                saveTooltip: 'Potvrdi',
                cancelTooltip: 'Odustani'
            },
            addTooltip: 'Dodaj',
            deleteTooltip: 'Obriši',
            editTooltip: 'Izmijeni',
        },
        header: {
            actions: 'Akcije'
        },
        pagination: {
            firstTooltip: 'Prva stranica',
            previousTooltip: 'Prethodna stranica',
            nextTooltip: 'Iduća stranica',
            lastTooltip: 'Posljednja stranica',
            labelRowsSelect: 'redaka',
        },
        toolbar: {
            searchTooltip: 'Pretraži',
            searchPlaceholder: 'Pretraži',
            nRowsSelected: '{0} redaka odabrano',
            exportTitle: 'Izvezi',
            exportName: 'Izvezi kao .csv',
        }
    }



    render() {
        const { classes } = this.props;

        return (
            <div  >
                <Modal isOpen={this.state.deletePopup}  >
                    <ModalBody>
                        Jeste li sigurni da želite obrisati stavku?
              </ModalBody>
                    <ModalFooter>
                        <Button size="small" positive onClick={this.deleteLaw}>Izbriši</Button>{' '}
                        <Button size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.uploadPopup} centered={true} >
                    <ModalHeader toggle={this.cancelLawUploadPopUp}><h4>Prenesi datoteku</h4></ModalHeader>
                    <ModalBody>

                        <Segment placeholder>
                            <div>
                                <input type="file" hidden ref={this.inputReference} onChange={this.uploadFile} />
                                <Button content='Prenesi' icon="upload" className="ui button" onClick={this.fileUploadAction} color="orange" size='huge' style={{ borderRadius: 30 }} />
                            </div>
                            
                            <br></br>
                            <div style={{ display: this.state.progressBarHidden }}>
                                <Progress color="success" animated value={this.state.uploadPercentage} />
                                <center>{this.state.fileUploadState}</center>
                            </div>
                        </Segment>



                    </ModalBody>


                </Modal>



                <Modal isOpen={this.state.addPopup} centered={true} >
                    <ModalHeader toggle={this.cancelAddPopUp}><h4>Unesite podatke</h4></ModalHeader>
                    <ModalBody>
                        <h6>Podaci:</h6>
                        <div>
                            <Select style={{ width: 60 + '%' }} placeholder='Stanar' options={this.state.tenantOptions} value={this.state.newLawTenantId} onChange={this.handleSelectTenant} /><br></br><br></br>
                        </div><br></br>
                        <div>
                            <Select style={{ width: 60 + '%' }} placeholder='Odvjetnik' options={this.state.lawyerOptions} value={this.state.newLawLawyerId} onChange={this.handleSelectLawyer} /><br></br><br></br>
                        </div><br></br>
                        <div>
                        <Input labelPosition='right' type='text' placeholder='Iznos' value={this.state.newLawDebt} onChange={this.handleChange('newLawDebt')}>
                            <Label basic>HRK</Label>
                            <input />
                            <Label>.00</Label>
                        </Input>

                        </div><br></br>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Period od"
                                    minDate={new Date()}
                                    isClearable
                                    selected={this.state.newLawPeriodFrom}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {

                                        this.setState({ newLawPeriodFrom: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Period do"
                                    minDate={new Date()}
                                    isClearable
                                    selected={this.state.newLawPeriodTo}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {

                                        this.setState({ newLawPeriodTo: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>

                        <h6>Datoteka:</h6>
                        <div>
                            <input type="file" hidden ref={this.inputReference} onChange={(e)=>{this.setState({newFile: e.target.files[0]})}} />
                            <center>
                                <Button content='Prenesi' icon="upload" className="ui button" onClick={()=>{this.inputReference.current.click()}} color="orange" size='big' style={{ borderRadius: 30 }} />
                            </center>
                        </div>

                        {this.state.newFile === null ? <div></div> : <div><center><h6>{this.state.newFile.name}</h6></center></div>}

                        <br></br>
                        <div style={{ display: this.state.progressBarHidden }}>
                            <Progress color="success" animated value={this.state.uploadPercentage} />
                            <center>{this.state.fileUploadState}</center>
                        </div>

                    </ModalBody>

                    <ModalFooter>
                        <Button size="small" positive onClick={this.addLaw}>Stvori</Button>{' '}

                    </ModalFooter>

                </Modal>


                <Modal isOpen={this.state.editPopup} centered={true} >
                    <ModalHeader toggle={this.cancelEditPopUp}><h4>Izmijenite podatke</h4></ModalHeader>
                    <ModalBody>
                    <h5>Podaci:</h5>
                        <h6>Stanar:</h6>
                        <div>
                            <Select style={{ width: 60 + '%' }} placeholder='Stanar' options={this.state.tenantOptions} value={this.state.detailsLawTenant} onChange={this.handleSelectEditTenant} /><br></br><br></br>
                        </div><br></br>
                        <h6>Odvjetnik:</h6>
                        <div>
                            <Select style={{ width: 60 + '%' }} placeholder='Odvjetnik' options={this.state.lawyerOptions} value={this.state.detailsLawLawyer} onChange={this.handleSelectEditLawyer} /><br></br><br></br>
                        </div><br></br>
                        <h6>Dug:</h6>
                        <div>
                        <Input labelPosition='right' type='text' placeholder='Iznos' value={this.state.detailsLawDebt} onChange={this.handleChange('detailsLawDebt')}>
                            <Label basic>HRK</Label>
                            <input />
                            <Label>.00</Label>
                        </Input>

                        </div><br></br>
                        <h6>Period od:</h6>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Period od"
                                    minDate={new Date()}
                                    isClearable
                                    selected={this.state.detailsLawPeriodFrom}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {

                                        this.setState({ detailsLawPeriodFrom: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>
                        <h6>Period do:</h6>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Period do"
                                    minDate={new Date()}
                                    isClearable
                                    selected={this.state.detailsLawPeriodTo}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {

                                        this.setState({ detailsLawPeriodTo: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>
                       
                    </ModalBody>

                    <ModalFooter>
                        <Button size="small" positive onClick={this.editLaw}>Izmijeni</Button>{' '}

                    </ModalFooter>

                </Modal>

                <Modal isOpen={this.state.detailsPopup} centered={true} >
                    <ModalHeader toggle={this.cancelDetailsPopUp}><h4>Detalji</h4></ModalHeader>
                    <ModalBody>
                        <p>
                            <h5><b>Podaci</b></h5><br></br>
                            <b>Stanar:</b> {this.state.detailsLawTenant}<br></br><br></br>
                            <b>Odvjetnik:</b> {this.state.detailsLawLawyer}<br></br><br></br>
                            <b>Dug:</b> {this.state.detailsLawDebt}<br></br><br></br>
                            <b>Period od:</b> {this.state.detailsLawPeriodFrom}<br></br><br></br>
                            <b>Period do:</b> {this.state.detailsLawPeriodTo}<br></br><br></br>
                        </p>

                    </ModalBody>

                </Modal>


                <Dimmer active={this.state.loading} inverted>
                    <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
                </Dimmer>

                <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                    <h5>{this.state.messageText}</h5>
                </Alert>

                <MaterialTable
                    icons={this.tableIcons}
                    title="Popis"
                    localization={this.tableTextLocalization}
                    columns={columns()}
                    options={{
                        actionsColumnIndex: -1,
                        exportButton: { pdf: false, csv: true },
                        headerStyle: {
                            backgroundColor: '#E86A22',
                            color: '#FFF'
                        },
                        maxBodyHeight: 600,
                        pageSize: 10,
                        pageSizeOptions: [10, 20, 50, 100]
                    }}
                    data={this.state.laws}
                    actions={[
                        {
                            icon: () => {
                                return (
                                    <Icon size="small" name='add' />
                                );
                            },
                            tooltip: 'Dodaj',
                            isFreeAction: true,
                            hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                            onClick: () => this.toogleAddPopUp(),

                        },

                        {
                            icon: () => <Icon name="file alternate" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Detalji',
                            onClick: (event, rowData) => {
                                this.setState({ lawId: rowData.id })
                                this.setState({ detailsLawTenant: rowData.ten })
                                this.setState({ detailsLawLawyer: rowData.lawy })
                                this.setState({ detailsLawDebt: rowData.debt })
                                this.setState({ detailsLawPeriodFrom: rowData.periodFrom })
                                this.setState({ detailsLawPeriodTo: rowData.periodTo })
                                this.toogleDetailsPopUp();
                            }
                        },

                        {
                            icon: () => <Icon name="download" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Preuzmi datoteku',
                            onClick: (event, rowData) => {
                                this.downloadLaw(rowData.id)
                            }
                        },

                        {
                            icon: () => <Icon name="upload" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Prenesi novu datoteku',
                            onClick: (event, rowData) => {
                                this.setState({ lawId: rowData.id })
                                this.toogleLawUploadPopUp();
                            }
                        },

                        {
                            icon: () => <Icon name="pencil" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Izmijeni podatke',
                            onClick: (event, rowData) => {
                                this.setState({ lawId: rowData.id })
                                this.setState({ detailsLawTenant: rowData.ten })
                                this.setState({ detailsLawLawyer: rowData.lawy })
                                this.setState({ detailsLawDebt: rowData.debt })
                                this.setState({ detailsLawPeriodFrom: makeJSDate(rowData.periodFrom) })
                                this.setState({ detailsLawPeriodTo: makeJSDate(rowData.periodTo) })
                                this.toogleEditPopUp();
                            }
                        },

                        rowData => ({
                            icon: () => <Icon name="trash" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Obriši ugovor',
                            hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                            onClick: () => {
                                this.setState({ lawId: rowData.id });
                                this.toogleDeletePopUp();
                            }
                        })
                    ]}
                />
            </div>
        );
    }
}

LawListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LawListTable);