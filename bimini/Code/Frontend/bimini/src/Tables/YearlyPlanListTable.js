import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Form, TextArea } from 'semantic-ui-react';
import { parseBoolean, parseDateJS, parseDateTimeJS, parseResource, parseDate, parseDateTime, addOneHour, convertDates, parseWorkers } from '../Parser';
import { sortData } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";
import axios from 'axios';


registerLocale("hr", hr);

const styles = theme => ({

})


class YearlyPlanListTable extends Component {


  constructor(props) {
    super(props);

    this.state = {
      token: '',
      role: '',
      yearlyPlans: [],
      deletePopup: false,
      addPopup: false,
      editPopup: false,
      loading: true,
      messageText: '',
      messageAlert: false,
      messageColor: "danger",

      yearlyPlanId: '',

      newYearlyPlanDescription: '',
      newYearlyPlanDate: '',

      downloadPopup: false,
      uploadPopup: false,
      fileUploadState: "",
      uploadPercentage: 0,
      progressBarHidden: "none",
      fileDownloadState: "",
      newFile: null,
      file: null,
      fileSizeExceeded: false,

      columns: [
        { title: 'Identifikator', field: 'id', editable: "never", hidden:true },
        { title: 'Opis', field: 'description' },
        { title: 'Datum', field: 'date' },
      ]
    };
    this.inputReference = React.createRef();

  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };


  parseDataSet = (dataSet) => {
    let newDataSet = []
    for (let i = 0; i < dataSet.length; ++i) {
      let entry = dataSet[i]
      entry.date = parseDate(entry.date)
      newDataSet[i] = entry
    }
    return sortData(newDataSet)
  }


  componentDidMount() {

    const token = cookie.load('token');
    this.setState({ token: token });
    const role = cookie.load('role');
    this.setState({ role: role });
    const buildingId = parseInt(cookie.load('buildingId'));
    this.setState({ buildingId: buildingId });

    this.setState({ loading: true })
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/finance/building/'+buildingId+'/type/godisnji_plan', options)
      .then(data => data.json())
      .then(yearlyPlans => {
        this.setState({ yearlyPlans: this.parseDataSet(yearlyPlans) })

      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )



  };

  fileUploadAction = () =>{
    this.setState({progressBarHidden: ""})
    this.inputReference.current.click();
  }

  parseFetchResponse = response => response.json().then(text => ({
    json: text,
    meta: response,
  }));

  setUploadState = (newState) => {
    this.setState({ fileUploadState: newState });
  }

  setPercentage = (newPercentage) => {
    this.setState({ uploadPercentage: newPercentage });
  } 

  uploadFile = ({ target: { files } }) => {
    if (files[0].size > 2000000) {
        this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
        return
    }
    this.setState({ fileUploadState: "Učitavam..." })
    let data = new FormData();
    data.append("file", files[0]);
    const options = {
        onUploadProgress: (progressEvent) => {
            const { loaded, total } = progressEvent;
            let percent = Math.floor((loaded * 100) / total)
            console.log(loaded + " kb of " + total + " kb | " + percent + " %")

            if (percent < 100) {
                this.setState({ uploadPercentage: percent })
            }
        },
        headers: {
            'Authorization': 'Bearer ' + this.state.token,
        }
    }
    axios.put('finance/file/' + this.state.yearlyPlanId, data, options).then(res => {

        if (res.status != 200) {
            this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
            this.setState({ uploadPercentage: 0 });
            this.setState({ messageAlert: true }, () => {
                window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                }, 6000)
            });
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno prenesen godišnji plan!" })
            this.clearTextFields()
            this.cancelYearlyPlanUploadPopUp()
            this.componentDidMount()

        } else {
            this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
            this.setState({ uploadPercentage: 100 }, () => {
                setTimeout(() => {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješno prenesen godišnji plan!" })
                   this.clearTextFields()
                   this.cancelYearlyPlanUploadPopUp()
                   this.componentDidMount()
                   
                }, 4000)
            })
        }

    })

}

downloadYearlyPlan = (yearlyPlanId) => {
  const options = {
      method: 'GET',
      headers: {
          'Authorization': 'Bearer ' + this.state.token,
      }
  };
  fetch('finance/download/' + yearlyPlanId, options)
      .then(response => {
          if (response.headers.get('Content-Disposition') === null) {
              this.setState({ fileDownloadState: "Datoteka nije pronađena!" })
              return
          }
          const filename = response.headers.get('Content-Disposition').split('filename=')[1];
          response.blob().then(blob => {
              let url = window.URL.createObjectURL(blob);
              let a = document.createElement('a');
              a.href = url;
              a.download = filename;
              a.click();
          });
      });
}

  addNewYearlyPlan = () => {
    this.setState({progressBarHidden: ''})
    this.setState({ loading: true })
    //let buildingId = this.props.buildingId
    const data = {
      type: 'godisnji_plan'
    };
    let postData = new FormData();
    postData.append("bill", this.state.newFile);
    postData.append("data", JSON.stringify(data))
    if (this.state.newFile.size > 2000000) {
        this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
        return
    }
    this.setState({ fileUploadState: "Učitavam..." })
    const options = {
        onUploadProgress: (progressEvent) => {
            const { loaded, total } = progressEvent;
            let percent = Math.floor((loaded * 100) / total)
            console.log(loaded + " kb of " + total + " kb | " + percent + " %")

            if (percent < 100) {
                this.setState({ uploadPercentage: percent })
            }
        },
        headers: {
            'Authorization': 'Bearer ' + this.state.token,
        }
    }
    axios.post('/finance/upload/building/'+this.state.buildingId, postData, options).then(res => {

        if (res.status != 200) {
            this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
            this.setState({ uploadPercentage: 0 });
            this.setState({ messageAlert: true }, () => {
                window.setTimeout(() => {
                    this.setState({ messageAlert: false })
                }, 6000)
            });
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno stvoren godišnji plan!" })
            this.cancelAddPopUp()
            this.setState({ loading: false })
            this.componentDidMount()

        } else {
            this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
            this.setState({ uploadPercentage: 100 }, () => {
                setTimeout(() => {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješno stvoren godišnji plan!" })
                    this.clearTextFields()
                    this.cancelAddPopUp()
                    this.setState({ loading: false })
                    this.componentDidMount()
                }, 4000)
            })
        }

    })
  };


  deleteYearlyPlan = () => {

    this.setState({ loading: true })
    const options = {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      }
    };
    return fetch('/finance/' + this.state.yearlyPlanId, options)
      .then(response => {
        if (response.status === 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno obrisan godišnji plan!" })
          this.cancelDeletePopUp();
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno obrisan godišnji plan!" })
          this.cancelDeletePopUp();

        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  };

  clearTextFields = () => {
    this.setState({ newYearlyPlanDate: '' })
    this.setState({ newYearlyPlanDescription: '' })
    this.setState({ yearlyPlanId: '' })
    this.setState({file: null})
    this.setState({fileUploadState: ''})
    this.setState({uploadPercentage: 0})
    this.setState({progressBarHidden: "none"})
    this.setState({fileDownloadState:""})
    this.setState({newFile: null})
  }

  toogleDeletePopUp = () => {
    this.setState({ deletePopup: true })
  }

  cancelDeletePopUp = () => {
    this.setState({ deletePopup: false })
  }

  toogleAddPopUp = () => {
    this.setState({ addPopup: true })
  }

  cancelAddPopUp = () => {
    this.setState({ addPopup: false })
    this.clearTextFields();
  }

  toogleEditPopUp = () => {
    this.setState({ editPopup: true })
  }

  cancelEditPopUp = () => {
    this.setState({ editPopup: false })
    this.clearTextFields();
  }

  toogleYearlyPlanUploadPopUp = () => {
    this.setState({ uploadPopup: true })
  }

  cancelYearlyPlanUploadPopUp = () => {
      this.setState({ uploadPopup: false })
      this.clearTextFields();
  }

  dismissAlert = () => {
    this.setState({ messageAlert: false })
  }

  tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  tableTextLocalization = {
    body: {
      emptyDataSourceMessage: 'Nema podataka za prikazati',
      editRow: {
        deleteText: 'Jeste li sigurni?',
        saveTooltip: 'Potvrdi',
        cancelTooltip: 'Odustani'
      },
      addTooltip: 'Dodaj',
      deleteTooltip: 'Obriši',
      editTooltip: 'Izmijeni',
    },
    header: {
      actions: 'Akcije'
    },
    pagination: {
      firstTooltip: 'Prva stranica',
      previousTooltip: 'Prethodna stranica',
      nextTooltip: 'Iduća stranica',
      lastTooltip: 'Posljednja stranica',
      labelRowsSelect: 'redaka',
    },
    toolbar: {
      searchTooltip: 'Pretraži',
      searchPlaceholder: 'Pretraži',
      nRowsSelected: '{0} redaka odabrano',
      exportTitle: 'Izvezi',
      exportName: 'Izvezi kao .csv',
    }
  }



  render() {
    const { classes } = this.props;

    return (


      <div  >
        <Modal isOpen={this.state.addPopup} centered={true} >
          <ModalHeader toggle={this.cancelAddPopUp}><h4>Unesite podatke o godišnjem planu</h4></ModalHeader>
          <ModalBody>
            <h6>Datoteka godišnjeg plana:</h6>
            <br></br>
            <div>
                <input type="file" hidden ref={this.inputReference} onChange={(e)=>{this.setState({newFile: e.target.files[0]})}} />
                <center>
                    <Button content='Prenesi' icon="upload" className="ui button" onClick={()=>{this.inputReference.current.click()}} color="orange" size='big' style={{ borderRadius: 30 }} />
                </center>
            </div>

            {this.state.newFile === null ? <div></div> : <div><center><h6>{this.state.newFile.name}</h6></center></div>}

            <br></br>
            <div style={{ display: this.state.progressBarHidden }}>
                <Progress color="success" animated value={this.state.uploadPercentage} />
                <center>{this.state.fileUploadState}</center>
            </div>

          </ModalBody>
          <ModalFooter>
            <Button size="personal" positive onClick={this.addNewYearlyPlan}>Dodaj</Button>

          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.uploadPopup} centered={true} >
            <ModalHeader toggle={this.cancelYearlyPlanUploadPopUp}><h4>Prenesi datoteku</h4></ModalHeader>
            <ModalBody>

                <Segment placeholder>
                    <div>
                        <input type="file" hidden ref={this.inputReference} onChange={this.uploadFile} />
                        <Button content='Prenesi' icon="upload" className="ui button" onClick={this.fileUploadAction} color="orange" size='huge' style={{ borderRadius: 30 }} />
                    </div>
                    
                    <br></br>
                    <div style={{ display: this.state.progressBarHidden }}>
                        <Progress color="success" animated value={this.state.uploadPercentage} />
                        <center>{this.state.fileUploadState}</center>
                    </div>
                </Segment>



            </ModalBody>


        </Modal>

        <Modal isOpen={this.state.deletePopup}  >
          <ModalBody>
            Jeste li sigurni da želite obrisati godišnji plan?
              </ModalBody>
          <ModalFooter>
            <Button size="personal" positive onClick={this.deleteYearlyPlan}>Izbriši</Button>
            <Button size="personal" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
          </ModalFooter>
        </Modal>

        <Segment  >
          <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
          </Dimmer>

          <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
            <h5>{this.state.messageText}</h5>
          </Alert>

          <MaterialTable
            icons={this.tableIcons}
            title="Popis godišnjih planova"
            localization={this.tableTextLocalization}
            columns={this.state.columns}
            options={{
              actionsColumnIndex: -1,
              exportButton: { pdf: false, csv: true },
              headerStyle: {
                backgroundColor: '#E86A22',
                color: '#FFF'
              },
              maxBodyHeight: 600,
              pageSize: 10,
              pageSizeOptions: [10, 20, 50, 100]
            }}
            data={this.state.yearlyPlans}
            actions={[
              {
                icon: () => {
                  return (
                    <Icon size="small" name='add' />
                  );
                },
                tooltip: 'Dodaj godišnji plan',
                isFreeAction: true,
                hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                onClick: () => this.setState({ addPopup: true }),

              },

              {
                icon: () => <Icon name="download" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Preuzmi datoteku godišnjeg plana',
                onClick: (event, rowData) => {
                    this.downloadYearlyPlan(rowData.id)
                }
            },

            {
                icon: () => <Icon name="upload" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Prenesi novu datoteku godišnjeg plana',
                onClick: (event, rowData) => {
                    this.setState({ yearlyPlanId: rowData.id })
                    this.toogleYearlyPlanUploadPopUp();
                }
            },

              rowData => ({
                icon: () => <Icon name="trash" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Obriši godišnji plan',
                hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                onClick: () => {
                  this.setState({ yearlyPlanId: rowData.id });
                  this.toogleDeletePopUp();
                }

              })



            ]}

          />

        </Segment>

      </div>



    );
  }
}



YearlyPlanListTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(YearlyPlanListTable);