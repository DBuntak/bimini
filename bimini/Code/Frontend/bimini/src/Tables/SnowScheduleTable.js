import { withStyles } from '@material-ui/core/styles';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import { Select, Dimmer, Icon, Loader, Segment, Button } from 'semantic-ui-react';
import { sortData, parseTenants } from '../Parser'

const styles = theme => ({

})

const columns = () => [
  { title: 'Identifikator', field: 'id', editable: "never", hidden: true },
  {
    title: 'Period', field: 'period', customSort: (a, b) => {
      var date1 = a.period.split("-")[0].split(".");
      var date2 = b.period.split("-")[0].split(".");
      var d = new Date();
      var d1;
      var d2;
      if (date1[1] > 6) {
        d1 = new Date(d.getFullYear(), date1[1] - 1, date1[0]);
      }
      else {
        d1 = new Date(d.getFullYear() + 1, date1[1] - 1, date1[0]);
      }
      if (date2[1] > 6) {
        d2 = new Date(d.getFullYear(), date2[1] - 1, date2[0]);
      }
      else {
        d2 = new Date(d.getFullYear() + 1, date2[1] - 1, date2[0]);
      }

      return d1.getTime() - d2.getTime();
    }
  },
  { title: 'Grupa', field: 'apNames' },
]


const columnsTenants = () => [
  { title: 'Identifikator', field: 'id', editable: "never", width: 100, hidden: true },
  { title: 'Ime', field: 'name', width: 100 },
  { title: 'Prezime', field: 'surname', width: 100 },
  { title: 'Privatni email', field: 'privateEmail', width: 100 },
    { title: 'Poslovni email', field: 'workEmail', width: 100 },
  { title: 'Broj mobitela', field: 'mobilePhone', width: 100 },
  { title: 'Status', field: 'type', width: 100 },
  { title: 'Uloga', field: 'role', width: 100 },
]

class SnowScheduleTable extends Component {


  constructor(props) {
    super(props);

    this.state = {
      buildingId: '',
      token: '',
      role: '',
      schedule: [],
      editPeriod: '',
      snowEntryId: '',
      period: '',
      loading: true,
      messageText: '',
      messageAlert: false,
      messageColor: "danger",
      generateAlert: false,
      tenantsListPopup: false,
      selectedPeriod: '',
      selectedId: '',
      selectedTenants: [],
      changePopup: false,

      selectedTenant1: '',
      selectedTenant2: '',
      tenantOptions: [],
    };

  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };


  componentDidMount() {

    var buildingId = cookie.load("buildingId");
    this.setState({ buildingId: buildingId });
    const token = cookie.load('token');
    this.setState({ token: token });
    const role = cookie.load('role');
    this.setState({ role: role });

    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/snowSchedule/table/' + buildingId, options)
      .then(data => data.json())
      .then(schedule => {
        if (schedule.length > 0) {
          var pomSchedule = schedule;
          for (var i = 0; i < schedule.length; i++) {
            var names = "";
            for (var j = 0; j < schedule[i].apartments.length; j++) {
              var apName = schedule[i].apartments[j].name;
              var owner = ''
              if (schedule[i].tenants[j] !== undefined) {
                owner = schedule[i].tenants[j].surname;
              }
              var entry = apName + "(" + owner + ")"
              names += entry
              if (j != schedule[i].apartments.length - 1) {
                names += ", ";
              }
            }
            pomSchedule[i].apNames = names;

          }
          this.setState({ schedule: sortData(pomSchedule) });
          console.log(schedule)
        }
        else {
        }
      })
    fetch('/building/' + buildingId + "/apartments", options)
      .then(data => data.json())
      .then(apartments => {
        var tenantOptions = []
        for (var i = 0; i < apartments.length; ++i) {
          var option = { key: apartments[i].id, value: apartments[i].id, text: apartments[i].name + "|  Broj: " + apartments[i].number + "|  Kat: " + apartments[i].floor }
          tenantOptions.push(option)
        }
        this.setState({ tenantOptions: tenantOptions })
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  };

  parseFetchResponse = response => response.json().then(text => ({
    json: text,
    meta: response,
  }));




  dismissAlert = () => {
    this.setState({ messageAlert: false })
  }

  generateSnowSchedule = (snowEntryId) => {
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/snowSchedule/create-schedule/' + this.state.buildingId, options)
      .then(response => {
        if (response.status === 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno stvoren raspored!" })
          this.setState({ generateAlert: false })
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno stvoren raspored!" })
          this.setState({ generateAlert: false })
          this.componentDidMount();

        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )

  }

  changeApartments = () => {
    const options = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/snowSchedule/replace/apartment/' + this.state.selectedTenant1+"/"+this.state.selectedTenant2, options)
      .then(response => {
        if (response.status === 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno zamijenjeni stanovi!" })
          this.setState({ changePopup: false })
          
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno zamijenjeni stanovi!" })
          this.setState({ changePopup: false })
          this.componentDidMount();

        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )

  }

  getSelectedTenants = (selectedId) => {
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/snowSchedule/tenants/' + selectedId, options)
      .then(data => data.json())
      .then(selectedTenants => {
        parseTenants(selectedTenants)
        this.setState({ selectedTenants: (selectedTenants) })


      })
  }

  toggleTenantsPopup = (selectedId) => {
    this.getSelectedTenants(selectedId)
    this.setState({ tenantsListPopup: true })
  }

  handleChangeDropdown = (e, { value }) => {
    this.setState({ addApartmentId: value });
  }

  cancelGenerateAlert = () => {
    this.setState({ generateAlert: false })
  }

  cancelTenantsPopup = () => {
    this.setState({ tenantsListPopup: false })
  }

  cancelChangePopup = () => {
    this.setState({ changePopup: false })
  }

  handleSelectTenant1 = (e, { value }) => {
    this.setState({ selectedTenant1: value })
  }

  handleSelectTenant2 = (e, { value }) => {
    this.setState({ selectedTenant2: value })
  }

  tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  tableTextLocalization = {
    body: {
      emptyDataSourceMessage: 'Nema podataka za prikazati',
      editRow: {
        deleteText: 'Jeste li sigurni?',
        saveTooltip: 'Potvrdi',
        cancelTooltip: 'Odustani'
      },
      addTooltip: 'Dodaj',
      deleteTooltip: 'Obriši',
      editTooltip: 'Izmijeni',
    },
    header: {
      actions: 'Akcije'
    },
    pagination: {
      firstTooltip: 'Prva stranica',
      previousTooltip: 'Prethodna stranica',
      nextTooltip: 'Iduća stranica',
      lastTooltip: 'Posljednja stranica',
      labelRowsSelect: 'redaka',
    },
    toolbar: {
      searchTooltip: 'Pretraži',
      searchPlaceholder: 'Pretraži',
      nRowsSelected: '{0} redaka odabrano',
      exportTitle: 'Izvezi',
      exportName: 'Izvezi kao .csv',
    }
  }



  render() {
    const { classes } = this.props;
    return (

      <div>

        <Modal isOpen={this.state.tenantsListPopup} centered={true} size="lg">
          <ModalHeader toggle={this.cancelTenantsPopup}><h4>{"Popis stanara za period: " + this.state.selectedPeriod}</h4></ModalHeader>
          <ModalBody>
            <MaterialTable
              icons={this.tableIcons}
              title="Popis stanara"
              localization={this.tableTextLocalization}
              columns={columnsTenants()}
              options={{
                actionsColumnIndex: -1,
                exportButton: { pdf: false, csv: true },
                headerStyle: {
                  backgroundColor: '#E86A22',
                  color: '#FFF'
                },
                maxBodyHeight: 600,
                pageSize: 10,
                pageSizeOptions: [10, 20, 50, 100]
              }}
              data={this.state.selectedTenants}

            />
          </ModalBody>
        </Modal>

        <Modal isOpen={this.state.generateAlert}  >
          <ModalBody>
            Jeste li sigurni da želite stvoriti novi raspored čišćenja snijega?
              </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.generateSnowSchedule}>Potvrdi</Button>{' '}
            <Button size="small" negative onClick={this.cancelGenerateAlert}>Odustani</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.changePopup}  >
          <ModalHeader toggle={this.cancelChangePopup}><h4>Zamijeni stanare u rasporedu čišćenja snijega</h4></ModalHeader>
          <ModalBody>
            <div>
              <Select style={{width: 50+'vh'}} placeholder='Odaberite stan' options={this.state.tenantOptions} value={this.state.selectedTenant1} onChange={this.handleSelectTenant1} /><br></br><br></br>
            </div>
            <div>
              <Select style={{width: 50+'vh'}} placeholder='Odaberite stan' options={this.state.tenantOptions} value={this.state.selectedTenant2} onChange={this.handleSelectTenant2} /><br></br><br></br>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.changeApartments}>Potvrdi</Button>
          </ModalFooter>
        </Modal>

        <Segment  >

          <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
          </Dimmer>

          <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
            <h5>{this.state.messageText}</h5>
          </Alert>


          <MaterialTable
            icons={this.tableIcons}
            title="Raspored čišćenja snijega"
            localization={this.tableTextLocalization}
            columns={columns()}
            options={{
              actionsColumnIndex: -1,
              exportButton: { pdf: true, csv: true },
              exportFileName: "Raspored ciscenja snijega",
              headerStyle: {
                backgroundColor: '#E86A22',
                color: '#FFF'
              },
              maxBodyHeight: 600,
              pageSize: 10,
              pageSizeOptions: [10, 20, 50, 100]
            }}
            data={this.state.schedule}
            actions={[
              {
                icon: () => <Icon name="users" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Popis stanara',
                onClick: (event, rowData) => {
                  this.setState({ selectedId: rowData.id })
                  this.setState({ selectedPeriod: rowData.period })
                  this.toggleTenantsPopup(rowData.id);


                }
              },

              {
                icon: () => {
                  return (
                    <Icon size="small" name='calendar alternate' />
                  );
                },
                tooltip: 'Generiraj raspored',
                isFreeAction: true,
                hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                onClick: () => {
                  this.setState({ generateAlert: true });

                },
              },

              {
                icon: () => {
                  return (
                    <Icon size="small" name='exchange' />
                  );
                },
                tooltip: 'Zamijeni stanove',
                isFreeAction: true,
                hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                onClick: () => this.setState({ changePopup: true }),

              },
            ]}

          />
        </Segment>

      </div>



    );
  }

}

SnowScheduleTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SnowScheduleTable);