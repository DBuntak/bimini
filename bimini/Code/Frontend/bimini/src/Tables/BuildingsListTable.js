import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Redirect } from 'react-router-dom';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Select } from 'semantic-ui-react';
import {sortData, parseBuildings, deRefManagementType} from '../Parser'

const styles = theme => ({
  
})

const buildingTypeOptions = [
  { key: 'ps', value: 'PREDSTAVNIK', text: 'Predstavnik' },
  { key: 'sv', value: 'STAMBENO VIJEĆE', text: 'Stambeno vijeće' },
]

const columns = () => [
  { title: 'Identifikator', field: 'id', editable:"never" },
  { title: 'Ime', field: 'name'},
  { title: 'Adresa', field: 'address' },
  { title: 'Tip predstavljanja', field: 'managamentType' },
 
]
  

class BuildingsListTable extends Component{


  constructor(props) {
    super(props);

    this.state={
        token: '',
        error: false,
        buildings:[],
        deletePopup: false,
        addPopup: false,
        editPopup: false,
        newBuildingName: '',
        newBuildingAddress: '',
        newBuildingType: '',
        editBuildingName: '',
        editBuildingAddress: '',
        editBuildingType: '',
        loading: true,
        messageText: '',
        messageAlert: false,
        messageColor: "danger",
   
    };
      
  };

    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
      };

    parseFetchResponse = response => response.json().then(text => ({
      json: text,
      meta: response,
    }));


    componentDidMount(){
      const token = cookie.load('token');
      this.setState({token: token});
        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/building', options)
        .then(response => {
          if (response.ok) {
            response.json()
            .then(buildings=>{
              parseBuildings(buildings)
              this.setState({buildings:sortData(buildings)})
             })
          } else {
            this.setState({error: true})
          }
        })
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
      
    };


    addNewBuilding=()=>{

        this.setState({loading:true})
        
        const data={
          name: this.state.newBuildingName,
          address: this.state.newBuildingAddress,
          managamentType: deRefManagementType(this.state.newBuildingType),
        };
        const options={
            method:'POST',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        return fetch('/building',options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno stvorena zgrada!"})
              this.cancelAddPopUp();
              this.clearTextFields();
              this.componentDidMount();
            

            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno stvorena zgrada!"})
              this.cancelAddPopUp();
              this.componentDidMount();
              this.clearTextFields();
                       
              
            }
          })
          .then(
            setTimeout(() => {
              this.setState({
              loading: false
            })
            }, 500)
          )
        
    };

    editBuilding=()=>{

      this.setState({loading:true})

        const data={
          name: this.state.editBuildingName,
          address: this.state.editBuildingAddress,
          id: this.state.buildingId,
          managamentType: deRefManagementType(this.state.editBuildingType),
        };
        console.log(data)
        const options={
            method:'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        fetch('/building/'+this.state.buildingId,options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno izmjenjeni podaci!"})
              this.cancelEditPopUp();
              this.clearTextFields();
              this.componentDidMount();
            
            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno izmjenjeni podaci!"})
              this.cancelEditPopUp();
              this.clearTextFields();  
              this.componentDidMount();
            }
          })
          


    }

   
    deleteBuilding=()=>{  
       this.setState({loading:true}) 
        const options={
          method:'DELETE',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        }
        };
        return fetch('/building/'+this.state.buildingId, options)
        .then(response => {
          if(response.status === 200){
            this.setState({messageAlert: true}, ()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"success"});
            this.setState({messageText: "Uspješno obrisana zgrada!"})
            this.cancelDeletePopUp();
            this.componentDidMount();

          }
          else{
            this.setState({messageAlert: true}  ,()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"danger"});
            this.setState({messageText: "Neuspješno obrisana zgrada!"})
            this.cancelDeletePopUp();
            this.componentDidMount();
          
          }
        })  
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
    };

    clearTextFields=()=>{
      this.setState({newBuildingName: ''});
      this.setState({newBuildingAddress: ''});
      this.setState({newBuildingType: ''});
      this.setState({editBuildingName: ''});
      this.setState({editBuildingAddress: ''});
      this.setState({editBuildingType: ''});
    }

    toogleDeletePopUp=()=>{
      this.setState({deletePopup: true})
    }

    cancelDeletePopUp=()=>{
      this.setState({deletePopup: false})
    }

    toogleAddPopUp=()=>{
      this.setState({addPopup: true})
    }

    cancelAddPopUp=()=>{
      this.setState({addPopup: false})
    }

    toogleEditPopUp=()=>{
      this.setState({editPopup: true})
    }

    cancelEditPopUp=()=>{
      this.setState({editPopup: false})
    }

    dismissAlert=()=>{
      this.setState({messageAlert: false})
    }

    chooseBuilding=(buildingId, buildingName)=>{
      cookie.save('buildingId', buildingId, { path: '/' });
      cookie.save('buildingName', buildingName, { path: '/' });
      window.location.reload();
    };

    checkAuthorization = () => {
      if(this.state.error){
        return <Redirect to='/error'></Redirect>
      }
    }

    handleSelectChange = (e, { value }) => {
      this.setState({newBuildingType: value }) 
    }

    handleEditSelectChange = (e, { value }) => {
      this.setState({editBuildingType: value }) 
    }

 
    tableIcons = {
      Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}  />),
      Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
      Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Delete: forwardRef((props, ref) => <Icon name="trash" style={{color: '#E86A22'}} />),
      DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
      Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
      Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
      ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
      ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
      body: {
          emptyDataSourceMessage: 'Nema podataka za prikazati',
          editRow: {
            deleteText: 'Jeste li sigurni?',
            saveTooltip: 'Potvrdi',
            cancelTooltip: 'Odustani'
          },
          addTooltip:'Dodaj',
          deleteTooltip: 'Obriši',
          editTooltip: 'Izmijeni',
      },
      header:{
        actions: 'Akcije'
      },
      pagination:{
        firstTooltip: 'Prva stranica',
        previousTooltip: 'Prethodna stranica',
        nextTooltip:'Iduća stranica',
        lastTooltip:'Posljednja stranica',
        labelRowsSelect:'redaka',
      },
      toolbar:{
        searchTooltip:'Pretraži',
        searchPlaceholder:'Pretraži',
        nRowsSelected:'{0} redaka odabrano',
        exportTitle:'Izvezi',
        exportName:'Izvezi kao .csv',
      }
  }
    


    render(){
        const { classes } = this.props;
       
        return (
          
          
          <div  >
            {this.checkAuthorization()}
             <Modal isOpen={this.state.addPopup} centered={true} >
              <ModalHeader toggle={this.cancelAddPopUp}><h4>Unesite matične podatke o zgradi</h4></ModalHeader>
              <ModalBody>
                      
                      <Grid columns={2} relaxed='very' stackable>
                      <Grid.Column verticalAlign='middle'>
                        <h6>Upišite podatke o zgradi</h6>
                        <br></br>
                        <Input placeholder='Ime zgrade' value={this.state.newBuildingName} onChange={this.handleChange('newBuildingName')} /><br></br><br></br>
                        <Input placeholder='Adresa zgrade' value={this.state.newBuildingAddress} onChange={this.handleChange('newBuildingAddress')} /><br></br><br></br>
                        <Select placeholder='Odaberi tip predstavljanja' options={buildingTypeOptions} value={this.state.newBuildingType}  onChange={this.handleSelectChange}  />
                        </Grid.Column>
                        
                        <Grid.Column verticalAlign='middle'>
                          <h6>Prenesite datoteku sa podacima o zgradi</h6><br></br><br></br>
                          <Button content='Prenesi' icon='upload' size='medium' color="orange" />
                        </Grid.Column>
                      </Grid>
                      <Divider vertical>ILI</Divider>
                      
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.addNewBuilding}>Dodaj</Button>{' '}
               
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.editPopup} centered={true} >
              <ModalHeader toggle={this.cancelEditPopUp}><h4>Unesite matične podatke o zgradi</h4></ModalHeader>
              <ModalBody>

                        <h6>Upišite nove podatke o zgradi</h6><br></br>
                        <div>
                        <h6>Ime zgrade:</h6>
                        <Input placeholder='Ime zgrade' value={this.state.editBuildingName} onChange={this.handleChange('editBuildingName')} /><br></br><br></br>
                        </div>
                        <div>
                        <h6>Adresa zgrade:</h6>
                        <Input placeholder='Adresa zgrade' value={this.state.editBuildingAddress} onChange={this.handleChange('editBuildingAddress')} /><br></br><br></br>
                        </div>
                        <div>
                        <h6>Tip predstavljanja:</h6>
                        <Select placeholder='Odaberi tip predstavljanja' options={buildingTypeOptions} value={this.state.editBuildingType} onChange={this.handleEditSelectChange}/>
                        </div>

              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.editBuilding}>Izmijeni</Button>{' '}
                
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.deletePopup}  >
              <ModalBody>
                Jeste li sigurni da želite obrisati zgradu?
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.deleteBuilding}>Izbriši</Button>{' '}
                <Button  size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
              </ModalFooter>
            </Modal>

            <Segment  >
            <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
            </Dimmer>

              <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                <h5>{this.state.messageText}</h5>
              </Alert>

              <MaterialTable  
                icons={this.tableIcons}
                title="Popis zgrada"
                localization={this.tableTextLocalization}
                columns={columns()}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: { pdf: false, csv: true} ,
                  headerStyle: {
                    backgroundColor: '#E86A22',
                    color: '#FFF'
                  },
                  maxBodyHeight:600,
                  pageSize:10,
                    pageSizeOptions:[10,20,50,100]
                }}
                data={this.state.buildings}
                actions={[
                  {
                    icon: () =>{
                    return(
                      <Icon size="small" name='add' />
                    );
                    },
                    tooltip: 'Dodaj zgradu',
                    isFreeAction: true,
                    onClick: () => this.setState({addPopup:true}),
                
                  },

                  {
                    icon:() => <Icon name="hand point up" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Odaberi zgradu',
                    onClick: (event, rowData) => this.chooseBuilding(rowData.id, rowData.name),
                  },

                  {
                    icon:() => <Icon name="pencil" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Izmjeni podatke',
                    onClick: (event, rowData) =>  {
                      this.setState({buildingId: rowData.id});
                      this.setState({editBuildingName: rowData.name});
                      this.setState({editBuildingAddress: rowData.address});
                      this.setState({editBuildingType: rowData.managamentType});
                      this.setState({editPopup:true});
                    }
                  },
                  
                  rowData => ({
                    icon: () => <Icon name="trash" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Obriši zgradu',
                    onClick: () => { 
                    this.setState({buildingId : rowData.id});
                    this.toogleDeletePopUp();
                  }
                    
                  })
                

                
                ]}
             
              />
         
              </Segment>
         
          </div>
          
            
          
          ); 
    }
  }
    


BuildingsListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BuildingsListTable);