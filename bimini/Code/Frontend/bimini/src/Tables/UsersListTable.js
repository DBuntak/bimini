import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Select } from 'semantic-ui-react';
import {sortData, parseApartments} from '../Parser'

const styles = theme => ({
  
})
  

class UsersListTable extends Component{


  constructor(props) {
    super(props);

    this.state={
        token: '',
        username: '',
        users:[],
        deletePopup: false,
        addPopup: false,
        editPopup: false,
        buildingId: '',
        newUserUsername: '',
        newUserPassword: '',
        newUserApartmentId: '',
        editUserUsername: '',
        editUserPassword: '',
        editUserApartmentId: '',
        apartments: [],
        loading: true,
        messageText: '',
        messageAlert: false,
        messageColor: "danger",
        columns: [
          { title: 'Identifikator', field: 'id', editable:"never" },
          { title: 'Korisničko ime', field: 'username'},
          { title: 'Ime stana', field: 'apartment.name' },
          { title: 'Broj stana', field: 'apartment.number' },
          { title: 'Id stana', field: 'apartment.id', hidden: true }
        ]
    };
      
  };

    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
      };


    componentDidMount(){

      const token = cookie.load('token');
      this.setState({token: token});
      const buildingId = cookie.load('buildingId');
      this.setState({buildingId: buildingId});
      const username = cookie.load('userName');
      this.setState({username: username});
        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/user/admin/building/'+buildingId, options)
        .then(data=>data.json())
        .then(users=>this.setState({users:users}))
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
        
        
      
    };

    parseFetchResponse = response => response.json().then(text => ({
      json: text,
      meta: response,
    }));

    addNewUser=()=>{

        this.setState({loading:true})

        const data={
          username: this.state.newUserUsername,
          password: this.state.newUserPassword,
          apartmentId: this.state.newUserApartmentId,
          buildingId: this.state.buildingId
        };
        const options={
            method:'POST',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        var endpoint = "";
        if(this.state.newUserApartmentId === '' || this.state.newUserApartmentId === 99999 || this.state.newUserApartmentId === null){
          endpoint = "/user/register/admin"
        }
        else {
          endpoint = "/user/register/tenant"
        }
        return fetch(endpoint,options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno stvoren korisnik!"})
              this.cancelAddPopUp();
              this.clearTextFields();
              this.componentDidMount();
            

            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno stvoren korisnik!"})
              this.cancelAddPopUp();
              this.componentDidMount();
              this.clearTextFields();
                       
              
            }
          })
          .then(
            setTimeout(() => {
              this.setState({
              loading: false
            })
            }, 500)
          )
        
    };

    editUser=()=>{

      this.setState({loading:true})

        const data={
            username: this.state.editUserUsername,
            password: this.state.editUserPassword,
            apartmentId: this.state.editUserApartmentId,
            buildingId: this.state.buildingId
        };
        const options={
            method:'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        var endpoint = "";
        if(this.state.editUserApartmentId === '' || this.state.editUserApartmentId === 99999 || this.state.editUserApartmentId === null){
          endpoint = "/user/"+this.state.userId
        }
        else {
          endpoint = "/user/tenant/"+this.state.userId
        }
        fetch(endpoint,options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno izmjenjeni podaci!"})
              this.cancelEditPopUp();
              this.clearTextFields();
              this.componentDidMount();
            
            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno izmjenjeni podaci!"})
              this.cancelEditPopUp();
              this.clearTextFields();  
              this.componentDidMount();
            }
          })
          


    }

   
    deleteUser=()=>{  
       this.setState({loading:true}) 
        const options={
          method:'DELETE',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        }
        };
        return fetch('/user/'+this.state.userId, options)
        .then(response => {
          if(response.status === 200){
            this.setState({messageAlert: true}, ()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"success"});
            this.setState({messageText: "Uspješno obrisan korisnik!"})
            this.cancelDeletePopUp();
            this.componentDidMount();

          }
          else{
            this.setState({messageAlert: true}  ,()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"danger"});
            this.setState({messageText: "Neuspješno obrisan korisnik!"})
            this.cancelDeletePopUp();
            this.componentDidMount();
          
          }
        })  
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
    };

    getApartments=()=>{
        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/building/'+this.state.buildingId+'/apartments', options)
        .then(data=>data.json())
        .then(apartments=>this.setState({apartments:parseApartments(apartments)}))
      }

    clearTextFields=()=>{
      this.setState({newUserUsername: ''});
      this.setState({newUserPassword: ''});
      this.setState({newUserApartmentId: ''});
      this.setState({editUserUsername: ''});
      this.setState({editUserPassword: ''});
      this.setState({editUserApartmentId: ''});
    }

    toogleDeletePopUp=()=>{
      this.setState({deletePopup: true})
    }

    cancelDeletePopUp=()=>{
      this.setState({deletePopup: false})
    }

    toogleAddPopUp=()=>{
      this.setState({addPopup: true})
      this.getApartments();
    }

    cancelAddPopUp=()=>{
      this.setState({addPopup: false})
    }

    toogleEditPopUp=()=>{
      this.setState({editPopup: true})
      this.getApartments();
    }

    cancelEditPopUp=()=>{
      this.setState({editPopup: false})
    }

    dismissAlert=()=>{
      this.setState({messageAlert: false})
    }

    handleChangeDropdownAdd = (e, { value }) => this.setState({ newUserApartmentId: value })

    handleChangeDropdownEdit = (e, { value }) => this.setState({ editUserApartmentId: value })

 
    tableIcons = {
      Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}  />),
      Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
      Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Delete: forwardRef((props, ref) => <Icon name="trash" style={{color: '#E86A22'}} />),
      DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
      Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
      Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
      ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
      ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
      body: {
          emptyDataSourceMessage: 'Nema podataka za prikazati',
          editRow: {
            deleteText: 'Jeste li sigurni?',
            saveTooltip: 'Potvrdi',
            cancelTooltip: 'Odustani'
          },
          addTooltip:'Dodaj',
          deleteTooltip: 'Obriši',
          editTooltip: 'Izmijeni',
      },
      header:{
        actions: 'Akcije'
      },
      pagination:{
        firstTooltip: 'Prva stranica',
        previousTooltip: 'Prethodna stranica',
        nextTooltip:'Iduća stranica',
        lastTooltip:'Posljednja stranica',
        labelRowsSelect:'redaka',
      },
      toolbar:{
        searchTooltip:'Pretraži',
        searchPlaceholder:'Pretraži',
        nRowsSelected:'{0} redaka odabrano',
        exportTitle:'Izvezi',
        exportName:'Izvezi kao .csv',
      }
  }
    


    render(){
        const { classes } = this.props;
       
        return (
          
          
          <div  >
             <Modal isOpen={this.state.addPopup} centered={true} >
              <ModalHeader toggle={this.cancelAddPopUp}><h4>Unesite podatke o korisniku</h4></ModalHeader>
              <ModalBody>
                      
                      <Grid columns={1} relaxed='very' stackable>
                      <Grid.Column verticalAlign='middle'>
                        <h6>Upišite podatke o korisniku</h6>
                        <br></br>
                        <Input placeholder='Korisničko ime' value={this.state.newUserUsername} onChange={this.handleChange('newUserUsername')} /><br></br><br></br>
                        <Input placeholder='Lozinka' value={this.state.newUserPassword} onChange={this.handleChange('newUserPassword')} />
                        <br></br><br></br>
                        <Select placeholder='Odaberi stan' options={this.state.apartments} onChange={this.handleChangeDropdownAdd}/>
                        </Grid.Column>
                      </Grid>
                      
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.addNewUser}>Dodaj</Button>{' '}
               
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.editPopup} centered={true} >
              <ModalHeader toggle={this.cancelEditPopUp}><h4>Unesite podatke o korisniku</h4></ModalHeader>
              <ModalBody>

                        <h6>Upišite nove podatke o korisniku</h6><br></br>
                        <div>
                        <h6>Korisničko ime:</h6>
                        <Input placeholder='Korisničko ime' value={this.state.editUserUsername} onChange={this.handleChange('editUserUsername')} /><br></br><br></br>
                        </div>
                        <div>
                        <h6>Lozinka:</h6>
                        <Input placeholder='Lozinka' value={this.state.editUserPassword} onChange={this.handleChange('editUserPassword')} />
                        </div>
                        {this.state.editUserApartmentId === null ? <div></div> : <div> <br></br>
                          <h6>Odaberi stan:</h6>
                        <Select placeholder='Odaberi stan' options={this.state.apartments} onChange={this.handleChangeDropdownEdit}/>
                        </div>
                      }
                    
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.editUser}>Izmijeni</Button>{' '}
                
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.deletePopup}  >
              <ModalBody>
                Jeste li sigurni da želite obrisati korisnika?
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.deleteUser}>Izbriši</Button>{' '}
                <Button  size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
              </ModalFooter>
            </Modal>

            <Segment  >
            <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
            </Dimmer>

              <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                <h5>{this.state.messageText}</h5>
              </Alert>

              <MaterialTable  
                icons={this.tableIcons}
                title="Popis korisnika"
                localization={this.tableTextLocalization}
                columns={this.state.columns}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: { pdf: false, csv: true} ,
                  headerStyle: {
                    backgroundColor: '#E86A22',
                    color: '#FFF'
                  },
                  maxBodyHeight:600,
                  pageSize:10,
                    pageSizeOptions:[10,20,50,100]
                }}
                data={this.state.users}
                actions={[
                  {
                    icon: () =>{
                    return(
                      <Icon size="small" name='add circle' />
                    );
                    },
                    tooltip: 'Dodaj korisnika',
                    isFreeAction: true,
                    onClick: () => this.toogleAddPopUp(),
                
                  },

                  rowData => ({
                    icon:() => <Icon name="pencil" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Izmjeni podatke',
                    hidden: this.state.username === rowData.username ? true : false,
                    onClick: (event, rowData) =>  {
                      this.setState({userId: rowData.id});
                      this.setState({editUserUsername: rowData.username});
                      this.setState({editUserApartmentId: rowData.apartment !== null ? rowData.apartment.id : null});
                      this.toogleEditPopUp()
                    }
                  }),
                  
                  rowData => ({
                    icon: () => <Icon name="trash" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Obriši korisnika',
                    hidden: this.state.username === rowData.username ? true : false,
                    onClick: () => { 
                    this.setState({userId : rowData.id});
                    this.toogleDeletePopUp();
                  }
                    
                  })
                

                
                ]}
             
              />
         
              </Segment>
         
          </div>
          
            
          
          ); 
    }
  }
    


UsersListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UsersListTable);