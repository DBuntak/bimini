import { green, red } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Icon, Loader, Segment, Input, Dropdown, Item, Divider, Checkbox, TextArea, Form, Select } from 'semantic-ui-react';
import { sortData, parseTenants, parseDate,addOneHour } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";


registerLocale("hr", hr);



const styles = theme => ({
  success: {
    backgroundColor: green[600],
  },
  fail: {
    backgroundColor: red[600],
  },
});

const columns = () => [
  { title: 'Identifikator', field: 'id', editable: "never", hidden: true },
  { title: 'Naziv firme', field: 'firm' },
  { title: 'Opis djelatnosti', field: 'description' },
  { title: 'Ime', field: 'name' },
  { title: 'Prezime', field: 'surname' },
  { title: 'Email', field: 'email' },
  { title: 'Telefon', field: 'mobilePhone' },
]

class SupplierListTable extends Component {


  constructor(props) {
    super(props);

    this.state = {
      token: '',
      buildingId: '',
      suppliers: [],
      supplierId: '',
      deletePopup: false,
      newSupplierFirm: '',
      newSupplierDescription: '',
      newSupplierName: '',
      newSupplierSurname: '',
      newSupplierEmail: '',
      newSupplierMobilePhone: '',
      editSupplierFirm: '',
      editSupplierDescription: '',
      editSupplierName: '',
      editSupplierSurname: '',
      editSupplierEmail: '',
      editSupplierMobilePhone: '',
      editPopup: '',
      
      loading: true,
      messageText: '',
      messageAlert: false,
      messageColor: "danger",
    };
  };


  componentDidMount() {

    var buildingId = parseInt(cookie.load("buildingId"));
    this.setState({ buildingId: buildingId });
    const token = cookie.load('token');
    this.setState({ token: token });

    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/supplier/building/' + buildingId, options)
      .then(data => data.json())
      .then(suppliers => {
        this.setState({ suppliers: sortData(suppliers) })
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )

  };

  parseFetchResponse = response => response.json().then(text => ({
    json: text,
    meta: response,
  }));

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };


  cleanTextFields = () => {
    this.setState({ newSupplierFirm: '' });
    this.setState({ newSupplierDescription: '' });

    this.setState({ newSupplierName: '' });
    this.setState({ newSupplierSurname: '' });
    this.setState({ newSupplierEmail: '' });
    this.setState({ newSupplierMobilePhone: '' });


    this.setState({ editSupplierFirm: '' });
    this.setState({ editSupplierDescription: '' });
    this.setState({ editSupplierName: '' });
    this.setState({ editSupplierSurname: '' });
    this.setState({ editSupplierEmail: '' });
    this.setState({ editSupplierMobilePhone: '' });
    this.setState({ supplierId: '' });

  }

  addNewSupplier = () => {
    this.setState({ loading: true })

    const data = {
        buildingId: this.state.buildingId,
        firm: this.state.newSupplierFirm,
        description: this.state.newSupplierDescription,
        name: this.state.newSupplierName,
        surname: this.state.newSupplierSurname,
        email: this.state.newSupplierEmail,
        mobilePhone: this.state.newSupplierMobilePhone,
    };
    const options = {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
    };


    fetch('/supplier', options)
    .then(this.parseFetchResponse)
    .then(({ json, meta }) => {
        if (meta.status !== 200) {
            this.setState({ messageAlert: true }, () => {
                window.setTimeout(() => {
                this.setState({ messageAlert: false })
                }, 6000)
            });
            this.setState({ messageColor: "danger" });
            this.setState({ messageText: "Neuspješno dodan dobavljač!" })
            this.cancelAddSupplierPopUp();
            this.cleanTextFields();
            this.componentDidMount();

        }
        else {
            this.setState({ messageAlert: true }, () => {
                window.setTimeout(() => {
                this.setState({ messageAlert: false })
                }, 6000)
            });
            this.setState({ messageColor: "success" });
            this.setState({ messageText: "Uspješno dodan dobavljač!" })
            this.cancelAddSupplierPopUp();
            this.cleanTextFields();
            this.componentDidMount();
        }
    })
    .then(
        setTimeout(() => {
        this.setState({
            loading: false
        })
        }, 500)
    )
  }

  editSupplier = () => {
    this.setState({ loading: true })
    const data = {
        firm: this.state.editSupplierFirm,
        description: this.state.editSupplierDescription,
        name: this.state.editSupplierName,
        surname: this.state.editSupplierSurname,
        email: this.state.editSupplierEmail,
        mobilePhone: this.state.editSupplierMobilePhone,
    };
    const options = {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    };
    fetch('/supplier/' + this.state.supplierId, options)
      .then(this.parseFetchResponse)
      .then(({ json, meta }) => {
        if (meta.status !== 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno izmijenjeni podaci o dobavljaču!" })
          this.cancelEditPopUp();
          this.cleanTextFields();
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno izmijenjeni podaci o dobavljaču!" })
          this.cancelEditPopUp();
          this.cleanTextFields();
          this.componentDidMount();
        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  };

  deleteSupplier = () => {
    this.setState({ loading: true })
    const options = {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        'Content-Type': 'application/json',
      }
    };
    return fetch('/supplier/' + this.state.supplierId, options)
      .then(response => {
        if (response.status === 200) {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "success" });
          this.setState({ messageText: "Uspješno obrisan dobavljač!" })
          this.cancelDeletePopUp();
          this.componentDidMount();

        }
        else {
          this.setState({ messageAlert: true }, () => {
            window.setTimeout(() => {
              this.setState({ messageAlert: false })
            }, 6000)
          });
          this.setState({ messageColor: "danger" });
          this.setState({ messageText: "Neuspješno obrisan dobavljač!" })
          this.cancelDeletePopUp();
          this.componentDidMount();

        }
      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  }

  toggleDeletePopUp = () => {
    this.setState({ deletePopup: true })
  }

  cancelDeletePopUp = () => {
    this.setState({ deletePopup: false })
  }

  toggleEditPopUp = () => {
    this.setState({ editPopup: true })
  }

  cancelEditPopUp = () => {
    this.setState({ editPopup: false })
    this.cleanTextFields()
  }

  dismissAlert = () => {
    this.setState({ messageAlert: false })
  }

  toogleAddSupplierPopUp = () => {
    this.setState({ addSupplierPopUp: true })
  }

  cancelAddSupplierPopUp = () => {
    this.setState({ addSupplierPopUp: false })
    this.cleanTextFields();
  }


  tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  tableTextLocalization = {
    body: {
      emptyDataSourceMessage: 'Nema podataka za prikazati',
      editRow: {
        deleteText: 'Jeste li sigurni?',
        saveTooltip: 'Potvrdi',
        cancelTooltip: 'Odustani'
      },
      addTooltip: 'Dodaj',
      deleteTooltip: 'Obriši',
      editTooltip: 'Izmijeni',
    },
    header: {
      actions: 'Akcije'
    },
    pagination: {
      firstTooltip: 'Prva stranica',
      previousTooltip: 'Prethodna stranica',
      nextTooltip: 'Iduća stranica',
      lastTooltip: 'Posljednja stranica',
      labelRowsSelect: 'redaka',
    },
    toolbar: {
      searchTooltip: 'Pretraži',
      searchPlaceholder: 'Pretraži',
      nRowsSelected: '{0} redaka odabrano',
      exportTitle: 'Izvezi',
      exportName: 'Izvezi kao .csv',
    }
  }


  render() {
    const { classes } = this.props;

    return (
      <div  >

        <Modal isOpen={this.state.deletePopup}  >
          <ModalBody>
            Jeste li sigurni da želite obrisati dobavljača?
              </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.deleteSupplier}>Izbriši</Button>{' '}
            <Button size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
          </ModalFooter>
        </Modal>


        <Modal isOpen={this.state.addSupplierPopUp} centered={true} >
          <ModalHeader toggle={this.cancelAddSupplierPopUp}><h4>Unesite podatke o dobavljaču</h4></ModalHeader>
          <ModalBody>
            <div>
              <Input placeholder='Naziv firme' value={this.state.newSupplierNumber} onChange={this.handleChange('newSupplierFirm')} /><br></br><br></br>
            </div>
            <div>
              <Input placeholder='Opis djelatnosti' value={this.state.newSupplierNumber} onChange={this.handleChange('newSupplierDescription')} /><br></br><br></br>
            </div>
            <div>
              <Input placeholder='Ime' value={this.state.newSupplierNumber} onChange={this.handleChange('newSupplierName')} /><br></br><br></br>
            </div>
            <div>
              <Input placeholder='Prezime' value={this.state.newSupplierNumber} onChange={this.handleChange('newSupplierSurname')} /><br></br><br></br>
            </div>
            <div>
              <Input placeholder='Email' value={this.state.newSupplierNumber} onChange={this.handleChange('newSupplierEmail')} /><br></br><br></br>
            </div>
            <div>
              <Input placeholder='Telefon' value={this.state.newSupplierNumber} onChange={this.handleChange('newSupplierMobilePhone')} /><br></br><br></br>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.addNewSupplier}>Dodaj</Button>{' '}

          </ModalFooter>
        </Modal>


        <Modal isOpen={this.state.editPopup} centered={true} >
          <ModalHeader toggle={this.cancelEditPopUp}><h5>Promijeni podatke o dobavljaču</h5></ModalHeader>
          <ModalBody>
          <div>
              <h6>Naziv firme:</h6>
              <Input placeholder='Naziv firme' value={this.state.editSupplierFirm} onChange={this.handleChange('editSupplierFirm')} /><br></br><br></br>
            </div>
            <div>
            <h6>Opis djelatnosti:</h6>
              <Input placeholder='Opis djelatnosti' value={this.state.editSupplierDescription} onChange={this.handleChange('editSupplierDescription')} /><br></br><br></br>
            </div>
            <div>
            <h6>Ime:</h6>
              <Input placeholder='Ime' value={this.state.editSupplierName} onChange={this.handleChange('editSupplierName')} /><br></br><br></br>
            </div>
            <div>
            <h6>Prezime:</h6>
              <Input placeholder='Prezime' value={this.state.editSupplierSurname} onChange={this.handleChange('editSupplierSurname')} /><br></br><br></br>
            </div>
            <div>
            <h6>Email:</h6>
              <Input placeholder='Email' value={this.state.editSupplierEmail} onChange={this.handleChange('editSupplierEmail')} /><br></br><br></br>
            </div>
            <div>
            <h6>Telefon:</h6>
              <Input placeholder='Telefon' value={this.state.editSupplierMobilePhone} onChange={this.handleChange('editSupplierMobilePhone')} /><br></br><br></br>
            </div>
            <div>
              <Button size="small" positive onClick={this.editSupplier}>Izmijeni podatke</Button>
            </div>
          </ModalBody>
          <br></br>
          </Modal>

        <Segment  >

          <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
          </Dimmer>

          <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
            <h5>{this.state.messageText}</h5>
          </Alert>


          <MaterialTable
            icons={this.tableIcons}
            title="Popis dobavljača"
            localization={this.tableTextLocalization}
            columns={columns()}
            options={{
              actionsColumnIndex: -1,
              exportButton: { pdf: false, csv: true },
              headerStyle: {
                backgroundColor: '#E86A22',
                color: '#FFF'
              },
              maxBodyHeight: 600,
              pageSize: 10,
              pageSizeOptions: [10, 20, 50, 100]
            }}
            data={this.state.suppliers}
            actions={[
              {
                icon: () => {
                  return (
                    <Icon size="small" name='add' />
                  );
                },
                tooltip: 'Dodaj dobavljača',
                isFreeAction: true,
                onClick: () => this.toogleAddSupplierPopUp(),

              },
              {
                icon: () => <Icon name="pencil" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Izmjeni podatke',
                onClick: (event, rowData) => {
                  this.setState({ supplierId: rowData.id });
                  this.setState({ editSupplierFirm: rowData.firm });
                  this.setState({ editSupplierDescription: rowData.description });
                  this.setState({ editSupplierName: rowData.name });
                  this.setState({ editSupplierSurname: rowData.surname });
                  this.setState({ editSupplierEmail: rowData.email });
                  this.setState({ editSupplierMobilePhone: rowData.mobilePhone });
                  this.toggleEditPopUp()
                }
              },

              rowData => ({
                icon: () => <Icon name="trash" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Obriši dobavljača',
                onClick: () => {
                  this.setState({ supplierId: rowData.id });
                  this.toggleDeletePopUp();
                }
              })

            ]}

          />
        </Segment>

      </div>


    );

  }

}

SupplierListTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SupplierListTable);