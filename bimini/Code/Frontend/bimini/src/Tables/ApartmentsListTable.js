import { withStyles } from '@material-ui/core/styles';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import axios from 'axios';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';
import { Select, Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Label, Checkbox, Dropdown,TextArea, Form } from 'semantic-ui-react';
import {sortData, parseWorkers, parseTenants} from '../Parser'

const styles = theme => ({
  
})

const typeOptions = [
  { key: 'vanjski_vlasnik', value: 'vanjski_vlasnik', text: 'Vanjski vlasnik' },
  { key: 'vlasnik', value: 'vlasnik', text: 'Vlasnik' },
  { key: 'stanar', value: 'stanar', text: 'Stanar' },
  { key: 'glavni_podstanar', value: 'glavni_podstanar', text: 'Glavni podstanar' },
  { key: 'podstanar', value: 'podstanar', text: 'Podstanar' },
]

const roleOptions = [
  { key: 'ništa', value: 'ništa', text: 'Ništa' },
  { key: 'prestavnik', value: 'predstavnik', text: 'Predstavnik' },
  { key: 'stambeno_vijeće', value: 'stambeno_vijeće', text: 'Član stambenog vijeća' },
]


const columns = () => [
  { title: 'Identifikator', field: 'id', editable:"never", width:200, hidden:true },
  { title: 'Naziv stana', field: 'name', width:50+'vh'},
  { title:"Broj stana", field:'number', width:50+'vh'},
  { title:"Kat", field:'floor', width:50+'vh'},
  { title:"Kvadratura (m^2)", field:'quadrature', width:50+'vh'},
  { title:"Udio kvadrature (%)", field:'share', width:50+'vh'},
  { title:"Iznajmljen", field:'rented', width:50+'vh'},
  { title:"Komentar", field:'comment', width:50+'vh'},
  { title:"Dug stana(HRK)", field:'debt', width:50+'vh'},
  
];

const columnsTenants = () => [
  { title: 'Identifikator', field: 'id', editable:"never", width:100, hidden:true },
  { title: 'Ime', field: 'name', width:100},
  { title: 'Prezime', field: 'surname', width:100},
  { title: 'Privatni email', field: 'privateEmail', width:100},
  { title: 'Poslovni email', field: 'workEmail', width:100},
  { title: 'Broj mobitela', field: 'mobilePhone', width:100},
  { title: 'Status', field: 'type', width:100},
  { title: 'Uloga', field: 'role', width:100},
]
  

class ApartmentsListTable extends Component{


  constructor(props) {
    super(props);

    this.state={
        token: '',
        buildingId: '',
        apartments:[],
        addExistingTenants: [],
        addExistingTenantId: '',
        addExistingTenantPopup: false,
        deletePopup: false,
        addTenantPopup: false,
        addApartmentPopup: false,
        deleteTenantPopup: false,
        debtPopup: false,
        debtApartmentName: '',
        contractPopup: false,
        editPopup: false,
        editApartmentName: '',
        editApartmentRented: false,
        editApartmentComment: '',
        editApartmentFloor: null,
        editApartmentNumber: null,
        editApartmentSquare: '',
        editApartmentSquarePerc: '',
        apartmentId: '',
        apartmentName: '',
        isSelectedApartmentRented: '',
        loading: true,
        tenantsListPopup: false,
        tenants: [],
        newTenantName:'',
        newTenantSurname:'',
        newTenantEmail: '',
        newTenantEmailOptional: '',
        newTenantPhone: '',
        newTenantType: '',
        newTenantRole: '',
        optionalMailChecked: false,
        newApartmentName: '',
        newApartmentRented: false,
        newApartmentComment: '',
        newApartmentFloor: null,
        newApartmentNumber: null,
        newApartmentSquare: '',
        newApartmentSquarePerc: '',
        removeTenantId: '',
        messageText: '',
        messageAlert: false,
        messageColor: "danger",
        fileUploadState:"",
        uploadPercentage: 0,
        progressBarHidden : "none",
        fileDownloadState:"",
        newDebtValue: '',
        file: null,
        

       
    };
    this.inputReference = React.createRef();
      
  };

    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
      };

      handleChecked = (name, old) => {
        var newOld = !old;
        this.setState({
          [name]: newOld,
        });
        this.setState({newTenantEmailOptional: ''})
      };

    handleDropdown = (e, result) => {
      const { name, value } = result;
      this.setState({
          [name]: value
        });
    };


    componentDidMount(){
        
        var buildingId = cookie.load("buildingId");
        this.setState({buildingId:buildingId});
        const token = cookie.load('token');
        this.setState({token: token});

        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/building/'+buildingId+"/apartments", options)
        .then(data=>data.json())
        .then(apartments=>{
          
          for(var i = 0; i<apartments.length; ++i){
            
            if(apartments[i].rented === true){
              apartments[i].rented = "DA"
            }
            else{
              apartments[i].rented = "NE"
            }
          }
          this.setState({apartments:sortData(apartments)})
          }
          )
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )     
    };

    parseFetchResponse = response => response.json().then(text => ({
      json: text,
      meta: response,
    }));

    addNewTenant=()=>{
      this.setState({loading: true})
        if(this.state.newTenantRole === "ništa"){
          this.state.newTenantRole = null
        }
        const data={
          name: this.state.newTenantName,
          surname: this.state.newTenantSurname,
          privateEmail: this.state.newTenantEmail,
          workEmail: this.state.newTenantEmailOptional,
          mobilePhone: this.state.newTenantPhone,
          type: this.state.newTenantType,
          role: this.state.newTenantRole,
          apartmentId: this.state.apartmentId
        };
        const options={
            method:'POST',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        fetch('/tenant',options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno dodan stanar!"})
              this.cancelAddTenantPopUp();
              this.cleanTextFields();
              this.componentDidMount();
              
            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno dodan stanar!"})
              this.cancelAddTenantPopUp();
              this.cleanTextFields();
              this.componentDidMount();

              
            }
          })
          .then(
            setTimeout(() => {
              this.setState({
              loading: false
            })
            }, 500)
          )
        
    };

    addNewApartment=()=>{
      this.setState({loading: true})
      const data={
        name: this.state.newApartmentName,
        buildingId: this.state.buildingId,
        rented: this.state.newApartmentRented,
        floor: this.state.newApartmentFloor,
        number: this.state.newApartmentNumber,
        comment: this.state.newApartmentComment,
        quadrature: this.state.newApartmentSquare,
        share: this.state.newApartmentSquarePerc
      };
      const options={
          method:'POST',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        },
          body: JSON.stringify(data)
      };
      fetch('/apartment',options)
      .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          if(meta.status !==200){
            this.setState({messageAlert: true}  ,()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"danger"});
            this.setState({messageText: "Neuspješno dodan stan!"})
            this.cancelAddApartmentPopUp();
            this.cleanTextFields();
            this.componentDidMount();
            
          }
          else{
            this.setState({messageAlert: true}, ()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"success"});
            this.setState({messageText: "Uspješno dodan stan!"})
            this.cancelAddApartmentPopUp();
            this.cleanTextFields();
            this.componentDidMount();            
          }
        })
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
      
  };

  editApartment=()=>{
    this.setState({loading: true})
    if(this.state.editApartmentRented === "DA"){
      this.state.editApartmentRented = true
    }
    else{
      this.state.editApartmentRented = false
    }

    const data={
      name: this.state.editApartmentName,
      id:this.state.apartmentId,
      rented: this.state.editApartmentRented,
      floor: this.state.editApartmentFloor,
      number: this.state.editApartmentNumber,
      comment: this.state.editApartmentComment,
      quadrature: this.state.editApartmentSquare,
      share: this.state.editApartmentSquarePerc
    };
    const options={
        method:'PUT',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
      },
        body: JSON.stringify(data)
    };
    fetch('/apartment/'+this.state.apartmentId,options)
    .then(this.parseFetchResponse)
      .then(({ json, meta }) => {
        if(meta.status !== 200){
          this.setState({messageAlert: true}  ,()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"danger"});
          this.setState({messageText: "Neuspješno izmijenjen stan!"})
          this.cancelEditPopUp();
          this.cleanTextFields();
          this.componentDidMount();
          
        }
        else{
          this.setState({messageAlert: true}, ()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"success"});
          this.setState({messageText: "Uspješno izmijenjen stan!"})
          this.cancelEditPopUp();
          this.cleanTextFields();
          this.componentDidMount();            
        }
      })
      .then(
        setTimeout(() => {
          this.setState({
          loading: false
        })
        }, 500)
      )
    
    };

   
    deleteApartment=()=>{   
      this.setState({loading: true})
        const options={
          method:'DELETE',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        }
        };
        fetch('/apartment/'+this.state.apartmentId, options)
        .then(response => {
          if(response.status === 200){
            this.setState({messageAlert: true}, ()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"success"});
            this.setState({messageText: "Uspješno obrisan stan!"})
            this.cancelDeletePopUp();
            this.componentDidMount();

          }
          else{
            this.setState({messageAlert: true}  ,()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"danger"});
            this.setState({messageText: "Neuspješno obrisan stan!"})
            this.cancelDeletePopUp();
            this.componentDidMount();
          }
        })
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
    };

    removeTenant=()=>{   
      const options={
        method:'POST',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
      }
      };
      return fetch('/tenant/'+this.state.removeTenantId+'/apartment/'+this.state.apartmentId, options)
      .then(response => {
        if(response.status === 200){
          this.setState({messageAlert: true}, ()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"success"});
          this.setState({messageText: "Uspješno uklonjen stanar!"})
          this.cancelTenantsPopup();
          this.cancelDeleteTenantPopUp();
          this.componentDidMount();

        }
        else{
          this.setState({messageAlert: true}  ,()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"danger"});
          this.setState({messageText: "Neuspješno uklonjen stanar!"})
          this.cancelDeleteTenantPopUp();
          this.componentDidMount();
        
        }
      })  
  };

    downloadContract = () => {
      const options={
        method:'GET',
        headers: {
          'Authorization': 'Bearer ' + this.state.token,
      }
      };
        fetch('purchase_contract/download/'+this.state.apartmentId, options)
          .then(response => {
            if(response.headers.get('Content-Disposition')=== null ){
              this.setState({fileDownloadState: "Datoteka nije pronađena!"})
              return
            }
            const filename =  response.headers.get('Content-Disposition').split('filename=')[1];
            response.blob().then(blob => {
              let url = window.URL.createObjectURL(blob);
              let a = document.createElement('a');
              a.href = url;
              a.download = filename;
              a.click();
          });
        });
    }

    addDebt=()=>{

      this.setState({loading:true})

      const data={
        debt: this.state.newDebtValue,
      };
      const options={
          method:'POST',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        },
          body: JSON.stringify(data)
      };
      fetch('/apartment/update/debt/'+this.state.apartmentId,options)
      .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          if(meta.status !== 200){
            this.setState({messageAlert: true}  ,()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"danger"});
            this.setState({messageText: "Neuspješno izmijenjen dug!"})
            this.cancelDebtPopUp();
            this.cleanTextFields();
            this.componentDidMount();
          
          }
          else{
            this.setState({messageAlert: true}, ()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"success"});
            this.setState({messageText: "Uspješno izmijenjen dug!"})
            this.cancelDebtPopUp();
            this.cleanTextFields();  
            this.componentDidMount();
          }
        })

    }

    addExistingTenant=()=>{
      this.setState({loading: true})
      const options={
          method:'POST',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        },
      };
      fetch('/tenant/'+this.state.addExistingTenantId+'/add/apartment/'+this.state.apartmentId,options)
      .then(response => {
        if(response.status === 200){
          this.setState({messageAlert: true}, ()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"success"});
          this.setState({messageText: "Uspješno dodan stanar!"})
          this.cancelAddExistingTenantPopUp();
          this.cancelTenantsPopup();
          this.componentDidMount();
          this.setState({addExistingTenantId: ''})
        }
        else{
          this.setState({messageAlert: true}  ,()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"danger"});
          this.setState({messageText: "Neuspješno dodan stanar!"})
          this.cancelAddExistingTenantPopUp();
          this.cancelTenantsPopup();
          this.componentDidMount();
          this.setState({addExistingTenantId: ''})
        }
      })
    };

    toogleAddExistingTenantPopUp=()=>{
      const options={
        method:'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        },
      };
      fetch('/tenant/not-in-apartment', options)
      .then(data=>data.json())
      .then(tenants=>this.setState({addExistingTenants:parseWorkers(tenants)}))
  
      this.setState({addExistingTenantPopup: true})
    }
  
    cancelAddExistingTenantPopUp=()=>{
      this.setState({addExistingTenantPopup: false})
      this.setState({addExistingTenantId: ''});
    }
  
    handleChangeDropdown = (e, { value }) => {
      this.setState({ addExistingTenantId: value });
    }


    toogleDeletePopUp=()=>{
      this.setState({deletePopup: true})
    }

    cancelDeletePopUp=()=>{
      this.setState({deletePopup: false})
    }

    toogleEditPopUp=()=>{
      this.setState({editPopup: true})
    }

    cancelEditPopUp=()=>{
      this.setState({editPopup: false})
    }

    toogleDebtPopUp=()=>{
      this.setState({debtPopup: true})
    }

    cancelDebtPopUp=()=>{
      this.setState({debtPopup: false})
      this.cleanTextFields();
    }


    toogleDeleteTenantPopUp=(tenantId)=>{
      this.setState({removeTenantId: tenantId});
      this.setState({deleteTenantPopup: true});
    }

    cancelDeleteTenantPopUp=()=>{
      this.setState({deleteTenantPopup: false})
    }

    toogleAddTenantPopUp=()=>{
      this.setState({addTenantPopup: true})
    }

    cancelAddTenantPopUp=()=>{
      this.setState({addTenantPopup: false})
      this.cleanTextFields();
    }

    toogleAddApartmentPopUp=()=>{
      this.setState({addApartmentPopup: true})
    }

    cancelAddApartmentPopUp=()=>{
      this.setState({addApartmentPopup: false})
      this.cleanTextFields();
    }

    toogleContractPopUp=()=>{
      this.setState({contractPopup: true})
    }

    cancelContractPopUp=()=>{
      this.setState({contractPopup: false})
      this.cleanTextFields();
    }

    cancelTenantsPopup=()=>{
      this.setState({tenantsListPopup: false})
    }

    dismissAlert=()=>{
      this.setState({messageAlert: false})
    }   


    cleanTextFields=()=>{
      this.setState({newTenantName: ''})
      this.setState({newTenantSurname: ''})
      this.setState({newTenantEmail: ''})
      this.setState({newTenantPhone: ''})
      this.setState({newTenantType: ''})
      this.setState({newTenantRole: ''})
      this.setState({newTenantEmailOptional: ''})
      this.setState({optionalMailChecked: false})
      this.setState({newApartmentName: ''})
      this.setState({newApartmentNumber: null})
      this.setState({newApartmentFloor: null})
      this.setState({newApartmentComment: ''})
      this.setState({newApartmentRented: false})
      this.setState({newApartmentSquare: ''})
      this.setState({newApartmentSquarePerc: ''})
      this.setState({editApartmentName: ''})
      this.setState({editApartmentNumber: null})
      this.setState({editApartmentFloor: null})
      this.setState({editApartmentComment: ''})
      this.setState({editApartmentSquare: ''})
      this.setState({editApartmentSquarePerc: ''})
      this.setState({editApartmentRented: false})
      this.setState({file: null})
      this.setState({fileUploadState: ''})
      this.setState({uploadPercentage: 0})
      this.setState({progressBarHidden: "none"})
      this.setState({fileDownloadState:""})
      this.setState({newDebtValue: ''})
    }

    toogleTenantsPopup=(apartmentId)=>{
      this.getAllTenants(apartmentId)      
      this.setState({tenantsListPopup: true})
    }

    getAllTenants=(apartmentId)=>{
      const options={
        method:'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        },
      };
      fetch('/apartment/'+apartmentId+"/tenants", options)
      .then(data=>data.json())
      .then(tenants=>{
        parseTenants(tenants)
        this.setState({tenants:sortData(tenants)})
      
      
      })
    }

   

    fileUploadAction = () =>{
      this.setState({progressBarHidden: ""})
      this.inputReference.current.click();
    }

    uploadFile=({target: {files} })=>{
      //console.log(files[0]);
      if(files[0].size > 2000000){
        this.setState({fileUploadState:"Datoteka je prevelika! (Maksimalna veličina 2MB)"})
        return
      }
      this.setState({fileUploadState:"Učitavam..."})
      let data = new FormData();
      data.append("file", files[0]);
      const options={
        onUploadProgress: (progressEvent) =>{
          const {loaded, total} = progressEvent;
          let percent = Math.floor((loaded*100)/total)
          console.log(loaded+" kb of "+total+" kb | "+percent+" %")

          if(percent < 100){
            this.setState({uploadPercentage: percent})
          }
        },
        headers:{
          'Authorization': 'Bearer ' + this.state.token,
        }
      }
      axios.post('purchase_contract/upload/apartment/'+this.state.apartmentId, data, options).then(res=>{
        
        if(res.status != 200){
          this.setState({fileUploadState:"Neuspješno prenesena datoteka!"})
          this.setState({uploadPercentage : 0});
         
        }else{
          this.setState({fileUploadState:"Upješno prenesena datoteka!"})
          this.setState({uploadPercentage : 100}, ()=>{
            setTimeout(()=>{
              this.setState({uploadPercentage: 0})
              this.setState({fileUploadState: ''})
              this.setState({progressBarHidden: "none"})
            }, 4000)
          })
        }
        
      })

    }


 
    tableIcons = {
      Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}  />),
      Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
      Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Delete: forwardRef((props, ref) => <Icon name="trash" style={{color: '#E86A22'}} />),
      DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
      Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
      Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
      ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
      ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
      body: {
          emptyDataSourceMessage: 'Nema podataka za prikazati',
          editRow: {
            deleteText: 'Jeste li sigurni?',
            saveTooltip: 'Potvrdi',
            cancelTooltip: 'Odustani'
          },
          addTooltip:'Dodaj',
          deleteTooltip: 'Obriši',
          editTooltip: 'Izmijeni',
      },
      header:{
        actions: 'Akcije'
      },
      pagination:{
        firstTooltip: 'Prva stranica',
        previousTooltip: 'Prethodna stranica',
        nextTooltip:'Iduća stranica',
        lastTooltip:'Posljednja stranica',
        labelRowsSelect:'redaka',
      },
      toolbar:{
        searchTooltip:'Pretraži',
        searchPlaceholder:'Pretraži',
        nRowsSelected:'{0} redaka odabrano',
        exportTitle:'Izvezi',
        exportName:'Izvezi kao .csv',
      }
  }
    


    render(){
        const { classes } = this.props;
        return (

          <div  >
            <Modal isOpen={this.state.tenantsListPopup} centered={true} size="lg">
              <ModalHeader toggle={this.cancelTenantsPopup}><h4>{"Popis stanara za "+this.state.apartmentName}</h4></ModalHeader>
              {this.state.isSelectedApartmentRented === "DA" ? 
                <Alert color="dark">Stan je iznajmljen!</Alert>
                : <div></div>}
              <ModalBody>
              <MaterialTable  
                icons={this.tableIcons}
                title="Lista stanara"
                localization={this.tableTextLocalization}
                columns={columnsTenants()}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: { pdf: false, csv: true} ,
                  headerStyle: {
                    backgroundColor: '#E86A22',
                    color: '#FFF'
                  },
                  maxBodyHeight:600,
                  pageSize:10,
                    pageSizeOptions:[10,20,50,100]
                }}
                data={this.state.tenants} 
                actions={[

                 

                  {
                    icon:() => <Icon name="user delete" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Ukloni stanara',
                    onClick: (event, rowData) =>{
                      
                      this.toogleDeleteTenantPopUp(rowData.id);
                      
                    }
                  },      
                ]}            
              /> 
              </ModalBody>
            </Modal>

            <Modal isOpen={this.state.addExistingTenantPopup} centered={true} >
              <ModalHeader toggle={this.cancelAddExistingTenantPopUp} ><h4>Odaberite stan</h4></ModalHeader>
              <ModalBody>
                <div>

                 <Select placeholder='Odaberi stanara' options={this.state.addExistingTenants} onChange={this.handleChangeDropdown}/>
                 <br></br>
                 <br></br>
               
                </div>
                           
              </ModalBody>
              <ModalFooter>
               <Button  size="small" positive onClick={this.addExistingTenant}>Dodaj Stanara</Button>
                
              </ModalFooter>
            </Modal>

             <Modal isOpen={this.state.addTenantPopup} centered={true} >
              <ModalHeader toggle={this.cancelAddTenantPopUp}><h4>Unesite osobne podatke o stanaru</h4></ModalHeader>
              <ModalBody>

                <div>
                <Input placeholder='Ime' value={this.state.newTenantName} onChange={this.handleChange('newTenantName')} /><br></br><br></br>
                </div>
                <div>
                <Input placeholder='Prezime' value={this.state.newTenantSurname} onChange={this.handleChange('newTenantSurname')} /><br></br><br></br>
                </div>
                <div>
                <Input placeholder='Privatni email' value={this.state.newTenantEmail} onChange={this.handleChange('newTenantEmail')} /><br></br><br></br>
                </div>
                <div>
                <Checkbox toggle label='Dodatan email?' checked={this.state.optionalMailChecked} onChange={() => this.handleChecked('optionalMailChecked', this.state.optionalMailChecked)} /><br></br><br></br>
                </div>
                {this.state.optionalMailChecked ?
                 <div><Input placeholder='Poslovni email' value={this.state.newTenantEmailOptional} onChange={this.handleChange('newTenantEmailOptional')} /><br></br><br></br></div>
                : <div></div>
                }
                <div>
                <Input placeholder='Broj telefona' value={this.state.newTenantPhone} onChange={this.handleChange('newTenantPhone')} /><br></br><br></br>
                </div>
                <div>
                <Dropdown
                  placeholder='Tip stanara'
                  name="newTenantType"
                  value={this.state.newTenantType}
                  fluid
                  search
                  selection
                  options={typeOptions}
                  onChange={this.handleDropdown}
                /><br></br>
                </div>
                <div>
                <Dropdown
                  placeholder='Uloga stanara'
                  name="newTenantRole"
                  value={this.state.newTenantRole}
                  fluid
                  search
                  selection
                  options={roleOptions}
                  onChange={this.handleDropdown}
                /><br></br>
                </div>
                                          
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.addNewTenant}>Dodaj</Button>{' '}
               
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.addApartmentPopup} centered={true} >
              <ModalHeader toggle={this.cancelAddApartmentPopUp}><h4>Unesite podatke o stanu</h4></ModalHeader>
              <ModalBody>
                <div>
                <Input placeholder='Naziv stana' value={this.state.newApartmentName} onChange={this.handleChange('newApartmentName')} /><br></br><br></br>
                </div>
                <div>
                <Input placeholder='Kat stana' value={this.state.newApartmentFloor} onChange={this.handleChange('newApartmentFloor')} /><br></br><br></br>
                </div>
                <div>
                <Input placeholder='Broj stana' value={this.state.newApartmentNumber} onChange={this.handleChange('newApartmentNumber')} /><br></br><br></br>
                </div>
                <div>
                <Input placeholder='Kvadratura' value={this.state.newApartmentSquare} onChange={this.handleChange('newApartmentSquare')} label={{ basic: true, content: <p>m<sup>2</sup></p> }} labelPosition='right'/><br></br><br></br>
                </div>
                <div>
                <Input placeholder='Udio kvadrature' value={this.state.newApartmentSquarePerc} onChange={this.handleChange('newApartmentSquarePerc')} label={{ basic: true, content:'%' }} labelPosition='right'/><br></br><br></br>
                </div>
                <div>
                <Checkbox toggle label='Iznajmljen' checked={this.state.newApartmentRented} onChange={() => this.handleChecked('newApartmentRented', this.state.newApartmentRented)} /><br></br><br></br>
                </div>
                <div>
                <Form><TextArea style={{width:60+'vh'}}  placeholder='Unesite komentar' value={this.state.newApartmentComment} onChange={this.handleChange('newApartmentComment')}/></Form>
                </div>
                           
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.addNewApartment}>Dodaj</Button>{' '}
               
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.editPopup} centered={true} >
              <ModalHeader toggle={this.cancelEditPopUp}><h4>Unesite podatke o stanu</h4></ModalHeader>
              <ModalBody>
                <div>
                <h6>Naziv stana:</h6>
                <Input placeholder='Naziv stana' value={this.state.editApartmentName} onChange={this.handleChange('editApartmentName')} /><br></br><br></br>
                </div>
                <div>
                <h6>Kat stana:</h6>
                <Input placeholder='Kat stana' value={this.state.editApartmentFloor} onChange={this.handleChange('editApartmentFloor')} /><br></br><br></br>
                </div>
                <div>
                <h6>Broj stana:</h6>
                <Input placeholder='Broj stana' value={this.state.editApartmentNumber} onChange={this.handleChange('editApartmentNumber')} /><br></br><br></br>
                </div>
                <div>
                <h6>Kvadratura:</h6>
                <Input placeholder='Kvadratura' value={this.state.editApartmentSquare} onChange={this.handleChange('editApartmentSquare')}  label={{ basic: true, content: <p>m<sup>2</sup></p> }} labelPosition='right'/><br></br><br></br>
                </div>
                <div>
                <h6>Udio kvadrature:</h6>
                <Input placeholder='Udio kvadrature' value={this.state.editApartmentSquarePerc} onChange={this.handleChange('editApartmentSquarePerc')}  label={{ basic: true, content: '%' }} labelPosition='right'/><br></br><br></br>
                </div>
                <div style={{display:'flex', 'flex-direction':'row'}}>
                <h6 style={{'margin-right': 3+'vh'}}>Iznajmljen:</h6>
                <Checkbox toggle  checked={this.state.editApartmentRented} onChange={() => this.handleChecked('editApartmentRented', this.state.editApartmentRented)} /><br></br><br></br>
                </div>
                <div >
                <h6>Komentar:</h6>
                <Form><TextArea style={{width:60+'vh'}}  placeholder='Unesite komentar' value={this.state.editApartmentComment} onChange={this.handleChange('editApartmentComment')}/></Form>
                </div>
                 
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.editApartment}>Izmijeni</Button>{' '}
               
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.debtPopup} centered={true} >
              <ModalHeader toggle={this.cancelDebtPopUp}><h4>Unesite nove podatke o dugu</h4></ModalHeader>
              <ModalBody>

                        <h6>Dug za stan: {this.state.debtApartmentName}</h6><br></br>
                        
                        <div>
                        <Input labelPosition='right' type='text' placeholder='Dug stana' value={this.state.newDebtValue} onChange={this.handleChange('newDebtValue')}>
                          <Label basic>HRK</Label>
                          <input />
                          <Label>.00</Label>
                        </Input>
                      
                        </div>
                      
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.addDebt}>Izmijeni</Button>
                
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.contractPopup} centered={true} >
              <ModalHeader toggle={this.cancelContractPopUp}><h4>Vlasnički list</h4></ModalHeader>
              <ModalBody>

              <Segment placeholder>
                <Grid columns={2} relaxed='very' stackable>
                <Grid.Column>
                    <Button content='Preuzmi' icon='download' size='huge' color="orange"  style={{borderRadius: 30}} onClick={this.downloadContract}/>
                    <center>{this.state.fileDownloadState}</center>
                </Grid.Column>

                <Grid.Column verticalAlign='middle'>
                
                <div>
                <input type="file" hidden ref={this.inputReference} onChange={this.uploadFile} />
                <Button content='Prenesi' icon="upload" className="ui button" onClick={this.fileUploadAction} color="orange" size='huge'  style={{borderRadius: 30}} />
                </div>

                <br></br>
                <div  style={{display: this.state.progressBarHidden}}>
                <Progress color="success"   animated  value={this.state.uploadPercentage} />
                <center>{this.state.fileUploadState}</center>
                </div>
           
                </Grid.Column>
                </Grid>
                

                <Divider vertical>ILI</Divider>
              </Segment>
              
                
                           
              </ModalBody>
              
            
            </Modal>

            <Modal isOpen={this.state.deletePopup}  >
              <ModalBody>
                Jeste li sigurni da želite obrisati stan?
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.deleteApartment}>Izbriši</Button>{' '}
                <Button  size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.deleteTenantPopup}  >
              <ModalBody>
                Jeste li sigurni da želite ukloniti stanara iz stana?
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.removeTenant}>Ukloni</Button>{' '}
                <Button  size="small" negative onClick={this.cancelDeleteTenantPopUp}>Odustani</Button>
              </ModalFooter>
            </Modal>

            <Segment  >
              
              <Dimmer active={this.state.loading} inverted>
              <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
              </Dimmer>

              <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                <h5>{this.state.messageText}</h5>
              </Alert>

              <MaterialTable  
                icons={this.tableIcons}
                title="Popis stanova"
                localization={this.tableTextLocalization}
                columns={columns()}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: { pdf: false, csv: true} ,
                  headerStyle: {
                    backgroundColor: '#E86A22',
                    color: '#FFF'
                  },
                  maxBodyHeight:600,
                  pageSize:10,
                    pageSizeOptions:[10,20,50,100]
                }}
                data={this.state.apartments}
                actions={[
                  {
                    icon: () =>{
                    return(
                      <Icon size="small" name='add' />
                    );
                    },
                    tooltip: 'Dodaj stan',
                    isFreeAction: true,
                    onClick: () => this.toogleAddApartmentPopUp(),
                
                  },


                  {
                    icon:() => <Icon name="users" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Popis stanara',
                    onClick: (event, rowData) =>{
                      this.setState({apartmentId: rowData.id});
                      this.setState({apartmentName: rowData.name});
                      this.setState({isSelectedApartmentRented: rowData.rented});
                      this.toogleTenantsPopup(rowData.id);
                      
                    }
                  },
                  {
                    icon:() => <Icon name="add user" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Dodaj stanara',
                    onClick: (event, rowData) =>{
                      this.setState({apartmentId : rowData.id});
                      this.toogleAddTenantPopUp(rowData.id);
                      
                    }
                  },
                  {
                    icon:() => <Icon name="pencil" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Izmjeni podatke',
                    onClick: (event, rowData) =>  {
                      this.setState({apartmentId: rowData.id});
                      this.setState({editApartmentName: rowData.name});
                      this.setState({editApartmentComment: rowData.comment});
                      this.setState({editApartmentFloor: rowData.floor});
                      this.setState({editApartmentNumber: rowData.number});
                      this.setState({editApartmentRented: rowData.rented});
                      this.setState({editApartmentSquare: rowData.quadrature});
                      this.setState({editApartmentSquarePerc: rowData.share});
                      this.setState({editPopup:true});
                    }
                  },
                  {
                    icon:() => <Icon name="money" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Izmijeni dugovanje',
                    onClick: (event, rowData) =>  {
                      this.setState({apartmentId: rowData.id});
                      this.setState({debtApartmentName: rowData.name})
                      this.setState({newDebtValue: rowData.debt})
                      this.toogleDebtPopUp();
                      
                    }
                  },


                  {
                    icon:() => <Icon name="file pdf" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Vlasnički list',
                    onClick: (event, rowData) =>{
                      this.setState({apartmentId: rowData.id})
                      this.toogleContractPopUp();
                    }
                  },

                  
                  
                  rowData => ({
                    icon: () => <Icon name="trash" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Obriši stan',
                    onClick: () => { 
                    this.setState({apartmentId : rowData.id});
                    this.toogleDeletePopUp();
                  }
                    
                  })
                  
                

                
                ]}
             
              />
              </Segment>
         
          </div>
          
            
          
          ); 
    }

}

ApartmentsListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ApartmentsListTable);