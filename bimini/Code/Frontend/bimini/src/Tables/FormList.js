import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';
import { Header, Button, Dimmer, Icon, Loader, Segment, Input, Dropdown, Item, Divider, Checkbox, TextArea, Form, Select } from 'semantic-ui-react';
import { sortData, parseContracts, makeJSDate } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";
import axios from 'axios';


registerLocale("hr", hr);

const styles = theme => ({

})


class FormList extends Component {


    constructor(props) {
        super(props);

        this.state = {
            token: '',
            role: '',
            messageText: '',
            messageAlert: false,
            messageColor: "danger",

            buildingId: '',
            consentDescription: '',
            consentOptional: false,
            consentOptionalColumn: '',
            referendumQuestion: '',
            referendumColumn1: '',
            referendumColumn2: '',


        };

    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };



    componentDidMount() {
        const token = cookie.load('token');
        this.setState({ token: token });
        const role = cookie.load('role');
        this.setState({ role: role });
        const buildingId = parseInt(cookie.load('buildingId'));
        this.setState({ buildingId: buildingId });

    };



    parseFetchResponse = response => response.json().then(text => ({
        json: text,
        meta: response,
    }));

    handleChecked = (name, old) => {
        var newOld = !old;
        this.setState({
            [name]: newOld,
        });
    };



    generateConsent = () => {
        var opt = 0
        if (this.state.consentOptional === false) {
            opt = 0
        } else {
            opt = 1
        }
        this.setState({ loading: true })
        const data = {
            theme: this.state.consentDescription,
            optional: opt,
            optionalName: this.state.consentOptionalColumn,
            buildingId: this.state.buildingId
        };
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        fetch('/form/consent', options)
            .then(response => {
                if (response.headers.get('Content-Disposition') === null) {
                    this.setState({ fileDownloadState: "Datoteka nije pronađena!" })
                    return
                }
                const filename = response.headers.get('Content-Disposition').split('filename=')[1];
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = filename;
                    a.click();
                });
                this.clearTextFields()
            });


    };

    generateReferendum = () => {
        this.setState({ loading: true })
        const data = {
            theme: this.state.referendumQuestion,
            referendum1: this.state.referendumColumn1,
            referendum2: this.state.referendumColumn2,
            buildingId: this.state.buildingId
        };
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        fetch('/form/referendum', options)
            .then(response => {
                if (response.headers.get('Content-Disposition') === null) {
                    this.setState({ fileDownloadState: "Datoteka nije pronađena!" })
                    return
                }
                const filename = response.headers.get('Content-Disposition').split('filename=')[1];
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = filename;
                    a.click();
                });
                this.clearTextFields()
            });
    };

    clearTextFields = () => {
        this.setState({ consentDescription: '' })
        this.setState({ consentOptional: 0 })
        this.setState({ consentOptionalColumn: '' })
        this.setState({ referendumQuestion: '' })
        this.setState({ referendumColumn1: '' })
        this.setState({ referendumColumn2: '' })

    }

    dismissAlert = () => {
        this.setState({ messageAlert: false })
    }

    render() {
        const { classes } = this.props;

        return (
            <div  >

                <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                    <h5>{this.state.messageText}</h5>
                </Alert>
                <br></br>
                <Divider horizontal>
                    <Header as='h3'>
                        Obrazac za suglasnost stanara
                </Header>
                </Divider>
                <br></br>

                <center>
                    <div>
                        <h6>Opis:</h6>
                        <Form><TextArea style={{ width: 60 + 'vh' }} placeholder='Opis' value={this.state.consentDescription} onChange={this.handleChange('consentDescription')} /></Form>
                    </div><br></br>

                    <div>
                        <Checkbox toggle label='Dodatan stupac?' checked={this.state.consentOptional} onChange={() => this.handleChecked('consentOptional', this.state.consentOptional)} /><br></br><br></br>
                    </div>

                    <div>
                        {this.state.consentOptional ?
                            <div>
                                <h6>Naziv dodatnog stupca:</h6>
                                <Input style={{ width: 60 + 'vh' }} placeholder='Naziv stupca' value={this.state.consentOptionalColumn} onChange={this.handleChange('consentOptionalColumn')} /><br></br><br></br>
                            </div> :
                            <div></div>
                        }
                    </div>


                    <div>
                        <Button circular style={{ width: 18 + 'vh', backgroundColor: '#E86A22' }} size="large" positive onClick={this.generateConsent}>Stvori</Button>
                    </div>

                </center>

                <br></br><br></br>
                <Divider horizontal>
                    <Header as='h3'>
                        Obrazac za referendum
                </Header>
                </Divider>
                <br></br>

                <center>
                    <div>
                        <h6>Referendumsko pitanje:</h6>
                        <Form><TextArea style={{ width: 60 + 'vh' }} placeholder='Unesite referendumsko pitanje' value={this.state.referendumQuestion} onChange={this.handleChange('referendumQuestion')} /></Form>
                    </div><br></br>
                    <div>
                        <h6>Naziv prvog stupca:</h6>
                        <Input style={{ width: 60 + 'vh' }} placeholder='Prvi stupac' value={this.state.referendumColumn1} onChange={this.handleChange('referendumColumn1')} /><br></br><br></br>
                    </div>
                    <div>
                        <h6>Naziv drugog stupca:</h6>
                        <Input style={{ width: 60 + 'vh' }} placeholder='Drugi stupac' value={this.state.referendumColumn2} onChange={this.handleChange('referendumColumn2')} /><br></br><br></br>
                    </div>
                    <div>
                        <Button circular style={{ width: 18 + 'vh', backgroundColor: '#E86A22' }} size="large" positive onClick={this.generateReferendum}>Stvori</Button>
                    </div>

                </center>

                <br></br><br></br>


            </div>
        );
    }
}

FormList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormList);