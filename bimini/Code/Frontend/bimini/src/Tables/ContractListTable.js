import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Form, TextArea } from 'semantic-ui-react';
import { sortData, parseContracts, makeJSDate } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";
import axios from 'axios';


registerLocale("hr", hr);

const styles = theme => ({

})

const columns = () => [
    { title: 'Identifikator', field: 'id', editable: "never", width: 200, hidden: true },
    { title: 'Naziv ugovora', field: 'contractName', width: 50 + 'vh' },
    { title: "Broj ugovora", field: 'contractId', width: 50 + 'vh' },
    { title: "Firma", field: 'firm', width: 50 + 'vh' },
    { title: "Datum sklapanja", field: 'conclusionDate', width: 50 + 'vh' },
    { title: "Datum isteka", field: 'expirationDate', width: 50 + 'vh' },
    { title: "Opis", field: 'description', width: 50 + 'vh' },
    { title: "Trajanje", field: 'duration', width: 50 + 'vh', hidden: true },
    { title: "Email", field: 'email', width: 50 + 'vh', hidden: true },
    { title: "Broj telefona", field: 'mobilePhone', width: 50 + 'vh', hidden: true },
    { title: "Broj hitne službe", field: 'emergencyNumber', width: 50 + 'vh', hidden: true },
    { title: "Ime", field: 'name', width: 50 + 'vh', hidden: true },
    { title: "Prezime", field: 'surname', width: 50 + 'vh', hidden: true },
    { title: "Otkazni rok", field: 'noticePeriod', width: 50 + 'vh', hidden: true },
];

class ContractListTable extends Component {


    constructor(props) {
        super(props);

        this.state = {
            token: '',
            role: '',
            contracts: [],
            deletePopup: false,
            addPopup: false,
            editPopup: false,
            detailsPopup: false,
            downloadPopup: false,
            uploadPopup: false,
            loading: true,
            messageText: '',
            messageAlert: false,
            messageColor: "danger",

            newContractName: '',
            newContractNumber: '',
            newContractEmergencyNumber: '',
            newContractDescription: '',
            newContractNoticePeriod: '',
            newContractConclusionDate: '',
            newContractDuration: '',
            newContractFirm: '',
            newContractPersonName: '',
            newContractPersonSurname: '',
            newContractPersonPhone: '',
            newContractPersonEmail: '',

            detailsContractName: '',
            detailsContractNumber: '',
            detailsContractEmergencyNumber: '',
            detailsContractDescription: '',
            detailsContractNoticePeriod: '',
            detailsContractConclusionDate: '',
            detailsContractExpirationDate: '',
            detailsContractDuration: '',
            detailsContractFirm: '',
            detailsContractPersonName: '',
            detailsContractPersonSurname: '',
            detailsContractPersonPhone: '',
            detailsContractPersonEmail: '',


            contractId: '',

            fileUploadState: "",
            uploadPercentage: 0,
            progressBarHidden: "none",
            fileDownloadState: "",
            newFile: null,
            file: null,
            fileSizeExceeded: false,


        };
        this.inputReference = React.createRef();

    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };



    componentDidMount() {
        var type = "type"
        const token = cookie.load('token');
        this.setState({ token: token });
        const role = cookie.load('role');
        this.setState({ role: role });
        const buildingId = parseInt(cookie.load('buildingId'));
        this.setState({ buildingId: buildingId });

        this.setState({ loading: true })
        const options = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
            },
        };
        fetch('/building_contracts/building/' + buildingId + '/type/' + this.props.fileType, options)
            .then(data => data.json())
            .then(contracts => {
                this.setState({ contracts: parseContracts(contracts) })

            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )
    };

    fileUploadAction = () =>{
        this.setState({progressBarHidden: ""})
        this.inputReference.current.click();
      }


    parseFetchResponse = response => response.json().then(text => ({
        json: text,
        meta: response,
    }));


    setUploadState = (newState) => {
        this.setState({ fileUploadState: newState });
    }

    setPercentage = (newPercentage) => {
        this.setState({ uploadPercentage: newPercentage });
    }



    uploadFile = ({ target: { files } }) => {
        if (files[0].size > 2000000) {
            this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
            return
        }
        this.setState({ fileUploadState: "Učitavam..." })
        let data = new FormData();
        data.append("file", files[0]);
        const options = {
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor((loaded * 100) / total)
                console.log(loaded + " kb of " + total + " kb | " + percent + " %")

                if (percent < 100) {
                    this.setState({ uploadPercentage: percent })
                }
            },
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        }
        axios.put('building_contracts/file/' + this.state.contractId, data, options).then(res => {

            if (res.status != 200) {
                this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 0 });
                this.setState({ messageAlert: true }, () => {
                    window.setTimeout(() => {
                        this.setState({ messageAlert: false })
                    }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno prenesen ugovor!" })
                this.clearTextFields()
                this.cancelContractUploadPopUp()
                this.componentDidMount()

            } else {
                this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 100 }, () => {
                    setTimeout(() => {
                        this.setState({ messageAlert: true }, () => {
                            window.setTimeout(() => {
                                this.setState({ messageAlert: false })
                            }, 6000)
                        });
                        this.setState({ messageColor: "success" });
                        this.setState({ messageText: "Uspješno prenesen ugovor!" })
                       this.clearTextFields()
                       this.cancelContractUploadPopUp()
                       this.componentDidMount()
                       
                    }, 4000)
                })
            }

        })

    }


    downloadContract = (contractId) => {
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        };
        fetch('building_contracts/download/' + contractId, options)
            .then(response => {
                if (response.headers.get('Content-Disposition') === null) {
                    this.setState({ fileDownloadState: "Datoteka nije pronađena!" })
                    return
                }
                const filename = response.headers.get('Content-Disposition').split('filename=')[1];
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = filename;
                    a.click();
                });
            });
    }




    addContract = () => {
        this.setState({progressBarHidden: ''})
        this.setState({ loading: true })
        const data = {
            firm: this.state.newContractFirm,
            contractType: this.props.fileType,
            conclusionDate: this.state.newContractConclusionDate,
            duration: this.state.newContractDuration,
            noticePeriod: this.state.newContractNoticePeriod,
            name: this.state.newContractPersonName,
            surname: this.state.newContractPersonSurname,
            email: this.state.newContractPersonEmail,
            mobilePhone: this.state.newContractPersonPhone,
            emergencyNumber: this.state.newContractEmergencyNumber,
            contractId: this.state.newContractNumber,
            description: this.state.newContractDescription,
            buildingId: this.state.buildingId,
            contractName: this.state.newContractName
        };

        let postData = new FormData();
        postData.append("file", this.state.newFile);
        postData.append("data", JSON.stringify(data))
        if (this.state.newFile.size > 2000000) {
            this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
            return
        }
        this.setState({ fileUploadState: "Učitavam..." })
        const options = {
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor((loaded * 100) / total)
                console.log(loaded + " kb of " + total + " kb | " + percent + " %")

                if (percent < 100) {
                    this.setState({ uploadPercentage: percent })
                }
            },
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        }
        axios.post('/building_contracts/upload/building/'+this.state.buildingId, postData, options).then(res => {

            if (res.status != 200) {
                this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 0 });
                this.setState({ messageAlert: true }, () => {
                    window.setTimeout(() => {
                        this.setState({ messageAlert: false })
                    }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno stvoren ugovor!" })
                this.cancelAddPopUp()
                this.setState({ loading: false })
                this.componentDidMount()

            } else {
                this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 100 }, () => {
                    setTimeout(() => {
                        this.setState({ messageAlert: true }, () => {
                            window.setTimeout(() => {
                                this.setState({ messageAlert: false })
                            }, 6000)
                        });
                        this.setState({ messageColor: "success" });
                        this.setState({ messageText: "Uspješno stvoren ugovor!" })
                        this.clearTextFields()
                        this.cancelAddPopUp()
                        this.setState({ loading: false })
                        this.componentDidMount()
                    }, 4000)
                })
            }

        })
    };


    editContract = () => {
        this.setState({ loading: true })
        const data = {
            firm: this.state.detailsContractFirm,
            contractType: this.props.fileType,
            conclusionDate: this.state.detailsContractConclusionDate,
            duration: this.state.detailsContractDuration,
            noticePeriod: this.state.detailsContractNoticePeriod,
            name: this.state.detailsContractPersonName,
            surname: this.state.detailsContractPersonSurname,
            email: this.state.detailsContractPersonEmail,
            mobilePhone: this.state.detailsContractPersonPhone,
            emergencyNumber: this.state.detailsContractEmergencyNumber,
            contractId: this.state.detailsContractNumber,
            description: this.state.detailsContractDescription,
            contractName: this.state.detailsContractName
        };
        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        fetch('/building_contracts/' + this.state.contractId, options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
                if (meta.status !== 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno izmijenjen ugovor!" })
                    this.cancelEditPopUp();
                    this.clearTextFields();
                    this.componentDidMount();

                }
                else {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješno izmijenjen ugovor!" })
                    this.cancelEditPopUp();
                    this.clearTextFields();
                    this.componentDidMount();
                }
            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )

    };


    deleteContract = () => {

        this.setState({ loading: true })
        const options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            }
        };
        return fetch('/building_contracts/' + this.state.contractId, options)
            .then(response => {
                if (response.status === 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješno obrisan ugovor!" })
                    this.cancelDeletePopUp();
                    this.componentDidMount();

                }
                else {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno obrisan ugovor!" })
                    this.cancelDeletePopUp();

                }
            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )
    };

    clearTextFields = () => {
        this.setState({ contractId: '' })
        this.setState({ newContractName: '' })
        this.setState({ newContractNumber: '' })
        this.setState({ newContractEmergencyNumber: '' })
        this.setState({ newContractDescription: '' })
        this.setState({ newContractNoticePeriod: '' })
        this.setState({ newContractConclusionDate: '' })
        this.setState({ newContractDuration: '' })
        this.setState({ newContractFirm: '' })
        this.setState({ newContractPersonName: '' })
        this.setState({ newContractPersonSurname: '' })
        this.setState({ newContractPersonPhone: '' })
        this.setState({ newContractPersonEmail: '' })
        this.setState({file: null})
        this.setState({newFile: null})
        this.setState({fileUploadState: ''})
        this.setState({uploadPercentage: 0})
        this.setState({progressBarHidden: "none"})
        this.setState({fileDownloadState:""})
        this.setState({newDebtValue: ''})
    }

    toogleDeletePopUp = () => {
        this.setState({ deletePopup: true })
    }

    cancelDeletePopUp = () => {
        this.setState({ deletePopup: false })
    }

    toogleAddPopUp = () => {
        this.setState({ addPopup: true })
    }

    cancelAddPopUp = () => {
        this.setState({ addPopup: false })
        this.clearTextFields();
    }

    toogleEditPopUp = () => {
        this.setState({ editPopup: true })
    }

    cancelEditPopUp = () => {
        this.setState({ editPopup: false })
        this.clearTextFields();
    }

    toogleDetailsPopUp = () => {
        this.setState({ detailsPopup: true })
    }

    cancelDetailsPopUp = () => {
        this.setState({ detailsPopup: false })
        this.clearTextFields();
    }

    toogleContractUploadPopUp = () => {
        this.setState({ uploadPopup: true })
    }

    cancelContractUploadPopUp = () => {
        this.setState({ uploadPopup: false })
        this.clearTextFields();
    }

    dismissAlert = () => {
        this.setState({ messageAlert: false })
    }

    tableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
        body: {
            emptyDataSourceMessage: 'Nema podataka za prikazati',
            editRow: {
                deleteText: 'Jeste li sigurni?',
                saveTooltip: 'Potvrdi',
                cancelTooltip: 'Odustani'
            },
            addTooltip: 'Dodaj',
            deleteTooltip: 'Obriši',
            editTooltip: 'Izmijeni',
        },
        header: {
            actions: 'Akcije'
        },
        pagination: {
            firstTooltip: 'Prva stranica',
            previousTooltip: 'Prethodna stranica',
            nextTooltip: 'Iduća stranica',
            lastTooltip: 'Posljednja stranica',
            labelRowsSelect: 'redaka',
        },
        toolbar: {
            searchTooltip: 'Pretraži',
            searchPlaceholder: 'Pretraži',
            nRowsSelected: '{0} redaka odabrano',
            exportTitle: 'Izvezi',
            exportName: 'Izvezi kao .csv',
        }
    }



    render() {
        const { classes } = this.props;

        return (
            <div  >
                <Modal isOpen={this.state.deletePopup}  >
                    <ModalBody>
                        Jeste li sigurni da želite obrisati ugovor?
              </ModalBody>
                    <ModalFooter>
                        <Button size="small" positive onClick={this.deleteContract}>Izbriši</Button>{' '}
                        <Button size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.uploadPopup} centered={true} >
                    <ModalHeader toggle={this.cancelContractUploadPopUp}><h4>Prenesi datoteku</h4></ModalHeader>
                    <ModalBody>

                        <Segment placeholder>
                            <div>
                                <input type="file" hidden ref={this.inputReference} onChange={this.uploadFile} />
                                <Button content='Prenesi' icon="upload" className="ui button" onClick={this.fileUploadAction} color="orange" size='huge' style={{ borderRadius: 30 }} />
                            </div>
                            
                            <br></br>
                            <div style={{ display: this.state.progressBarHidden }}>
                                <Progress color="success" animated value={this.state.uploadPercentage} />
                                <center>{this.state.fileUploadState}</center>
                            </div>
                        </Segment>



                    </ModalBody>


                </Modal>



                <Modal isOpen={this.state.addPopup} centered={true} >
                    <ModalHeader toggle={this.cancelAddPopUp}><h4>Unesite podatke o ugovoru</h4></ModalHeader>
                    <ModalBody>
                        <h6>Podaci o ugovoru:</h6>
                        <div >
                            <Input style={{ width: 60 + '%' }} placeholder='Naziv ugovora' value={this.state.newContractName} onChange={this.handleChange('newContractName')} />
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Naziv firme' value={this.state.newContractFirm} onChange={this.handleChange('newContractFirm')} />
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Broj ugovora' value={this.state.newContractNumber} onChange={this.handleChange('newContractNumber')} />
                        </div><br></br>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Datum sklapanja ugovora"
                                    minDate={new Date()}
                                    isClearable
                                    selected={this.state.newContractConclusionDate}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {

                                        this.setState({ newContractConclusionDate: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Trajanje ugovora' value={this.state.newContractDuration} onChange={this.handleChange('newContractDuration')} label={{ basic: true, content:'Broj godina' }} labelPosition='right'/>
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Otkazni rok' value={this.state.newContractNoticePeriod} onChange={this.handleChange('newContractNoticePeriod')} label={{ basic: true, content:'Broj godina' }} labelPosition='right'/>
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Broj hitne intervencije' value={this.state.newContractEmergencyNumber} onChange={this.handleChange('newContractEmergencyNumber')} />
                        </div><br></br>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <TextArea placeholder='Opis ugovora' value={this.state.newContractDescription}
                                    onChange={this.handleChange('newContractDescription')} />
                            </Form>
                        </div><br></br>

                        <h6>Podaci o kontakt osobi:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Ime' value={this.state.newContractPersonName} onChange={this.handleChange('newContractPersonName')} />
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Prezime' value={this.state.newContractPersonSurname} onChange={this.handleChange('newContractPersonSurname')} />
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Broj telefona' value={this.state.newContractPersonPhone} onChange={this.handleChange('newContractPersonPhone')} />
                        </div><br></br>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Email' value={this.state.newContractPersonEmail} onChange={this.handleChange('newContractPersonEmail')} />
                        </div><br></br>

                        <h6>Datoteka ugovora:</h6>
                        <div>
                            <input type="file" hidden ref={this.inputReference} onChange={(e)=>{this.setState({newFile: e.target.files[0]})}} />
                            <center>
                                <Button content='Prenesi' icon="upload" className="ui button" onClick={()=>{this.inputReference.current.click()}} color="orange" size='big' style={{ borderRadius: 30 }} />
                            </center>
                        </div>

                        {this.state.newFile === null ? <div></div> : <div><center><h6>{this.state.newFile.name}</h6></center></div>}

                        <br></br>
                        <div style={{ display: this.state.progressBarHidden }}>
                            <Progress color="success" animated value={this.state.uploadPercentage} />
                            <center>{this.state.fileUploadState}</center>
                        </div>

                    </ModalBody>

                    <ModalFooter>
                        <Button size="small" positive onClick={this.addContract}>Stvori</Button>{' '}

                    </ModalFooter>

                </Modal>


                <Modal isOpen={this.state.editPopup} centered={true} >
                    <ModalHeader toggle={this.cancelEditPopUp}><h4>Izmijenite podatke o ugovoru</h4></ModalHeader>
                    <ModalBody>
                        <h5>Podaci o ugovoru</h5><br></br>
                        <h6>Naziv ugovora:</h6>
                        <div >
                            <Input style={{ width: 60 + '%' }} placeholder='Naziv ugovora' value={this.state.detailsContractName} onChange={this.handleChange('detailsContractName')} />
                        </div><br></br>
                        <h6>Naziv firme:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Naziv firme' value={this.state.detailsContractFirm} onChange={this.handleChange('detailsContractFirm')} />
                        </div><br></br>
                        <h6>Broj ugovora:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Broj ugovora' value={this.state.detailsContractNumber} onChange={this.handleChange('detailsContractNumber')} />
                        </div><br></br>
                        <h6>Datum sklapanja ugovora:</h6>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Datum sklapanja ugovora"
                                    minDate={new Date()}
                                    isClearable
                                    selected={(this.state.detailsContractConclusionDate)}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {
                                        this.setState({ detailsContractConclusionDate: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>
                        <h6>Trajanje ugovora:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Trajanje ugovora' value={this.state.detailsContractDuration} onChange={this.handleChange('detailsContractDuration')} label={{ basic: true, content:'Broj godina' }} labelPosition='right'/>
                        </div><br></br>
                        <h6>Otkazni rok:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Otkazni rok' value={this.state.detailsContractNoticePeriod} onChange={this.handleChange('detailsContractNoticePeriod')} label={{ basic: true, content:'Broj godina' }} labelPosition='right'/>
                        </div><br></br>
                        <h6>Broj hitne intervencije:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Broj hitne intervencije' value={this.state.detailsContractEmergencyNumber} onChange={this.handleChange('detailsContractEmergencyNumber')} />
                        </div><br></br>
                        <h6>Opis:</h6>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <TextArea placeholder='Opis ugovora' value={this.state.detailsContractDescription}
                                    onChange={this.handleChange('detailsContractDescription')} />
                            </Form>
                        </div><br></br>
                        <Divider></Divider>
                        <h5>Podaci o kontakt osobi</h5><br></br>
                        <h6>Ime:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Ime' value={this.state.detailsContractPersonName} onChange={this.handleChange('detailsContractPersonName')} />
                        </div><br></br>
                        <h6>Prezime:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Prezime' value={this.state.detailsContractPersonSurname} onChange={this.handleChange('detailsContractPersonSurname')} />
                        </div><br></br>
                        <h6>Broj telefona:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Broj telefona' value={this.state.detailsContractPersonPhone} onChange={this.handleChange('detailsContractPersonPhone')} />
                        </div><br></br>
                        <h6>Email:</h6>
                        <div>
                            <Input style={{ width: 60 + '%' }} placeholder='Email' value={this.state.detailsContractPersonEmail} onChange={this.handleChange('detailsContractPersonEmail')} />
                        </div><br></br>

                    </ModalBody>

                    <ModalFooter>
                        <Button size="small" positive onClick={this.editContract}>Izmijeni</Button>{' '}

                    </ModalFooter>

                </Modal>

                <Modal isOpen={this.state.detailsPopup} centered={true} >
                    <ModalHeader toggle={this.cancelDetailsPopUp}><h4>Detalji o ugovoru</h4></ModalHeader>
                    <ModalBody>
                        <p>
                            <h5><b>Podaci o ugovoru</b></h5><br></br>
                            <b>Naziv ugovora:</b> {this.state.detailsContractName}<br></br><br></br>
                            <b>Naziv firme:</b> {this.state.detailsContractFirm}<br></br><br></br>
                            <b>Broj ugovora:</b> {this.state.detailsContractNumber}<br></br><br></br>
                            <b>Datum sklapanja ugovora:</b> {this.state.detailsContractConclusionDate}<br></br><br></br>
                            <b>Trajanje ugovora:</b> {this.state.detailsContractDuration + " godina"}<br></br><br></br>
                            <b>Datum isteka ugovora:</b> {this.state.detailsContractExpirationDate}<br></br><br></br>
                            <b>Otkazni rok:</b> {this.state.detailsContractNoticePeriod + " godina"}<br></br><br></br>
                            <b>Broj hitne intervencije:</b> {this.state.detailsContractEmergencyNumber}<br></br><br></br>
                            <b>Opis ugovora:</b> {this.state.detailsContractDescription}<br></br><br></br>
                            <Divider></Divider>
                            <h5><b>Podaci o kontakt osobi</b></h5><br></br>
                            <b>Ime:</b> {this.state.detailsContractPersonName}<br></br><br></br>
                            <b>Prezime:</b> {this.state.detailsContractPersonSurname}<br></br><br></br>
                            <b>Broj mobitela:</b> {this.state.detailsContractPersonPhone}<br></br><br></br>
                            <b>Email:</b> {this.state.detailsContractPersonEmail}<br></br><br></br>

                        </p>

                    </ModalBody>

                </Modal>


                <Dimmer active={this.state.loading} inverted>
                    <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
                </Dimmer>

                <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                    <h5>{this.state.messageText}</h5>
                </Alert>

                <MaterialTable
                    icons={this.tableIcons}
                    title="Popis ugovora"
                    localization={this.tableTextLocalization}
                    columns={columns()}
                    options={{
                        actionsColumnIndex: -1,
                        exportButton: { pdf: false, csv: true },
                        headerStyle: {
                            backgroundColor: '#E86A22',
                            color: '#FFF'
                        },
                        maxBodyHeight: 600,
                        pageSize: 10,
                        pageSizeOptions: [10, 20, 50, 100]
                    }}
                    data={this.state.contracts}
                    actions={[
                        {
                            icon: () => {
                                return (
                                    <Icon size="small" name='add' />
                                );
                            },
                            tooltip: 'Dodaj ugovor',
                            isFreeAction: true,
                            hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                            onClick: () => this.setState({ addPopup: true }),

                        },

                        {
                            icon: () => <Icon name="file alternate" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Detalji o ugovoru',
                            onClick: (event, rowData) => {
                                this.setState({ contractId: rowData.id })
                                this.setState({ detailsContractName: rowData.contractName })
                                this.setState({ detailsContractNoticePeriod: rowData.noticePeriod })
                                this.setState({ detailsContractNumber: rowData.contractId })
                                this.setState({ detailsContractPersonEmail: rowData.email })
                                this.setState({ detailsContractPersonName: rowData.name })
                                this.setState({ detailsContractPersonSurname: rowData.surname })
                                this.setState({ detailsContractPersonPhone: rowData.mobilePhone })
                                this.setState({ detailsContractEmergencyNumber: rowData.emergencyNumber })
                                this.setState({ detailsContractDuration: rowData.duration })
                                this.setState({ detailsContractDescription: rowData.description })
                                this.setState({ detailsContractFirm: rowData.firm })
                                this.setState({ detailsContractConclusionDate: rowData.conclusionDate })
                                this.setState({ detailsContractExpirationDate: rowData.expirationDate })
                                this.toogleDetailsPopUp();
                            }
                        },

                        {
                            icon: () => <Icon name="download" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Preuzmi datoteku ugovora',
                            onClick: (event, rowData) => {
                                this.downloadContract(rowData.id)
                            }
                        },

                        {
                            icon: () => <Icon name="upload" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Prenesi novu datoteku ugovora',
                            onClick: (event, rowData) => {
                                this.setState({ contractId: rowData.id })
                                this.toogleContractUploadPopUp();
                            }
                        },

                        {
                            icon: () => <Icon name="pencil" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Izmijeni podatke o ugovoru',
                            onClick: (event, rowData) => {
                                this.setState({ contractId: rowData.id })
                                this.setState({ detailsContractName: rowData.contractName })
                                this.setState({ detailsContractNoticePeriod: rowData.noticePeriod })
                                this.setState({ detailsContractNumber: rowData.contractId })
                                this.setState({ detailsContractPersonEmail: rowData.email })
                                this.setState({ detailsContractPersonName: rowData.name })
                                this.setState({ detailsContractPersonSurname: rowData.surname })
                                this.setState({ detailsContractPersonPhone: rowData.mobilePhone })
                                this.setState({ detailsContractEmergencyNumber: rowData.emergencyNumber })
                                this.setState({ detailsContractDuration: rowData.duration })
                                this.setState({ detailsContractDescription: rowData.description })
                                this.setState({ detailsContractFirm: rowData.firm })
                                this.setState({ detailsContractConclusionDate: makeJSDate(rowData.conclusionDate) })
                                this.setState({ detailsContractExpirationDate: rowData.expirationDate })
                                this.toogleEditPopUp();
                            }
                        },

                        rowData => ({
                            icon: () => <Icon name="trash" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Obriši ugovor',
                            hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                            onClick: () => {
                                this.setState({ contractId: rowData.id });
                                this.toogleDeletePopUp();
                            }
                        })
                    ]}
                />
            </div>
        );
    }
}

ContractListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContractListTable);