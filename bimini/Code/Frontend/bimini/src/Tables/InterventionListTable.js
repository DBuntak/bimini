import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader, Progress } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Form, TextArea, Label, Select } from 'semantic-ui-react';
import { sortData, parseInterventions, makeJSDate, addOneHour, parseDate } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";
import axios from 'axios';


registerLocale("hr", hr);

const styles = theme => ({

})



const columns = () => [
    { title: 'Identifikator', field: 'id', editable: "never", width: 200, hidden: true },
    { title: 'Stan', field: 'apartmentName', width: 50 + 'vh' },
    { title: "Datum", field: 'date', width: 50 + 'vh' },
    { title: "Iznos (HRK)", field: 'amount', width: 50 + 'vh' },
    { title: "Opis", field: 'description', width: 50 + 'vh' },
];

class InterventionListTable extends Component {


    constructor(props) {
        super(props);

        this.state = {
            token: '',
            role: '',
            interventions: [],
            deletePopup: false,
            addPopup: false,
            editPopup: false,
            detailsPopup: false,
            downloadPopup: false,
            uploadPopup: false,
            reportPopup: false,
            loading: true,
            messageText: '',
            messageAlert: false,
            messageColor: "danger",
            apartmentOptions: [],

            newInterventionApartment: '',
            newInterventionDate: '',
            newInterventionAmount: '',
            newInterventionDescription: '',

            detailsInterventionApartment: '',
            detailsInterventionDate: '',
            detailsInterventionAmount: '',
            detailsInterventionDescription: '',

            selectedYear: '',
            report: '',
            yearOptions: [],

            interventionId: '',
            apartmentId: '',

            fileUploadState: "",
            uploadPercentage: 0,
            progressBarHidden: "none",
            fileDownloadState: "",
            newFile: null,
            file: null,
            fileSizeExceeded: false,


        };
        this.inputReference = React.createRef();

    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };



    componentDidMount() {
        var type = "type"
        const token = cookie.load('token');
        this.setState({ token: token });
        const role = cookie.load('role');
        this.setState({ role: role });
        const buildingId = parseInt(cookie.load('buildingId'));
        this.setState({ buildingId: buildingId });

        var currentYear = new Date().getFullYear()
        var yOptions = []
        for(var i=2020; i<= currentYear; ++i){
            var option = {key: i, value: i, text:i+"."}
            yOptions.push(option)
        }
        this.setState({yearOptions: yOptions})

        this.setState({ loading: true })
        const options = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
            },
        };
        fetch('/building/' + buildingId + "/apartments", options)
            .then(data => data.json())
            .then(apartments => {
                var apartmentOptions = []
                for (var i = 0; i < apartments.length; ++i) {
                    var option = { key: apartments[i].id, value: apartments[i].id, text: apartments[i].name + "|  Broj: " + apartments[i].number + "|  Kat: " + apartments[i].floor }
                    apartmentOptions.push(option)
                }
                this.setState({ apartmentOptions: apartmentOptions })
            })
        fetch('/intervention/building/' + buildingId, options)
            .then(data => data.json())
            .then(interventions => {
                this.setState({ interventions: parseInterventions(interventions) })

            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )
    };

    fileUploadAction = () => {
        this.setState({ progressBarHidden: "" })
        this.inputReference.current.click();
    }


    parseFetchResponse = response => response.json().then(text => ({
        json: text,
        meta: response,
    }));


    setUploadState = (newState) => {
        this.setState({ fileUploadState: newState });
    }

    setPercentage = (newPercentage) => {
        this.setState({ uploadPercentage: newPercentage });
    }


    handleSelectYear = (e, { value }) => {
        this.setState({ selectedYear: value })
        const options = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
        };
        fetch('/intervention/report/building/' + this.state.buildingId + '/year/' + value, options)
            .then(data => data.json())
            .then(report => {
                report.date = parseDate(report.date)
                this.setState({ report: (report) })

        })
    }

    uploadFile = ({ target: { files } }) => {
        if (files[0].size > 2000000) {
            this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
            return
        }
        this.setState({ fileUploadState: "Učitavam..." })
        let data = new FormData();
        data.append("file", files[0]);
        const options = {
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor((loaded * 100) / total)
                console.log(loaded + " kb of " + total + " kb | " + percent + " %")

                if (percent < 100) {
                    this.setState({ uploadPercentage: percent })
                }
            },
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        }
        axios.put('intervention/file/' + this.state.interventionId, data, options).then(res => {

            if (res.status != 200) {
                this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 0 });
                this.setState({ messageAlert: true }, () => {
                    window.setTimeout(() => {
                        this.setState({ messageAlert: false })
                    }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno prenesena intervencija!" })
                this.clearTextFields()
                this.cancelInterventionUploadPopUp()
                this.componentDidMount()

            } else {
                this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 100 }, () => {
                    setTimeout(() => {
                        this.setState({ messageAlert: true }, () => {
                            window.setTimeout(() => {
                                this.setState({ messageAlert: false })
                            }, 6000)
                        });
                        this.setState({ messageColor: "success" });
                        this.setState({ messageText: "Uspješno prenesena intervencija!" })
                        this.clearTextFields()
                        this.cancelInterventionUploadPopUp()
                        this.componentDidMount()

                    }, 4000)
                })
            }

        })

    }


    downloadIntervention = (interventionId) => {
        const options = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        };
        fetch('intervention/download/' + interventionId, options)
            .then(response => {
                if (response.headers.get('Content-Disposition') === null) {
                    this.setState({ fileDownloadState: "Datoteka nije pronađena!" })
                    return
                }
                const filename = response.headers.get('Content-Disposition').split('filename=')[1];
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = filename;
                    a.click();
                });
            });
    }




    addIntervention = () => {
        this.setState({ loading: true })
        this.setState({ progressBarHidden: "" })
        const data = {

            description: this.state.newInterventionDescription,
            amount: this.state.newInterventionAmount,
            date: addOneHour(this.state.newInterventionDate)
        };

        let postData = new FormData();
        postData.append("record", this.state.newFile);
        postData.append("data", JSON.stringify(data))
        if (this.state.newFile.size > 2000000) {
            this.setState({ fileUploadState: "Datoteka je prevelika! (Maksimalna veličina 2MB)" })
            return
        }
        this.setState({ fileUploadState: "Učitavam..." })
        const options = {
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor((loaded * 100) / total)
                console.log(loaded + " kb of " + total + " kb | " + percent + " %")

                if (percent < 100) {
                    this.setState({ uploadPercentage: percent })
                }
            },
            headers: {
                'Authorization': 'Bearer ' + this.state.token,
            }
        }
        axios.post('/intervention/upload/apartment/' + this.state.newInterventionApartment, postData, options).then(res => {

            if (res.status != 200) {
                this.setState({ fileUploadState: "Neuspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 0 });
                this.setState({ messageAlert: true }, () => {
                    window.setTimeout(() => {
                        this.setState({ messageAlert: false })
                    }, 6000)
                });
                this.setState({ messageColor: "danger" });
                this.setState({ messageText: "Neuspješno stvorena intervencija!" })
                this.cancelAddPopUp()
                this.setState({ loading: false })
                this.componentDidMount()

            } else {
                this.setState({ fileUploadState: "Uspješno prenesena datoteka!" })
                this.setState({ uploadPercentage: 100 }, () => {
                    setTimeout(() => {
                        this.setState({ messageAlert: true }, () => {
                            window.setTimeout(() => {
                                this.setState({ messageAlert: false })
                            }, 6000)
                        });
                        this.setState({ messageColor: "success" });
                        this.setState({ messageText: "Uspješno stvorena intervencija!" })
                        this.clearTextFields()
                        this.cancelAddPopUp()
                        this.setState({ loading: false })
                        this.componentDidMount()
                    }, 4000)
                })
            }

        })
    };


    editIntervention = () => {
        this.setState({ loading: true })
        const data = {
            description: this.state.detailsInterventionDescription,
            amount: this.state.detailsInterventionAmount,
            date: addOneHour(this.state.detailsInterventionDate)
        };
        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        fetch('/intervention/' + this.state.interventionId, options)
            .then(this.parseFetchResponse)
            .then(({ json, meta }) => {
                if (meta.status !== 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno izmijenjena intervencija!" })
                    this.cancelEditPopUp();
                    this.clearTextFields();
                    this.componentDidMount();

                }
                else {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješno izmijenjena intervencija!" })
                    this.cancelEditPopUp();
                    this.clearTextFields();
                    this.componentDidMount();
                }
            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )

    };


    deleteIntervention = () => {

        this.setState({ loading: true })
        const options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.token,
                'Content-Type': 'application/json',
            }
        };
        return fetch('/intervention/' + this.state.interventionId, options)
            .then(response => {
                if (response.status === 200) {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "success" });
                    this.setState({ messageText: "Uspješno obrisana intervencija!" })
                    this.cancelDeletePopUp();
                    this.componentDidMount();

                }
                else {
                    this.setState({ messageAlert: true }, () => {
                        window.setTimeout(() => {
                            this.setState({ messageAlert: false })
                        }, 6000)
                    });
                    this.setState({ messageColor: "danger" });
                    this.setState({ messageText: "Neuspješno obrisana intervencija!" })
                    this.cancelDeletePopUp();

                }
            })
            .then(
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 500)
            )
    };

    clearTextFields = () => {
        this.setState({ interventionId: '' })
        this.setState({ newInterventionAmount: '' })
        this.setState({ newInterventionApartment: '' })
        this.setState({ newInterventionDate: '' })
        this.setState({ newInterventionDescription: '' })
        this.setState({ file: null })
        this.setState({ newFile: null })
        this.setState({ fileUploadState: '' })
        this.setState({ uploadPercentage: 0 })
        this.setState({ progressBarHidden: "none" })
        this.setState({ fileDownloadState: "" })
        this.setState({selectedYear: ''})
    }

    handleSelectInterventionApartment = (e, { value }) => {
        this.setState({ newInterventionApartment: value })
    }

    toogleDeletePopUp = () => {
        this.setState({ deletePopup: true })
    }

    cancelDeletePopUp = () => {
        this.setState({ deletePopup: false })
    }

    toogleAddPopUp = () => {
        this.setState({ addPopup: true })
    }

    cancelAddPopUp = () => {
        this.setState({ addPopup: false })
        this.clearTextFields();
    }

    toogleEditPopUp = () => {
        this.setState({ editPopup: true })
    }

    cancelEditPopUp = () => {
        this.setState({ editPopup: false })
        this.clearTextFields();
    }

    toogleDetailsPopUp = () => {
        this.setState({ detailsPopup: true })
    }

    cancelDetailsPopUp = () => {
        this.setState({ detailsPopup: false })
        this.clearTextFields();
    }

    toogleInterventionUploadPopUp = () => {
        this.setState({ uploadPopup: true })
    }

    cancelInterventionUploadPopUp = () => {
        this.setState({ uploadPopup: false })
        this.clearTextFields();
    }

    toggleReportPopup = () => {
        this.setState({ reportPopup: true })
    }

    cancelReportPopup = () => {
        this.setState({ reportPopup: false })
        this.clearTextFields()
    }

    dismissAlert = () => {
        this.setState({ messageAlert: false })
    }

    tableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
        body: {
            emptyDataSourceMessage: 'Nema podataka za prikazati',
            editRow: {
                deleteText: 'Jeste li sigurni?',
                saveTooltip: 'Potvrdi',
                cancelTooltip: 'Odustani'
            },
            addTooltip: 'Dodaj',
            deleteTooltip: 'Obriši',
            editTooltip: 'Izmijeni',
        },
        header: {
            actions: 'Akcije'
        },
        pagination: {
            firstTooltip: 'Prva stranica',
            previousTooltip: 'Prethodna stranica',
            nextTooltip: 'Iduća stranica',
            lastTooltip: 'Posljednja stranica',
            labelRowsSelect: 'redaka',
        },
        toolbar: {
            searchTooltip: 'Pretraži',
            searchPlaceholder: 'Pretraži',
            nRowsSelected: '{0} redaka odabrano',
            exportTitle: 'Izvezi',
            exportName: 'Izvezi kao .csv',
        }
    }



    render() {
        const { classes } = this.props;

        return (
            <div  >
                <Modal isOpen={this.state.deletePopup}  >
                    <ModalBody>
                        Jeste li sigurni da želite obrisati intervenciju?
              </ModalBody>
                    <ModalFooter>
                        <Button size="small" positive onClick={this.deleteIntervention}>Izbriši</Button>{' '}
                        <Button size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.uploadPopup} centered={true} >
                    <ModalHeader toggle={this.cancelInterventionUploadPopUp}><h4>Prenesi zapisnik intervencije</h4></ModalHeader>
                    <ModalBody>

                        <Segment placeholder>
                            <div>
                                <input type="file" hidden ref={this.inputReference} onChange={this.uploadFile} />
                                <Button content='Prenesi' icon="upload" className="ui button" onClick={this.fileUploadAction} color="orange" size='huge' style={{ borderRadius: 30 }} />
                            </div>

                            <br></br>
                            <div style={{ display: this.state.progressBarHidden }}>
                                <Progress color="success" animated value={this.state.uploadPercentage} />
                                <center>{this.state.fileUploadState}</center>
                            </div>
                        </Segment>



                    </ModalBody>


                </Modal>



                <Modal isOpen={this.state.addPopup} centered={true} >
                    <ModalHeader toggle={this.cancelAddPopUp}><h4>Unesite podatke o intervenciji</h4></ModalHeader>
                    <ModalBody>
                        <div>
                            <Select style={{ width: 60 + '%' }} placeholder='Stan' options={this.state.apartmentOptions} value={this.state.newInterventionApartment} onChange={this.handleSelectInterventionApartment} /><br></br><br></br>
                        </div>
                        <div >
                            <Input style={{ width: 60 + '%' }} placeholder='Opis intervencije' value={this.state.newInterventionDescription} onChange={this.handleChange('newInterventionDescription')} />
                        </div><br></br>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Datum intervencije"
                                    minDate={new Date()}
                                    isClearable
                                    selected={this.state.newInterventionDate}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {

                                        this.setState({ newInterventionDate: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>
                        <div>
                            <Input labelPosition='right' type='text' placeholder='Iznos' value={this.state.newInterventionAmount} onChange={this.handleChange('newInterventionAmount')}>
                                <Label basic>HRK</Label>
                                <input />
                                <Label>.00</Label>
                            </Input>

                        </div>
                        <br></br>

                        <h6>Zapisnik intervencije:</h6>
                        <div>
                            <input type="file" hidden ref={this.inputReference} onChange={(e) => { this.setState({ newFile: e.target.files[0] }) }} />
                            <center>
                                <Button content='Prenesi' icon="upload" className="ui button" onClick={() => { this.inputReference.current.click(); }} color="orange" size='big' style={{ borderRadius: 30 }} />
                            </center>
                        </div>

                        {this.state.newFile === null ? <div></div> : <div><center><h6>{this.state.newFile.name}</h6></center></div>}

                        <br></br>
                        <div style={{ display: this.state.progressBarHidden }}>
                            <Progress color="success" animated value={this.state.uploadPercentage} />
                            <center>{this.state.fileUploadState}</center>
                        </div>

                    </ModalBody>

                    <ModalFooter>
                        <Button size="small" positive onClick={this.addIntervention}>Stvori</Button>{' '}

                    </ModalFooter>

                </Modal>


                <Modal isOpen={this.state.editPopup} centered={true} >
                    <ModalHeader toggle={this.cancelEditPopUp}><h4>Izmijenite podatke o intervenciji</h4></ModalHeader>
                    <ModalBody>
                        <h6>Opis:</h6>
                        <div >
                            <Input style={{ width: 60 + '%' }} placeholder='Opis intervencije' value={this.state.detailsInterventionDescription} onChange={this.handleChange('detailsInterventionDescription')} />
                        </div><br></br>
                        <h6>Datum intervencije:</h6>
                        <div>
                            <Form style={{ width: 60 + '%' }}>
                                <DatePicker
                                    withPortal
                                    placeholderText="Datum intervencije"
                                    minDate={new Date()}
                                    isClearable
                                    selected={this.state.detailsInterventionDate}
                                    todayButton="Danas"
                                    dateFormat="dd.MM.yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={date => {

                                        this.setState({ detailsInterventionDate: date })
                                    }}
                                    locale="hr"
                                />
                            </Form>
                        </div><br></br>
                        <h6>Iznos:</h6>
                        <div>
                            <Input labelPosition='right' type='text' placeholder='Iznos' value={this.state.detailsInterventionAmount} onChange={this.handleChange('detailsInterventionAmount')}>
                                <Label basic>HRK</Label>
                                <input />
                                <Label>.00</Label>
                            </Input>

                        </div>
                        <br></br>

                    </ModalBody>

                    <ModalFooter>
                        <Button size="small" positive onClick={this.editIntervention}>Izmijeni</Button>{' '}

                    </ModalFooter>

                </Modal>

                <Modal isOpen={this.state.detailsPopup} centered={true} >
                    <ModalHeader toggle={this.cancelDetailsPopUp}><h4>Detalji o intervenciji</h4></ModalHeader>
                    <ModalBody>
                        <p>
                            <b>Opis intervencije:</b> {this.state.detailsInterventionDescription}<br></br><br></br>
                            <b>Datum:</b> {this.state.detailsInterventionDate}<br></br><br></br>
                            <b>Stan:</b> {this.state.detailsInterventionApartment}<br></br><br></br>
                            <b>Iznos:</b> {this.state.detailsInterventionAmount}<br></br><br></br>

                        </p>

                    </ModalBody>

                </Modal>

                <Modal isOpen={this.state.reportPopup} centered={true} >
                    <ModalHeader toggle={this.cancelReportPopup}><h4>Godišnji izvještaj</h4></ModalHeader>
                    <ModalBody>
                        <div>
                            <Select placeholder='Odaberi godinu' options={this.state.yearOptions} value={this.state.selectedYear} onChange={this.handleSelectYear} /><br></br><br></br>
                        </div>
                        {this.state.selectedYear === '' ? <div></div> :
                            <p>
                                <b>Opis:</b> {this.state.report.description}<br></br><br></br>
                                <b>Datum:</b> {this.state.report.date}<br></br><br></br>
                                <b>Iznos:</b> {this.state.report.amount} HRK<br></br><br></br>

                            </p>
                        }


                    </ModalBody>

                </Modal>


                <Dimmer active={this.state.loading} inverted>
                    <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
                </Dimmer>

                <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                    <h5>{this.state.messageText}</h5>
                </Alert>

                <MaterialTable
                    icons={this.tableIcons}
                    title="Popis intervencija"
                    localization={this.tableTextLocalization}
                    columns={columns()}
                    options={{
                        actionsColumnIndex: -1,
                        exportButton: { pdf: false, csv: true },
                        headerStyle: {
                            backgroundColor: '#E86A22',
                            color: '#FFF'
                        },
                        maxBodyHeight: 600,
                        pageSize: 10,
                        pageSizeOptions: [10, 20, 50, 100]
                    }}
                    data={this.state.interventions}
                    actions={[
                        {
                            icon: () => {
                                return (
                                    <Icon size="small" name='add' />
                                );
                            },
                            tooltip: 'Dodaj intervenciju',
                            isFreeAction: true,
                            hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                            onClick: () => this.setState({ addPopup: true }),

                        },
                        {
                            icon: () => {
                                return (
                                    <Icon size="small" name='file' />
                                );
                            },
                            tooltip: 'Godišnji izvještaj',
                            isFreeAction: true,
                            hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                            onClick: this.toggleReportPopup,

                        },


                        {
                            icon: () => <Icon name="file alternate" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Detalji o intervenciji',
                            onClick: (event, rowData) => {
                                this.setState({ interventionId: rowData.id })
                                this.setState({ detailsInterventionAmount: rowData.amount })
                                this.setState({ detailsInterventionDate: rowData.date })
                                this.setState({ detailsInterventionDescription: rowData.description })
                                this.setState({ detailsInterventionApartment: rowData.apartmentName })
                                this.toogleDetailsPopUp();
                            }
                        },

                        {
                            icon: () => <Icon name="download" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Preuzmi zapisnik',
                            onClick: (event, rowData) => {
                                this.downloadIntervention(rowData.id)
                            }
                        },

                        {
                            icon: () => <Icon name="upload" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Prenesi novi zapisnik',
                            onClick: (event, rowData) => {
                                this.setState({ interventionId: rowData.id })
                                this.toogleInterventionUploadPopUp();
                            }
                        },

                        {
                            icon: () => <Icon name="pencil" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Izmijeni podatke o intervenciji',
                            onClick: (event, rowData) => {
                                this.setState({ interventionId: rowData.id })
                                this.setState({ detailsInterventionAmount: rowData.amount })
                                this.setState({ detailsInterventionDate: makeJSDate(rowData.date) })
                                this.setState({ detailsInterventionDescription: rowData.description })
                                this.setState({ detailsInterventionApartment: rowData.apartmentName })
                                this.toogleEditPopUp();
                            }
                        },

                        rowData => ({
                            icon: () => <Icon name="trash" style={{ color: '#E86A22' }}></Icon>,
                            tooltip: 'Obriši intervenciju',
                            hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                            onClick: () => {
                                this.setState({ interventionId: rowData.id });
                                this.toogleDeletePopUp();
                            }
                        })
                    ]}
                />
            </div>
        );
    }
}

InterventionListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InterventionListTable);