import { green, red } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Icon, Loader, Segment, Input, Dropdown,Item, Divider, Select, Checkbox } from 'semantic-ui-react';
import {sortData, parseTenants, deRefRole, deRefType} from '../Parser'



const styles = theme => ({
  success: {
    backgroundColor: green[600],
  },
  fail: {
    backgroundColor: red[600],
  },
  });

  const typeOptions = [
    { key: 'vanjski_vlasnik', value: 'VANJSKI VLASNIK', text: 'Vanjski vlasnik' },
    { key: 'vlasnik', value: 'VLASNIK', text: 'Vlasnik' },
    { key: 'stanar', value: 'STANAR', text: 'Stanar' },
    { key: 'glavni_podstanar', value: 'GLAVNI PODSTANAR', text: 'Glavni podstanar' },
    { key: 'podstanar', value: 'PODSTANAR', text: 'Podstanar' },
  ]

  const parkingTypeOptions = [
    { key: 'unutarnje', value: 'unutarnje', text: 'Unutarnje' },
    { key: 'vanjsko', value: 'vanjsko', text: 'Vanjsko' },
  ]

  
  const roleOptions = [
    { key: null, value: 'NIŠTA', text: 'Ništa' },
    { key: 'prestavnik', value: 'PREDSTAVNIK', text: 'Predstavnik' },
    { key: 'stambeno_vijeće', value: 'ČLAN STAMBENOG VIJEĆA', text: 'Član stambenog vijeća' },
  ]

  const columns = () => [
    { title: 'Identifikator', field: 'id', editable:"never", hidden:true },
    { title: 'Ime', field: 'name'},
    { title: 'Prezime', field: 'surname'},
    { title: 'Privatni email', field: 'privateEmail'},
    { title: 'Poslovni email', field: 'workEmail'},
    { title: 'Broj mobitela', field: 'mobilePhone'},
    { title: 'Tip', field: 'type'},
    { title: 'Uloga', field: 'role'},
  ]

class TenantsListTable extends Component{


  constructor(props) {
    super(props);

    this.state={
        token: '',
        buildingId: '',
        tenants:[],
        tenantId:'',
        deletePopup: false,
        editPopup: '',
        editTenantName: '',
        editTenantSurname: '',
        editTenantPrivateEmail: '',
        editTenantWorkEmail: '',
        editTenantPhone: '',
        editTenantType: '',
        editTenantRole: '',
        loading:true,
        messageText: '',
        messageAlert: false,
        messageColor: "danger",
        contractPopup: false,
        repPopup: false,
        repType: "",
        repTooltip: "",
        representatives : [],
        parkingPopup: false,
        selectedParking: '',
        selectedParkingType: '',
        selectedParkingHasRented: false,
        parkingOptions : [],
    };
      
  };


    componentDidMount(){

        var buildingId = cookie.load("buildingId");
        const token = cookie.load('token');
        this.setState({token: token});
        this.setState({buildingId: buildingId});


        const options={
          method:'GET',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/building/'+buildingId+"/tenants", options)
        .then(data=>data.json())
        .then(tenants=>{
          parseTenants(tenants)
          this.setState({tenants:sortData(tenants)})
        } )
        fetch('/tenant/building/'+buildingId+"/with-role", options)
        .then(data=>data.json())
        .then(reps=>{
          this.setState({repType: reps.role})
          if(reps.role ==="predstavnik"){
            this.setState({repTooltip: "Predstavnik"})
          }else{
            this.setState({repTooltip: "Stambeno vijeće"})
          }
          this.setState({representatives: reps.tenants})
        })
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
 
    };

    parseFetchResponse = response => response.json().then(text => ({
      json: text,
      meta: response,
    }));
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };


    cleanTextFields=()=>{
      this.setState({editTenantName: ''});
      this.setState({editTenantSurname: ''});
      this.setState({editTenantPrivateEmail: ''});
      this.setState({editTenantWorkEmail: ''});
      this.setState({editTenantPhone: ''});
      this.setState({editTenantType: ''})
      this.setState({editTenantRole: ''})
      this.setState({selectedParking: ''})
      this.setState({selectedParkingType: ''})
    }

    editTenant=()=>{
      this.setState({loading: true})
        const data={
          name: this.state.editTenantName,
          surname: this.state.editTenantSurname,
          privateEmail: this.state.editTenantPrivateEmail,
          workEmail: this.state.editTenantWorkEmail,
          mobilePhone: this.state.editTenantPhone,
          type: deRefType( this.state.editTenantType),
          role: deRefRole( this.state.editTenantRole),
          id: this.state.tenantId
        };
        const options={
            method:'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
            body: JSON.stringify(data)
        };
        fetch('/tenant/'+this.state.tenantId, options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno izmijenjen stanar!"})
              this.cancelEditPopUp();
              this.cleanTextFields();
              this.componentDidMount();
              
            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno izmijenjen stanar!"})
              this.cancelEditPopUp();
              this.cleanTextFields();
              this.componentDidMount();

              
            }
          })
          .then(
            setTimeout(() => {
              this.setState({
              loading: false
            })
            }, 500)
          )
        
    };


    getParkings(type){
      const options={
        method:'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        },
      };
      fetch('/parking/building/'+this.state.buildingId+"/"+type, options)
      .then(data=>data.json())
      .then(parkings=>{
        var poptions = []
        for(var i = 0; i<parkings.length; ++i){
          var option = { key: parkings[i].id, value: parkings[i].id, text: parkings[i].number }
          poptions.push(option)
        }
        this.setState({parkingOptions: poptions})
      } )
    };


    attachParking=()=>{
      var parkingOption = ''
      if(this.state.selectedParkingHasRented){
        parkingOption = "rented"
      }
      else{
        parkingOption = "owner"
      }
      this.setState({loading: true})
        const options={
            method:'PUT',
            headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + this.state.token,
              'Content-Type': 'application/json',
          },
        };
        fetch('/parking/'+this.state.selectedParking+"/"+parkingOption+"/"+this.state.tenantId, options)
        .then(this.parseFetchResponse)
          .then(({ json, meta }) => {
            if(meta.status !== 200){
              this.setState({messageAlert: true}  ,()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"danger"});
              this.setState({messageText: "Neuspješno izmijenjen stanar!"})
              this.cancelParkingPopup();
              this.cleanTextFields();
              this.componentDidMount();
              
            }
            else{
              this.setState({messageAlert: true}, ()=>{
                window.setTimeout(()=>{
                  this.setState({messageAlert:false})
                },6000)
              });
              this.setState({messageColor:"success"});
              this.setState({messageText: "Uspješno izmijenjen stanar!"})
              this.cancelParkingPopup();
              this.cleanTextFields();
              this.componentDidMount();

              
            }
          })
          .then(
            setTimeout(() => {
              this.setState({
              loading: false
            })
            }, 500)
          )
        
    };

    deleteTenant=()=>{
      this.setState({loading:true}) 
      const options={
        method:'DELETE',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
      }
      };
      return fetch('/tenant/'+this.state.tenantId, options)
      .then(response => {
        if(response.status === 200){
          this.setState({messageAlert: true}, ()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"success"});
          this.setState({messageText: "Uspješno obrisan stanar!"})
          this.cancelDeletePopUp();
          this.componentDidMount();

        }
        else{
          this.setState({messageAlert: true}  ,()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"danger"});
          this.setState({messageText: "Neuspješno obrisan stanar!"})
          this.cancelDeletePopUp();
          this.componentDidMount();
        
        }
      })  
      .then(
        setTimeout(() => {
          this.setState({
          loading: false
        })
        }, 500)
      )
    }

    toggleDeletePopUp=()=>{
      this.setState({deletePopup: true})
    }

    cancelDeletePopUp=()=>{
      this.setState({deletePopup: false})
    }

    toggleEditPopUp=()=>{
      this.setState({editPopup: true})
    }

    cancelEditPopUp=()=>{
      this.setState({editPopup: false})
    }

    toggleRepresentativePopup=()=>{
      this.setState({repPopup : true})
    }
    cancelRepresentativePopup=()=>{
      this.setState({repPopup : false})
    }
    cancelParkingPopup=()=>{
      this.setState({parkingPopup : false})
      this.cleanTextFields()
    }

    dismissAlert=()=>{
      this.setState({messageAlert: false})
    }  
    
    handleSelectRole = (e, { value }) => {
      this.setState({editTenantRole: value }) 
    }

    handleSelectType = (e, { value }) => {
      this.setState({editTenantType: value }) 
    }

    handleSelectParking = (e, { value }) => {
      this.setState({selectedParking: value }) 
    }

    handleSelectParkingType = (e, { value }) => {
      this.setState({selectedParkingType: value }) 
      this.getParkings(value)
    }

    handleChecked = (name, old) => {
      var newOld = !old;
      this.setState({
        [name]: newOld,
      });
    };

 
    tableIcons = {
      Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}  />),
      Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
      Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Delete: forwardRef((props, ref) => <Icon name="trash" style={{color: '#E86A22'}} />),
      DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
      Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
      Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
      ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
      ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    tableTextLocalization = {
      body: {
          emptyDataSourceMessage: 'Nema podataka za prikazati',
          editRow: {
            deleteText: 'Jeste li sigurni?',
            saveTooltip: 'Potvrdi',
            cancelTooltip: 'Odustani'
          },
          addTooltip:'Dodaj',
          deleteTooltip: 'Obriši',
          editTooltip: 'Izmijeni',
      },
      header:{
        actions: 'Akcije'
      },
      pagination:{
        firstTooltip: 'Prva stranica',
        previousTooltip: 'Prethodna stranica',
        nextTooltip:'Iduća stranica',
        lastTooltip:'Posljednja stranica',
        labelRowsSelect:'redaka',
      },
      toolbar:{
        searchTooltip:'Pretraži',
        searchPlaceholder:'Pretraži',
        nRowsSelected:'{0} redaka odabrano',
        exportTitle:'Izvezi',
        exportName:'Izvezi kao .csv',
      }
  }
    


    render(){
        const { classes } = this.props;
        
        return (
          <div  >

            <Modal isOpen={this.state.deletePopup}  >
              <ModalBody>
                Jeste li sigurni da želite obrisati stanara?
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.deleteTenant}>Izbriši</Button>{' '}
                <Button  size="small" negative onClick={this.cancelDeletePopUp}>Odustani</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.repPopup} centered={true} size="lg">
              <ModalHeader toggle={this.cancelRepresentativePopup}><h4>{
                this.state.repType === "predstavnik" ?
                "Predstavnik stanara": "Članovi stambenog vijeća"
                }</h4></ModalHeader>
            

              <ModalBody>
                  <Item.Group>
                  {this.state.representatives.map((rep, i) => {     
                      return(
                        <Item>
                        <Item.Content>
                            <Item.Header as='a'>{rep.name} {rep.surname}</Item.Header>
                            <Item.Meta>
                                Email: {rep.email} <br></br>
                                Broj mobitela: {rep.mobilePhone}
                            </Item.Meta>   
                            <Divider />
                        </Item.Content>
                   
                        </Item>
                        
                      )
                     
                    })}
                    
                </Item.Group>
              
              </ModalBody>
            </Modal>


            <Modal isOpen={this.state.editPopup} centered={true} >
              <ModalHeader toggle={this.cancelEditPopUp}><h4>Unesite podatke o stanaru</h4></ModalHeader>
              <ModalBody>
                <div>
                <h6>Ime:</h6>
                <Input placeholder='Ime' value={this.state.editTenantName} onChange={this.handleChange('editTenantName')} /><br></br><br></br>
                </div>
                <div>
                <h6>Prezime:</h6>
                <Input placeholder='Prezime' value={this.state.editTenantSurname} onChange={this.handleChange('editTenantSurname')} /><br></br><br></br>
                </div>
                <div>
                <h6>Privatni email:</h6>
                <Input placeholder='Privatni email' value={this.state.editTenantPrivateEmail} onChange={this.handleChange('editTenantPrivateEmail')} /><br></br><br></br>
                </div>
                <div>
                <h6>Poslovni email:</h6>
                <Input placeholder='Poslovni email' value={this.state.editTenantWorkEmail} onChange={this.handleChange('editTenantWorkEmail')} /><br></br><br></br>
                </div>
                <div>
                <h6>Broj telefona:</h6>
                <Input placeholder='Broj telefona' value={this.state.editTenantPhone} onChange={this.handleChange('editTenantPhone')} /><br></br><br></br>
                </div>
                <div>
                <h6>Tip stanara:</h6>
                <Select placeholder='Tip stanara' options={typeOptions} value={this.state.editTenantType} onChange={this.handleSelectType}/>
                <br></br><br></br>
                </div>
                <div>
                <h6>Uloga stanara:</h6>
                <Select placeholder='Uloga stanara' options={roleOptions} value={this.state.editTenantRole} onChange={this.handleSelectRole}/>
                <br></br>
                </div>
               
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.editTenant}>Izmijeni</Button>
              </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.parkingPopup} centered={true} >
              <ModalHeader toggle={this.cancelParkingPopup}><h4>Odaberite parkirno mjesto</h4></ModalHeader>
              <ModalBody>
              <Checkbox toggle label='U najmu' checked={this.state.selectedParkingHasRented} onChange={() => this.handleChecked('selectedParkingHasRented', this.state.selectedParkingHasRented)} /><br></br><br></br>
                <div>
                <Select placeholder='Tip parkirnog mjesta' options={parkingTypeOptions} value={this.state.selectedParkingType} onChange={this.handleSelectParkingType}/><br></br><br></br>
                </div>
                <div>
                <Select placeholder='Broj parkirnog mjesta' options={this.state.parkingOptions} value={this.state.selectedParking} onChange={this.handleSelectParking}/>
                </div>
               
              </ModalBody>
              <ModalFooter>
                <Button size="small" positive onClick={this.attachParking}>Pridruži</Button>
              </ModalFooter>
            </Modal>
           
            
            <Segment  >

              <Dimmer active={this.state.loading} inverted>
              <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
              </Dimmer>

              <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
                <h5>{this.state.messageText}</h5>
              </Alert>


              <MaterialTable  
                icons={this.tableIcons}
                title="Popis stanara"
                localization={this.tableTextLocalization}
                columns={columns()}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: { pdf: false, csv: true} ,
                  headerStyle: {
                    backgroundColor: '#E86A22',
                    color: '#FFF'
                  },
                  maxBodyHeight:600,
                  pageSize:10,
                    pageSizeOptions:[10,20,50,100]
                }}
                data={this.state.tenants}
                actions={[         
                  {
                    icon:() => <Icon name="pencil" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Izmjeni podatke',
                    onClick: (event, rowData) =>  {
                      this.setState({tenantId: rowData.id});
                      this.setState({editTenantName: rowData.name});
                      this.setState({editTenantSurname: rowData.surname});
                      this.setState({editTenantPrivateEmail: rowData.privateEmail});
                      this.setState({editTenantWorkEmail: rowData.workEmail});
                      this.setState({editTenantPhone: rowData.mobilePhone});
                      this.setState({editTenantType: rowData.type});
                      this.setState({editTenantRole: rowData.role});
                      this.setState({editPopup:true});
                    }
                  },
                  {
                    icon:() => <Icon name="car" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Pridruži parkirno mjesto',
                    onClick: (event, rowData) =>  {
                      this.setState({tenantId: rowData.id});
                      
                      this.setState({parkingPopup:true});
                    }
                  },
                  {
                    icon: () =>{
                    return(
                      <Icon size="small" name='handshake' />
                    );
                    },
                    tooltip: this.state.repTooltip,
                    isFreeAction: true,
                    onClick: () => this.toggleRepresentativePopup(),
                
                  },

                  rowData => ({
                    icon: () => <Icon name="trash" style={{color: '#E86A22'}}></Icon>,
                    tooltip: 'Obriši stanara',
                    onClick: () => { 
                      this.setState({tenantId : rowData.id});
                      this.toggleDeletePopUp();
                  }
                  })             
                ]}
              />
              </Segment>
         
          </div>
            
          
          ); 
    
  }
    
}

TenantsListTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TenantsListTable);