import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import MaterialTable from 'material-table';
import PropTypes from 'prop-types';
import React, { Component, forwardRef } from 'react';
import cookie from 'react-cookies';
import { Alert, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Button, Dimmer, Divider, Grid, Icon, Loader, Segment, Input, Form, TextArea, Label } from 'semantic-ui-react';
import { parseWaterApartments } from '../Parser'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import hr from "date-fns/locale/hr";


registerLocale("hr", hr);

const styles = theme => ({

})

const columns = () => [
  { title: "Identifikator", field: 'id', width: 50 + 'vh', hidden: true },
  { title: "Identifikator stana", field: 'apartment.id', width: 50 + 'vh', hidden: true },
  { title: "Naziv stana", field: 'apartment.name', width: 50 + 'vh' },
  { title: "Broj stana", field: 'apartment.number', width: 50 + 'vh' },
  { title: 'Broj vodomjera', field: 'number', editable: "never", width: 50 + 'vh' },
  { title: 'Sistemski broj vodomjera', field: 'systemNumber', width: 50 + 'vh' },
  { title: 'Broj članova kućanstva', field: 'tenantsNumber', width: 50 + 'vh' },
  { title: 'Datum zadnje izmjene', field: 'lastChange', width: 50 + 'vh' },
];

class WaterApartmentListTable extends Component {


  constructor(props) {
    super(props);

    this.state = {
      token: '',
      role: '',
      waterApartments: [],
      editPopup: false,
      changePopup: false,
      loading: true,
      messageText: '',
      messageAlert: false,
      messageColor: "danger",

      waterApartmentId: '',
      tenantsNumber: '',
      waterNumber: '',
      systemWaterNumber: '',

      newTenantsNumber: '',

    };

  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  componentDidMount() {

    const token = cookie.load('token');
    this.setState({ token: token });
    const role = cookie.load('role');
    this.setState({ role: role });
    const buildingId = cookie.load('buildingId');
    this.setState({ buildingId: buildingId });

    this.setState({ loading: true })
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    };
    fetch('/watermeter/building/' + buildingId, options)
      .then(data => data.json())
      .then(waterApartments => {
        this.setState({ waterApartments:parseWaterApartments(waterApartments) })

      })
      .then(
        setTimeout(() => {
          this.setState({
            loading: false
          })
        }, 500)
      )
  };

  editWaterApartment=()=>{
   
    const data={
      number: this.state.waterNumber,
      systemNumber:this.state.systemWaterNumber,
    };
    const options={
        method:'PUT',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
          'Content-Type': 'application/json',
      },
        body: JSON.stringify(data)
    };
    fetch('/watermeter/'+this.state.waterApartmentId,options)
    .then(this.parseFetchResponse)
      .then(({ json, meta }) => {
        if(meta.status !== 200){
          this.setState({messageAlert: true}  ,()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"danger"});
          this.setState({messageText: "Neuspješno izmijenjeni podaci o vodomjeru!"})
          this.cancelEditPopUp();
          this.clearTextFields();
          this.componentDidMount();
          
        }
        else{
          this.setState({messageAlert: true}, ()=>{
            window.setTimeout(()=>{
              this.setState({messageAlert:false})
            },6000)
          });
          this.setState({messageColor:"success"});
          this.setState({messageText: "Uspješno izmijenjeni podaci o vodomjeru!"})
          this.cancelEditPopUp();
          this.clearTextFields();
          this.componentDidMount();            
        }
      })
      .then(
        setTimeout(() => {
          this.setState({
          loading: false
        })
        }, 500)
      )
    
    };


    changeWaterApartmentTenants=()=>{
   
      const data={
        tenantsNumber: this.state.newTenantsNumber,
      };
      const options={
          method:'PUT',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            'Content-Type': 'application/json',
        },
          body: JSON.stringify(data)
      };
      fetch('/watermeter/tenants-number/'+this.state.waterApartmentId,options)
      .then(this.parseFetchResponse)
        .then(({ json, meta }) => {
          if(meta.status !== 200){
            this.setState({messageAlert: true}  ,()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"danger"});
            this.setState({messageText: "Neuspješno izmijenjeni podaci o broju članova kućanstva!"})
            this.cancelChangePopUp();
            this.clearTextFields();
            this.componentDidMount();
            
          }
          else{
            this.setState({messageAlert: true}, ()=>{
              window.setTimeout(()=>{
                this.setState({messageAlert:false})
              },6000)
            });
            this.setState({messageColor:"success"});
            this.setState({messageText: "Uspješno izmijenjeni podaci o broju članova kućanstva!"})
            this.cancelChangePopUp();
            this.clearTextFields();
            this.componentDidMount();            
          }
        })
        .then(
          setTimeout(() => {
            this.setState({
            loading: false
          })
          }, 500)
        )
      
      };

  parseFetchResponse = response => response.json().then(text => ({
    json: text,
    meta: response,
  }));

  clearTextFields = () => {
    this.setState({ waterApartmentId: '' })
    this.setState({ systemWaterNumber: '' })
    this.setState({ waterNumber: '' })
    this.setState({ tenantsNumber: '' })
    this.setState({ newTenantsNumber: '' })
    
  }

  toggleEditPopUp = () => {
    this.setState({ editPopup: true })
  }

  cancelEditPopUp = () => {
    this.setState({ editPopup: false })
    this.clearTextFields();
  }

  toggleChangePopUp = () => {
    this.setState({ changePopup: true })
  }

  cancelChangePopUp = () => {
    this.setState({ changePopup: false })
    this.clearTextFields();
  }

  dismissAlert = () => {
    this.setState({ messageAlert: false })
  }

  tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <Icon name="trash" style={{ color: '#E86A22' }} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  tableTextLocalization = {
    body: {
      emptyDataSourceMessage: 'Nema podataka za prikazati',
      editRow: {
        deleteText: 'Jeste li sigurni?',
        saveTooltip: 'Potvrdi',
        cancelTooltip: 'Odustani'
      },
      addTooltip: 'Dodaj',
      deleteTooltip: 'Obriši',
      editTooltip: 'Izmijeni',
    },
    header: {
      actions: 'Akcije'
    },
    pagination: {
      firstTooltip: 'Prva stranica',
      previousTooltip: 'Prethodna stranica',
      nextTooltip: 'Iduća stranica',
      lastTooltip: 'Posljednja stranica',
      labelRowsSelect: 'redaka',
    },
    toolbar: {
      searchTooltip: 'Pretraži',
      searchPlaceholder: 'Pretraži',
      nRowsSelected: '{0} redaka odabrano',
      exportTitle: 'Izvezi',
      exportName: 'Izvezi kao .csv',
    }
  }



  render() {
    const { classes } = this.props;

    return (


      <div>
        <Modal isOpen={this.state.changePopup} centered={true} >
          <ModalHeader toggle={this.cancelChangePopUp}><h4>Unesite nove podatke o broju članova kućanstva</h4></ModalHeader>
          <ModalBody>

            <h6>Trenutni broj članova kućanstva: {this.state.tenantsNumber}</h6><br></br>

            <div>
              <Input icon='users' iconPosition='right' type='text' placeholder='Novi broj članova' value={this.state.newTenantsNumber} onChange={this.handleChange('newTenantsNumber')}>
              </Input>

            </div>

          </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.changeWaterApartmentTenants}>Izmijeni</Button>

          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.editPopup} centered={true} >
          <ModalHeader toggle={this.cancelEditPopUp}><h4>Unesite nove podatke o kućanstvu</h4></ModalHeader>
          <ModalBody>
            <h6>Broj vodomjera:</h6>
            <Input placeholder='Broj vodomjera' value={this.state.waterNumber} onChange={this.handleChange('waterNumber')} /><br></br><br></br>
            <h6>Sistemski broj vodomjera:</h6>
            <Input placeholder='Sistemski broj vodomjera' value={this.state.systemWaterNumber} onChange={this.handleChange('systemWaterNumber')} /><br></br><br></br>
          </ModalBody>
          <ModalFooter>
            <Button size="small" positive onClick={this.editWaterApartment}>Izmijeni</Button>

          </ModalFooter>
        </Modal>

        <Segment  >
          <Dimmer active={this.state.loading} inverted>
            <Loader active={this.state.loading} inverted size="massive" >Učitavam</Loader>
          </Dimmer>

          <Alert color={this.state.messageColor} isOpen={this.state.messageAlert} toggle={this.dismissAlert}>
            <h5>{this.state.messageText}</h5>
          </Alert>

          <MaterialTable
            icons={this.tableIcons}
            title="Popis kućanstva"
            localization={this.tableTextLocalization}
            columns={columns()}
            options={{
              actionsColumnIndex: -1,
              exportButton: { pdf: false, csv: true },
              headerStyle: {
                backgroundColor: '#E86A22',
                color: '#FFF'
              },
              maxBodyHeight: 600,
              pageSize: 10,
              pageSizeOptions: [10, 20, 50, 100]
            }}
            data={this.state.waterApartments}
            actions={[
              {
                icon: () => <Icon name="pencil" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Izmijeni podatke',
                hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                onClick: (event, rowData) => {
                  this.setState({ waterApartmentId: rowData.id });
                  this.setState({systemWaterNumber: rowData.systemNumber})
                  this.setState({waterNumber: rowData.number})
                  this.toggleEditPopUp()
                }
              },

              {
                icon: () => <Icon name="exchange" style={{ color: '#E86A22' }}></Icon>,
                tooltip: 'Promijeni broj članova kućanstva',
                hidden: this.state.role === 'ROLE_ADMIN' ? false : true,
                onClick: (event, rowData) => {
                  this.setState({ waterApartmentId: rowData.id });
                  this.setState({ tenantsNumber: rowData.tenantsNumber })
                  this.toggleChangePopUp()
                }
              },

            ]}
          />
        </Segment>

      </div>

    );
  }
}



WaterApartmentListTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(WaterApartmentListTable);